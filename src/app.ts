import { GameOptions } from './game/models/common/game/game-options';
import { DawnbringerPalette } from './common/models/colors';
import WorldMapScene from './game/scenes/world-scene';
import { ResourceTypesList } from './game/models/trade/resource/resource-types-list';
import { UnitTypes } from './game/models/military/type/unit-types';
import { Player } from './game/models/player/player';
import { EventManager } from './common/models/events';
import { TurnManager } from './game/managers/infrastructure/turn-manager';
import { GuiManager } from './game/managers/gui/gui-manager';
import * as BABYLON from 'babylonjs';
import "@babylonjs/loaders/glTF";
import { GameLoadingState } from "./game/models/infrastructure/loading/game-loading-state";
import { Bootstrapper } from "./game/managers/infrastructure/bootstrapper";
import { ConstructableTypes } from "./game/models/building/type/constructable";
import { GameMapClick } from "./common/models/input/game-map-click"; // dont forget to enable gltf loading
import { SceneManager } from './game/managers/world/scene-manager';

const logStr = "<App> ";
globalThis.GAME_HAS_STARTED = false;
globalThis.COLORS = DawnbringerPalette;
initializeDefaultOptions();

window.addEventListener('DOMContentLoaded', () => onDomContentLoaded());

function onDomContentLoaded() {
    bootstrap();
    // default data json files
    globalThis.JSON_LOADER.run();
    // initialize resource & military types
    initializeTsEntityArrays();
    // observer json loaded
    globalThis.JSON_LOADER.onAllLoaded$.add(() => onJsonDataLoaded() );
}

function bootstrap() {
    globalThis.LOADING_STATE = new GameLoadingState();
    globalThis.EVENTS = new EventManager();
    // after event manager created; creates JSON_LOADER
    globalThis.LOADING_STATE.nextStage();
    globalThis.BOOTSTRAPPER = new Bootstrapper();
    globalThis.BOOTSTRAPPER.run();
    // register to asset loader
    globalThis.ASSETS.onAllAssetsLoaded$.add(() => afterAssetsLoaded());
}

function createPlayerObject() {
    globalThis.PLAYER = new Player();
    globalThis.PLAYER_M.onPlayerObjCreated$.notifyObservers(globalThis.PLAYER);
}

function onJsonDataLoaded(): void {
    // create player?
    createEntityManagers();
    createPlayerObject();
    // create UI
    createGuiManager();
    // Create the game using the 'renderCanvas'
    createScene();
}

function initializeDefaultOptions() {
    globalThis.OPTIONS = new GameOptions();
}

export function afterAssetsLoaded() {
    initializeInput();
    // Start render loop.
    globalThis.GAME_HAS_STARTED = true;
    render();
    showIngameMenues();
}

function initializeTsEntityArrays() {
    console.log(logStr + "initializing entity types ..");
    globalThis.RESOURCE_TYPES = new ResourceTypesList();
    globalThis.SPECIAL_RES = [];
    globalThis.UNIT_TYPES = new UnitTypes();
    globalThis.CONSTRUCTABLE = new ConstructableTypes();
}

function createEntityManagers() {
    globalThis.BOOTSTRAPPER.createEntityManagers();
}

function createScene() {
    globalThis.SCENE_MGR = new SceneManager();
    globalThis.SCENE_MGR.loadWorld();
}

function initializeInput() {
    globalThis.SCENE.onKeyboardObservable.add(kbInfo => {
        if (! kbInfo) return;
        if (kbInfo.type !== BABYLON.KeyboardEventTypes.KEYUP) { return; }
        if (kbInfo.event.code) {
            const keyStr = kbInfo.event.code.replace("Key", "")
            globalThis.INPUT.onKeyUp(keyStr);
        }
    });
    globalThis.SCENE.onPointerDown = (event: PointerEvent, pickResult) => {
        const mapClickObj = new GameMapClick(event, pickResult);
        globalThis.INPUT.onMapClick$.notifyObservers(mapClickObj);
    };
}

function render() {
    globalThis.WORLD.doRender();
}

function createGuiManager() {
    // initialize properties
    globalThis.DIALOG = null;
    // start gui manager
    globalThis.GUI = new GuiManager();
}

function showIngameMenues() {
    globalThis.GUI.build();
    globalThis.GUI.onShowIngameMenues$.notifyObservers();
}