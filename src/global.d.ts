import { GameLoadingState } from "./game/models/infrastructure/loading/game-loading-state";
import { GameOptions } from "./game/models/common/game/game-options";
import { JsonDataLoader } from "./game/managers/infrastructure/json-data-loader";
import { DawnbringerPalette } from "./common/models/colors";
import { WorldMap } from "./game/world/map";
import WorldMapScene from "./game/scenes/world-scene";
import { ResourceTypesList } from "./game/models/trade/resource/resource-types-list";
import { TradeResource } from "./game/models/trade/resource/resource";
import { UnitTypes } from "./game/models/military/type/unit-types";
import { ConstructableTypes } from "./game/models/building/type/constructable";
import { Player } from "./game/models/player/player";
import { GuiManager } from "./game/managers/gui/gui-manager";
import { IDialog } from "./game/gui/dialogs/common/dialog.interfaces";
import { DomNodes } from "./game/gui/elements/controls-dom";
import { AssetsLoader } from "./game/managers/infrastructure/asset-loader";
import { EventManager } from "./common/models/events";
import { FileManager } from "./game/managers/infrastructure/file-manager";
import { SelectionManager } from "./game/managers/infrastructure/selection-manager";
import { InputManager } from "./game/managers/infrastructure/input-manager";
import { TurnManager } from "./game/managers/infrastructure/turn-manager";
import { PlayerManager } from "./game/managers/diplomacy/player-manager";
import { ArmyManager } from "./game/managers/military/army-manager";
import { UnitManager } from "./game/managers/military/unit-manager";
import { UnitGearManager } from "./game/managers/military/unit-gear-manager";
import { CharacterManager } from "./game/managers/diplomacy/character-manager";
import { CityManager } from "./game/managers/city/city-manager";
import { TradeManager } from "./game/managers/trade/trade-manager";
import { BuildingManager } from "./game/managers/city/building-manager";
import { ConnectionManager } from "./game/managers/trade/connection-manager";
import { FactionManager } from "./game/managers/diplomacy/faction-manager";
import * as BABYLON from "babylonjs";
import { GlobalObservables } from "./common/game-observables";
import { TargetCursor } from "./game/world/cursor";
import { EntityPlacer } from "./game/managers/world/entity-placer";
import { SceneManager } from "./game/managers/world/scene-manager";


export {}

declare global {
    // initialization & observanles
    var LOADING_STATE: GameLoadingState;
    var GLOOBS: GlobalObservables;
    var OPTIONS: GameOptions;
    var JSON_LOADER: JsonDataLoader;
    var GAME_HAS_STARTED: boolean;
    var COLORS: DawnbringerPalette;
    // map
    var MAP_NAME: string;
    var MAP: WorldMap;
    var WORLD: WorldMapScene;
    // scene
    var SCENE: BABYLON.Scene;
    var CAMERA: BABYLON.ArcRotateCamera;
    // types
    var RESOURCE_TYPES: ResourceTypesList;
    var SPECIAL_RES: Array<TradeResource>;
    var UNIT_TYPES: UnitTypes;
    var CONSTRUCTABLE: ConstructableTypes;
    var UNIT_TYPES: UnitTypes;
    // instances
    var PLAYER: Player;
    // gui
    var GUI: GuiManager;
    var DIALOG: IDialog;
    var DOM_NODES: DomNodes;
    // assets
    var MODEL_NAMES: string[];
    var ASSETS: AssetsLoader;
    // managers
    var EVENTS: EventManager;
    var FILES: FileManager;
    var SELECTION: SelectionManager;
    var INPUT: InputManager;
    var TURNS: TurnManager;
    var PLAYER_M: PlayerManager;
    var ARMIES: ArmyManager;
    var UNITS: UnitManager;
    var GEAR: UnitGearManager;
    var CHARS: CharacterManager;
    var CITIES: CityManager;
    var TRADE: TradeManager;
    var BLD: BuildingManager;
    var CONN: ConnectionManager;
    var FACTIONS: FactionManager;
    var CURSOR: TargetCursor;
    var PLACER: EntityPlacer;
    var SCENE_MGR: SceneManager;
}