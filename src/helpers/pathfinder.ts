// common
import { Loggable } from "../common/models/log";
// world
import { MapPos } from "../game/world/pos";
import { MapTile } from "../game/world/tile/tile";
import { tileAt } from "../game/world/map";

export class TilePathFinder extends Loggable {
    sourcePos: MapPos;
    targetPos: MapPos;
    // tiles
    startTile: MapTile;

    constructor(sourcePos: MapPos, targetPos: MapPos) {
        super("TilePathFinder");
        this.sourcePos = sourcePos;
        this.targetPos = targetPos;
        this.startTile = tileAt(sourcePos.x, sourcePos.z);
    }

    findSimplePosPath(): Array<MapPos> {
        let posArr: Array<MapPos> = [];
        let lastPos: MapPos = this.sourcePos;
        let isAtTarget = lastPos.equals(this.sourcePos);
        while (! isAtTarget ) {
            const oldPos = lastPos;
            posArr.push( oldPos );
            // lastPos = this.findNextSimplePos( oldPos );

        }
        return posArr;
    }
}