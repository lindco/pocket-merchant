// common
import { GameOptions } from '../game/models/common/game/game-options';
import { RandomRotation } from './random';
// world
import { MapPos } from '../game/world/pos';
import { MapTile } from '../game/world/tile/tile';
import { TerrainTypes } from '../game/world/tile/terrain-types';
// babylon
import * as BABYLON from 'babylonjs';

export class WorldTools {

    static setMeshScenePos(mesh: BABYLON.AbstractMesh, worldPos: MapPos) {
        const scenePos: BABYLON.Vector3 = WorldTools.worldToScenePos(worldPos);
        mesh.position = scenePos;
        // add terrain offset
        const terrainChar = globalThis.MAP.tileAtPos(worldPos).terrainChar;
        if (terrainChar) {
            const yOffset = TerrainTypes.heightOf(terrainChar) /2;
            mesh.position.y += yOffset;
        }
    }

    static setMeshLOD(mesh: BABYLON.AbstractMesh): BABYLON.AbstractMesh {
        if (mesh instanceof BABYLON.Mesh) {
            const fullMesh = mesh as BABYLON.Mesh;
            const viewDist = globalThis.OPTIONS.getNum(GameOptions.MAP_VIEW_DISTANCE);
            fullMesh.addLODLevel(viewDist, null);
            return fullMesh;
        }
        return mesh;
    }

    static setMeshScaling(mesh: BABYLON.AbstractMesh, scale: number) {
        mesh.scaling.set(scale, scale, scale);
    }

    static placeMesh(mesh: BABYLON.AbstractMesh, worldPos: MapPos, autoscale: boolean, autorotate?: boolean): BABYLON.AbstractMesh {
        if (! mesh) {
            console.warn(`cant place mesh ${mesh.name}`); return;
        }
        WorldTools.setMeshScenePos(mesh, worldPos);
        WorldTools.setMeshLOD(mesh);
        if (autoscale) { WorldTools.setMeshScaling(mesh, 2); }
        if (autorotate) { RandomRotation.onMesh(mesh); }
        return mesh;
    }

    /**
     * Spawns a mesh in the world scene
     *
     * @param assetName name of the mesh without the ending, p.e. ".obj
     * @param createdMeshName name of the created mesh/babylon node
     * @param worldPos tile position this mesh is to be placed in
     * @param autoscale automatically scale size
     * @param autorotate choose a random rotation
     */
    static placeAssetInstance(assetName: string, createdMeshName: string, worldPos: MapPos,
        autoscale: boolean, autorotate?: boolean): BABYLON.AbstractMesh {
        let assetInstance: BABYLON.AbstractMesh = null;
        if (globalThis.ASSETS) {
            assetInstance = globalThis.ASSETS.getAsset(assetName).getMeshInstance(createdMeshName)
        }
        if (! assetInstance) { return; }
        return WorldTools.placeMesh(assetInstance, worldPos, autoscale, autorotate);
    }

    static getMeshDistance(mesh1: BABYLON.AbstractMesh, mesh2: BABYLON.AbstractMesh): number {
        if (! mesh1 && mesh2) { return; }
        return WorldTools.getVec3Distance(mesh1.position, mesh2.position);
    }

    static getVec3Distance(source: BABYLON.Vector3, target: BABYLON.Vector3): number {
        return BABYLON.Vector3.Distance(source, target);
    }

    static getWorldPosDistance(pos1: MapPos, pos2: MapPos): number {
        const scenePos1 = WorldTools.worldToScenePos(pos1);
        const scenePos2 = WorldTools.worldToScenePos(pos2);
        return WorldTools.getVec3Distance(scenePos1, scenePos2);
    }

    static worldToScenePos(worldPos: MapPos): BABYLON.Vector3 {
        if (! globalThis.WORLD) { return; }
        const hexSize = MapTile.HEXSIZE;
        const worldTopLeft = globalThis.WORLD.topleft;
        const xOffset = globalThis.WORLD.getOffsetAt(worldPos.z);
        const x: number = (worldTopLeft.x + worldPos.x * hexSize + xOffset) * 0.9;
        const z: number = (worldTopLeft.z - worldPos.z * hexSize) * 0.8;
        return new BABYLON.Vector3(x, 0, z);
    }

    /* LANDSCAPE CREATION */
    public static createTileHexMesh(tileName: string, terrainType: string, diameterBottom: number) {
        let mesh: BABYLON.Mesh = BABYLON.Mesh.CreateCylinder(
            tileName,
            TerrainTypes.heightOf(terrainType),
            TerrainTypes.topDiameterOf(terrainType),
            diameterBottom,
            6, // tesselation
            1, // subdivision
            globalThis.SCENE,
            false // updatable
        );
        return mesh;
    }

    public static adjustTileMexHex(mesh: BABYLON.Mesh, terrainType: string) {
        mesh.rotation.y = Math.PI / 2;
        mesh.position.y = TerrainTypes.heightOf(terrainType) / 2;
        // set material & shading
        mesh.convertToFlatShadedMesh();
        mesh.layerMask = 0;
        mesh.material = WorldTools.getMaterialById(terrainType);
        const viewDist = globalThis.OPTIONS.getNum(GameOptions.MAP_VIEW_DISTANCE);
        mesh.addLODLevel(viewDist, null);
        // freeze mesh material & world matrix TODO: world matrix?
        if (mesh.material) {
            mesh.material.freeze();
        }
        mesh.freezeWorldMatrix();
    }

    public static getMaterialById(key: string): BABYLON.StandardMaterial {
        return globalThis.MAP.mats.get(key);
    }
}
