export class Strings {
    static capitalizeFirst(src: string): string {
        const firstLetter = src[0].toUpperCase();
        return firstLetter + src.substring(1);
    }
}