// common
import { DawnbringerPalette } from "../common/models/colors";
import { RandomElement } from "./random";
// babylon
import * as BABYLON from 'babylonjs';

export class ColorPalette {
    static NameAsNumArray(colorName: string): Array<number> {
        let rgb: Array<number> = DawnbringerPalette[colorName];
        return rgb;
    }

    static NameAsColor3(colorName: string): BABYLON.Color3 {
        let rgb: Array<number> = ColorPalette.NameAsNumArray(colorName);
        return BABYLON.Color3.FromArray(rgb);
    }

    static NameAsHexStr(colorName: string): string {
        const color3 = ColorPalette.NameAsColor3(colorName);
        return color3.toHexString();
    }

    static RandomColorName(): string {
        return RandomElement.of(DawnbringerPalette.ALL)
    }
}