// world
import {MapPos} from "../game/world/pos";
// babylon
import * as BABYLON from 'babylonjs';
import {Strings} from "./strings";

/* == RANDOM INTEGER == */
export class RandomInt {
    static below(belowNum: number): number {
        return Math.floor(Math.random() * belowNum);
    }
}

/* == RANDOM ARRAY ELEMENT == */
export class RandomElement {
    static of(arr: Array<any>): any {
        let index: number = RandomInt.below(arr.length);
        return arr[index];
    }
}

/* == RANDOM DICE ROLL == */
export class RandomDice {
    static roll(sides: number): boolean {
        let result: number = RandomInt.below(sides);
        if ( result == 0 ) {
            return true;
        }
        return false;
    }
}

/* == RANDOM NAME == */ 
export class RandomName {
    static from(prefixes: Array<string>, postfixes: Array<String>, capitalizeAfterSpace?: boolean): string {
        let rndPrefix: string = RandomElement.of( prefixes );
        let rndPostfix: string = RandomElement.of( postfixes );
        // capitalist first char in postpix (p.e. "Green H[!]ills")?
        if (capitalizeAfterSpace && rndPostfix.charAt(0) == " ") { rndPostfix = Strings.capitalizeFirst(rndPostfix); }
        return rndPrefix + rndPostfix;
    }
}

/* == RANDOM WORLD POSITION == */
export class RandomWorldPos {
    static around(centerPos: MapPos, radius: number, excludeCenter?: boolean): MapPos {
        // calculate boundary positions
        const topleft = new MapPos(centerPos.x - radius, centerPos.z - radius);
        const btmright = new MapPos(centerPos.x + radius, centerPos.z + radius);
        let positionsInRadius: MapPos[] = [];
        // add every position between boundaries to array ..
        for (let z = topleft.z; z <= btmright.z; z++) {
            for (let x = topleft.x; x <= btmright.x; x++) {
                const currentPos = new MapPos(x, z);
                const isCenterPos = currentPos.x == centerPos.x && currentPos.z == centerPos.z;
                const isExcluded = isCenterPos && excludeCenter;
                // .. if they're not the center and excludeCenter is true
                if (! isExcluded) { positionsInRadius.push(currentPos); }
            }
        }
        // choose & return a random element from the position array
        return RandomElement.of(positionsInRadius);
    }
}

export class RandomRotation {
    static inDeg(no90Lock?: boolean): number {
        let angleDeg: number = RandomInt.below(4) * 90;
        if (no90Lock && no90Lock == true) {
            angleDeg = RandomInt.below(360);
        }
        return angleDeg;
    }

    static inRad(no90Lock?: boolean): number {
        const angleDeg = RandomRotation.inDeg(no90Lock);
        return BABYLON.Tools.ToRadians(angleDeg);
    }

    static onMesh(mesh: BABYLON.AbstractMesh, no90Lock?: boolean): BABYLON.AbstractMesh {
        mesh.rotation.y = RandomRotation.inRad(no90Lock);
        return mesh;
    }
}