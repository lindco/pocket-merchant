import * as BABYLON from 'babylonjs';
import { WorldMap } from "../game/world/map";
import { City } from "../game/models/city/city";

export class GlobalObservables {
    onBootstrappingFinished$ = new BABYLON.Observable<void>();
    onAllJsonFilesLoaded$ = new BABYLON.Observable<void>();
    onAllAssetsLoaded$ = new BABYLON.Observable<void>();
    onMapObjCreated$ = new BABYLON.Observable<WorldMap>();
    onCitiesLoaded$ = new BABYLON.Observable<Array<City>>();
    onTurnFinishing$ = new BABYLON.Observable<number>();
    onPlayerObjCreated$ = new BABYLON.Observable<void>();
}