
export class GameMovementKeys {
    public static get MOVE_WEST(): string { return 'A'; }
    public static get EAST(): string { return 'D'; }
    public static get MOVE_NORTHWEST(): string { return 'Q'; }
    public static get MOVE_NORTHEAST(): string { return 'E'; }
    public static get MOVE_SOUTHWEST(): string { return 'Y'; }
    public static get MOVE_SOUTHEAST(): string { return 'X'; }
}