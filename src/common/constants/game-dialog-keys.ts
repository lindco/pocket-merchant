export class GameDialogKeys {
    /* MAIN DIALOGS */
    public static get DIAG_GAME_MENU(): string { return 'diag-game-menu'; }
    public static get DIAG_LOADING(): string { return 'diag-loading'; }
    /* ARMY & MILITARY DIALOGS */
    public static get DIAG_ARMIES(): string { return 'diag-armies'; }
    public static get DIAG_ARMY(): string { return 'diag-army'; }
    public static get DIAG_RECRUIT(): string { return 'diag-recruit'; }
    /* FACTION DIALOGS */
    public static get DIAG_FAMILIES(): string { return 'diag-families'; }
    public static get DIAG_POPS(): string { return 'diag-population' }
    /* LOCATION DIALOGS */
    public static get DIAG_CITY(): string { return 'diag-city'; }
    public static get DIAG_RESLIST(): string { return 'diag-reslist'; }
    public static get DIAG_BUILDINGS(): string { return 'diag-buildings'; }
    public static get DIAG_CONSTRUCTION(): string { return 'diag-construction'; }
    public static get DIAG_CONSUMPTION(): string { return 'diag-consumption'; }

    /* TRADE DIALOGS */
    public static get DIAG_TRADEHIST(): string { return 'diag-tradehist'; }
    public static get DIAG_TRANSFER(): string { return 'diag-transfer'; }
    public static get DIAG_CONNECTIONS(): string { return 'diag-connections'; }
    /* PLAYER DIALOGS */
    public static get DIAG_PLAYER(): string { return 'diag-player'; }

}