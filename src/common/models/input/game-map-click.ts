import { Loggable } from "../log";
import * as BABYLON from 'babylonjs';

export class GameMapClick extends Loggable {
    event: PointerEvent;
    pick: BABYLON.PickingInfo;
    constructor(event: PointerEvent, pick: BABYLON.PickingInfo) {
        super("GameMapClick");
        this.event = event;
        this.pick = pick;
    }
}