// common
import { ILoggable, Loggable } from "./log";
import { Properties } from "./properties/properties";

/* == INTERFACES == */
export interface IBaseEvent {
    id: string;
    properties: Properties;
}

export interface IEventListener extends ILoggable {
    isMuted: boolean;
    // methods
    onEvent(ev: IBaseEvent): void;
}

interface IEventManager {
    currentEvent: IBaseEvent;
    eventHistory: Array<IBaseEvent>;
    // methods
    registerListener(listener: IEventListener);
    fireEvent(ev: IBaseEvent, verbose?: boolean);
}

/* == EVENT == */
export class BaseEvent implements IBaseEvent {
    public id: string;
    public properties: Properties = new Properties();

    constructor(id: string, data?: Properties) {
        this.id = id;
        if (data) { this.properties = data; }
    }
}

/* == LISTENER == */
export class BaseEventListener extends Loggable implements IEventListener {
    // flags
    isMuted: boolean = false;
    isPrioritized: boolean = false;

    constructor(logName: string, isPrioritized?: boolean) {
        super(logName);
        // optional properties
        if ( isPrioritized && isPrioritized == true ) { this.isPrioritized = true; }
        // register to eventmanager
        if (! globalThis.EVENTS) {
            this.error(`cant create listener ${logName}: EventManager not started`);
        } else {
            globalThis.EVENTS.registerListener(this, this.isPrioritized);
        }
    }

    public onEvent(ev: IBaseEvent) {

    }
}

/* == MANAGER == */
export class EventManager extends Loggable implements IEventManager {
    currentEvent: IBaseEvent = null;
    eventHistory: Array<IBaseEvent> = [];
    // listener arrays
    // private listeners: Array<IEventListener> = [];
    private defaultListeners: Array<IEventListener> = [];
    private prioritizedListeners: Array<IEventListener> = [];
    private debugMode: boolean = false;

    constructor(debugMode?: boolean) {
        super("EventManager");
        if ( debugMode ) { this.debugMode = true; }
        this.log(`created`);
    }

    registerListener(listener: IEventListener, prioritize?: boolean) {
        if ( prioritize ) {
            this.prioritizedListeners.push( listener );
        } else {
            this.defaultListeners.push( listener );
        }
        
    }

    fireEvent(ev: IBaseEvent, verbose?: boolean) {
        this.currentEvent = ev;
        // fire onEvent for every listener
        for (const listener of this.listeners) {
            if ( !listener ) { return; }
            let showLog: boolean = this.debugMode || verbose;
            if (showLog) { this.log(`firing ${ev.id} in ${listener}`); }
            listener.onEvent(ev);
        }
        // add event to history and clear currentEvent
        this.eventHistory.push(ev);
        this.currentEvent = null;
    }

    /* GETTERS & SETTERS */
    get listeners(): Array<IEventListener> {
        let allListeners: Array<IEventListener> = [];
        allListeners.concat(this.prioritizedListeners);
        allListeners.concat(this.defaultListeners);
        return allListeners;
    }

    /* DEBUG FUNCTIONS */

    toString(): string {
        return `<EventManager listeners=${this.listeners.length} currentEvent=${this.currentEvent}>`;
    }
}

/* == CONSTANTS == */
export const EV_CAT_GAME_LOADING = 0;
export const EV_CAT_ENTITIES = 2;