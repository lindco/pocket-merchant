export class Timespan {
    startYear: number = 0;
    endYear: number = 2000;

    constructor(startYear?: number, endYear?: number) {
        if (startYear) { this.startYear = startYear; } 
        if (endYear) { this.endYear = endYear; } 
    }

    static FromString(timespanStr: string): Timespan {
        if (! timespanStr) { return new Timespan(); }
        const values = timespanStr.split("-");
        const yearsArray = [ parseInt(values[0]), parseInt(values[1]) ];
        return new Timespan(yearsArray[0], yearsArray[1]);
    }
}