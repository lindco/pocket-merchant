
// common
import {Loggable} from '../log';
// events
// babylon
import * as BABYLON from 'babylonjs';
import 'babylonjs-loaders';

/* INTERFACES */
export interface IAsset {
    name: string;
    meshes: Array<BABYLON.Mesh>;
    // flags
    isLoaded: boolean;
    hasError: boolean;
    errorMessage: string;
}

/* ASSET MODEL */
export class Asset extends Loggable implements IAsset {
    fileName: string;
    name: string;
    meshes: Array<BABYLON.Mesh> = [];
    // flags
    isLoaded: boolean = false;
    hasError: boolean = false;
    errorMessage: string;

    constructor(modelFileName: string) {
        super("Asset");
        this.fileName = modelFileName;
        this.name = modelFileName.split(".")[0];
    }

    setMeshes(meshes: Array<BABYLON.AbstractMesh>) {
        const meshArray: Array<BABYLON.Mesh> = [];
        for (const abstrMesh of meshes) {
            const mesh = abstrMesh as BABYLON.Mesh;
            meshArray.push(mesh);
        }
        this.meshes = meshArray;
        // set root mesh name
        this.isLoaded = true;
        // set main mesh name
        const mainMesh = this.getMesh();
        if (mainMesh) { mainMesh.id = "asset-" + this.name; }
    }

    getMesh(): BABYLON.Mesh {
        if (this.meshes.length < 1) {
            this.error(`has no meshes loaded`); return;
        }
        const mainMeshIndex = this.meshes.length -1;
        return this.meshes[mainMeshIndex] as BABYLON.Mesh;
    }

    getMeshInstance(name: string, autoRotate?: boolean): BABYLON.InstancedMesh {
        return this.getMesh().createInstance(name);
    }

    /* ASSET GETTERS */
    get fileExtension(): string { return this.fileName.split(".")[1]; }

    toString(): string {
        return `<Asset "${this.name}" meshes=${this.meshes.length} error=${this.hasError}>`
    }
}

/* == ASSET CONSTANTS == */
export const MODEL_FOLDER_PATH = "./res/models/";
export const MODEL_FILE_ENDINGS = ["obj", "gltf", "glb"];