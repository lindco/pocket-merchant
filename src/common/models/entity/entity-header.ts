import { IEntity } from "./entity";
import { Loggable } from "../log";


export interface IEntityHeader {
    entityType: string;
    parent: IEntity
    num: number;
    // methods
    uid(): string;
}

export class EntityHeader extends Loggable implements IEntityHeader {
    parent: IEntity;

    constructor(entityObj: IEntity) {
        super("EntityHeader");
        this.parent = entityObj;
        
    }

    get num(): number {
        if ( this.parent.hasOwnProperty("num") ) { return this.parent.num; }
        return -1;
    }

    get entityType(): string {
        return this.parent.constructor.name;
    }

    uid(): string {
        let uidStr: string = this.entityType;
        const entityNum = this.num;
        if (this.num && this.num >= 0) {
            uidStr += `:${this.num}`;
        }
        return uidStr;
    }
}