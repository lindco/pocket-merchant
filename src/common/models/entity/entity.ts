import { BaseEventListener } from "../events";
import { ICommandable, IBaseCommand } from "../command";
import { EntityHeader, IEntityHeader } from "./entity-header";
import { MapPos } from "../../../game/world/pos";
import { Loggable } from "../log";
import { WorldTools } from "../../../helpers/world-tools";
import { IEntityEffect } from "./entity-effect";
import { ISelectable } from "../../../game/world/selection";
// babylon
import * as BABYLON from 'babylonjs';

export interface IUniqueInstance {
    _entityType: string;
    num: number;
    uid: string;
}

/* ENTITY INTERFACES */
export interface IEntity extends IUniqueInstance {
    header: IEntityHeader;
    onTurn(): void;
}

export interface IOnTurnEntity extends  IEntity {
    onTurn(): void;
}

export interface IWorldEntity extends IOnTurnEntity {
    pos: MapPos;
    mesh: BABYLON.AbstractMesh;
    distanceToPlayer: number;
    // observables
    onClicked$: BABYLON.Observable<void>;
}

export interface ICommandableWorldEntity extends IWorldEntity, ICommandable {

}

export interface ICommandableMovableEntity extends ICommandableWorldEntity {
    moveInDir(dirStr: string): void;
}

/* BASE ENTITY */ 
export class AbstractEntity extends Loggable implements IOnTurnEntity {
    _entityType: string = "abstract";
    header: IEntityHeader;
    num: number = -1;
    effects: Array<IEntityEffect> = [];

    constructor(num: number) {
        super("Entity");
        this.num = num;
        this.header = new EntityHeader(this);
    }

    onTurn() {
        
    }

    get uid(): string { return this.header.uid(); }

    addEffect(effect: IEntityEffect): void {
        effect.parent = this;
        this.effects.push(effect);
    }
}

export class AbstractEventEntity extends BaseEventListener implements IEntity {
    _entityType: string = "abstract-event";
    header: IEntityHeader;
    num: number = -1;

    constructor(num: number) {
        super("Entity");
        this.num = num;
        this.header = new EntityHeader(this);
    }

    onTurn() {
        
    }

    get uid(): string { return this.header.uid(); }
}

/* MAP ENTITY */
export class WorldEntity extends AbstractEntity implements IWorldEntity, ISelectable {
    pos: MapPos;
    mesh: BABYLON.AbstractMesh;
    // observables
    onClicked$: BABYLON.Observable<void> = new BABYLON.Observable<void>();
    onSelected$: BABYLON.Observable<void> = new BABYLON.Observable<void>();

    constructor(num: number, pos: MapPos) {
        super(num);
        this.pos = pos;
    }

    get distanceToPlayer(): number {
        if (globalThis.PLAYER && globalThis.Player.pos) { 
            return WorldTools.getWorldPosDistance(this.pos, globalThis.PLAYER.pos); }
        return;
    }

    onTurn() {

    }

    setAsSelected() {
    }
}

export class WorldEventEntity extends AbstractEventEntity implements IWorldEntity, ISelectable {
    pos: MapPos;
    mesh: BABYLON.AbstractMesh;
    // observables
    onClicked$: BABYLON.Observable<void> = new BABYLON.Observable<void>();
    onSelected$: BABYLON.Observable<void> = new BABYLON.Observable<void>();

    constructor(num: number, pos: MapPos) {
        super(num);
        this.pos = pos;
    }

    setAsSelected() {
        throw new Error("Method not implemented.");
    }

    get distanceToPlayer(): number {
        if (globalThis.PLAYER && globalThis.PLAYER.pos) {
            return WorldTools.getWorldPosDistance(this.pos, globalThis.PLAYER.pos); }
        return;
    }
}

/* COMMANDABLE */
export class CommandableMapEntity extends WorldEntity implements ICommandableWorldEntity {
    public commands: Array<IBaseCommand> = [];
    // observables
    onCommand$ = new BABYLON.Observable<IBaseCommand>();

    public onCommand(cmd: IBaseCommand) {

    }

    public addCommand(cmd: IBaseCommand) {

    }
}

/* MOVABLE ENTITY */
export class MovableEntity extends CommandableMapEntity implements ICommandableMovableEntity {
    public moveInDir(dirStr: string) {

    }
}