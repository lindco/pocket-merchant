import { IEntity } from "./entity";

export interface IEntityEffect {
    parent: IEntity;
    propertyKey: string;
    amount: number;
    // flags
    isBoolean: boolean;
    isLeveled: boolean;
}

export class EntityPropertyEffect implements IEntityEffect {
    amount: number;
    propertyKey: string;
    isBoolean: boolean = false;
    isLeveled: boolean = false;
    parent: IEntity;

    constructor(parent: IEntity, propertyKey?: string, amount?: number, isLevel?: boolean, isBoolean?: boolean) {
        this.parent = parent;
        this.propertyKey = propertyKey;
        this.amount = amount;
        if (isLevel) { this.isLeveled = true; }
        if (isBoolean) { this.isBoolean = true; }
    }

    public static ofValues(propertyKey: string, amount: number, isLeveled?: boolean,
                           isBoolean?: boolean): EntityPropertyEffect {
        const effect = new EntityPropertyEffect(null);
        effect.propertyKey = propertyKey;
        effect.amount = amount;
        effect.isLeveled = isLeveled;
        effect.isBoolean = isBoolean;
        return effect;
    }

    public static FromStr(parent: IEntity, effectStr: string) {
        const effectStrSlittedArr = effectStr.split(" ");
        const propertyKey = effectStrSlittedArr[1];
        const fullAmountStr = effectStrSlittedArr[2];
        // cut away "+" char
        let amountStr = fullAmountStr.replace("+", "");
        // last char is "l" -> isLeveled
        const lastCharIndex = fullAmountStr.length -1;
        const isLeveled = effectStrSlittedArr[lastCharIndex] === "l";
        if (isLeveled) {
            amountStr = amountStr.substring(0, amountStr.length - 2);
        }
        // parse amount to number
        const amount = parseInt(amountStr);
        return EntityPropertyEffect.ofValues(propertyKey, amount, isLeveled, false);
    }
}
