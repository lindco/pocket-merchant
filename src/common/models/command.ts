import { Loggable } from "./log";
import { Properties } from "./properties/properties";
import * as BABYLON from 'babylonjs';

/* == INTERFACES == */
export interface IBaseCommand {
    id: string;
    props: Properties;
    justOnce: boolean;
    // methods
    onEnter();
    onTurn(): boolean;
    onExit();
}

export interface ICommandable {
    commands: Array<IBaseCommand>;
    onCommand(cmd: IBaseCommand): void;
    addCommand(cmd: IBaseCommand): void;
    // observables
    onCommand$: BABYLON.Observable<IBaseCommand>;
}

export interface ICommandQueue {
    queue: Array<IBaseCommand>;
    add(cmd: IBaseCommand);
    endCurrent();
    onTurn(): boolean;
}

/* == COMMAND MODEL == */
export class BaseCommand extends Loggable implements IBaseCommand {
    public commandable: ICommandable;
    public id: string;
    public props: Properties = new Properties();
    public justOnce: boolean = true;

    constructor(id: string, commandable?: ICommandable, props?: Properties) {
        super("BaseCommand");
        this.id = id;
        if (commandable) { this.commandable = commandable; }
        if (props) { this.props = props; }
    }

    public get canExecute(): boolean {
        if (this.commandable) {
            return true;
        }
        return false;
    }

    private checkCanExecute(): boolean {
        if (! this.canExecute) {
            this.error(`cant execute! no commandable assigned`);
            return false;
        }
        return true;
    }

    public onEnter() {

    }

    public onTurn(): boolean {
        if (! this.checkCanExecute()) {
            return;
        }
        return this.justOnce;
    }

    public onExit() {

    }

    public toString(): string {
        return `<Cmd "${this.id}">`;
    }
}

/* == COMMAND QUEUE == */
export class CommandQueue implements ICommandQueue {
    queue: IBaseCommand[];

    add(cmd: IBaseCommand) {
        throw new Error("Method not implemented.");
    }

    endCurrent() {
        throw new Error("Method not implemented.");
    }

    onTurn(): boolean {
        throw new Error("Method not implemented.");
    }

    get current(): IBaseCommand {
        if (this.queue && this.queue.length > 0) {
            return this.queue[0];
        }
        return null;
    }
}