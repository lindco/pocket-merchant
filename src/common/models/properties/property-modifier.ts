// common
import { Loggable } from "../log";
import { IProperty, NumProperty } from "./properties";

export interface IPropertyModifier {
    property: IProperty;
    absAmount: any;
}

export class AbstractPropertyModifier extends Loggable implements IPropertyModifier {
    property: IProperty;
    absAmount: any;

    constructor() {
        super("PropertyModifier");
    }
}

export class NumPropertyModifier extends AbstractPropertyModifier {
    property: NumProperty;
    // amount
    private _strValue: string;
    absAmount: number = 0.0;

    constructor(parent: NumProperty, strValue: string) {
        super();
        this.property = parent;
        // set value
        this._strValue = strValue;
        this.absAmount = parseFloat(strValue);
    }

    /* getters */
    get isIncrease(): boolean {
        return this.absAmount >= 0;
    }
}