// common
import { Loggable } from "../log";
import { DawnbringerPalette } from "../colors";
import { IPropertyModifier, NumPropertyModifier } from "./property-modifier";
// world
import { MapPos } from "../../../game/world/pos";

/* == INTERFACES == */
export interface IProperty {
    id: string;
    _value: any;
    value: any;
    color: Array<number>;
    // modifiers
    canAddModifiers: boolean;
    modifiers: Array<IPropertyModifier>;
    addModifier(modifier: IPropertyModifier);
}

export interface IPropertyHolder {
    properties: Properties;
}

/* == ABSTRACT MODEL == */
export class AbstractProperty implements IProperty {
    id: string;
    color: Array<number> = DawnbringerPalette.WHITE;
    _value: any;
    // modifiers
    canAddModifiers: boolean = false;
    modifiers: Array<IPropertyModifier> = [];

    constructor(id: string, value: any, color?: Array<number>, canAddModifiers?: boolean) {
        this.id = id;
        this._value = value;
        if (color) { this.color = color; }
        if (canAddModifiers) { this.canAddModifiers = true; }
    }

    get value(): any {
        if (this.modifiers.length <= 0) { return this._value; }
    }

    set value(value: any) { this._value = value; }

    addModifier(modifier: IPropertyModifier) {
        if (this.canAddModifiers) {
            this.modifiers.push(modifier);
        }
    }
}

/* == PROPERTY TYPES == */
/* == STRING PROPERTY == */
export class StrProperty extends AbstractProperty implements IProperty {
    _value: string = "";

    constructor(id: string, value: string, color?: Array<number>) {
        super(id, value, color);
        this._value = value;
    }

    get value(): string { return this._value; }
    set value(value: string) { this._value = value; }
}

/* == NUMBER PROPERTY == */
export class NumProperty extends AbstractProperty implements IProperty {
    _value: number = -1;

    constructor(id: string, value: number, color?: Array<number>) {
        super(id, value, color);
        this._value = value;
    }

    // return total value with modifiers
    get value(): number {
        let totalValue: number = this._value;
        for (const modifier of this.modifiers) {
            if (modifier! instanceof NumPropertyModifier) { continue; }
            totalValue += modifier.absAmount;
        }
        return totalValue;
    }

    set value(value: number) { this._value = value; }
}

/* == BOOLEAN PROPERTY == */
export class BoolProperty extends AbstractProperty implements IProperty {
    _value: boolean = false;

    constructor(id: string, value: boolean, color?: Array<number>) {
        super(id, value, color);
        this._value = value;
    }

    get value(): boolean { return this._value; }
    set value(value: boolean) { this._value = value; }
}

/* == WORLD POSITION PROPERTY == */
export class WorldPosProperty extends AbstractProperty implements IProperty {
    _value: MapPos = new MapPos(0, 0);

    constructor(id: string, value: MapPos, color?: Array<number>) {
        super(id, value, color);
        this._value = value;
    }

    get value(): MapPos { return this._value; }
    set value(value: MapPos) { this._value = value; }
}

/* == STRING ARRAY PROPERTY == */
export class StrArrayProperty extends AbstractProperty implements IProperty {
    _value: Array<string> = []

    constructor(id: string, value: Array<string>, color?: Array<number>) {
        super(id, value, color);
        this._value = value;
    }

    get value(): Array<string> { return this._value; }
    set value(value: Array<string>) { this._value = value; }
}

/* == NUMBER ARRAY PROPERTY */
export class NumArrayProperty extends AbstractProperty implements IProperty {
    _value: Array<number> = []

    constructor(id: string, value: Array<number>, color?: Array<number>) {
        super(id, value, color);
        this._value = value;
    }

    get value(): Array<number> { return this._value; }
    set value(value: Array<number>) { this._value = value; }
}

/* == PROPERTY LIST == */
export class Properties extends Loggable {
    name: string = null;
    props: Array<IProperty> = [];
    private ids: Array<string> = [];

    constructor(name?: string) {
        super("Properties");
        if ( name ) { this.name = name; }
    }

    public get(id: string): any {
        for (const prop of this.props) {
            if (prop.id === id) { return prop._value; }
        }
        console.error('no property named "' + id + '"');
        return null;
    }

    public getProperty(id: string) {
        for ( const prop of this.props ) {
            if ( prop.id == id ) { return prop; }
        }
        // this.error(`cant get property "${id}"`);
        return null;
    }

    add(prop: IProperty): void {
        this.ids.push(prop.id);
        this.props.push(prop);
    }

    addString(id: string, value: string, color?: Array<number>) {
        this.add( new StrProperty(id, value, color) );
    }

    addInteger(id: string, value: number, color?: Array<number>, randomTo?: number) {
        // generate random?
        if( randomTo && randomTo > 1) { value = Math.floor(Math.random() * randomTo); }
        this.add( new NumProperty(id, value, color) );
    }

    addFloat(id: string, value: number, color?: Array<number>) {
        this.add( new NumProperty(id, value, color) );
    }

    addBoolean(id: string, value: boolean, color?: Array<number>) {
        this.add( new BoolProperty(id, value, color) );
    }

    addWorldPos(id: string, value: MapPos, color?: Array<number>) {
        this.add( new WorldPosProperty(id, value, color) );
    }

    addStrArray(id: string, value: Array<string>, color?: Array<number>) {
        this.add( new StrArrayProperty(id, value, color) );
    }

    addNumArray(id: string, value: Array<number>, color?: Array<number>) {
        this.add( new NumArrayProperty(id, value, color) );
    }

    // getters
    getStr(id: string): string { return this.get(id) as string; }
    getNum(id: string): number { return this.get(id) as number; }
    getBool(id: string): boolean { return this.get(id) as boolean; }
    getWorldPos(id: string): MapPos { return this.get(id) as MapPos; }
    getStrArray(id: string): Array<string> { return this.get(id) as Array<string>; }
    getNumArray(id: string): Array<number> { return this.get(id) as Array<number>; }
}