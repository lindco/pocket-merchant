import { BaseEventListener } from "./events";
import * as BABYLON from 'babylonjs';

export class TurnStageNumber {
    public static get INITIALIZING() { return 0; }
    public static get ACTIVE() { return 1; }
    public static get PROCESSING() { return 2; }
    public static get FINISHED() { return 3; }
}

export interface ITurn {
    num: number;
    stage: number;
    nextStage(): number;
    onStageFinished(stage: number): void;
    // observables
    onAfterTurnStage$: BABYLON.Observable<number>;
}

export class Turn extends BaseEventListener implements ITurn {
    public num: number;
    public stage: number = 1;
    // observables
    onAfterTurnStage$ = new BABYLON.Observable<number>();

    constructor(num: number) {
        super(`Turn ${num}`);
        this.num = num;
    }

    public nextStage(): number {
        this.onStageFinished(this.stage);
        const newStageNum = this.stage + 1;
        if (newStageNum == TurnStageNumber.FINISHED) {
            this.onTurnFinished();
            return null;
        } else {
            this.stage = newStageNum;
            return this.stage;
        }
        
    }

    public onStageFinished(stage: number) {
        // this.log(`stage finished: ${stage}`);
    }

    public onTurnFinished(): void { globalThis.TURNS.onTurnFinished(this); }

    public toString(): string {
        return `<Turn ${this.num} stage=${this.stage}>`;
    }
}