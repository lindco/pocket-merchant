import * as BABYLON from 'babylonjs';

export class DawnbringerPalette {
    // 1-8
    static get BLACK() { return [0, 0, 0]; }
    static get VALHALLA() { return [0.13, 0.12, 0.20]; }
    static get LOULOU() { return [0.27, 0.16, 0.24]; }
    static get OILEDCEDAR() { return [0.40, 0.22, 0.20]; }
    static get ROPE() { return [0.56, 0.34, 0.23]; }
    static get TAHITIGOLD() { return [0.88, 0.44, 0.15]; }
    static get TWINE() { return [0.85, 0.63, 0.4]; }
    static get PANCHO() { return [0.93, 0.77, 0.6]; }
    // 9-16
    static get GOLDENFIZZ() { return [0.98, 0.95, 0.21]; }
    static get ATLANTIS() { return [0.6, 0.9, 0.31]; }
    static get CHRISTI() { return [0.41, 0.75, 0.16]; }
    static get ELFGREEN() { return [0.22, 0.58, 0.43]; }
    static get DELL() { return [0.30, 0.41, 0.18]; }
    static get VERDIGRIS() { return [0.32, 0.29, 0.14]; }
    static get OPAL() { return [0.2, .24, 0.23]; }
    static get DEEPKOAMARU() { return [0.25, 0.25, 0.46]; }
    // 17-24
    static get VENICEBLUE() { return [0.2, 0.23, 0.52]; }
    static get ROYALBLUE() { return [0.36, 0.43, 0.88]; }
    static get CORNFLOWER() { return [0.4, 0.61, 1.0]; }
    static get VIKING() { return [0.38, 0.8, 0.89]; }
    static get LIGHTSTEEL() { return [0.8, 0.86, 0.99]; }
    static get WHITE() { return [1.0, 1.0, 1.0]; }
    static get HEATHER() { return [0.61, 0.68, 0.72]; }
    static get TOPAZ() { return [0.52, 0.5, 0.53]; }
    // 25-32
    static get DIMGRAY() { return [0.41, 0.42, 0.42]; }
    static get SMOKEYASH() { return [0.35, 0.34, 0.44]; }
    static get CLAIRVOYANT() { return [0.47, 0.26, 0.54]; }
    static get BROWN() { return [0.68, 0.2, 0.2]; }
    static get MANDY() { return [0.85, 0.34, 0.39]; }
    static get PLUM() { return [0.84, 0.48, 0.73]; }
    static get RAINFOREST() { return [0.56, 0.59, 0.29]; }
    static get STINGER() { return [0.54, 0.43, 0.19]; }
    // all
    static get ALL() {
        return [
            "BLACK", "VALHALLA", "LOULOU", "OILEDCEDAR", "ROPE", "TAHITIGOLD", "TWINE", "PANCHO",
            "GOLDENFIZZ", "ATLANTIS", "CHRISTI", "ELFGREEN", "DELL", "VERDIGRIS", "OPAL", "DEEPKOAMARU",
            "VENICEBLUE", "ROYALBLUE", "CORNFLOWER", "VIKING", "LIGHTSTEEL", "WHITE", "HEATHER", "TOPAZ",
            "DIMGRAY", "SMOKEYASH", "CLAIRVOYANT", "BROWN", "MANDY", "PLUM", "RAINFOREST", "STINGER"
        ];
    }
}

export class GameColor {
    public rgb: Array<number> = [0, 0, 0];
    private isNamed: boolean = false;

    constructor(rgbOrName: Array<number> | string) {
        if (typeof rgbOrName == "string") {
            this.isNamed = true;
            const namedColorRgb = DawnbringerPalette[rgbOrName];
            this.rgb = namedColorRgb;
        } else {
            this.rgb = rgbOrName;
        }
    }
}

export class GrayTone {
    public percBlack: number;

    constructor(percBlack: number) {
        this.percBlack = percBlack;
    }

    public asArray(): Array<number> {
        return [this.percBlack, this.percBlack, this.percBlack];
    }

    public toBabylon(): BABYLON.Color3 {
        return new BABYLON.Color3(this.percBlack, this.percBlack, this.percBlack);
    }

}