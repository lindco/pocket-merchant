export interface ILoggable {
    logName: string;
    log(logstr: string): void;
}

export class Loggable implements ILoggable {
    logName: string;

    constructor(logName: string) {
        this.logName = logName;
    }

    success(message: string): void {
        console.log(this + " SUCCESS " + message);
    }

    log(message: string): void {
        console.log(this + " " + message);
    }

    warn(message: string): void {
        console.warn(this + " WARNING: " + message);
    }

    error(message: string): void {
        console.error(this + " ERROR: " + message);
    }

    /* LOG */
    toString(): string {
        return `<${this.logName}>`;
    }
}