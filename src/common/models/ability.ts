import { Loggable } from "./log";
// models
import { IEntity } from "./entity/entity";

/* == ABILITY INTERFACES == */
export interface IAbility {
    key: string;
    value: number;
    extra: string;
}

export interface IAbilityList {
    length: number;
    parent: any;
    _abilities: Array<IAbility>;
    // methods
    _addAbility(abilityObj: IAbility): IAbility;
    addAbility(key: string, value?: number, extra?: string): IAbility;
    hasAbility(key: string, minValue?: number): boolean;
    getAbility(key: string): IAbility;
}

/* == BASE ABILITY MODEL == */
export class BaseAbility extends Loggable implements IAbility {
    key: string;
    value: number = 1;
    extra: string = null;

    constructor(key: string, value?: number, extra?: string) {
        super("Ability");
        this.key = key;
        if (value) { this.value = value; }
        if (extra) { this.extra = extra; }
    }

    static FromString(abilityStr: string): BaseAbility {
        const values = abilityStr.split(" ");
        const abilityObj: BaseAbility = new BaseAbility(values[0]);
        if (values.length > 1) {
            abilityObj.value = parseFloat(values[1]);
            if (values.length > 2) { abilityObj.extra = values[2]; }
        }
        return abilityObj;
    }
}

/* == ABILITIES LIST == */
export class AbilityList extends Loggable implements IAbilityList {
    parent: any = null;
    _abilities: Array<IAbility> = [];
    _abilityKeys: Array<string> = [];

    constructor(parent?: any) {
        super("AbilityList");
        if (parent) { this.parent = parent; }
    }

    /* ABILITY GETTERS */
    getAbility(key: string): IAbility {
        if (! this._abilityKeys.includes(key)) { return null; }
        const abilityNum: number = this._abilityKeys.indexOf(key);
        return this._abilities[abilityNum];
    }

    /* returns value int of the ability or 0 when not included */
    getValue(key: string): number {
        if(! this.hasAbility(key)) { return 0; }
        return this.getAbility(key).value;
    }

    getExtra(key: string): string {
        if(! this.hasAbility(key)) { return; }
        return this.getAbility(key).extra;
    }

    get length(): number { return this._abilities.length; }

    /* ABILITY ADDING */
    _addAbility(abilityObj: IAbility): IAbility {
        this._abilityKeys.push(abilityObj.key);
        this._abilities.push(abilityObj);
        return abilityObj;
    }

    addAbility(key: string, value?: number, extra?: string): IAbility {
        const abilityObj: BaseAbility = new BaseAbility(key, value, extra);
        return this._addAbility(abilityObj);
    }

    /* ABILITY CHECKS */
    hasAbility(key: string, minValue?: number): boolean {
        const ability: IAbility = this.getAbility(key);
        // ability not in list?
        if (ability) {
            if (! minValue) { return true; }
            if (minValue && ability.value && ability.value >= minValue) { return true; }
        }
        return false;
    }

    /* ABILITY STATIC METHODS */
    static FromString(abilitiesStr, setParent?: any): AbilityList {
        // create new AbilityList object
        const abilityList = new AbilityList();
        if (setParent) { abilityList.parent = setParent; }
        // return empty list when string is empty
        const processString: boolean = abilitiesStr && abilitiesStr.length > 0;
        if (! processString) { return abilityList; }
        // or add abilities from the comma seperated strings
        const splittedStrings = abilitiesStr.split(', ');
        for (const abilityStr of splittedStrings) {
            abilityList._addAbility( BaseAbility.FromString(abilityStr) );
        }
        return abilityList;
    }
}

/* == ABILITY CONSTANTS == */
export const ABILITY_REALM_BATTLE = 0;
export const ABILITY_REALM_BATTLE_STATIC = 1;
export const ABILITY_REALM_WORLD = 2;
export const ABILITY_REALM_WORLD_STATIC = 3;