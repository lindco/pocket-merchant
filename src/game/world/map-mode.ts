import { Loggable } from "../../common/models/log";
import * as BABYLON from 'babylonjs';

export interface IMapMode {
    id: string;
    meshes: Array<BABYLON.AbstractMesh>;
    // methods
    apply(): void;
    revert(): void;
}


export class AbstractMapMode extends Loggable implements IMapMode {
    id: string = MAP_MODE_ABSTRACT;
    meshes: Array<BABYLON.AbstractMesh> = [];

    constructor() {
        super("AbstractMapMode");
    }

    apply() {

    }
    
    revert() {
        this.deleteMapModeMeshes();
    }

    private deleteMapModeMeshes() {
        for (let mapModeMesh of this.meshes) {
            mapModeMesh.dispose;
        }
    }
}

export const MAP_MODE_ABSTRACT = null;