import { DawnbringerPalette, GrayTone } from '../../common/models/colors';
import { GameOptions } from '../models/common/game/game-options';
import { Properties } from '../../common/models/properties/properties';
import { MapTile } from './tile/tile';
import { TerrainTypes } from './tile/terrain-types';
import {
    RandomStartPosLoader,
    SpecialResLoader,
    WorldLandscapeLoader,
    WorldPropertiesLoader,
    WorldTilesLoader
} from '../managers/world/world-loaders';
import { Size, MapPos } from './pos';
import { LandscapeFeature } from './feature';
import * as BABYLON from 'babylonjs';
import { IWorldMap } from "./interfaces/map.interface";
import { AbstractWorldMap } from "./tile/abstract-map";
import { TileMaterials } from "./tile/tile-materials";

/* == WORLD MAP MODEL == */
export class WorldMap extends AbstractWorldMap implements IWorldMap {
    // properties
    name: string;
    size: Size = new Size(100, 100);
    properties: Properties = null;
    // tiles
    terrainIds: Array<string>;
    tiles: MapTile[][] = [];
    playerStartingPos: MapPos = null;
    // landscape
    features: Array<LandscapeFeature> = [];
    // materials
    mats: TileMaterials = null;

    constructor(name: string, size: Size) {
        super("WorldMap");
        this.name = name;
        this.size = size;
        this.terrainIds = TerrainTypes.LIST;
        this.generateOrLoadTiles();
    }

    public generateOrLoadTiles(): void {
        let generate: boolean = globalThis.OPTIONS.getBool(GameOptions.GENERATE_LANDSCAPE);
        this.log(`loading tiles: random? ${generate}`);
        if (generate) {
            this._generateTiles();
        } else {
            this.loadMapContent();
        }
    }

    public loadMapContent() {
        super.loadMapContent();
        this._loadProperties();
        this._loadSpecialRes();
        this._loadTiles();
        this._loadFeatures();
        this._loadStartingPos();
    }

    private _loadProperties() {
        this.properties = WorldPropertiesLoader.load();
    }

    private _loadStartingPos(): void {
        this.playerStartingPos = RandomStartPosLoader.load();
    }

    private _generateTiles(): void {
        let generatedTilesCount: number = 0;
        for (let z = 0; z < this.size.z; z++) {
            let tileRow: Array<MapTile> = [];
            for (let x = 0; x < this.size.x; x++) {
                let terrain: string = this.getRandomTerrain();
                // add tile & increase counter
                tileRow.push(new MapTile(new MapPos(x, z), terrain));
                generatedTilesCount += 1;
            }  
            this.tiles.push(tileRow);
        }
        this.log(`${generatedTilesCount} tiles generated`);
    }

    private _loadTiles(): void {
        this.tiles = new WorldTilesLoader().load();
        // reset map size
        this.size = new Size(this.tiles[0].length, this.tiles.length);
        this.log(`resized to ${this.size.x}x${this.size.z}`);
    }

    private _loadFeatures(): void {
        this.features = new WorldLandscapeLoader().load();
    }

    private _loadSpecialRes(): void {
        globalThis.SPECIAL_RES = new SpecialResLoader().load();
    }

    private getRandomTerrain(): string {
        const landscapeTerrains = TerrainTypes.LANDSCAPE;
        const rndInt = Math.floor(Math.random() * landscapeTerrains.length);
        return landscapeTerrains[rndInt];
    }

    tileAt(x: number, z: number): MapTile {
        let row: Array<MapTile> = this.tiles[z];
        if (! row) {
            this.warn(`cant find row ${z} in ${this.tiles.length} tile array rows`); return;
        }
        let tileAt: MapTile = row[x];
        if (! tileAt) {
            console.error(`cant get tile at ${x} ${z}`); return null;
        }
        return tileAt;
    }

    tileAtPos(pos: MapPos): MapTile {
        return this.tileAt(pos.x, pos.z);
    }

    // ???
    public createMaterials(): void {
        /* == DEFINE TERRAIN COLORS HERE == */
        this.mats = new TileMaterials(this);
        this.mats.add(TerrainTypes.WATER, 
            this.paletteColor(DawnbringerPalette.CORNFLOWER), new GrayTone(0.5).toBabylon(), 128);
        this.mats.add(TerrainTypes.GRASS, 
            this.paletteColor(DawnbringerPalette.CHRISTI), new GrayTone(0.05).toBabylon(), 128);
        this.mats.add(TerrainTypes.HILL, 
            this.paletteColor(DawnbringerPalette.ROPE), new GrayTone(0.05).toBabylon(), 128);
        this.mats.add(TerrainTypes.FOREST, 
        this.paletteColor(DawnbringerPalette.DELL), new GrayTone(0.05).toBabylon(), 128);
        this.mats.add(TerrainTypes.STONE, 
            this.paletteColor(DawnbringerPalette.SMOKEYASH), new GrayTone(0.01).toBabylon(), 128);
        this.mats.add(TerrainTypes.BARREN, 
            this.paletteColor(DawnbringerPalette.DIMGRAY), new GrayTone(0.01).toBabylon(), 128);
        this.mats.add(TerrainTypes.MOUNTAIN, 
            this.paletteColor(DawnbringerPalette.TOPAZ), new GrayTone(0.01).toBabylon(), 128);
        this.mats.add(TerrainTypes.PEAK, 
            this.paletteColor(DawnbringerPalette.LIGHTSTEEL), new GrayTone(0.01).toBabylon(), 128);
        this.mats.add(TerrainTypes.CAVE, 
            this.paletteColor(DawnbringerPalette.SMOKEYASH), new GrayTone(0.01).toBabylon(), 128);
        this.mats.add(TerrainTypes.EARTH, this.paletteColor(DawnbringerPalette.ROPE));
        this.mats.add(TerrainTypes.DESERT, this.paletteColor(DawnbringerPalette.PANCHO));
        this.mats.add(TerrainTypes.SAVANNAH, this.paletteColor(DawnbringerPalette.RAINFOREST));
        this.mats.add(TerrainTypes.CITY, this.paletteColor(DawnbringerPalette.PANCHO));
        this.mats.add(TerrainTypes.ARCTIC, this.paletteColor(DawnbringerPalette.LIGHTSTEEL));
        this.mats.add(TerrainTypes.BRIDGE, this.paletteColor(DawnbringerPalette.VIKING));
        this.mats.add(TerrainTypes.LAVA, this.paletteColor(DawnbringerPalette.TAHITIGOLD));
        this.log(`created ${this.mats.length} tile materials`);
    }

    private paletteColor(rgb: Array<number>): BABYLON.Color3 {
        return new BABYLON.Color3(rgb[0], rgb[1], rgb[2]);
    }

    isInsideOfMap(pos: MapPos): boolean {
        return !(pos.x <0 || pos.x > this.size.x || pos.z <0 || pos.z > this.size.z);
    }

    public getMaterial(key: string): BABYLON.StandardMaterial {
        return this.mats.get(key);
    }

    public toString(): string {
        return `<WorldMap "${this.name}>`;
    }

    public makePos(x: number, z: number): MapPos {
        return new MapPos(x, z);
    }
}

/* GLOBAL METHODS */
export function tileAt(x: number, z: number): MapTile {
    return globalThis.MAP.tileAt(x, z);
}