import { MapPos } from "./pos";

/* == INTERFACES == */
export interface ILandscapeFeature {
    key: string;
    pos: MapPos;
}

/* == MODEL == */
export class LandscapeFeature implements ILandscapeFeature {
    key: string;
    pos: MapPos;

    constructor(key: string, pos: MapPos) {
        this.key = key;
        this.pos = pos;
    }

    static FromString(landscapeFeatureStr: string): LandscapeFeature {
        const featureData: Array<string> = landscapeFeatureStr.split(" ");
        const featureId: string = featureData[0];
        const featurePosStr: string = featureData[1] + " " + featureData[2];
        const mapPos: MapPos = MapPos.FromString(featurePosStr);
        return new LandscapeFeature(featureId, mapPos);
    }
}