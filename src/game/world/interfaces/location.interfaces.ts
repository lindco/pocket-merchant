import { IWorldEntity } from "../../../common/models/entity/entity";
import { LocationProperties } from "../../models/city/location-properties";
import { IRoad } from "./road.interface";
import { MapTile } from "../tile/tile";
import { Building } from "../../models/building/building";
import { TradeConnection } from "../../models/trade/connection/connection";
import { ITradeHistory, ITradeHistoryKeeper } from "../../models/trade/interfaces/trade-history.interface";
import { TradeNode } from "../../models/trade/trade-node/trade-node";
import { StoredResource } from "../../models/trade/resource/stored-resource";
import * as BABYLON from "babylonjs";
import { IEmpire, IFaction, IFamily } from "../../models/faction/faction-interfaces";

export interface ILocation extends IWorldEntity {
    // basic properties
    key: string;
    level: number;
    name: string;
    mesh: BABYLON.AbstractMesh;
    properties: LocationProperties;
    money: number;
    // flags
    canTrade: boolean;
    // world links
    roads: Array<IRoad>;
    tile: MapTile;
    neighborTiles: Array<MapTile>;
    neighborTerrainIds: Array<string>;
    terrainId: string;
    // buildings
    buildings: Array<Building>;
    // methods
    equals(otherLocation: ILocation): boolean;
    addNewBuilding(bldType: string): void;
}

export interface IConnectedLocation extends ILocation {
    // connections
    connections: Array<TradeConnection>;
    // methods
    getConnectionTo(target: IConnectedLocation): TradeConnection;
    addConnectionTo(targetLocation: IConnectedLocation, addMirrored?: boolean): TradeConnection;
    getConnectedLocations(): Array<IConnectedLocation>;
}

export interface IOwnedLocation extends IConnectedLocation {
    owner: IFaction;
    ownerEmpire: IEmpire;
    ownerFamily: IFamily;
    ownerName: string;
    _ownerFactionNum: number;
    _factionKey: string;
    // methods
    getOwner(): IFaction;
}

export interface ITradeLocation extends IOwnedLocation, ITradeHistoryKeeper {
    tradeNode: TradeNode;
    tradeHistory: ITradeHistory;
    // methods
    addTradeConnectionTo(targetLocation: ITradeLocation, addMirrored?: boolean): TradeConnection
    formCaravan(target: ITradeLocation, resId: string): void;
    canAfford(resId: string, amount: number): boolean;
    getStoredResources(): Array<StoredResource>;
    canProduce(resId: string): boolean;
    produceResource(resId: string, amount: number): void;
}