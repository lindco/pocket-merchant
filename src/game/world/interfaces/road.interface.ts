import { MapTile } from "../tile/tile";
import { IConnectedLocation } from "./location.interfaces";

export interface IRoad {
    num: number;
    start: IConnectedLocation;
    target: IConnectedLocation;
    level: number;
    tiles: Array<MapTile>;
    name: string;
    // methods
    findRoadPath(): void;
}