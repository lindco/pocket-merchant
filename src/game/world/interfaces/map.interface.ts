import { Size, MapPos } from "../pos";
import { Properties } from "../../../common/models/properties/properties";
import { IMapTile } from "./tile.interface";
import { ILandscapeFeature } from "../feature";
import { MapTile } from "../tile/tile";

export interface IBaseMap {
    name: string;
    size: Size;
    properties: Properties;
    tiles: IMapTile[][];
    // Methods
    generateOrLoadTiles(): void;
    tileAtPos(pos: MapPos): MapTile;
    loadMapContent(): void;
}

export interface IWorldMap extends IBaseMap {
    name: string;
    size: Size;
    // tiles
    playerStartingPos: MapPos;
    tiles: IMapTile[][];
    // landscape features
    features: Array<ILandscapeFeature>;
    // methods
    tileAtPos(pos: MapPos): MapTile;
    isInsideOfMap(pos: MapPos): boolean
}