import { IPos } from "../pos";
import { IConnection } from "../../models/trade/interfaces/connection.interfaces";
import * as BABYLON from "babylonjs";

export interface ITile {
    pos: IPos;
    terrainChar: string;
    // meshes
    meshBlueprint: BABYLON.Mesh;
    meshInstance: BABYLON.InstancedMesh;
}

export interface IMapTile extends ITile {
    offset: number;
    connections: Array<IConnection>;
}