import { DawnbringerPalette } from "../../common/models/colors";
import { WorldEventEntity } from "../../common/models/entity/entity";
import { Building, IBuildingInstance } from "../models/building/building";
import { MapTile } from "./tile/tile";
import { MapPos } from "./pos";
import { MarketTrade } from "../models/trade/market-trade";
import { TradeNode } from "../models/trade/trade-node/trade-node";
import { TradeConnection } from "../models/trade/connection/connection";
import { StoredResource } from "../models/trade/resource/stored-resource";
import { ITradeHistoryKeeper } from "../models/trade/interfaces/trade-history.interface";
import { TradeHistory } from "../models/trade/trade-history";
import { LocationProperties } from "../models/city/location-properties";
import { IRoad } from "./interfaces/road.interface";
import { IConnectedLocation, ILocation, IOwnedLocation, ITradeLocation } from "./interfaces/location.interfaces";
import * as BABYLON from 'babylonjs';
import { IEmpire, IFaction, IFamily } from "../models/faction/faction-interfaces";

/* == ABSTRACT LOCATION MODEL == */
export class AbstractLocation extends WorldEventEntity implements ILocation  {
    // basic properties
    key: string = null;
    level = 1;
    mesh: BABYLON.AbstractMesh;
    name: string;
    properties: LocationProperties = new LocationProperties(this);
    // flags
    canTrade: boolean = false;
    // world properties
    neighborTiles: Array<MapTile> = [];
    roads: Array<IRoad> = [];
    // buildings
    buildings: Array<Building> = [];
    // observables
    onConstructionFinished$ = new BABYLON.Observable<IBuildingInstance>();

    constructor(num: number, pos: MapPos, name: string) {
        super(num, pos);
        this.name = name;
        this.randomizeStartingMoney();
    }

    randomizeStartingMoney() {
        this.properties.addInteger(PROPERTY_MONEY, -1, DawnbringerPalette.GOLDENFIZZ, 1000);
    }

    public equals(otherCity: ILocation) {
        return this.name == otherCity.name;
    }

    set money(value: number) { this.properties.getProperty(PROPERTY_MONEY).value = value; }
    get money(): number { return this.properties.getNum(PROPERTY_MONEY); }

    /* TILE */
    get tile(): MapTile {
        return globalThis.MAP.tileAtPos(this.pos);
    }

    get neighborTerrainIds(): string[] { return this.tile.neighborTerrains; }
    get terrainId(): string { return this.tile.terrainChar; }

    /* BUILDINGS */
    public addNewBuilding(bldType: string, level?: number) {
        // this.log(`adding building ${bldType}`);
        if (! level) { level = 0; }
        const bldInstance: Building = new Building(bldType, "", level);
        this.buildings.push(bldInstance);
        // fire ConstructionFinishedEvent
        this.onConstructionFinished$.notifyObservers(bldInstance);
    }
}

export class ConnectedLocation extends AbstractLocation implements IConnectedLocation {
    connections: Array<TradeConnection> = [];

    constructor(num: number, pos: MapPos, name: string) {
        super(num, pos, name);
    }

    getConnectionTo(target: IConnectedLocation): TradeConnection {
        for (const connection of this.connections) {
            const isToTarget = connection.target.equals(target);
            if (isToTarget) { return connection; }
        }
        return null;
    }

    addConnectionTo(targetLocation: IConnectedLocation, addMirrored?: boolean): TradeConnection {
        let connection: TradeConnection = new TradeConnection(this, targetLocation);
        // this.log(`additional connection between ${this.name} and ${targetCity.name} created`);
        this.connections.push(connection);
        // this.tradeNode.linkedMarkets.push(targetLocation.tradeNode);
        if (addMirrored) { targetLocation.addConnectionTo(this, false); }
        return connection;
    }

    getConnectedLocations(): Array<IConnectedLocation> {
        let connected: Array<IConnectedLocation> = [];
        for (const connection of this.connections) {
            if (connection.target.key != this.key) connected.push(connection.target);
        } // end of connections loop
        return connected;
    }
}

/* == OWNED LOCATION MODEL == */
export class OwnedLocation extends ConnectedLocation implements IOwnedLocation {
    owner: IFaction = null;
    _ownerFactionNum: number;
    _familyNum: number;
    _factionKey: string;

    constructor(num: number, pos: MapPos, name: string, owner?: IFaction) {
        super(num, pos, name);
        if (owner) { this.owner = owner; }
    }

    getOwner(): IFaction {
        return this.owner;
    }

    get ownerEmpire(): IEmpire {
        if (! globalThis.FACTIONS) {
            this.warn(`cant get empire of "${ this.name }"; FactionManager not found`); return;
        }
        return globalThis.FACTIONS.getEmpireByKey(this._factionKey);
    }

    get ownerFamily(): IFamily {
        if (! globalThis.FACTIONS) {
            this.warn(`cant get family of "${ this.name }"; FactionManager not found`); return;
        }
        if (this._familyNum) {
            return globalThis.FACTIONS.getFamilyByNum(this._familyNum);
        }
        return;
    }

    get ownerName(): string {
        if (this.ownerFamily) { return this.ownerFamily.name; }
        if (this.ownerEmpire) { return this.ownerEmpire.name; }
        return "Independent";
    }
    
}

/* == TRADE LOCATION MODELS == */
export class TradeLocation extends OwnedLocation implements ITradeLocation, ITradeHistoryKeeper {
    tradeNode: TradeNode;
    tradeHistory: TradeHistory;
    canTrade: boolean = true;

    constructor(num: number, pos: MapPos, name: string, owner?: IFaction) {
        super(num, pos, name, owner);
        this.tradeHistory = new TradeHistory(this);
    }

    addTradeConnectionTo(targetLocation: ITradeLocation, addMirrored?: boolean): TradeConnection {
        let connection = super.addConnectionTo(targetLocation, addMirrored);
        this.tradeNode.linked.push(targetLocation.tradeNode);
        return connection;
    }

    tradeIsExport(trade: MarketTrade): boolean { return this.tradeNode.tradeIsExport(trade); }

    /* RESOURCES */
    canAfford(resId: string, amount: number): boolean {
        const singlePrice = this.tradeNode.priceOf(resId);
        const totalPrice = amount * singlePrice;
        return this.money >= totalPrice;

    }

    getStoredResources(): Array<StoredResource> {
        let actuallyStored: Array<StoredResource> = []
        for (const storedRes of this.tradeNode.asStoresResArr()) {
            let resAmount = this.tradeNode.amountOf(storedRes.resId);
            if ( resAmount > 0 ) {
                let resPrice: number = this.tradeNode.priceOf(storedRes.resId);
                actuallyStored.push( new StoredResource(storedRes.resId, resAmount, resPrice) );
            }
        }
        return actuallyStored;
    }

    canProduce(resId: string): boolean {
        const resObj = globalThis.TRADE.GetRes(resId);
        // can build on city tile terrain?
        if (resObj.canBuildProdBldOn(this.terrainId)) { return true; }
        // can build on neighboring tile terrain?
        for (const terrainChar of this.neighborTerrainIds) {
            if (resObj.canBuildProdBldOn(terrainChar)) { return true; }
        }
        return false;
    }

    produceResource(resId: string, amount: number): void {
        this.tradeNode.add(resId, amount);
    }

    /* CARAVAMS */
    formCaravan(target: ITradeLocation, resId: string, teleport?: boolean) {
        const resName = globalThis.TRADE.getResName(resId);
        this.log(`forming ${resName} caravan to ${target.name}`);
    }

    /* CONNECTIONS */
    getConnectedTradeLocations(): Array<ITradeLocation> {
        let tradeLocations: Array<ITradeLocation> = [];
        for (const connected of this.getConnectedLocations()) {
            if (connected instanceof TradeLocation) { tradeLocations.push(connected); }
        }
        return tradeLocations;
    }
}

/* == CONSTANTS == */
export const PROPERTY_MONEY = "money";