import { Loggable } from "../../common/models/log";
import { MapPos } from "./pos";
import * as BABYLON from 'babylonjs';

export class TargetCursor extends Loggable {
    pos: MapPos;
    mesh: BABYLON.AbstractMesh;

    constructor() {
        super("TargetCursor");
    }

    /* STATIC CURSOR CONSTANTS */
    static get NODE_ID() { return "cursor"; }
    static get DEFAULT_ASSET_NAME() { return "selection_circle"; }
}