import {RandomInt} from "../../helpers/random";
import {WorldTools} from "../../helpers/world-tools";
import * as BABYLON from 'babylonjs';

/* POS INTERFACES */
export interface IVec2 {
    x: number;
    z: number;
}

export interface IPos extends IVec2 {
    type: string;
}

/* SIZE */
export class Size implements IVec2 {
    x: number;
    z: number;

    constructor(x: number, z: number) {
        this.x = x;
        this.z = z;
    }

    toString() {
        return `${this.x}x${this.z}`;
    }
}

/* POS TYPE ENUM */
export class PosTypes {
    static get ABSTRACT() { return "abstract"; }
    static get WORLD() { return "world"; }
    static get WORLD_REL() { return "rel-world"; }
    static get LOCATION() { return "location"; }
}

export class AbstractPos implements IPos {
    x: number = -1;
    z: number = -1;
    type: string = PosTypes.ABSTRACT;
    extraData: string = null;

    constructor(x: number, z: number, parseInts?: boolean ) {
        if ( parseInts ) {
            this.x = Math.round(x);
            this.z = Math.round(z);
        } else {
            this.x = x;
            this.z = z;
        }
    }
}

/**
 * 2D-Tile-Position on the world map with an x & y, but no z value
 */
export class MapPos extends AbstractPos {
    type: string = PosTypes.WORLD;

    static random(): MapPos {
        if (! globalThis.MAP) {
            console.error(`WorldPos: cant generate random; map not loaded`);
            return null;
        }
        const mapSize = globalThis.MAP.size;
        let rndXZ: Array<number> = [ RandomInt.below(mapSize.x), RandomInt.below(mapSize.z) ];
        return new MapPos(rndXZ[0], rndXZ[1]);
    }

    inDirection(dir: HexDirection): IPos {
        const relPos = dir.relPos;
        const xz = [this.x + relPos.x, this.z + relPos.z];
        return new MapPos(xz[0], xz[1]);
    }

    static fromXZArray(xz: Array<number>) {
        return new MapPos( xz[0], xz[1] );
    }

    static FromString(str: string, pipeDivider?: boolean): MapPos {
        let dividerChar: string = " ";
        if ( pipeDivider ) {
            dividerChar = "|"
        }
        // get x & z position values from string
        const asArray = str.split(dividerChar);
        const xz = [ parseInt(asArray[0]), parseInt(asArray[1]) ];
        const pos = MapPos.fromXZArray(xz);
        // add extra info str?
        if( asArray.length > 2 ) { pos.extraData = asArray[2]; }
        return pos;
    }

    equals(otherPos: MapPos): boolean {
        return this.x == otherPos.x && this.z == otherPos.z;
    }

    asString(): string {
        const posDivider = "|";
        return this.x + posDivider + this.z;
    }

    withRelPos(relPos: MapPos): MapPos {
        const absPosNumArr: Array<number> = [ this.x + relPos.x, this.z + relPos.z ];
        return MapPos.fromXZArray(absPosNumArr);
    }

    /* returns the sreen x y coordianes of a world position: currently doesnt works tho */
    toWindowPos(): Array<number> {
        const coords = BABYLON.Vector3.Project(
            WorldTools.worldToScenePos(this),
            BABYLON.Matrix.Identity(),
            globalThis.SCENE.getTransformMatrix(),
            globalThis.CAMERA.viewport.toGlobal(
                globalThis.WORLD.engine.getRenderWidth(),
                globalThis.WORLD.engine.getRenderHeight()
            )
        )
        return null;
    }

    /* returns whether a pos has an uneven z value and is with that moved half a tile space to the right */
    hasUnevenZ(): boolean {
        return this.z % 2 == 1;
    }

    get neighborRelPositions(): Array<MapPos> {
        let neighborRelPositions: Array<MapPos> = [ new MapPos(-1, 0), new MapPos(1, 0) ];
        let relStartX = 0;
        if (this.hasUnevenZ) { relStartX = -1 }
        // add two pos to the top
        neighborRelPositions.concat( [ new WorldRelPos(relStartX, -1), new WorldRelPos(relStartX +1, -1) ] );
        // .. and to the bottom of current pos
        neighborRelPositions.concat( [ new WorldRelPos(relStartX, 1), new WorldRelPos(relStartX +1, 1) ] );
        return neighborRelPositions;
    }

    get neighborAbsPositions(): Array<MapPos> {
        let neighborAbsPositions: Array<MapPos> = [];
        for (const neighborRelPos of this.neighborRelPositions) {
            const absPos: MapPos = this.withRelPos(neighborRelPos);
            neighborAbsPositions.push(absPos);
        }
        return neighborAbsPositions;
    }

    distanceTo(otherPos: MapPos): number {
        const distX = Math.abs(this.x - otherPos.x);
        const distZ = Math.abs(this.z - otherPos.z);
        return distX + distZ;
    }

    closestPosToTarget(targetPos: MapPos): MapPos {
        let dist = [ this.x - targetPos.x, this.z - targetPos.z ];
        let distAbs = [ Math.abs( dist[0] ), Math.abs( dist[1] ) ]; 
        // x or z axis?
        let relXZ = [ dist[0] / distAbs[0], 0 ];
        if ( distAbs[1] > distAbs[0 ]) {
            relXZ = [ 0, dist[1] / distAbs[1] ];
        }
        return new MapPos(this.x + relXZ[0], this.z + relXZ[1]);
    }

}

export class WorldRelPos extends MapPos {
    type: string = PosTypes.WORLD_REL;

    constructor(x: number, z: number, parseInts?: boolean) {
        super(x, z, parseInts);
    }
}

export class LocationPos extends AbstractPos {
    type: string = PosTypes.LOCATION;
}

/* DIRECTION */
export class HexDirection {
    public dirStr: string;
    public static get WEST() { return "W"; }
    public static get EAST() { return "E"; }
    public static get NORTHWEST() { return "NW"; }
    public static get NORTHEAST() { return "NE"; }
    public static get SOUTHWEST() { return "SW"; }
    public static get SOUTHEAST() { return "SE"; }
    public static get ALL() {
        return [HexDirection.WEST, HexDirection.EAST, HexDirection.NORTHWEST,
            HexDirection.NORTHEAST, HexDirection.SOUTHWEST, HexDirection.SOUTHEAST];
    }

    constructor(dirStr: string) {
        this.dirStr = dirStr;
    }

    public get relPos(): MapPos {
        switch (this.dirStr) {
            case "W": return new MapPos(-1, 0);
            case "E": return new MapPos(1, 0);
            case "NW": return new MapPos(-1, -1);
            case "NE": return new MapPos(0, -1);
            case "SW": return new MapPos(-1, 1);
            case "SE": return new MapPos(1, 1);
            default: return new MapPos(0, 0);
        }
    }
}

export const AXIS_X = 0;
export const AXIS_Z = 1;