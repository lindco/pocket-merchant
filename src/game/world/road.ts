import { IUniqueInstance, WorldEntity } from "../../common/models/entity/entity";
import { IRoad } from "./interfaces/road.interface";
import { MapTile } from "./tile/tile";
import { IConnectedLocation } from "./interfaces/location.interfaces";

export class Road extends WorldEntity implements IRoad, IUniqueInstance {
    num: number = -1;
    start: IConnectedLocation;
    target: IConnectedLocation;
    level: number = 1;
    tiles: Array<MapTile> = [];

    constructor(start: IConnectedLocation, target: IConnectedLocation, num?: number) {
        super(num, start.pos);
        // cities
        this.start = start;
        this.target = target;
        // road num
        if (num) { this.num = num; }
    }

    get name(): string {
        return `${this.start.name}-${this.target.name}`;
    }

    toString(): string {
        return `<Road "${this.name}">`;
    }

    getMirrored(): IRoad {
        if (! globalThis.TRADE) {
            this.warn(`cant get mirrored road from ${this.target.name} to ${this.start.name}`);
            return null;
        }
        return globalThis.TRADE.getReturnRoad(this);
    }

    /* PATHFINDING */
    findRoadPath(): void {
        const SKIP = true;
        if (SKIP) { return; }
        let currentTile: MapTile = this.start.tile;
        let isAtTarget = currentTile == null;
        // next tile
        while(! isAtTarget) {
            this.tiles.push(currentTile);
            currentTile = this._getNextTile(currentTile);
        }
    }

    private _getNextTile(fromTile: MapTile): MapTile {
        if (fromTile.pos.equals(this.target.tile.pos)) {
            return null;
        }
        const distX = this.target.pos.x - fromTile.pos.x;
        const distZ = this.target.pos.z = fromTile.pos.z;
        if (Math.abs(distX) > Math.abs(distZ)) {
            const isPositive = distX > 0;
        } else {

        }
    }
}

/* == CONSTANTS == */