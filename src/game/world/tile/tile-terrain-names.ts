import { TerrainTypes } from "./terrain-types";


export class TerainTypeNames {
    static getName(terrainChar: string): string {
        switch (terrainChar) {
            case("G"):  return "grass";
            case("H"):  return "hills";
            case("F"):  return "forest";
            case("O"):  return "stone";
            case("M"):  return "mountain";
            case("P"):  return "peak";
            case("V"):  return "cave";
            case("E"):  return "earth";
            case("W"):  return "water";
            case("D"):  return "desert";
            case("S"):  return "savannah";
            case("C"):  return "city";
            case("A"):  return "arctic";
            case("R"):  return "barren";
            case("B"):  return "bridge";
            case("L"):  return "lava";
            case("U"):  return "unknown";
            default: return null;
        }
    }
}