/* == TILE TERRAIN TYPES == */
export class TerrainTypes {
    static get GRASS() { return "G"; }
    static get HILL() { return "H"; }
    static get FOREST() { return "F"; }
    static get STONE() { return "O"; }
    static get MOUNTAIN() { return "M"; }
    static get PEAK() { return "P"; }
    static get CAVE() { return "V"; }
    static get EARTH() { return "E"; }
    static get WATER() { return "W"; }
    static get DESERT() { return "D"; }
    static get SAVANNAH() { return "S"; }
    static get CITY() { return "C"; }
    static get ARCTIC() { return "A"; }
    static get BARREN() { return "R"; }
    static get BRIDGE() { return "B"; }
    static get LAVA() { return "L"; }
    static get UNKNOWN() { return "U"; }
    static get LIST() { return ["G", "H", "F", "O", "M", "P", "V", "E", "W", "D", "S", "C", "A", "R", "B", "L", "U"]; }
    static get LANDSCAPE() { return ["G", "H", "F", "M", "P", "E", "D", "S", "A", "R", "L"]; }
    static get TILES_WITH_MESHES() { return ["G", "H", "M", "W", "D", "F", "S", "B", "A"]; }

    static heightOf(terrId: string): number {
        switch (terrId) {
            case TerrainTypes.MOUNTAIN: return 2.1;
            case TerrainTypes.PEAK: return 2.6;
            case TerrainTypes.FOREST: return 0.9;
            case TerrainTypes.HILL: return 1.33;
            case TerrainTypes.SAVANNAH: return 0.65;
            // lower than default
            case TerrainTypes.DESERT: return 0.30;
            case TerrainTypes.BARREN: return 0.45;
            case TerrainTypes.WATER: return .01;
            case TerrainTypes.LAVA: return .02;
            case TerrainTypes.BRIDGE: return .22;
            default: return .50;
        }
    }
    static topDiameterOf(terrId: string): number {
        const HEXSIZE = 3;
        switch (terrId) {
            case TerrainTypes.UNKNOWN: return 0;
            default: return HEXSIZE;
        }
    }

    static movementSpeedOn(terrId: string): number {
        const specialMovementTerrains: {} = { "F": .6, "H": 0.75, "M": .3, "P": 0, "D": .8, "R": .65, "S": .7, "A": .5 };
        if (terrId in specialMovementTerrains) {
            return specialMovementTerrains[terrId];
        }
        return 1;
    }
}