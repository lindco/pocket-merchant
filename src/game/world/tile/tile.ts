import { HexDirection, IPos, MapPos } from '../pos';
import { Road } from '../road';
import { City } from '../../models/city/city';
import { TerrainTypes } from './terrain-types';
import { IConnection } from "../../models/trade/interfaces/connection.interfaces";
import { IMapTile } from "../interfaces/tile.interface";
// babylon
import * as BABYLON from 'babylonjs';
import { AbstractTile } from "./abstract-tile";


/* == TILE MODEL == */
export class MapTile extends AbstractTile implements IMapTile {
    pos: MapPos;
    terrainChar: string;
    offset: number = 0;
    // models
    connections: IConnection[] = [];
    roads: Array<Road> = [];
    city: City = null;
    // meshes
    meshBlueprint: BABYLON.Mesh = null;
    meshInstance: BABYLON.InstancedMesh = null;

    constructor(pos: MapPos, terrainChar: string) {
        super("WorldTile");
        this.pos = pos;
        this.terrainChar = terrainChar;
    }

    toString(): string {
        const posStr = `${this.pos.x}|${this.pos.z}`;
        return `<WorldTile "${this.terrainChar}" pos=${posStr}>`;
    }

    /* GETTERS */
    static get HEXSIZE(): number { return 3; }

    get isOnLand(): boolean {
        return !(this.terrainChar === TerrainTypes.WATER);
    }

    get meshName(): string {
        return `tile-${this.terrainChar}-${this.pos.asString()}`;
    }

    get isOnRoad(): boolean { return this.roads.length > 0; }

    get neighborTerrains(): string[] {
        let neighbTerrains: string[] = [];
        const neighbDirs: string[] = HexDirection.ALL;
        for (const dirStr of neighbDirs) {
            const dirObj: HexDirection = new HexDirection(dirStr);
            const relPos = dirObj.relPos;
            const absPos = new MapPos(this.pos.x + relPos.x, this.pos.z + relPos.z);
            const neighbTileTerrain = globalThis.MAP.tileAtPos(absPos).terrainChar;
            neighbTerrains.push(neighbTileTerrain);
        }
        return neighbTerrains;
    }
}