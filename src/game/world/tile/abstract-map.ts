import { IBaseMap } from "../interfaces/map.interface";
import { Size, MapPos } from "../pos";
import { Properties } from "../../../common/models/properties/properties";
import { IMapTile } from "../interfaces/tile.interface";
import { MapTile } from "./tile";
import { Loggable } from "../../../common/models/log";

export abstract class AbstractWorldMap extends Loggable implements IBaseMap {
    name: string;
    properties: Properties;
    size: Size;
    tiles: IMapTile[][];

    generateOrLoadTiles(): void {
    }

    loadMapContent(): void {
    }

    tileAtPos(pos: MapPos): MapTile {
        return undefined;
    }


}