import { WorldMap } from "../map";
import * as BABYLON from "babylonjs";

/* TILE MATERIALS */
export class TileMaterials {
    private _map: WorldMap;
    private _keys: Array<string> = [];
    private _mats: Array<BABYLON.StandardMaterial> = [];

    constructor(map: WorldMap) {
        this._map = map;
    }

    add(name: string, diffuseColor: BABYLON.Color3, specularColor?: BABYLON.Color3, specularPower?: number) {
        let mat: BABYLON.StandardMaterial = new BABYLON.StandardMaterial(name, globalThis.SCENE);
        mat.diffuseColor = diffuseColor;
        if (specularColor) {
            mat.specularColor = specularColor;
            if (specularPower) {
                mat.specularPower = specularPower;
            }
        }
        this._keys.push(name);
        this._mats.push(mat);
        // console.log(`material ${name} created: ${mat}`);
    }

    get(key: string): BABYLON.StandardMaterial {
        const matIndex: number = this._keys.indexOf(key);
        return this._mats[matIndex];
    }

    public get length(): number {
        return this._keys.length;
    }
}