import { Loggable } from "../../../common/models/log";
import { ITile } from "../interfaces/tile.interface";
import { IPos } from "../pos";
import * as BABYLON from "babylonjs";

export abstract class AbstractTile extends Loggable implements ITile{
    meshBlueprint: BABYLON.Mesh;
    meshInstance: BABYLON.InstancedMesh;
    pos: IPos;
    terrainChar: string;

}