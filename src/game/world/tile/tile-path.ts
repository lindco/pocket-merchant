// world
import { MapTile } from "./tile";
import { MapPos } from "../pos";

/* INTERFACES */
export interface IPathNode {
    parent: IPathNode;
    pos: MapPos;
    findNext(): IPathNode;
}

export interface ITilePathNode extends IPathNode {
    tile: MapTile;
}

export interface ITilePath {
    start: ITilePathNode;
    target: ITilePathNode;
    isFinished: boolean;
}

/* NODE */
export class TilePathNode implements ITilePathNode {
    tile: MapTile;
    pos: MapPos;
    parent: IPathNode = null;
    next: IPathNode = null;

    constructor(tile: MapTile, parent?: IPathNode) {
        this.tile = tile;
        this.pos = tile.pos;
        if (parent) {
            this.parent = parent;
        }
    }

    add(child: TilePathNode) {
        this.next = child;
        child.parent = this;
    }

    findNext(): TilePathNode {
        return null;
    }
}

/* PATH */
export class TilePath implements ITilePath {
    start: ITilePathNode;
    target: ITilePathNode = null;
    isFinished: boolean = false;

    constructor(startTile: MapTile, targetTile?: MapTile) {
        this.start = new TilePathNode(startTile);
        if (targetTile) {
            this.target = new TilePathNode(targetTile);
        }
    }
}