import { Loggable } from "../../common/models/log";
import { MapPos } from "./pos";
import { IWorldEntity } from "../../common/models/entity/entity";
import { ICity } from "../models/city/city";
import * as BABYLON from 'babylonjs';
import { IUnitArmy } from "../models/military/interfaces/unit-army.interface";

/* == SELECTION INTERFACES == */
export interface ISelectable {
    setAsSelected(): void;
    onSelected$: BABYLON.Observable<void>;
}

/* == WORLD SELECTION MODEL == */
export class WorldSelection extends Loggable {
    _lastPos: MapPos = null;
    _pos: MapPos = null;
    entity: SelectedEntity;

    constructor(pos: MapPos) {
        super("WorldSelection");
        this._pos = pos;
        // create selected entity holder
        this.entity = new SelectedEntity(SELECTED_ENTITY_TYPE_NONE);
        this.entity.parent = this;
    }

    setToPlayer() { this.entity.setToPlayer() }
    setToCity(city: ICity) { this.entity.setToCity(city); }
    setToArmy(army: IUnitArmy) { this.entity.setToArmy(army); }
    setToNone() { this.entity.setToNone(); }

    updatePos(newPos: MapPos) {
        this._lastPos = this._pos;
        this._pos = newPos;
    }
    
}

export class SelectedEntity extends Loggable {
    parent: WorldSelection;
    entityTypeNum: number = SELECTED_ENTITY_TYPE_NONE;
    entityKey: string;
    _entity: IWorldEntity;

    constructor(entityTypeNum: number, entityKey?: string) {
        super("SelectedEntity");
        this.entityTypeNum = entityTypeNum;
        if (entityTypeNum == SELECTED_ENTITY_TYPE_CITY && entityKey) {
            this.setToCityByKey(entityKey);
        }
    }

    setToNone() { this.unset(); }

    /* SET TO PLAYER, CITY OR ARMY */
    setToPlayer() {
        this.entityTypeNum = SELECTED_ENTITY_TYPE_PLAYER;
        this.entityKey = null;
        // unset entity but update pos
        this._entity = null;
        this.updateParentPos()
    }

    setToCity(selectedCity: ICity) {
        // update selected entity type & key
        this.entityTypeNum = SELECTED_ENTITY_TYPE_CITY;
        this.entityKey = selectedCity.key;
        // and model
        this._setToEntity(selectedCity);
    }

    private _setToEntity(entity: IWorldEntity) {
        this._entity = entity;
        this.updateParentPos();
    }

    private unset() {
        this.entityTypeNum = SELECTED_ENTITY_TYPE_NONE;
        this._entity = null;
        this.parent.updatePos(null);
    }

    updateParentPos() {
        if (this.entity && this.parent) { 
            this.parent.updatePos(this.entity.pos);
        }
    }

    private setToCityByKey(selectedCityKey: string) {
        if (! globalThis.CITIES) {
            this.log(`cant update selection with city "${selectedCityKey}": CityManager not found`); return;
        }
        const city: ICity = globalThis.CITIES.getByKey(selectedCityKey);
        this.setToCity(city);
    }

    setToArmy(selectedArmy: IUnitArmy) {
        this.entityTypeNum = SELECTED_ENTITY_TYPE_ARMY;
        this.entityKey = "" + selectedArmy.num;
        this._setToEntity(selectedArmy);
    }

    /* SELECTED ENTITY GETTERS */
    get entity(): IWorldEntity {
        if (this.isNone) { return; }
        // is player?
        if (this.isPlayer) { return globalThis.PLAYER; }
        // is city or army?
        if (this.isCity && this._entity) { return this._entity; }
        if (this.isArmy && this._entity ) { return this._entity; }
        // throw warning if not
        this.warn(`cant find entity of type ${this.entityTypeName}`);
        return null;
    }

    get entityPos(): MapPos {
        if (this.entity) { return this.entity.pos; }
        return;
    }

    get entityTypeName(): string {
        return SELECTED_ENTITY_TYPE_NAMES[this.entityTypeNum];
    }

    /* SELECTED ENTITY FLAGS */
    get isNone(): boolean { return this.entityTypeNum == SELECTED_ENTITY_TYPE_NONE; }
    get isPlayer(): boolean { return this.entityTypeNum == SELECTED_ENTITY_TYPE_PLAYER; }
    get isCity(): boolean { return this.entityTypeNum == SELECTED_ENTITY_TYPE_CITY; }
    get isArmy(): boolean { return this.entityTypeNum == SELECTED_ENTITY_TYPE_ARMY; }

}

/* == SELECTION CONSTANTS == */
export const SELECTED_ENTITY_TYPE_NONE = 0;
export const SELECTED_ENTITY_TYPE_PLAYER = 1;
export const SELECTED_ENTITY_TYPE_CITY = 2;
export const SELECTED_ENTITY_TYPE_ARMY = 3;
export const SELECTED_ENTITY_TYPE_NAMES = ["None", "Player", "City", "Army"];