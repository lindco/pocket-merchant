import { Loggable } from "../../../common/models/log";
// gui
import * as GUI_CONST from './constants';
// babylon
import * as GUI from 'babylonjs-gui';

/* == SIZE INTERFACE == */
export interface IGuiSize {
    parent: GUI.Control;
    widthStr: string;
    heightStr: string;
    toString(): string;
}

/* == SIZE MODEL == */
export class GuiSize extends Loggable implements IGuiSize {
    parent: GUI.Control;
    widthStr: string;
    heightStr: string;

    constructor(control: GUI.Control) {
        super("GuiSize");
        if ( control ) {
            this.parent = control;
            this.updateSizeFromParent();
        }
    }

    updateSizeFromParent() {
        this.widthStr = this.parent.width as string;
        this.heightStr = this.parent.height as string;
    }

    toString(): string {
        return this.widthStr + DEFAULT_STR_DIVIDER + this.heightStr;
    }

    /* SETTERS */
    set(widthStr: string, heightStr: string) {
        this.parent.width = widthStr;
        this.parent.height = heightStr;
        this.updateSizeFromParent();
    }

    setInPx(widthPx: number, heightPx: number) {
        this.parent.widthInPixels = widthPx;
        this.parent.heightInPixels = heightPx;
        this.updateSizeFromParent();
    }

    /* STATIC METHODS */
    static FromStr(guiSizeStr: string): GuiSize {
        const guiSize = new GuiSize(null);
        const widthHeightStrArr = guiSizeStr.split(DEFAULT_STR_DIVIDER);
        guiSize.widthStr = widthHeightStrArr[0];
        guiSize.heightStr = widthHeightStrArr[1];
        return guiSize;
    }


}

/* == SIZE CONSTANTS == */
export const DEFAULT_STR_DIVIDER = " ";