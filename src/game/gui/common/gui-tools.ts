// common
import { DawnbringerPalette } from '../../../common/models/colors';
// gui
import * as GUI_CONST from './constants';
import { GuiLabel } from '../elements/babylon/labels';
// babylon
import * as GUI from 'babylonjs-gui';

export class GuiTools {
  /* == SIZE SETTERS == */
  static setToDefaultSize(control: GUI.Control) {
    control.width = GUI_CONST.DEFAULT_WIDTH;
    control.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
  }

  static setSizeInPx(control: GUI.Control, width: number, height: number) {
    control.widthInPixels = width;
    control.heightInPixels = height;
  }

  static setSizeFrom(control: GUI.Control, other: GUI.Control) {
    control.width = other.width;
    control.height = other.height;
  }

  static setFullPadding(control: GUI.Control, paddingStr: string) {
    control.paddingLeft = paddingStr;
    control.paddingRight = paddingStr;
    control.paddingTop = paddingStr;
    control.paddingBottom = paddingStr;
  }

  static addNewGuiLabel(parent: GUI.Container, labelId: string, text: string,
    isClickable?: boolean, hasSmallHeight?: boolean, hasNoBackground?: boolean): GuiLabel {
      const labelElem = new GuiLabel(labelId, text, isClickable, hasSmallHeight, hasNoBackground);
      parent.addControl(labelElem);
      return labelElem;
    }

    static setAlignments(parent: GUI.Control, alignmentsNumArray: Array<number>) {
      if (alignmentsNumArray && alignmentsNumArray.length > 1) {
        parent.horizontalAlignment = alignmentsNumArray[0];
        parent.verticalAlignment = alignmentsNumArray[1];
      }
    }

    static addNewDivider(parent: GUI.Control) {

    }

    /* == FONT SETTERS == */
    static setDefaultFontStyle(control: GUI.Control) {
        control.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
        control.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
    }

    static setHighlightedFontColor(control: GUI.Control) {
      const hexColor = BABYLON.Color3.FromArray(DawnbringerPalette.GOLDENFIZZ).toHexString();
      control.color = hexColor;
    }
  }
