// common
import { BaseEventListener } from "../../../common/models/events";
// events
// models
import { Size } from "../../world/pos";
// gui
import { IGuiContainer } from "./layouts";
// babylon
import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';

/* INTERFACES */
export interface IGuiElement{
    // properties
    id: string;
    parent: IGuiElement;
    babylon: GUI.Control;
    isActive: boolean;
    // functions
    init(): void;
    build(): void;
    dispose(): void;
}

/* ABSTRACT CLASSES */
export class AbstractGuiElement extends BaseEventListener implements IGuiElement {
    id: string;
    size: Size = new Size(0, 0);
    isActive: boolean = true;
    // childs & parent
    parent: IGuiElement = null;
    babylon: GUI.Control = null;
    
    constructor(id: string, parent?: IGuiContainer) {
        super("GuiElem");
        this.id = id;
        // add to parent
        if (parent) {
            parent.add(this);
            this.parent = parent;
        }
    }

    public init(): void {
        this.error(`cant build AbstractGuiElement; have you forgotten to implement build()?`);
    }

    public build(): void {
        this.error(`cant show AbstractGuiElement; have you forgotten to implement show()?`);
        return null;
    }

    public dispose(): void {
        if (! this.babylon ) {
            return;
        }
        this.isActive = false;
        this.babylon.dispose();
    }

    /* LOG */
    public toString(): string {
        return `<GuiEl "${this.id}">`;
    }
}

/* STYLE TYPES */

export class StyleBackground {
    static get TRANSPARENT() { return 0; }
    static get PAPER() { return 1; }
    static get LEATHER() { return 2; }

    static get ALL(): Array<number> {
        return [
            StyleBackground.TRANSPARENT, StyleBackground.PAPER, StyleBackground.LEATHER
        ];
    }
}

export class StyleBorder {
    static get NONE() { return null; }
    static get THIN_FRAME() { return "frame-thin"; }
    static get OUTTER_FRAME() { return "frame-outter"; }

    static get ALL(): Array<string> {
        return [
            StyleBorder.NONE, 
            StyleBorder.THIN_FRAME,
            StyleBorder.OUTTER_FRAME
        ];
    }
}

/* STYLED ELEMENTS */

export class GuiStyle {
    public bg: number = StyleBackground.TRANSPARENT;
    public extra: string = StyleBorder.NONE;

    constructor(bg?: number, extra?: string) {
        // set optional properties
        if (bg) { this.bg = bg; }
        if (extra) { this.extra = extra; }
    }
}

export class StyledGuiElement extends AbstractGuiElement implements IGuiElement {
    public style: GuiStyle = new GuiStyle();

    constructor(id: string, parent?: IGuiContainer, style?: GuiStyle) {
        super(id, parent);
        this.style = style;
    }
}