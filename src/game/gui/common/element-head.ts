// common
import { Loggable } from "../../../common/models/log";
// events
import { BaseEventListener, IBaseEvent } from "../../../common/models/events";
// gui
import * as GUI_CONST from '../common/constants';
import { IGuiRectangle } from "./rectangle";
import { GUI3DManager } from "babylonjs-gui";
// babylon
import * as GUI from 'babylonjs-gui';

export class GuiElementHead extends BaseEventListener {
    elemId: string;
    isDeleted: boolean = false;
    parent: IGuiRectangle;

    constructor(elemId: string, parent: IGuiRectangle) {
        super(`GuiHead "${elemId}"`, true);
        this.elemId = elemId;
        this.parent = parent;
    }

    onEvent(ev: IBaseEvent) {
        this.parent.onEvent(ev);
    }

    delete() {
        this.parent = null;
        this.isDeleted = true;
    }

    setSizeInPixels(width: number, height?: number) {
        this.parent.setSizeInPixels(width, height);
    }
    
}