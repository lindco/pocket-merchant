/* == GUI CONSTANTS == */

// layout directions
export const DIR_HOR = 1;
export const DIR_VERT = 2;
/* == ELEMENT ALIGMENT == */
// element alignment
export const ALIGN_H_LEFT = 0;
export const ALIGN_H_RIGHT = 1;
export const ALIGN_H_CENTER = 2;
export const ALIGN_V_TOP = 0;
export const ALIGN_V_BOTTOM = 1;
export const ALIGN_V_CENTER = 2;
// text alignment
export const ALIGN_TEXT_V_TOP = 0;
export const ALIGN_TEXT_V_BOTTOM = 1;
export const ALIGN_TEXT_V_CENTER = 2;
export const ALIGN_TEXT_H_LEFT = 0;
export const ALIGN_TEXT_H_RIGHT = 1;
export const ALIGN_TEXT_H_CENTER = 2;
// menues
export const MENU_HEIGHT_SMALL = "35px";
export const MENU_TEXT_COLOR_NAME = "GOLDENFIZZ";
// body
export const BODY_MARGIN_X_PX = 30;

/* == GUI ELEMENT PREFIXES == */
export const ELEM_PREFIX_DATAROW = "datarow-";

/* == ELEMENT DEFAULT VALUES == */
// sizing
export const WIDTH_FULL = "100%";
export const DEFAULT_WIDTH = "400px";
export const DEFAULT_INNER_WIDTH = "400px";
export const DEFAULT_INNER_WIDTH_PX = 400;
export const NARROW_WIDTH = "100px";
export const NARROW_WIDTH_PX = 100;
export const DEFAULT_ELEMENT_HEIGHT = "60px";
export const DEFAULT_ELEMENT_HEIGHT_PX = 60;
export const LOWER_ELEMENT_HEIGHT_PX = 48;
export const LOWER_ELEMNT_HEIGHT = `${LOWER_ELEMENT_HEIGHT_PX}px`;
// icons
export const DEFAULT_ICON_SIZE_S = "32px";
export const DEFAULT_ICON_PADDING_S = "5px";
// shadow
export const SHADOW_COLOR_DARK_HEX = "#00000088";
export const SHADOW_COLOR_BRIGHT_HEX = "ffffff88";
// labels
export const DEFAULT_LABEL_HEIGHT = "35px";
export const DEFAULT_LABEL_HEIGHT_PX = 35;
export const SMALL_LABEL_HEIGHT = "24px";
export const SMALL_LABEL_HEIGHT_PX = 24;
// dialogs
export const DIALOG_OUTTER_WIDTH_PX = 600;
export const DIALOG_OUTTER_WIDTH = `${DIALOG_OUTTER_WIDTH_PX}px`;
export const DIALOG_HEIGHT_PX = 600;
export const DIALOG_HEIGHT = `${DIALOG_HEIGHT_PX}px`;

/* == TEXT & FONT == */
export const FONT_HIGHLIGHTED = "Rediviva";
export const FONT_PARAGRAPH = "Prince Valiant";

export const TEXTSIZE_SMALLER = "16px";
export const TEXTSIZE_NORMAL = "20px";
export const TEXTSIZE_BIGGER = "26px";
export const TEXTSIZE_BIGGEST = "32px";

/* == RESOURCES == */
export const FOLDER_RES = "res/";
export const FOLDER_IMG = FOLDER_RES + "img/";
// icons
export const FOLDER_ICONS = FOLDER_IMG + "icons/";
export const FOLDER_ICONS_INLINE = FOLDER_ICONS + "inline/";
export const FOLDER_ICONS_RES = FOLDER_ICONS + "/resources/";
// ui
export const FOLDER_UI_IMG = FOLDER_IMG + "ui/";
export const FOLDER_UI_BG = FOLDER_UI_IMG + "backgrounds/";
export const FOLDER_BUTTONS_IMG = FOLDER_UI_IMG + "buttons/";
export const FOLDER_CTL_IMG = FOLDER_UI_IMG + "controller/";
// backgrounds
export const BG_FILE_PREFIX = FOLDER_UI_BG + "bg_";
export const BG_LEATHER = BG_FILE_PREFIX + "leather.png";
export const BG_PAPER = BG_FILE_PREFIX + "paper.png";
export const BG_BUTTON1 = BG_FILE_PREFIX + "button1.png";
export const BG_HEADER = BG_FILE_PREFIX + "header.png";
export const BG_MENU_TOP = BG_FILE_PREFIX + "menu-top.png";
export const BG_SECTION = BG_FILE_PREFIX + "section.png";
