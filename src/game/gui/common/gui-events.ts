// common
import { BaseEvent } from "../../../common/models/events";

/* == SWITCHER EVENTS == */
export class GuiSwitcherChangeEvent extends BaseEvent {
    constructor(elemId: string, selectedOptionStr: string) {
        super(ID_SWITCHER_CHANGE);
        this.properties.addString("elemId", elemId);
        this.properties.addString("selected", selectedOptionStr);
    }

}

/* == CONSTANTS == */
export const ID_SWITCHER_CHANGE = "SwitcherChange";