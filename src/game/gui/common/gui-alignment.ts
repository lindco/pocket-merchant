import { Loggable } from "../../../common/models/log";
// gui
import * as GUI_CONST from './constants';
// babylon
import * as GUI from 'babylonjs-gui';

export interface IGuiAlignment {
    parent: GUI.Control;
    horizontal: number;
    vertical: number;
    set(hor: number, vert: number);
}

export class GuiAlignment extends Loggable implements IGuiAlignment{
    parent: GUI.Control;
    horizontal: number;
    vertical: number;

    constructor(parent: GUI.Control) {
        super("GuiAlignment");
        this.parent = parent;
        this.updateFromParent();
    }

    updateFromParent() {
        this.horizontal = this.parent.horizontalAlignment;
        this.vertical = this.parent.verticalAlignment;
    }

    set(hor: number, vert: number) {
        this.parent.horizontalAlignment = hor;
        this.parent.verticalAlignment = vert;
        this.updateFromParent();
    }

    setFromArray(numArr: Array<number>) {
        this.set(numArr[0], numArr[1]);
    }
}