import { Loggable } from '../../../common/models/log';
import { IBaseEvent, BaseEventListener, IEventListener } from '../../../common/models/events';
import { GuiElementHead } from './element-head';
import { GuiSize } from './gui-size';
import { GuiAlignment } from './gui-alignment';
import { GuiTools } from './gui-tools';
import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';

/* == INTERFACES == */
export interface IGuiElement extends IEventListener, Loggable {
    head: GuiElementHead;
    size: GuiSize;
    align: GuiAlignment;
    onEvent(ev: IBaseEvent): void;
    isHovered: boolean;
    // observables
    onElementClicked$: BABYLON.Observable<void>;
    onHoverChange$: BABYLON.Observable<boolean>;
    // methods
    setSizeInPixels(width: number, height?: number): void;
}

export interface IGuiRectangle extends IGuiElement {
    buildCustomContent(): void;
}

export class GuiRectangle extends GUI.Rectangle implements IGuiRectangle {
    head: GuiElementHead;
    size: GuiSize;
    align: GuiAlignment;
    isHovered: boolean = false;
    // observables
    onElementClicked$ = new BABYLON.Observable<void>();
    onHoverChange$ = new BABYLON.Observable<boolean>();

    constructor(controlId: string, alignments?: Array<number>) {
        super(controlId);
        this.name = controlId;
        // remove outter white border
        this.thickness = 0;
        // add element head & size object
        this.size = new GuiSize(this);
        this.head = new GuiElementHead(controlId, this);
        // this.align = new GuiAlignment(this);
        // custom alignment?
        if (alignments && alignments.length > 1) {
            this.horizontalAlignment = alignments[0];
            this.verticalAlignment = alignments[1];
        }
        // add observables
        this.registerToObservables();
    }

    buildCustomContent() {
    }

    /* INITIALIZATION */
    registerToObservables(noHover?: boolean) {
        this.onPointerDownObservable.add(() => {
            this.onElementClicked$.notifyObservers();
            globalThis.GUI.onElementClicked(this.name);
        });
        if(noHover && noHover == true) { return; }
        // add hover observables
        this.onPointerEnterObservable.add(() => { this.onHoverChange$.notifyObservers(true); });
        this.onPointerOutObservable.add(() => { this.onHoverChange$.notifyObservers(false); });
    }

    /* SET OUTTER SIZE */
    setSizeInPixels(width: number, height?: number) {
        let elemHeight = width;
        if (height) { elemHeight = height; }
        // set rectangle width & height properties
        this.widthInPixels = width;
        this.heightInPixels = elemHeight;
    }

    /* set outter size as string; per example "12em", "8%", "100px" */
    setSize(widthStr: string, heightStr?: string) {
        let elemHeightStr = widthStr;
        if (heightStr) { elemHeightStr = heightStr; }
        this.size.set(widthStr, elemHeightStr);
    }

    setToDefaultSize() {
      GuiTools.setToDefaultSize(this);
    }

    setFullPadding(paddingStr: string) {
        GuiTools.setFullPadding(this, paddingStr);
    }

    /* SET ALIGNMENT IN PARENT */
    setAlignment(horizontal: number, vertical: number) {
        this.align.set(horizontal, vertical);
    }

    /* DELETION CLEAN UP */
    dispose() {
        // delete head
        this.head.delete();
        this.head = null;
        // delete size & align
        this.size.parent = null;
        this.size = null;
        // dispose babylon control
        super.dispose();
    }

    /* ELEMENT INTERACFTION */
    onClick() {
        this.log(`clicked!`);

    }

    /* LOGGING TO HEADER */
    success(message: string) { this.head.success(message); }
    log(message: string) { this.head.log(message); }
    warn(message: string) { this.head.warn(message); }
    error(message: string) { this.head.error(message); }
    get isMuted(): boolean { return this.head.isMuted; }
    get logName(): string { return this.head.logName; }

    /* EVENT HANDLING */
    /* events get passed from the element head object */
    onEvent(ev: IBaseEvent): boolean {

        return true;
    }
}
