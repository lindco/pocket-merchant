import { Size } from "../../world/pos";
import { AbstractGuiElement, IGuiElement } from "./element";
import { Switcher } from "../elements/switcher";
import * as GUI_CONST from "./constants";
import { SingleDataRow } from "../elements/data-row";
import { DawnbringerPalette } from "../../../common/models/colors";
import { SectionHeader } from "../elements/section-header";
import { GuiTools } from "./gui-tools";
import * as BABYLON from "babylonjs";
import * as GUI from 'babylonjs-gui';

export interface IGuiContainer extends IGuiElement {
    babylon: GUI.Container;
    maxSize: Size;
    build();
    addCustomContent();
    updateCustomContent();
    addControl(control: GUI.Control, customSize?: boolean): GUI.Control;
    // children
    children: Array<IGuiElement>;
    childrenIds: Array<string>;
    add(elem: IGuiElement): void;
    clear(): void;
    // on events
    onElementAdded(elem: IGuiElement): void;
    onCleared(): void;
    // observables
    readonly onChildClicked$: BABYLON.Observable<string>;
}

export class AbstractGuiContainer extends AbstractGuiElement implements IGuiContainer {
    babylon: GUI.Container;
    id: string;
    parent: IGuiElement;
    // layout properties
    children: Array<IGuiElement> = [];
    childrenIds: Array<string> = [];
    dir: number;
    maxSize: Size = null;
    private dividers = 0;
    // defaults
    forceEmpty: boolean = false;
    // observables
    readonly onChildClicked$ = new BABYLON.Observable<string>();

    constructor(id: string) {
        super(id);
    }

    add(elem: IGuiElement) {
        this.log(`adding ${elem}`);
        if (this.forceEmpty) {
            this.error(`cant add ${elem}; is forced empty`);
        }
        this.childrenIds.push(elem.id);
        this.children.push(elem);
        this.onElementAdded(elem);
    }

    addMultiple(elements: Array<IGuiElement>) {
        for (const elem of elements) {
            this.add(elem);
        }
    }

    protected nextDividerNum(): number {
        this.dividers += 1;
        return this.dividers;
    }

    /**
     * overwrite this method to add inner controls after the container has been initialized
     */
    public addCustomContent() {
        // overwrite this method to add any BABYLON Controls as children
        this.warn(`not adding any content; have you overwritten addCustomContent()?`);
    }

    updateCustomContent() {
        // overwrite this mission to update content: per example change a label's text
    }

    clear(): void {
        for (const child of this.children) {
            child.dispose();
        }
        this.childrenIds = [];
        this.children = [];
        this.onCleared();
    }

    addControl(control: GUI.Control): GUI.Control {
        if (! this.babylon) {
            console.error(`cannot add ${control.name} to menu ${this}: container bas no inner element called babylon`);
            return;
        }
        return this.babylon.addControl(control);
    }

    /* ON CHANGE */
    onElementAdded(elem: IGuiElement) {
        elem.parent = this;
        this.log(`added element "${elem.id}"`);
    }

    onElementClicked(elemId: string): boolean {
        this.log(`click on element "${elemId}"`);
        return false;
    }

    onCleared() {
        this.log("cleared");
    }
}

export class FlexGuiLayout extends AbstractGuiContainer implements IGuiContainer {
    title: string = null;
    container: GUI.ScrollViewer;

    constructor(id: string, dir: number, title?: string, maxSize?: Size, parent?: IGuiContainer) {
        super(id);
        this.dir = dir;
        // optional properties
        if (title) { this.title = title; }
        if (maxSize) { this.maxSize = maxSize; }
        // build!
        this.init();
    }

    init(): void { }

    build(isWide?: boolean): void {
        // create empty StackPanel
        this.babylon = this.buildContainer(isWide);
        // add header label with title text
        this.addDialogHeader();
        // add method to allow custom layout content by overwriting it
        this.addCustomContent();
        this.adjustOutterSize();
    }

    private adjustOutterSize() {
        this.container.widthInPixels = this.babylon.widthInPixels + 40;
        this.container.height = "95%";
        this.container.horizontalBar.parent.isVisible = false;
        this.container.verticalBar.parent["thickness"] = 0;
    }

    addControl(control: GUI.Control, customSize?: boolean): GUI.Control {
        const container = this.babylon as GUI.Container;
        if (! container) {
            this.error(`cant add "${control.name}"; container StackPanel ${this.babylon} not initialized`);
            return;
        }
        // custom size?
        if (! customSize) {
            control.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
        }
        return container.addControl(control);
    }

    protected buildContainer(isWide?: boolean): GUI.Container {
        // build outter
        this.container = new GUI.ScrollViewer(this.id + "-outter");
        this.container.scrollBackground = "#00000000";
        this.container.thickness = 0;
        this.container.height = "90%";
        if (isWide) { this.container.width = GUI_CONST.DIALOG_OUTTER_WIDTH; }
        else { this.container.widthInPixels = GUI_CONST.DEFAULT_INNER_WIDTH_PX + 40; }
        // build container
        let container: GUI.StackPanel = new GUI.StackPanel(this.id);
        if (isWide) { container.widthInPixels = GUI_CONST.DIALOG_OUTTER_WIDTH_PX; }
        else { container.width = GUI_CONST.DEFAULT_INNER_WIDTH; }
        // add container to outter & outte to gui root
        this.container.addControl(container);
        globalThis.GUI.advTexture.addControl(this.container);
        return container;
    }

    /* == ADD STANDARD ELEMENTS == */
    protected addDialogHeader() {
        if (! this.title) { return; }
        // create container
        const diagHeaderContainer = new GUI.Rectangle(`${this.id}-header`);
        diagHeaderContainer.width = GUI_CONST.DEFAULT_WIDTH;
        diagHeaderContainer.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
        diagHeaderContainer.thickness = 0;
        // background image
        const bgImage = new GUI.Image(`${this.id}-img`, GUI_CONST.BG_HEADER);
        bgImage.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
        // actual label
        const label = new GUI.TextBlock(this.id, this.title);
        label.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
        label.fontSize = GUI_CONST.TEXTSIZE_BIGGEST;
        label.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
        // color: dawnbringer golden fizz
        label.color = BABYLON.Color3.FromArray(DawnbringerPalette.GOLDENFIZZ).toHexString();
        // add to container
        diagHeaderContainer.addControl(bgImage);
        diagHeaderContainer.addControl(label);
        this.addControl(diagHeaderContainer);
        this.addNewDivider(true);
    }

    addNewLabel(labelId: string, text: string, clickable?: boolean, smallHeight?: boolean, noBackground?: boolean, setParent?: GUI.Container): GUI.TextBlock {
        let parent: GUI.Container = this.babylon as GUI.Container;
        if ( setParent ) { parent = setParent; }
        const labelElem = GuiTools.addNewGuiLabel(parent, labelId, text, clickable, smallHeight, noBackground);
        // return actual label
        return labelElem.labelTextBlock;
    }

    addNewDivider(empty?: boolean, setParent?: GUI.Container): GUI.TextBlock {
        let parent: GUI.Container = this.babylon as GUI.Container;
        if ( setParent ) { parent = setParent; }
        let dividerNum: number = this.nextDividerNum();
        let dividerText = "";
        if (! empty) { dividerText = "--------"; }
        let dividerLabel: GUI.TextBlock = this.addNewLabel(`divider-${dividerNum}`, dividerText, false, true, false, parent);
        dividerLabel.width = GUI_CONST.DEFAULT_INNER_WIDTH;
        dividerLabel.textVerticalAlignment = GUI_CONST.ALIGN_V_CENTER;
        return dividerLabel;
    }

    addNewSwitcher( elemId: string, options: Array<string>) {
        const switcher = new Switcher( elemId, options );
        this.addControl( switcher );
        this.addNewDivider(true);
    }

    addNewSectionHeader( headerName: string ) {
      const sectionHeader = new SectionHeader(headerName);
      this.addControl(sectionHeader, true);
    }

    addNewDataRow(key: string, value: string, inlineIcon?: string, clickable?: boolean, overwriteElemId?: string, overwriteParent?: GUI.Container): SingleDataRow {
        const layout = this;
        const dataRow = new SingleDataRow(key, value, inlineIcon, overwriteElemId, clickable);
        if ( clickable ) {
            dataRow.onPointerDownObservable.add( () => {
                if (layout) { layout.onChildClicked$.notifyObservers(dataRow.name); }
            });
        }
        if (! overwriteParent) { this.addControl( dataRow, true ); }
        else { overwriteParent.addControl(dataRow); }
        return dataRow;
    }
}
