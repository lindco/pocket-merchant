// babylon
import * as GUI from 'babylonjs-gui';

export class CityResourcesOverview extends GUI.Rectangle {
    constructor(name?: string | undefined) {
        super(name);
        this.width = "300px";
        this.height = "60px";
        this.onAfterDrawObservable.add(this.drawContent);
    }

    drawContent() {
        
    }
}

export class CityBuildingsList {
    
}