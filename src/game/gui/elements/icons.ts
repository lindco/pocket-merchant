import * as GUI_CONST from '../common/constants';
import { GuiRectangle } from '../common/rectangle';
import * as GUI from 'babylonjs-gui';
import { GuiTools } from '../common/gui-tools';

export class InlineIcon extends GUI.Rectangle {
    iconName: string;
    hasOwnBackground: boolean = false;
    iconSizeInner: number = ICON_SIZE_PX;
    iconBg: GUI.Image;
    iconImage: GUI.Image;

    constructor(elemId: string, iconName: string, customSize?: number,
                hasOwnBackground?: boolean, hasShadow?: boolean) {
        super(elemId);
        if (customSize) { this.iconSizeInner = customSize; }
        this.initializeContainer();
        // background
        if (hasOwnBackground) { this.createIconBackground(); }
        // icon image
        if (iconName) {
            this.iconName = iconName;
            this.createIconImage();
        }
        // optional
        if (hasOwnBackground) { this.hasOwnBackground = hasOwnBackground }
    }

    initializeContainer() {
        this.widthInPixels = this.iconSizeWithPadding;
        this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.thickness = 0;
        GuiTools.setFullPadding(this, ICON_PADDING)
    }

    createIconBackground() {
        this.hasOwnBackground = true;
        const iconBgId = `${this.name}-bg`;
        const iconBg = new GUI.Image(iconBgId, ICON_PATH_BG);
        iconBg.widthInPixels = this.iconSizeWithPadding;
        iconBg.heightInPixels = this.iconSizeWithPadding;
        this.addControl( iconBg );
    }

    createIconImage(noShadow?: boolean) {
        const iconId = `${this.name}-iconImg`;
        this.iconImage = new GUI.Image(iconId, this.iconFilePath);
        this.iconImage.widthInPixels = this.iconSizeInner;
        this.iconImage.heightInPixels = this.iconSizeInner;
        if (! noShadow) {
            this.iconImage.shadowColor = GUI_CONST.SHADOW_COLOR_BRIGHT_HEX;
            this.iconImage.shadowBlur = 3;
        }
        this.addControl( this.iconImage );
    }

    setIconImage(iconName: string) {
        this.iconName = iconName;
        this.iconImage.source = this.iconFilePath;
    }

    get iconFilePath() { return ICONS_FOLDER + this.iconName + ".png"; }
    get iconSizeOutter(): number { return this.iconSizeInner + 10; }
    get iconSizeWithPadding(): number { return this.iconSizeOutter + 2 *(InlineIcon.DEFAULT_PADDING); }

    /* STATIC GETTERS */
    static get DEFAULT_PADDING(): number { return ICON_PADDING_PX; }
}

export class PropertyIconStack extends GuiRectangle {
    
}

/* == ICON CONSTANTS == */
export const ICONS_FOLDER = "res/img/icons/inline/";
export const ICON_PATH_BG = ICONS_FOLDER + "bg.png";
export const ICON_SHOW_BG = false;
// padding
export const ICON_PADDING_PX = 5;
export const ICON_PADDING = `${ICON_PADDING_PX}px`;
// icon sizing
export const ICON_SIZE_PX = 20;
export const ICON_SIZE_OUTTER_PX = 30;
export const ICON_WIDTH_PX = ICON_SIZE_OUTTER_PX + 2*ICON_PADDING_PX;
export const ICON_WIDTH_TOTAL_PX = ICON_WIDTH_PX;
export const ICON_WIDTH_TOTAL = `${ICON_WIDTH_TOTAL_PX}px`;