

export interface IDomNode {
    // properies
    domId: string;
    // flags
    isEmpty: boolean;
    // relationships
    parentNode?: IDomNode;
    childrenNodes?: IDomNode[];
}

export class DomNode implements IDomNode {
    domId: string;
    parentNode?: IDomNode = null;
    childrenNodes?: IDomNode[] = [];

    constructor( domId: string, parentId?: string) {
        this.domId = domId;
        if (parentId) {
            this.parentNode = DomNode.getById(parentId);

        }
    }

    get isEmpty(): boolean {
        return this.childrenNodes && this.childrenNodes.length > 0;
    }

    /* STATIC METHODS */
    static getRoot(): IDomNode {
        return DomNode.getById("root");
    }

    static getById(id: string): IDomNode {
        return globalThis.DOM_NODES.nodeById(id);
    }
}

export class DomNodes {
    _domIds: string[] = [];
    _nodes: IDomNode[] = [];

    registerNode(nodeObj: DomNode) {
        this._domIds.push(nodeObj.domId);
        this._nodes.push(nodeObj);
    }

    nodeById(nodeId: string): DomNode {
        let nodeIndex: number = this._domIds.indexOf(nodeId);
        return this._nodes[nodeIndex];
    }
}