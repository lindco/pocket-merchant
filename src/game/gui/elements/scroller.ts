// common
import { ColorPalette } from '../../../helpers/colorpalette';
// gui
import * as GUI_CONST from '../common/constants';
import * as GUI_EV from '../common/gui-events';
// babylon
import * as GUI from 'babylonjs-gui';

/* == SCROLLVIEW ELEMENT == */
export class GuiScrollView extends GUI.ScrollViewer {

    static setDefaultStyle(scrollViewer: GUI.ScrollViewer, dontSetWidth?: boolean) {
        scrollViewer.thickness = 0;
        // set scrolbar colors
        scrollViewer.barColor = ColorPalette.NameAsHexStr(SCROLLV_COLOR_PRIMARY);
        scrollViewer.scrollBackground = ColorPalette.NameAsHexStr(SCROLLV_COLOR_BG);
        if (! dontSetWidth) { scrollViewer.width = GUI_CONST.DEFAULT_WIDTH; }
    }
}

/* == SCROLLVIEW CONSTANTS == */
export const SCROLLV_COLOR_PRIMARY = "ELFGREEN";
export const SCROLLV_COLOR_BG = "TWINE";