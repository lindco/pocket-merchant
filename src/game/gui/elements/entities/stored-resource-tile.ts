import { StoredResource } from "../../../models/trade/resource/stored-resource";
import { GuiRectangle } from "../../common/rectangle";
import * as GUI_CONST from "../../common/constants";
import * as GUI from "babylonjs-gui";
import { DawnbringerPalette } from "../../../../common/models/colors";
import { TradeResource } from "../../../models/trade/resource/resource";

export class StoredResourceTile extends GuiRectangle {
    storedRes: StoredResource;
    colorByAmount: boolean;
    colorHexStr: string;
    tradeRes: TradeResource;

    constructor(storedRes: StoredResource, colorByAmount: boolean) {
        super(`storedRes-${storedRes.resId}-container`);
        this.width = "40px";
        this.storedRes = storedRes;
        this.tradeRes = globalThis.RESOURCE_TYPES.get(this.storedRes.resId);
        this.colorByAmount = colorByAmount;
        if (colorByAmount) { this.setColorHex(); }
    }

    private initContainer() {
        this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.color = "black";
        this.thickness = 0;
        // container.background = "black";
        this.widthInPixels = Math.floor(400 / 3);
        this.colorHexStr = BABYLON.Color3.FromArray(this.tradeRes.color).toHexString();
    }

    /** when a resource should be colored by its amount in storage,
           paint scarce ones red and well-stocked ones green */
    private setColorHex(): void {
        let amountColorRgb: Array<number> = DawnbringerPalette.DIMGRAY;
        if (this.storedRes.amount > 15) { amountColorRgb = DawnbringerPalette.DELL; }
        else if (this.storedRes.amount <= 5) { amountColorRgb = DawnbringerPalette.BROWN; }
        this.colorHexStr = BABYLON.Color3.FromArray(amountColorRgb).toHexString();
    }

    buildCustomContent() {
        super.buildCustomContent();
        this.initContainer();
        this.buildLabel();
    }

    buildLabel() {
        const labelText = `${this.storedRes.amount} ${this.tradeRes.name}`;
        // create resource label
        const label = new GUI.TextBlock(`storedRes-${this.storedRes.resId}-label`, labelText);
        this.addControl(label);
        label.width = this.width;
        label.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
        label.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
        label.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
        label.color = this.colorHexStr;
    }
}