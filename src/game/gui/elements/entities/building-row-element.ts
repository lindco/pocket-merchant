import {GuiRectangle} from "../../common/rectangle";
import {Building} from "../../../models/building/building";
import {GuiTools} from "../../common/gui-tools";
import * as GUI_CONST from '../../common/constants';
import * as GUI from 'babylonjs-gui';
import {BackgroundImage} from "../background";
import {FlexGuiLayout} from "../../common/layouts";
import {InlineIcon} from "../icons";

export class BuildingRowElement extends GuiRectangle {
    building: Building;
    // elements
    bgImage: GUI.Image;
    horLayout: GUI.StackPanel;
    buildingIcon: InlineIcon;
    buildingLabel: GUI.TextBlock;

    constructor(controlId: string, building: Building) {
        super(controlId);
        this.building = building;
        // set size
        this.width = GUI_CONST.DEFAULT_WIDTH;
        this.height = HEIGHT;
        this.buildCustomContent();
    }

    buildCustomContent() {
        super.buildCustomContent();
        this.createBgImage();
        this.createInner();
    }

    /* CREATE INNER ELEMENTS */
    private createBgImage() {
        const paperBgPath = GUI_CONST.BG_PAPER;
        this.bgImage = new GUI.Image("bg", paperBgPath);
        this.bgImage.width = WIDTH;
        this.bgImage.height = HEIGHT;
        this.addControl(this.bgImage);
    }

    private createInner() {
        this.horLayout = new GUI.StackPanel("inner");
        this.horLayout.isVertical = false;
        this.horLayout.width = WIDTH;
        this.horLayout.height = HEIGHT;
        this.addControl(this.horLayout);
        // create inner elements
        this.createHorLayoutContent();
    }

    private createHorLayoutContent() {
        this.createBuildingIcon();
        this.createBuildingLabel();
    }

    private createBuildingIcon() {
        const bldCat = this.building.bldType.category;
        const bldCatIconName = bldCat.icon;
        this.buildingIcon = new InlineIcon("icon", bldCatIconName, null, false, true);
        this.horLayout.addControl(this.buildingIcon);
    }

    private createBuildingLabel() {
        this.buildingLabel = new GUI.TextBlock(`bld-${this.bldTypeName}`, this.bldTypeName);
        this.buildingLabel.width = "300px";
        this.buildingLabel.horizontalAlignment = GUI.Rectangle.HORIZONTAL_ALIGNMENT_RIGHT;
        this.horLayout.addControl(this.buildingLabel);
    }

    get bldTypeKey(): string {
        return this.building.bldType.id;
    }

    get bldTypeName(): string {
        return this.building.bldType.name;
    }

}

export const WIDTH = GUI_CONST.DEFAULT_WIDTH;
export const HEIGHT = GUI_CONST.DEFAULT_LABEL_HEIGHT;