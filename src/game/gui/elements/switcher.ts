import * as GUI_CONST from '../common/constants';
import * as GUI_EV from '../common/gui-events';
import * as GUI from 'babylonjs-gui';
import * as BABYLON from 'babylonjs';

export class Switcher extends GUI.StackPanel {
    // properties
    currentOptionNum: number = 0;
    options: Array<string>;
    // babylon controls
    private btnLeft: GUI.Button;
    private btnRight: GUI.Button;
    private labelContainer: GUI.Rectangle;
    private labelBackground: GUI.Image;
    private label: GUI.TextBlock;
    // observables
    onOptionChanged$ = new BABYLON.Observable<string>();

    constructor(name: string, options: Array<string>) {
        super(name);
        // properties
        this.isVertical = false;
        this.options = options;
        // initialize children
        this.createInnerControls();
        // fire SwitcherChangeEvent observable when dialog is shown
        this.onOptionChanged$.add(
            (optionText) => {
                if (globalThis.DIALOG) {
                    globalThis.DIALOG.onSwitcherChanged$.notifyObservers(
                        new SwitcherChangeEvent(this.name, optionText) );
                }
        });
    }

    /* creates in this order: left btn, label containeer, label bg, label textblock, right btn */
    createInnerControls() {
        this.addSwitcherButton(false);
        this.addSwitcherLabel();
        this.addSwitcherButton(true);
    }

    /* SWITCHER MIDDLE CONTENT */
    private addSwitcherLabelContainer() {
        // container rectangle
        this.labelContainer = new GUI.Rectangle(SWITCHER_LABEL_CONTAINER_ID);
        this.labelContainer.widthInPixels = SWITCHER_LABEL_WIDTH;
        this.labelContainer.thickness = 0;
        // background image
        this.labelBackground = new GUI.Image(SWITCHER_LABEL_BG_ID, GUI_CONST.BG_PAPER);
        this.labelBackground.widthInPixels = SWITCHER_LABEL_WIDTH;
        this.labelContainer.addControl(this.labelBackground);
    }

    private addSwitcherLabel() {
        // create container & background
        this.addSwitcherLabelContainer()
        // create inner texblock label
        const startingOptionStr = this.options[0];
        this.label = new GUI.TextBlock(SWITCHER_LABEL_ID, startingOptionStr);
        this.label.widthInPixels = SWITCHER_LABEL_WIDTH;
        this.labelContainer.addControl(this.label);
        this.addControl(this.labelContainer);
    }

    /* SWITCHER ARROW BUTTONS */
    private addSwitcherButton(isRight?: boolean) {
        if (! isRight) { isRight = false; }
        // create button
        const btnId = Switcher.GetSwitcherBtnId(isRight);
        const btnText = Switcher.GetSwitcherButtonText(isRight);
        const switcherBtn = GUI.Button.CreateSimpleButton(btnId, btnText);
        // set bg and size
        switcherBtn.thickness = 0;
        switcherBtn.background = "brown";
        switcherBtn.widthInPixels = SWITCHER_BTN_WIDTH;
        this.addArrowOnClick( this, switcherBtn, ! isRight );
        this.addControl(switcherBtn);
        // save in switcher variable
        if (isRight) { this.btnRight = switcherBtn; }
        else { this.btnLeft = switcherBtn; }
    }

    /* returns either "<" or ">" depending on whether button is to the left or right */
    static GetSwitcherButtonText(isRight?: boolean) {
        let btnDirNum = 0;
        if (isRight == true) { btnDirNum = 1; }
        return SWITCHER_BTN_TEXTS[btnDirNum];
    }

    /* returns either "l" or "r" depending on whether button is to the left or right */
    static GetSwitcherDirChar(isRight?: boolean) {
        if (isRight == true) { return SWITCHER_DIR_STR[0]; }
        return SWITCHER_DIR_STR[1];
    }

    static GetSwitcherBtnId(isRight?: boolean) {
        return SWITCHER_BTN_PREFIX + Switcher.GetSwitcherDirChar(isRight);
    }

    private addArrowOnClick(parent: Switcher, elem: GUI.Button, isLeft: boolean) {
        elem.onPointerDownObservable.add(
            () => parent.onArrowClick(isLeft)
        );
    }

    onArrowClick(isLeft: boolean) {
        console.log(`<Switcher ${this.uniqueId}> ARROW left?${isLeft}`);
        if (isLeft) { this.switchBackwards(); }
        else { this.switchForwards(); }
    }

    private switchBackwards() {
        this.currentOptionNum -= 1;
        if ( this.currentOptionNum < 0 ) {
            this.currentOptionNum = this.options.length -1;
        }
        this.onChange();
    }

    private switchForwards() {
        this.currentOptionNum += 1;
        if ( this.currentOptionNum >= this.options.length ) {
            this.currentOptionNum = 0;
        }
        this.onChange();
    }

    private onChange() {
        // update label text
        const newText = this.options[this.currentOptionNum];
        this.label.text = newText;
        this.onOptionChanged$.notifyObservers(newText);
        // log
        const _tag = `<Switcher "${this.name}"> `;
        console.log(_tag + 'changed to option: ' + newText);
    }
}

export class SwitcherChangeEvent {
    switcherElemId: string;
    optionText: string;

    constructor(switcherElemeId: string, optionText: string) {
        this.switcherElemId = switcherElemeId;
        this.optionText = optionText;
    }

}

/* == CONSTANTS == */
// buttons
export const SWITCHER_DIR_STR = ["l", "r"];
export const SWITCHER_BTN_PREFIX = "switcher-btn-";
export const SWITCHER_BTN_TEXTS = ["<", ">"];
export const SWITCHER_BTN_WIDTH = 40;
// label
export const SWITCHER_LABEL_ID = "switcher-label";
export const SWITCHER_LABEL_WIDTH = 400 - 2*(SWITCHER_BTN_WIDTH);
export const SWITCHER_LABEL_CONTAINER_ID = "switcher-label-container";
export const SWITCHER_LABEL_BG_ID = "switcher-label-bg";