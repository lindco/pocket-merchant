// gui
import * as GUI_CONST from '../common/constants';
import * as GUI_EV from '../common/gui-events';
import { GuiRectangle } from '../common/rectangle';
import { GuiTools } from '../common/gui-tools';
// babylon
import * as GUI from 'babylonjs-gui';
import { InlineIcon } from './icons';
import { GuiLabel } from './babylon/labels';

/* == GUI CHECKBOX ELEMENT == */
export class GuiCheckBox extends GuiRectangle {
    text: string = "";
    _isChecked: boolean = false;
    isMultiple: boolean = false;
    // child elements
    checkboxBg: GUI.Image;
    innerLayout: GUI.StackPanel;
    checkboxIcon: InlineIcon;
    checkboxLabel: GuiLabel;

    constructor(elemId: string, text: string, isChecked?: boolean, isMultiple?: boolean) {
        super(elemId);
        // default properties
        this.text = text;
        // optional properties
        if (isChecked) { this._isChecked = true; }
        if (isMultiple) { this.isMultiple = true; }
        // update size & create children
        this.createChildren();
        this.adjustSize();
        this.update();
    }

    adjustSize() {
        this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
    }

    set isChecked(isChecked: boolean) {
        this._isChecked = isChecked;
        this.update();
    }

    get isChecked(): boolean { return this._isChecked; }

    createBackground() {
        const bgImageId = this.name + "-bg";
        this.checkboxBg = new GUI.Image(bgImageId, GUI_CONST.BG_PAPER);
        this.checkboxBg.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.checkboxBg.width = GUI_CONST.DEFAULT_INNER_WIDTH;
        this.addControl(this.checkboxBg);
    }

    createInnerLayout() {
        const innerLayoutId = this.name + "-layout";
        this.innerLayout = new GUI.StackPanel(innerLayoutId);
        this.innerLayout.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.innerLayout.horizontalAlignment = GUI_CONST.ALIGN_H_CENTER;
        this.innerLayout.isVertical = false;
        this.addControl(this.innerLayout);
    }

    createCheckboxIcon() {
        const inlineIconId = this.name + "-icon";
        let iconName = "checkbox_0";
        if (this._isChecked) { iconName = "checkbox_1"; }
        this.checkboxIcon = new InlineIcon(inlineIconId, iconName);
        this.checkboxIcon.width = CHECKB_ICON_WIDTH_OUTTER;
        this.checkboxIcon.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.innerLayout.addControl(this.checkboxIcon);
    }

    createCheckboxLabel() {
        this.checkboxLabel = GuiTools.addNewGuiLabel(this.innerLayout, this.ID_LABEL, this.text, true, true);
        this.checkboxLabel.width = CHECKB_LABEL_WIDTH;
        this.checkboxLabel.labelTextBlock.textHorizontalAlignment = GUI_CONST.ALIGN_TEXT_H_LEFT;
    }

    createChildren() {
        // create background
        this.createBackground();
        // create inner layout
        this.createInnerLayout();
        // create icon
        this.createCheckboxIcon();
        // create text label
        this.createCheckboxLabel();
    }

    /* CHILD UPDATE METHODS */
    update() {
        // update icon
        this.checkboxIcon.setIconImage(this.iconName);
    }

    /* GETTER METHODS */
    get iconName(): string {
        return GuiCheckBox.GetIconNameFromChecked(this.isChecked);
    }

    /* STATIC GETTERS */
    static GetIconNameFromChecked(isChecked: boolean): string {
        let checkedUncheckedNum: number = 0;
        if (isChecked) { checkedUncheckedNum = 1 };
        return CHECKB_ICON_PREFIX + checkedUncheckedNum;
    }

    get ID_LABEL(): string { return this.name + "cb-label"; }
}

/* == GUI CHECKBOX CONSTANTS == */
export const CHECKB_ICON_PREFIX = "checkbox_";
export const CHECKB_ICON_WIDTH_OUTTER = "50px";
export const CHECKB_LABEL_WIDTH = "350px";