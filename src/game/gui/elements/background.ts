import * as GUI_CONST from '../common/constants';
import * as GUI from 'babylonjs-gui';

export class BackgroundImage extends GUI.Image {

    constructor(parent: GUI.Control, bgTypeStr?: string) {
        super("bg");
        this.parent.addControl(this);
        this.source = GUI_CONST.BG_PAPER;
        this.width = this.parent.width;
        this.height = this.parent.height;
    }
}