// gui
import * as GUI_CONST from '../common/constants';
import { GuiRectangle } from '../common/rectangle';
import { GuiTools } from '../common/gui-tools';
// babylon
import * as GUI from 'babylonjs-gui';

export class SectionHeader extends GuiRectangle {
  sectionName: string;
  // child element
  bgImage: GUI.Image;
  headerImage: GUI.Image;
  headerTitle: GUI.TextBlock;

  constructor(sectionName: string) {
    const elemId = `section-header-${sectionName}`;
    // call super & set id & name
    super(elemId);
    this.sectionName = sectionName;
    // set babylon control size
    SectionHeader.setToSectionHeight(this);
    // create inner controls
    this.showBg();
    this.showHeaderImage();
    this.showHeaderTitle();
  }

  private showBg() {
    this.bgImage = new GUI.Image(`${this.name}.bg`, GUI_CONST.BG_PAPER);
    this.bgImage.width = this.width;
    // set size
    SectionHeader.setToSectionHeight(this.bgImage);
    // add to SectionHeader container
    this.addControl(this.bgImage);
  }

  private showHeaderImage() {
    const headerImageId: string = "header-img";
    this.headerImage = new GUI.Image(headerImageId, GUI_CONST.BG_SECTION);
    // set size
    SectionHeader.setToSectionHeight(this.headerImage);
    // add to SectionHeader container
    this.addControl(this.headerImage);
  }

  private showHeaderTitle() {
    const headerTitleId: string = "header-title-text";
    this.headerTitle = new GUI.TextBlock(headerTitleId, this.sectionName);
    // set size
    SectionHeader.setToSectionHeight(this.headerTitle);
    // set font stype
    this.headerTitle.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
    this.headerTitle.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
    GuiTools.setHighlightedFontColor(this.headerTitle);
    // add to SectionHeader container
    this.addControl(this.headerTitle);
  }

  static setToSectionHeight(control: GUI.Control) {
    control.width = GUI_CONST.DEFAULT_INNER_WIDTH;
    control.height = SECTION_HEADER_HEIGHT;
  }
}

/* == CONSTANTS == */
export const BG_HEADER_IMAGE = "bg_section.png";
export const SECTION_HEADER_HEIGHT = GUI_CONST.LOWER_ELEMNT_HEIGHT;
