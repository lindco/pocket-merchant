import * as GUI_CONST from '../common/constants';
import { GuiRectangle } from '../common/rectangle';
import { ICON_WIDTH_TOTAL_PX, InlineIcon } from './icons';
import { ColorPalette } from '../../../helpers/colorpalette';
import * as GUI from 'babylonjs-gui';

/* == DATAROW ELEMENT == */
/* a data row is a label including a key & value label, optionally preceded by an icon */
export class SingleDataRow extends GuiRectangle {
    key: string;
    valueText: string;
    // style
    inlineIconId: string;
    wideKeyLabel: boolean = false;
    /** show no ":" */
    plainKeyText: boolean = false;
    // behaviour
    clickable: boolean;
    // babylon controls
    stackpanel: GUI.StackPanel;
    iconImg: GUI.Image;
    keyLabel: GUI.TextBlock;
    valueLabel: GUI.TextBlock;
    valueColor: number[];

    constructor(key: string, valueText: string, inlineIcon?: string, overwriteElemId?: string, clickable?: boolean,
        valueColor?: number[], wideKeyLabel?: boolean, plainKeyText: boolean = false) {
        // get elemId and call super
        super(`datarow-${key}`);
        let elemId: string = `datarow-${key}`;
        // set basic properties
        this.name = elemId;
        this.key = key;
        this.valueText = valueText;
        // optional properties
        if (inlineIcon) { this.inlineIconId = inlineIcon; }
        if (wideKeyLabel) { this.wideKeyLabel = wideKeyLabel; }
        if (valueColor) { this.valueColor = valueColor; }
        if (overwriteElemId) { elemId = overwriteElemId; }
        if (clickable) { this.clickable = true; }
        if (plainKeyText) { this.plainKeyText = true; }
        // set babylon control size
        this.setSize(GUI_CONST.DEFAULT_INNER_WIDTH, GUI_CONST.DEFAULT_LABEL_HEIGHT);
        // initialize stack layout
        this.createChildElements();
        // this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
    }

    private createChildElements() {
        this.addLayoutAndBg();
        this.addIcon();
        this.addKeyLabel();
        this.addValueLabel();
    }

    /* add babylon Control directly */
    private addAndSetHeight( control: GUI.Control ) {
        control.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        super.addControl( control );
    }

    /* add babylon control to inner container */
    addControl(control: GUI.Control): GUI.Container {
        this.stackpanel.addControl( control );
        return this;
    }


    private addLayoutAndBg() {
        // bg
        const bgImage = new GUI.Image(`${this.name}.bg`, GUI_CONST.BG_PAPER);
        bgImage.width = this.width;
        // stackpanel
        this.stackpanel = new GUI.StackPanel(`${this.name}-outter`);
        this.stackpanel.isVertical = false;
        this.stackpanel.width = this.width;
        this.stackpanel.paddingLeftInPixels = 16;
        this.stackpanel.paddingRightInPixels = 16;
        this.addAndSetHeight(bgImage);
        this.addAndSetHeight(this.stackpanel);
    }

    addIcon() {
        const iconElemId = `${this.name}-icon`;
        const icon = new InlineIcon(iconElemId, this.inlineIconId);
        this.addControl(icon);
    }

    private addKeyLabel() {
        const keyLabelId: string = `${this.name}-key`;
        this.keyLabel = new DatarowKeyLabel(this, keyLabelId, this.key, null, this.clickable);
        this.addControl( this.keyLabel );
    }

    protected addValueLabel() {
        const valueLabelId = `${this.name}-value`;
        this.valueLabel = new DatarowValueLabel(valueLabelId, this.valueText, this.key);
        this.addControl( this.valueLabel );
    }

    onElementClicked(elemId: string) {
        // globalThis.EVENTS.fireEvent( new EV.GuiElementClickedEvent(elemId) );
        console.log(`<DataRow "${this.name}"> clicked element "${elemId}"`);
        globalThis.GUI.onElementClicked(elemId);
    }
}

/* == DATAROW KEY ELEMENT == */
export class DatarowKeyLabel extends GUI.TextBlock {
    dataRow: SingleDataRow;
    key: string;
    // flags
    isClickable: boolean = false;
    plainKeyText: boolean = false;


    constructor(dataRow: SingleDataRow, elemId: string, key: string, customText?: string, isClickable?: boolean, plainKeyText?: string) {
        super(elemId, key + ": ");
        this.dataRow = dataRow;
        if (customText) { this.text = customText + ": "; }
        // sizing
        this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.widthInPixels = LBL_KEY_WIDTH_PX;
        // and font style
        this.setDefaultFontStyle();
        // clickable?
        if (isClickable) { this.makeClickable(); }
        // show now ":" after text
        if (plainKeyText) { this.plainKeyText = true; }
    }

    setDefaultFontStyle() {
        // set default color
        const defaultColorName = TEXT_COLOR_NAME_DEFAULT;
        this.color = ColorPalette.NameAsHexStr(defaultColorName);
        this.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
        this.fontFamily = GUI_CONST.FONT_PARAGRAPH;
        this.textHorizontalAlignment = GUI.TextBlock.HORIZONTAL_ALIGNMENT_LEFT;
    }

    makeClickable() {
        this.setClickableFontStyle();
        const _this_: DatarowKeyLabel = this;
        this.onPointerDownObservable.add( function() {
            const elemId = _this_.name;
            _this_.onElementClicked(elemId);
        });
    }

    setClickableFontStyle() {
        this.isClickable = true;
        const linkColorName = TEXT_COLOR_NAME_LINK;
        this.color = ColorPalette.NameAsHexStr(linkColorName);
        this.fontWeight = "bold";
    }

    /* EVENT HANDLING */
    onElementClicked(elemId: string) {
        this.dataRow.onElementClicked(elemId);
    }


}

/* == DATAROW VALUE LABEL == */
export class DatarowValueLabel extends GUI.TextBlock {

    constructor(elemId: string, valueStr: string, key?: string) {
        super(elemId, valueStr);
        this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        this.widthInPixels = LBL_VALUE_WIDTH_PX;
        this.textHorizontalAlignment = GUI.TextBlock.HORIZONTAL_ALIGNMENT_LEFT;
        this.setDefaultFontStyle();
    }

    setDefaultFontStyle() {
        this.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
        this.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
    }
}

/* == CONSTANTS == */
// row labels
export const LABELS_WIDTH_PX = GUI_CONST.DEFAULT_INNER_WIDTH_PX - ICON_WIDTH_TOTAL_PX;
export const LBL_KEY_WIDTH_PX = Math.round(LABELS_WIDTH_PX /3);
export const LBL_VALUE_WIDTH_PX = LABELS_WIDTH_PX - LBL_KEY_WIDTH_PX;
// wide key label
export const LBL_KEY_WIDTH_WIDE_PX = Math.round(LABELS_WIDTH_PX /2);
export const LBL_VALUE_WIDTH_NARROW_PX = LABELS_WIDTH_PX - LBL_KEY_WIDTH_WIDE_PX;
// row text colors
export const TEXT_COLOR_NAME_DEFAULT = "VALHALLA";
export const TEXT_COLOR_NAME_LINK = "ROYALBLUE";
