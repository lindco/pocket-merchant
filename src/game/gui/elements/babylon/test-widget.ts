import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';

export class TestWidget extends GUI.Rectangle {
    constructor() {
        super("test-widget");
        this.widthInPixels = 200;
        this.heightInPixels = 60;
    }
}