
import * as GUI_CONST from '../../common/constants';
import { GuiTools } from '../../common/gui-tools';
import * as GUI from 'babylonjs-gui';

export class NarrowIconLabel extends GUI.Rectangle {
    // properties
    iconName: string;
    text: string;
    // children
    container: GUI.StackPanel;
    icon: GUI.Image;
    label: GUI.TextBlock;

    constructor(elemId: string, iconName: string, text: string) {
        super(elemId);
        this.name = elemId;
        this.iconName = iconName;
        this.text = text;
        // set size
        this.width = GUI_CONST.NARROW_WIDTH;
        this.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        // create inner elements
        // this._createContainer();
        this._createContent();
    }

    private _addControl( control: GUI.Control ) {
        control.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        super.addControl( control );
    }

    addControl(control: GUI.Control): GUI.Container {
        this.container.addControl( control );
        return this;
    }

    private _createContainer() {
        this.container = new GUI.StackPanel(`${this.name}-container`);
        this.container.isVertical = false;
        // size & margin
        this.container.width = this.width;
        this.container.paddingLeftInPixels = MARGIN_PX;
        this.container.paddingRightInPixels = MARGIN_PX;
        this.container.paddingTopInPixels = MARGIN_PX;
        this.container.paddingBottomInPixels = MARGIN_PX;
        this._addControl( this.container );
    }

    private _createContent() {
        // create icon

        // create label
    }
}

export class GuiLabel extends GUI.Rectangle {
    elemId: string;
    text: string;
    // child elements
    labelBg: GUI.Image;
    labelTextBlock: GUI.TextBlock;
    // flags
    isClickable: boolean = false;
    hasSmallHeight: boolean = false;
    hasNoBackground: boolean = false;

    constructor(elemId: string, text: string, clickable?: boolean, smallHeight?: boolean, noBackground?: boolean, customAlignments?: Array<number>) {
        super(elemId);
        // set basic properties
        GuiTools.setAlignments(this, customAlignments);
        this.text = text;
        // set flags
        if (clickable && clickable == true) { this.isClickable = true; }
        if (smallHeight && smallHeight == true) { this.hasSmallHeight = true; }
        if (noBackground && noBackground == true) { this.hasNoBackground = true; }
        // update container && create children
        this.updateContainer();
        this.createChildren();
    }

    updateContainer() {
        this.width = "400px";
        this.height = this.labelHeight;
        this.thickness = 0;
    }

    /* CHILD CREATION METHODS */
    createChildren() {
        if (! this.hasNoBackground) { this.createLabelBackground() }
        this.createLabelTextBlock();
        if (this.isClickable) { this.makeClickable(); }
    }

    createLabelBackground() {
        const bgImageId = this.elemId + "-bg";
        this.labelBg = new GUI.Image(bgImageId, GUI_CONST.BG_PAPER);
        this.labelBg.height = this.labelHeight;
        this.addControl(this.labelBg);
    }

    createLabelTextBlock() {
        // create TextBlock element
        const textBlockId = this.elemId + "-textblock";
        this.labelTextBlock = new GUI.TextBlock(textBlockId, this.text);
        this.labelTextBlock.height = this.labelHeight;
        // set label font
        this.labelTextBlock.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
        this.labelTextBlock.fontFamily = GUI_CONST.FONT_PARAGRAPH;
        this.addControl(this.labelTextBlock);
    }

    makeClickable() {
        const _this_ = this;
        this.labelTextBlock.onPointerDownObservable.add( function() {
            const elemId = _this_.name;
            _this_.onElementClicked(elemId);
        });
    }

    /* LABEL GETTERS */
    get labelHeight(): string {
        if (this.hasSmallHeight) { return GUI_CONST.SMALL_LABEL_HEIGHT; }
        return GUI_CONST.DEFAULT_LABEL_HEIGHT;
    }

    /* EVENT HANDLING */
    onElementClicked(elemId: string) {
        console.log(`<GuiLabel "${this.name}"> clicked!`);
        // inform GuiManager
        globalThis.GUI.onElementClicked(elemId);
    }
    
}

//const NARROW_ICON_SIZE_PX = 31;
const MARGIN_PX = 2;
const INNER_HEIGHT_PX = GUI_CONST.DEFAULT_LABEL_HEIGHT_PX - 2*(MARGIN_PX);