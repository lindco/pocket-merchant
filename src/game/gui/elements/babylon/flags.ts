import * as GUI_CONST from '../../common/constants';
import * as GUI from 'babylonjs-gui';
import { GuiTools } from '../../common/gui-tools';
import { ColorPalette } from '../../../../helpers/colorpalette';
import { IFactionFlag } from "../../../models/faction/faction-interfaces";

export class FactionFlagImage extends GUI.Rectangle {
    factionFlag: IFactionFlag;
    // child elements
    bg: GUI.Rectangle;
    icon: GUI.Image;

    constructor(factionFlag: IFactionFlag) {
        const flagImgId = "flag-" + factionFlag.factionKey;
        super(flagImgId);
        // set properties
        this.factionFlag = factionFlag;
        // adjust container & add children
        this.adjustContainer();
        this.addBackground();
        this.addFlagIcon();
        // tinting?
    }

    private adjustContainer() {
        this.thickness = 0;
        GuiTools.setSizeInPx(this, SIZE_OUTTER_PX, SIZE_OUTTER_PX);
        // set padding
        this.paddingLeft = "24px";
        this.paddingRight = "12px";
        // set alignments
        const alignments: Array<number> = [ GUI_CONST.ALIGN_H_RIGHT, GUI_CONST.ALIGN_V_TOP ]
        GuiTools.setAlignments(this, alignments);
    }

    private addBackground() {
        const bgId: string = this.name + "-bg";
        this.bg = new GUI.Rectangle(bgId);
        this.bg.thickness = 0;
        // add to parent
        this.addControl(this.bg);
        // place in center
        const bgAlignments: Array<number> = [ GUI_CONST.ALIGN_H_CENTER, GUI_CONST.ALIGN_V_CENTER ];
        GuiTools.setAlignments(this.bg, bgAlignments);
        // set flag bg color
        this.bg.background = ColorPalette.NameAsHexStr(this.factionFlag.bgColorName);
        // add shadow
        this.bg.shadowColor = "#0008";
        this.bg.shadowOffsetX = 0;
        this.bg.shadowOffsetY = 2;
        this.bg.shadowBlur = 5;
    }

    private addFlagIcon() {
        const iconId = this.name + "-icon";
        const flagIconPath = FLAG_ICON_FOLDER + "icon_" + this.factionFlag.iconName.toLowerCase() + ".png";
        console.log(`<FactionFlag> showing flag icon "${flagIconPath}"`);
        this.icon = new GUI.Image(iconId, flagIconPath);
        GuiTools.setSizeInPx(this.icon, SIZE_INNER_PX, SIZE_INNER_PX);
        this.icon.verticalAlignment = GUI_CONST.ALIGN_TEXT_V_TOP;
        this.icon.topInPixels = ICON_Y_OFFSET_PX;
        // icon shadow
        this.icon.shadowColor = ICON_SHADOW_COLOR_HEX;
        this.icon.shadowBlur = ICON_SHADOW_BLUR_PX;
        this.icon.color = "green";
        this.icon.alpha = .5;
        this.addControl(this.icon);
    }

}

/* == FACTION FLAG CONSTANTS == */
export const FLAG_ICON_FOLDER = "./res/img/flags/icons/";
export const SIZE_INNER_PX = 32;
export const SIZE_INNER = `${SIZE_INNER_PX}px`;
export const SIZE_OUTTER_PX = 80;
export const SIZE_OUTTER = `${SIZE_OUTTER_PX}px`;
export const ICON_Y_OFFSET_PX = 22;
export const ICON_SHADOW_COLOR_HEX = "#0009";
export const ICON_SHADOW_BLUR_PX = 18;