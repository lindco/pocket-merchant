import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';
import * as GUI_CONST from "../../common/constants";
import { ELEMENT_FULL_WIDTH, ELEMENT_FULL_WIDTH_PX } from "../top-bar";
import { InlineIcon } from "../../elements/icons";

export class MenuIconLabel extends GUI.Rectangle {
    key: string;
    text: string = "";
    iconName: string;
    // observables
    onTextChange$ = new BABYLON.Observable<string>();
    // children
    private textLabel: GUI.TextBlock;
    private inlineIcon: InlineIcon;

    public constructor(key: string, text: string, inlineIconName: string) {
        super(`menulabel-${key}`);
        this. key = key;
        this.text = text;
        // initialize
        this.initializeContainer();
        this.createChildren();
        // observables
        this.onTextChange$.add(
            text => this.text = text
        );
    }

    /* == INITIALIZATION == */
    private initializeContainer(): void {
        this.width = ELEMENT_FULL_WIDTH; // set to top bar elem width
        this.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
    }

    private createChildren(): void {
        // layout
        const horLayout = new GUI.StackPanel(`${this.name}-layout`);
        horLayout.isVertical = false;
        horLayout.width = this.width;
        horLayout.height = GUI_CONST.DEFAULT_ELEMENT_HEIGHT;
        // icon
        const iconElemId = `${this.name}-icon`;
        this.inlineIcon = new InlineIcon(iconElemId, this.iconName);
        this.inlineIcon.widthInPixels = 40;
        // this.inlineIcon.widthInPixels = MENUITEM_WIDTH_ICON_PX;
        // label
        this.textLabel = new GUI.TextBlock(`${this.name}-text`, this.text);
        this.textLabel.height = this.height;
        this.textLabel.width = "100px";
        this.textLabel.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
        this.textLabel.fontSize = GUI_CONST.TEXTSIZE_NORMAL;

        // horLayout.addControl(this.inlineIcon);
        // horLayout.addControl(this.textLabel);
        this.addControl(this.inlineIcon);
    }

}

const MENUITEM_WIDTH_FULL_PX = ELEMENT_FULL_WIDTH_PX;
const MENUITEM_WIDTH_ICON_PX = 40;
const MENUITEM_WIDTH_LABEL_PX = MENUITEM_WIDTH_FULL_PX - MENUITEM_WIDTH_ICON_PX;