import { ColorPalette } from "../../../helpers/colorpalette";
import { IBaseEvent, BaseEventListener } from '../../../common/models/events';
import * as GUI_CONST from '../common/constants';
import { SideBarMenu } from "./common/side-bar-menu";
import { InlineIcon } from "../elements/icons";
import { GuiLabel } from "../elements/babylon/labels";
import { GuiTools } from "../common/gui-tools";
import { FamilyCaravansDialog } from "../dialogs/family-caravans-dialog";
import { GameMenuDialog } from "../dialogs/game-menu-dialog";
import * as GUI from 'babylonjs-gui';

export class TopBarMenu extends SideBarMenu  {
    elements: {} = {};
    innerLabel: GUI.TextBlock = null;
    menuButtons: Array<GuiLabel> = [];

    constructor() {
        super(MENU_ID, false, false);
        // create children
        this.createMenuButtons();
        this.addMenuItems();
        this.registerToObservables();
    }

    /* INITIALIZE SELF & BACKGROUND IMAGE */
    createMenuButtons() {
        this.createTopBarButton(false);
        this.createTopBarButton(true);
    }

    registerToObservables(): void {
        super.registerToObservables();
        globalThis.GLOOBS.onPlayerObjCreated$.add(
            () => {
                globalThis.PLAYER.onCoinsChange$.add( // on Player coins changed
                    () => this.updateLabelValue("coins") );
                globalThis.PLAYER.onAfterPlayerMove$.add( // after player moved
                    () => this.updateLabelValue("pos") );
        });
        globalThis.GLOOBS.onTurnFinishing$.add(
            () => this.updateAll()
        );
    }

    /* CREATE MENU ELEMENTS */
    addMenuItems() {
        this.addIconValueLabel("family", "Lowborn", "name_bright"); // player family name
        this.addIconValueLabel("coins", "500", "money_bright", true); // player coins
        this.addIconValueLabel("cargo", "0", "storage", true); // player free cargo
        this.addIconValueLabel("pos", "0", "tile_bright", true); // player pos
        this.addIconValueLabel("turn", "0", "turn_bright", true); // turn number
        this.width = "100%";
    }

    /* CREATE MENU ELEMENT LABELS & ICONS */
    private setLabelFontStyle(label: GUI.TextBlock): GUI.TextBlock {
        label.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
        label.fontSize = GUI_CONST.TEXTSIZE_NORMAL;
        label.color = ColorPalette.NameAsHexStr(GUI_CONST.MENU_TEXT_COLOR_NAME);
        return label;
    }

    addIconValueLabel(key: string, valueText: string, customIcon?: string, shortValue?: boolean): GUI.StackPanel {
        this._createMenuItemContainer(key);
        const keyIcon = this._createKeyIcon(key, customIcon);
        const valueLabel = this._createValueLabel(key, valueText, shortValue);
        this.elements[key] = this.container;
        return this.container;
    }

    _createMenuItemContainer(key: string) {
        const elementId = ID_ELEMENT_PREFIX + key;
        this.container = new GUI.StackPanel( elementId );
        this.container.width = ELEMENT_FULL_WIDTH;
        this.container.height = GUI_CONST.MENU_HEIGHT_SMALL
        this.container.isVertical = false;
        this.outterLayout.addControl(this.container);
    }

    _createKeyIcon(key: string, customIcon?: string): InlineIcon {
        let iconId: string = TopBarMenu.GetKeyIconId(key);
        let iconImageName: string = key;
        if (customIcon) { iconImageName = customIcon; }
        const icon = new InlineIcon(iconId, iconImageName);
        // add to inner layout
        this.container.addControl(icon);
        return icon;
    }

    _createValueLabel(key: string, valueText: string, isShort?: boolean): GUI.TextBlock {
        const valueLabel = this.makeElementLabel( key, valueText, false);
        this.setLabelFontStyle(valueLabel);
        if (isShort) {
            this.container.width = ELEMENT_VALUE_WIDTH_SHORT;
            this.width = ELEMENT_SHORT_WIDTH;
            
        }
        this.container.addControl(valueLabel)
        return valueLabel;
    }

    setElementValueText(key: string, value: string) {
        const valueLabel = this.getElementLabel(key, false);
        if (valueLabel) { valueLabel.text = value; } 
    }

    /* returns babylon TextBlock component of key or value label 
        key: element key as string (p.e. "coins") */
    getElementLabel(key: string, isKey?: boolean): GUI.TextBlock {
        // look for value label?
        let isValue: boolean = true;
        if (isKey && isKey == true) { isValue = false; }
        let labelId: string = TopBarMenu.GetLabelId(key, isValue)
        for (const topBarChild of this.getDescendants()) {
            if (topBarChild.name == labelId) { return topBarChild as GUI.TextBlock }
        }
        return null;
    }

    private createTopBarButton(isGameMenu?: boolean) {
        let buttonId: string = ID_BTN_TRADERS;
        let btnHAlign: number = GUI_CONST.ALIGN_H_LEFT
        if (isGameMenu && isGameMenu == true) { 
            buttonId = ID_BTN_MENU; 
            btnHAlign = GUI_CONST.ALIGN_H_RIGHT;
        }
        const fullId = ID_BTN_PREFIX + buttonId;
        const btnText = buttonId.toLocaleUpperCase();
        // create label & set alignment
        const btnLabelElem = GuiTools.addNewGuiLabel(this, fullId, btnText, true, false, true);
        this.menuButtons.push(btnLabelElem);
        btnLabelElem.horizontalAlignment = btnHAlign;
        // style font
        btnLabelElem.color = ColorPalette.NameAsHexStr("LIGHTSTEEL");
        // btnLabelElem.fontFamily = GUI_CONST.FONT_PARAGRAPH;
        // btnLabelElem.fontWeight = "bold";
    }

    private makeElementLabel(key: string, text: string, isKey: boolean): GUI.TextBlock {
        let labelText = text + ": ";
        let labelTextAlign: number = GUI_CONST.ALIGN_TEXT_H_RIGHT;
        // is a value lavel?
        if (! isKey) {  
            labelText = text;
            labelTextAlign = GUI_CONST.ALIGN_TEXT_H_LEFT;
        }
        const isValue = ! isKey;
        const labelId = TopBarMenu.GetLabelId(key, isValue);
        const label = new GUI.TextBlock( labelId, labelText );
        // sizing
        label.width = this.getLabelWidth(key, isValue);
        label.height = GUI_CONST.MENU_HEIGHT_SMALL;
        label.textHorizontalAlignment = labelTextAlign;
        return label;
    }

    /* MENU UPDATE */
    updateAll() {
        const labelKeys = ["coins", "cargo", "family", "turn", "pos"];
        for (const labelKey of labelKeys) {
            this.updateLabelValue(labelKey);
        }
    }

    private getLabelValue(labelKey: string): string {
        switch (labelKey) {
            case "coins": return `${this.playerCoins}`;
            case "cargo": return `${this.freePlayerStorage}`;
            case "family": return this.playerFamilyName;
            case "turn": return `${this.turnNum}`;
            case "pos": return this.playerPosStr;
            default: return "";
        }
    }

    private updateLabelValue(labelKey: string): void {
        const strValue = this.getLabelValue(labelKey);
        this.setElementValueText(labelKey, strValue);
    }

    /* VALUE GETTERS */
    get playerCoins(): number {
        if (globalThis.PLAYER) { return globalThis.PLAYER.coins; }
        return 0;
    }

    get freePlayerStorage(): number {
        if (globalThis.PLAYER && globalThis.PLAYER.army) {
            return globalThis.PLAYER.army.resourceHandler.freeStorageSpace;
        }
        return 0;
    }

    get playerFamilyName(): string {
        if (globalThis.PLAYER && globalThis.PLAYER.family ) {
            return globalThis.PLAYER.getFamily().name;
        }
        return "Lowborn";
    }

    get turnNum(): number {
        if (globalThis.TURNS) {
            return globalThis.TURNS.active.num;
        }
        return 0;
    }

    get playerPosStr(): string {
        if (globalThis.PLAYER && globalThis.PLAYER.pos) {
            return `${globalThis.PLAYER.pos.asString()}`;
        }
        return "";
    }

    getLabelWidth(key: string, isValueLabel?: boolean): string {
        if (! isValueLabel) { return ELEMENT_KEY_WIDTH; }
        return ELEMENT_VALUE_WIDTH;
    }

    /* STATIC ELEMENT ID GETTERS */
    static GetValueLabelId(key: string): string {
        return ID_ELEMENT_PREFIX + key + "-valuelabel";
    }

    static GetKeyLabelId(key: string): string {
        return ID_ELEMENT_PREFIX + key + "-keylabel";
    }

    static GetLabelId(key: string, isValueLabel?: boolean): string {
        if (! isValueLabel) { return TopBarMenu.GetKeyLabelId(key); }
        return TopBarMenu.GetValueLabelId(key);
    }

    static GetKeyIconId(key: string): string {
        return ID_ELEMENT_PREFIX + key + "-keyicon";
    }

    static GetBtnIdFromElementId(elemId: string): string {
        return elemId.replace(ID_BTN_PREFIX, "");
    }

    /* EVENT HANDLING */
    onElementClicked(elemId: string): boolean {
        if (! elemId.startsWith("menu")) { return false; }
        console.log(`<TopBarMenu> click on menues element "${elemId}"`);
        // click on top bar button? (Traders or Game Menu)?
        if (elemId.startsWith(ID_BTN_PREFIX)) {
            const btnId = TopBarMenu.GetBtnIdFromElementId(elemId);
            // click on Traders button? show FamilyCaravansDialog for player
            if (btnId == ID_BTN_TRADERS) {
                globalThis.GUI.showDialog(new FamilyCaravansDialog());
                return true;
            } else if (btnId == ID_BTN_MENU) {
                globalThis.GUI.showDialog(new GameMenuDialog());
                return true;
            }
        }
        return false;
    }
}

/* == TOP BAR CONSTANTS == */
// gui ids
export const MENU_ID = "top";
export const ID_MENU_PREFIX = "menu-" + MENU_ID + "-";
export const ID_MENU_OUTTER = ID_MENU_PREFIX + "outter";
export const ID_MENU_BG = ID_MENU_PREFIX + "bg";
export const ID_MENU_LAYOUT = ID_MENU_PREFIX + "layout";
export const ID_ELEMENT_PREFIX = ID_MENU_PREFIX + "element-";
export const ID_INNER_LABEL = ID_MENU_LAYOUT + "-label";
export const ID_INNER_ICON = ID_MENU_LAYOUT + "-icon";
// top bar traders & menu buttons
export const ID_BTN_PREFIX = ID_MENU_PREFIX + "btn-";
export const ID_BTN_TRADERS = "traders";
export const ID_BTN_MENU = "menu";
export const MENU_BUTTON_IDS = [ID_BTN_TRADERS, ID_BTN_MENU];
// element children size
export const ELEMENT_SHORT_X_MINUS = 30;
export const ELEMENT_ICON_PADDING = "5px";
export const ELEMENT_KEY_WIDTH_PX = 90;
export const ELEMENT_KEY_WIDTH = `${ELEMENT_KEY_WIDTH_PX}px`;
export const ELEMENT_VALUE_WIDTH_PX = 130;
export const ELEMENT_VALUE_WIDTH_SHORT_PX = ELEMENT_VALUE_WIDTH_PX -ELEMENT_SHORT_X_MINUS;
export const ELEMENT_VALUE_WIDTH_SHORT = `${ELEMENT_VALUE_WIDTH_SHORT_PX}px`;
export const ELEMENT_VALUE_WIDTH = `${ELEMENT_VALUE_WIDTH_PX}px`;
// menu size 
export const ELEMENT_FULL_WIDTH_PX = ELEMENT_KEY_WIDTH_PX + ELEMENT_VALUE_WIDTH_PX;
export const ELEMENT_FULL_WIDTH = `${ELEMENT_FULL_WIDTH_PX}px`;
export const ELEMENT_SHORT_WIDTH_PX = ELEMENT_FULL_WIDTH_PX - ELEMENT_SHORT_X_MINUS;
export const ELEMENT_SHORT_WIDTH = `${ELEMENT_SHORT_WIDTH_PX}px`;