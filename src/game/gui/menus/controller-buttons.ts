// gui
import * as GUI_CONST from '../common/constants';
// babylon
import * as GUI from 'babylonjs-gui';

/* == CONTROLLER LAYOUT == */
export class ControllerButtons extends GUI.Rectangle {
    constructor() {
        super(CONTROLLER_OUTTER_ID);
        this.thickness = 0;
        // size & alignment
        this.verticalAlignment = GUI_CONST.ALIGN_TEXT_V_BOTTOM;
        this.horizontalAlignment = GUI_CONST.ALIGN_H_RIGHT;
        this.width = "320px";
        this.height = "150px";
        // calculate margin offset
        // add test label
        let testLabel = new GUI.TextBlock("test", "Controller");
        this.addControl(testLabel);
    }

    /* moves the element horizontally to create a space between the screen edge that acts as margin */
    calculateMarginOffset() {
        let marginXOffsetPx = GUI_CONST.BODY_MARGIN_X_PX;
        if ( this.horizontalAlignment == GUI_CONST.ALIGN_H_RIGHT) {
            marginXOffsetPx *= -1;
        }
        this.leftInPixels = marginXOffsetPx;
    }
}

/* == CONSTANTS == */
export const CONTROLLER_OUTTER_ID = "controller-outter";