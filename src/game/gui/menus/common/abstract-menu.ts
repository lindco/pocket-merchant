import { GuiRectangle } from '../../common/rectangle';
import { GuiElementHead } from '../../common/element-head';
import * as GUI from 'babylonjs-gui';


export interface IMenu {
    menuId: string;
    // methods
    onElementClicked(elemId: string): void;
}

export class AbstractMenu extends GuiRectangle implements IMenu {
    menuId: string;
    container: GUI.StackPanel = null;

    constructor(menuId: string) {
        super("menu-" + menuId);
        this.head = new GuiElementHead(this.name, this);
        this.thickness = 0;
    }

    onElementClicked(elemId: string) {
        
    }

}