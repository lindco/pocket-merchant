// gui
import * as GUI_CONST from '../../common/constants';
import { GuiElementHead } from '../../common/element-head';
import { AbstractMenu } from './abstract-menu';
// babylon
import * as GUI from 'babylonjs-gui';

export class SideBarMenu extends AbstractMenu {
    // default axis: y (top or bottom)
    axisNum: number = SIDEBAR_AXIS_Y;
    // menu flags
    isHorizontal: boolean = false;
    isRightOrBottom: boolean = false;
    // children
    backgroundImg: GUI.Image = null;
    outterLayout: GUI.StackPanel = null;

    constructor(menuId: string, isHorizontal?: boolean, isRightOrBottom?: boolean) {
        super(menuId);
        this.thickness = 0;
        // optional properties
        if (isHorizontal && isHorizontal == true) { this.axisNum = SIDEBAR_AXIS_X; }
        if (isRightOrBottom && isRightOrBottom == true) { this.isRightOrBottom = true; }
        // set alignment
        this.setAlignmentAndSize();
        // add children
        this.addBackgroundImage();
        this.addOutterLayout();
    }

    setAlignmentAndSize() {
        // horizontal alignment
        let horizontalAlign: number = GUI_CONST.ALIGN_H_CENTER;
        if (this.isHorizontal) {
            if (! this.isRightOrBottom) { horizontalAlign = GUI_CONST.ALIGN_H_LEFT; }
            // set size for horizontal sidebars
            else { horizontalAlign = GUI_CONST.ALIGN_H_RIGHT; }
            this.width = GUI_CONST.MENU_HEIGHT_SMALL;
            this.height = "100%";
        }
        this.horizontalAlignment = horizontalAlign;
        // vertical alignment
        let verticalAlign: number = GUI_CONST.ALIGN_V_CENTER;
        if (this.isVertical) {
            if (! this.isRightOrBottom) { verticalAlign = GUI_CONST.ALIGN_V_TOP; }
            else { verticalAlign = GUI_CONST.ALIGN_V_BOTTOM; }
            // set size for vertical ones
            this.width = GUI_CONST.WIDTH_FULL;
            this.height = GUI_CONST.MENU_HEIGHT_SMALL;
        }
        this.verticalAlignment = verticalAlign;
    }

    addBackgroundImage() {
        const bgImageId = this.name + "-bg";
        this.backgroundImg = new GUI.Image(bgImageId, GUI_CONST.BG_MENU_TOP);
        this.backgroundImg.horizontalAlignment = this.horizontalAlignment;
        this.backgroundImg.verticalAlignment = this.verticalAlignment;
        this.backgroundImg.width = this.width;
        this.backgroundImg.height = this.height;
        this.backgroundImg.stretch = GUI.Image.STRETCH_FILL;
        // add to menu element
        this.addControl(this.backgroundImg);
    }

    addOutterLayout() {
        const innerLayoutId = this.name + "-inner";
        this.outterLayout = new GUI.StackPanel( innerLayoutId );
        this.outterLayout.isVertical = this.innerLayoutIsVertical;
        // sizing
        let outterLayoutW: string = "50%";
        let outterLayoutH: string = GUI_CONST.MENU_HEIGHT_SMALL;
        if (this.isHorizontal) { 
            outterLayoutW = this.width as string;
            outterLayoutH = "50%";
        }
        this.width = outterLayoutW;
        this.height = outterLayoutH;
        // this.setSize(outterLayoutW, outterLayoutH);
        // inner layout aligment
        this.outterLayout.verticalAlignment = GUI_CONST.ALIGN_V_TOP;
        // add to menu element
        this.addControl(this.outterLayout);
    }

    /* SIDEBAR MENU GETTERS */
    get isVertical(): boolean { return ! this.isHorizontal; }
    get innerLayoutIsVertical(): boolean { return this.isHorizontal; }
}

/* == SIDE BAR MENU CONSTANTS == */
export const SIDEBAR_AXIS_X = 0; // left or right
export const SIDEBAR_AXIS_Y = 1; // top or bottom