import * as GUI_CONST from '../common/constants';
import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';
import { AbstractMenu } from './common/abstract-menu';
import { GuiTools } from "../common/gui-tools";

export class CircularButton extends AbstractMenu {
    // basic properties
    btnId: string;
    // children
    inner: GUI.Image = null;
    glow: GUI.Image = null;
    // image textures
    cachedTextures: Array<BABYLON.Texture> = [];

    constructor(btnId: string, horizontalAlign: number) {
        super( CircularButton.ShortElementId(btnId) );
        this.btnId = btnId;
        // on screen position
        this.verticalAlignment = GUI_CONST.ALIGN_V_CENTER;
        this.horizontalAlignment = horizontalAlign;
        // sizing & offset
        this.setSizeInPixels(CIRCBTN_SIZE_PX)
        this.calculateOffset();
        // add inner image
        this.addInnerImage();
        this.addGlowingImage();
    }

    registerToObservables(noHover?: boolean) {
        super.registerToObservables(noHover);
        this.onPointerOutObservable.add(
            () => this.glow.isVisible = false
        );
        this.onPointerEnterObservable.add(
            () => this.glow.isVisible = true
        );
    }

    calculateOffset() {
        this._addYOffset();
        this._addLeftMargin();
    }

    cacheTextures() {
        
    }

    _addLeftMargin() {
        let marginXOffsetPx = GUI_CONST.BODY_MARGIN_X_PX;
        if ( this.horizontalAlignment == GUI_CONST.ALIGN_H_RIGHT) {
            marginXOffsetPx *= -1;
        }
        this.leftInPixels = marginXOffsetPx;
    }

    _addYOffset() {
        this.top = CIRCBTN_V_OFFSET;
    }

    
    private addInnerImage() {
        // get name of button image in img/ui/buttons folder
        const innerImgId = CircularButton.FullElementId(this.btnId)
            .replace("outter", "inner");
        const imageFilePath = CircularButton.ImagePath(this.btnId, false);
        this.inner = new GUI.Image(innerImgId, imageFilePath);
        // sizing & add to outter
        GuiTools.setSizeInPx(this.inner, CIRCBTN_SIZE_PX, CIRCBTN_SIZE_PX);
        this.addControl(this.inner);
    }

    private addGlowingImage(): void {
        const glowingImgId = "cirbtn-glow";
        this.glow = new GUI.Image(glowingImgId, CircularButton.ImageGlowingPath);
        this.glow.isVisible = false;
        GuiTools.setSizeInPx(this.glow, CIRCBTN_SIZE_PX, CIRCBTN_SIZE_PX);
        this.addControl(this.glow);
    }

    updateImage() {
        this.inner.source = CircularButton.ImagePath(this.btnId, this.isHovered);
    }

    /* == STATIC METHODS == */

    /**
     *
     * @param btnId
     * @param isHovered
     * @constructor
     */
    static ImagePath(btnId: string, isHovered?: boolean): string {
        let fileEnding: string = ".png";
        if (isHovered) { fileEnding = "-glowing.png" }
        return GUI_CONST.FOLDER_BUTTONS_IMG + CIRCBTN_ID_PREFIX + btnId + fileEnding;
    }

    static get ImageGlowingPath(): string {
        return GUI_CONST.FOLDER_BUTTONS_IMG + 'circular-btn-glowing.png';
    }

    static FullElementId(btnId: string): string {
        return CIRCBTN_ID_PREFIX_FULL + btnId + "-outter";
    }

    static ShortElementId(btnId: string): string {
        return CIRCBTN_ID_PREFIX + btnId;
    }
}

export const CIRCBTN_ID_PREFIX = "circbtn-";
export const CIRCBTN_ID_PREFIX_FULL = "menu-circbtn-";
export const CIRCBTN_IMG_FILE_PREFIX = "circbtn-"
export const CIRCBTN_SIZE_PX = 100;
export const CIRCBTN_SIZE = `${CIRCBTN_SIZE_PX}px`;
export const CIRCBTN_V_OFFSET = "18%";
export const CIRCBTN_ICON_FOLDER = "./res/img/ui/buttons/";