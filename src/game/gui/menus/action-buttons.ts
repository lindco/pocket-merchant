import * as GUI_CONST from '../common/constants';
import {GuiRectangle} from "../common/rectangle";
import * as GUI from 'babylonjs-gui';
import {CircularButton, CIRCBTN_SIZE} from "./circular-button";

export class ActionButtonsBar extends GuiRectangle {
    hLayout: GUI.StackPanel;
    innerButtons: Array<CircularButton> = [];

    constructor() {
        super(ACTION_BUTTONS_ID, ACTION_BUTTONS_ALIGNMENTS);
        this.width = ACTION_BUTTONS_WIDTH;
        this.moveDown();
        this.addHLayout();
        this.addCircularButtons();
    }

    private addCircularButtons() {
        this.addNewCircularButton("build");
        this.addNewCircularButton("trade");
        this.addNewCircularButton("empires");
    }

    private addHLayout() {
        const hLayoutId = this.name + "-layout";
        this.hLayout = new GUI.StackPanel(hLayoutId);
        this.hLayout.adaptHeightToChildren = true;
        this.hLayout.isVertical = true;
        this.hLayout.width = ACTION_BUTTONS_WIDTH;
        this.addControl(this.hLayout);
    }

    addCircularButton(circBtnToAdd: CircularButton): CircularButton {
        this.hLayout.addControl(circBtnToAdd);
        this.innerButtons.push(circBtnToAdd);
        return circBtnToAdd;
    }

    addNewCircularButton(name: string): CircularButton {
        const newCircBtn = new CircularButton(name, GUI_CONST.ALIGN_H_LEFT);
        return this.addCircularButton(newCircBtn);
    }

    private moveDown() {
        this.top = "19%";
    }
}

export const ACTION_BUTTONS_ID = "action-buttons";
export const ACTION_BUTTONS_WIDTH = "160px";
export const ACTION_BUTTONS_ALIGNMENTS = [GUI_CONST.ALIGN_H_LEFT, GUI_CONST.ALIGN_V_BOTTOM];