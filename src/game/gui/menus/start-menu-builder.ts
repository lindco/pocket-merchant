// gui
import * as GUI_CONST from '../common/constants';
// babylon
import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';
// menues
import { AbstractMenu } from './common/abstract-menu';
import {TopBarMenu} from "./top-bar";
import {ControllerButtons} from "./controller-buttons";
import {CircularButton} from "./circular-button";
import {ActionButtonsBar} from "./action-buttons";

export class StartMenuBuilder {
    private static _logStr = "<StartMenuBuilder> ";

    static addToGuiRoot(menu: GUI.Control) {
        if (! menu) {
            console.error("<StartMenuBuilder> cannot add menu: is null");
            return;
        }
        globalThis.GUI.advTexture.addControl(menu);
        if (menu instanceof AbstractMenu) {
            globalThis.GUI.menues.push(menu as AbstractMenu);
        }
    }

    static showStartingMenues() {
        console.log(StartMenuBuilder._logStr + "showing menues");
        StartMenuBuilder.showTopBar();
        StartMenuBuilder.showController();
        StartMenuBuilder.showCircularButtons();
    }

    private static showTopBar() {
        const topBar: TopBarMenu = new TopBarMenu();
        globalThis.GUI.topBar = topBar;
        StartMenuBuilder.addToGuiRoot(topBar)
        globalThis.GUI.updateTopBar();
    }

    /**
     * create the gameboy like controller buttons to the bottom right
     * @private
     */
    private static showController() {
        const controller = new ControllerButtons();
        StartMenuBuilder.addToGuiRoot(controller);
    }

    private static showCircularButtons() {
        // add next turn button
        const nextTurnBtn = new CircularButton("turn", GUI_CONST.ALIGN_H_RIGHT);
        StartMenuBuilder.addToGuiRoot(nextTurnBtn);
        // action btns bar
        const actionBtns = new ActionButtonsBar();
        globalThis.GUI.actionButtonsBar = actionBtns;
        StartMenuBuilder.addToGuiRoot(actionBtns);
    }
}