import { EmptyDialog } from "./common/base-dialog";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";

export class SingleArmyDialog extends EmptyDialog {
    army: IUnitArmy;

    constructor(army: IUnitArmy) {
        super(DIAG_CARAVAN_UNITS, true, "Army");
        this.army = army;
    }
}

export const DIAG_CARAVAN_UNITS = "diag-caravan-units";