import { TradeNode } from "../../models/trade/trade-node/trade-node";
import { StoredResource } from "../../models/trade/resource/stored-resource";
import { EmptyDialog } from "./common/base-dialog";
import { CityTransferDialog } from "./city-transfer-dialog";
import { GuiScrollView } from "../elements/scroller";
import { CityTradeHistoryDialog } from "./city-tradehist-dialog";
import { GuiCheckBox } from "../elements/checkbox";
import * as GUI from 'babylonjs-gui';
import { ITradeLocation } from "../../world/interfaces/location.interfaces";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == RES LIST DIALOG == */
export class ResourceListDialog extends EmptyDialog {
    location: ITradeLocation;
    showAll: boolean = false;
    // children
    showAllCheckbox: GuiCheckBox;
    scroller: GUI.ScrollViewer;
    innerLayout: GUI.StackPanel;

    constructor(market: TradeNode) {
        super(GameDialogKeys.DIAG_RESLIST, true, `${market.parentLocation.name}: Market`,
            [DIAG_RESLIST_BTN_HISTORY_TEXT], null);
        // set TradeLocation of this dialog
        this.location = market.parentLocation;
        this.registerToObservables();
    }


    registerToObservables() {
        super.registerToObservables();
        this.onDialogBtnClicked$.add( // register to btn click
            (btnId) => {
                if (btnId == DIAG_RESLIST_BTN_HISTORY ) {
                    this.switchToDialog( new CityTradeHistoryDialog(this.location) );
                }
        });
        this.onChildClicked$.add( // register to child element clicked
            (elemId) => {
                if (elemId.startsWith(ID_REFIX_DATAROW)) {
                    this.onDataRowClicked(elemId);
                }
                else if ( elemId.startsWith(DIAG_RESLIST_CHECKB_SHOWALL) ) {
                    this.showAll =! this.showAll;
                    this.showAllCheckbox.isChecked = this.showAll;
                    // this.showAllCheckbox.update();
                }
        });
    }

    build(): void { super.build(); }

    addCustomContent() {
        this.addShowAllCheckbox();
        this.addResourceList();
        this.addNewButton(DIAG_RESLIST_BTN_HISTORY, DIAG_RESLIST_BTN_HISTORY_TEXT);
    }

    private addShowAllCheckbox() {
        this.showAllCheckbox = new GuiCheckBox(DIAG_RESLIST_CHECKB_SHOWALL, "Show All", this.showAll, false);
        this.addControl(this.showAllCheckbox, true);
    }

    private addResourceList() {
        // this.addScrollView();
        const storedResources = this.location.tradeNode.asStoresResArr();
        for (const storedRes of storedResources) {
            this.addResourceListElement(storedRes);
        }
    }

    private addScrollView() {
        this.scroller = new GUI.ScrollViewer(DIAG_RESLIST_SCROLLVIEW);
        GuiScrollView.setDefaultStyle(this.scroller);
        // sizing
        this.scroller.height = DIAG_RESLIST_INNER_HEIGHT;
        this.addInnerLayout();
        this.addControl(this.scroller, true);
        this.addNewDivider(true);
    }

    private addInnerLayout() {
        this.innerLayout = new GUI.StackPanel(DIAG_RESLIST_INNER);
        if (this.scroller) { this.scroller.addControl(this.innerLayout); }
        else { this.addControl(this.innerLayout); }
        
    }

    private addResourceListElement(storedRes: StoredResource) {
        const resName = globalThis.TRADE.getResName(storedRes.resId);
        const elemId = DIAG_RESLIST_LBL_PREFIX + storedRes.resId;
        const valueStr = ResourceListDialog.GetElementValueStr(storedRes);
        this.addNewDataRow(resName, valueStr, DIAG_RESLIST_STORAGE_ICON, true, elemId, this.innerLayout);
    }

    private onDataRowClicked(elemId: string) {
        const resId = ResourceListDialog.GetResIdFromDataRowId(elemId);
        const cityTransferDiag = new CityTransferDialog(this.location, resId);
        this.switchToDialog(cityTransferDiag);
    }

    /* STATIC GETTERS */
    static GetElementValueStr(storedRes: StoredResource): string {
        const amountStr = "" + storedRes.amount.toFixed(1);
        const priceStr = "" + storedRes.price.toFixed(0)  + "o";
        return amountStr + "  (" + priceStr + ")";
    }

    static GetResIdFromDataRowId(elemId: string): string {
        if (! globalThis.TRADE) {
            console.warn(`<DiagReslist> cant get resId from datarow "${elemId}"; TradeManager not found`); return;
        }
        // remove element prefix & postfix ("-key")
        const resName = elemId.replace(ID_REFIX_DATAROW, "").replace("-key", "");
        const tradeRes = globalThis.TRADE.getResByName(resName);
        if (! tradeRes) { 
            console.warn(`<ResListDiag> cant get trade resource with name "${resName}"`);
            return; 
        }
        return tradeRes.id;
    }
}

/* == RESLIST DIAG CONSTANTS == */
export const DIAG_RESLIST_INNER_HEIGHT = "400px";
export const ID_REFIX_DATAROW = "datarow-";
// children
export const DIAG_RESLIST_CHECKB_SHOWALL = GameDialogKeys.DIAG_RESLIST + "-showall";
export const DIAG_RESLIST_BTN_HISTORY = "btn-reslist-history";
export const DIAG_RESLIST_BTN_HISTORY_TEXT = "Trade History"
export const DIAG_RESLIST_LBL_PREFIX = "reslist-item-";
export const DIAG_RESLIST_STORAGE_ICON = "storage";
export const DIAG_RESLIST_SCROLLVIEW = "diag-reslist-scroll";
export const DIAG_RESLIST_INNER = "diag-reslist-inner";