import { StoredResource } from "../../models/trade/resource/stored-resource";
import { EmptyDialog } from "./common/base-dialog";
import { SingleArmyDialog } from "./caravan-units-dialog";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/**
 * Shows infos about the player caravan and buttons to view army & good details
 */
export class PlayerInfoDialog extends EmptyDialog {
    playerArmy: IUnitArmy;

    constructor() {
        super(GameDialogKeys.DIAG_PLAYER, false, "Player");
        this.playerArmy = globalThis.PLAYER.army;
        this.registerToObservables();
    }

    registerToObservables() {
        super.registerToObservables();
        this.onDialogBtnClicked$.add(
            (btnId) => {
                if (btnId === DIAG_PLAYER_BTN_INVENTORY) {
                    this.log("show inventory diag");
                } else if (btnId === DIAG_PLAYER_BTN_UNITS) {
                    // show player ArmyDialog
                    const armyDiag = new SingleArmyDialog(this.playerArmy);
                    globalThis.GUI.showDialog(armyDiag);
                }
        });
    }

    build() {
        super.build();
    }

    addCustomContent(): void {
        // add labels
        this.addNewDataRow("family", PLAYER.family.name);
        // show stored resources
        this.showResourceInventory();
        // add buttons
        this.addNewButton(DIAG_PLAYER_BTN_INVENTORY, "Inventory");
        this.addNewButton(DIAG_PLAYER_BTN_UNITS, "Units");
    }

    private showResourceInventory() {
        const storage = globalThis.PLAYER.army.resourceHandler.storedResources;
        for (const storedRes of storage) {
            this.showStoredResRow(storedRes);
        }
    }

    private showStoredResRow(storedRes: StoredResource) {
        let resName = globalThis.TRADE.getResName(storedRes.resId)
        let resPrice: number = storedRes.price;
        const elemId = `res-inv-${storedRes.resId}`;
        this.addNewDataRow(resName, resPrice + "o", "storage", false, elemId);
    }
}

/* == CONSTANTS ==*/
export const DIAG_PLAYER_BTN_INVENTORY = "diag-player-btn-inventory";
export const DIAG_PLAYER_BTN_UNITS = "diag-player-btn-units";
