import { EmptyDialog } from "./common/base-dialog";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class GameLoadingDialog extends EmptyDialog {

    constructor() {
        super(GameDialogKeys.DIAG_LOADING, false, "Laden ..");
    }
}