import { EmptyDialog } from "./common/base-dialog";
import { Unit } from "../../models/military/unit";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class SingleArmyDialog extends EmptyDialog {
    army: IUnitArmy;

    constructor(army: IUnitArmy) {
        super(GameDialogKeys.DIAG_ARMY, true, "Army", [], false,
            false, army.ownerFaction);
        this.army = army;
    }

    addCustomContent(): void {
        this.addNewDataRow("Name", this.army.name, "name");
        this.addNewDataRow("Owner", this.ownerFactionName, "empire");
        // show units
        this.showUnitList();
    }

    showUnitList(): void {
        this.addNewSectionHeader("Units");
        for (const unit of this.army.units) {
            this.showSingleUnitRow(unit);
        }
    }

    private showSingleUnitRow(unit: Unit) {
        this.addNewDataRow("", unit.type.name, "might");
    }

    get ownerFactionName(): string {
        return this.army.ownerFaction.name;
    }
}