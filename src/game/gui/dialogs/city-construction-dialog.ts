import { City } from "../../models/city/city";
import { BuildingCategory } from "../../models/building/building-category";
import * as GUI_CONST from '../common/constants';
import { EmptyDialog } from "./common/base-dialog";
import * as GUI from 'babylonjs-gui';
import { IConstructable } from "../../models/building/interfaces/constructable.interfaces";
import { SwitcherChangeEvent } from "../elements/switcher";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == CITY CONSTRUCTION DIALOG == */
export class CityConstructionDialog extends EmptyDialog {
    city: City;
    // chil elements
    scrollViewer: GUI.ScrollViewer;
    innerLayout: GUI.StackPanel;

    constructor(city: City) {
        super(GameDialogKeys.DIAG_CONSTRUCTION, false, `${city.name} Construction`);
        this.city = city;
        this.registerToObservables();
    }

    registerToObservables(): void {
        super.registerToObservables();
        this.onSwitcherChanged$.add(
            (changedEvent) => {
                if (changedEvent.switcherElemId == DIAG_CONSTR_SWITCHER) {
                    this.innerLayout.clearControls();
                    const bldCatName = changedEvent.optionText;
                    const bldCatNameIndex = DIAG_CONSTR_SWITCHER_OPTIONS_NAMES.indexOf(bldCatName);
                    const bldCatNum = DIAG_CONSTR_SWITCHER_OPTIONS_ID[bldCatNameIndex]
                    this.addConstructableList(bldCatNum, true);
                }
        });
    }

    addCustomContent(): void {
        this.addNewSwitcher(DIAG_CONSTR_SWITCHER, DIAG_CONSTR_SWITCHER_OPTIONS_NAMES);
        // inner layout
        this.createInnerLayout();
        // switcher
        const startOption = DIAG_CONSTR_SWITCHER_OPTIONS_NAMES[0];
        const switcherChangeEvent = new SwitcherChangeEvent(DIAG_CONSTR_SWITCHER, startOption);
        this.onSwitcherChanged$.notifyObservers(switcherChangeEvent);
    }

    private createInnerLayout() {
        // create scrollable outter container
        this.scrollViewer = new GUI.ScrollViewer(DIAG_CONSTR_CONTENT_OUTTER);
        this.scrollViewer.widthInPixels = 400;
        this.scrollViewer.heightInPixels = DIAG_CONSTR_CONTENT_HEIGHT_PX;
        // GuiScrollView.setDefaultStyle(this.scrollViewer, true); 
        // and stackpanel inside it
        this.innerLayout = new GUI.StackPanel(DIAG_CONSTR_CONTENT);
        this.innerLayout.heightInPixels = DIAG_CONSTR_CONTENT_HEIGHT_PX;
        const innerLayoutBgImg = new GUI.Image(DIAG_CONSTR_CONTENT_OUTTER + "img", GUI_CONST.BG_PAPER);
        innerLayoutBgImg.heightInPixels = DIAG_CONSTR_CONTENT_HEIGHT_PX;
        // add bg and inner stackpanel to bld list layout
        this.scrollViewer.addControl(innerLayoutBgImg);
        this.scrollViewer.addControl(this.innerLayout);
        // add to constr dialog
        this.addControl(this.scrollViewer, true);
    }

    private addConstructableList(bldCatNum: number, hideNonProducable?: boolean) {
        const bldCatName = DIAG_CONSTR_SWITCHER_OPTIONS_NAMES[bldCatNum];
        this.log(`showing constructable buildings of category: ${bldCatNum} "${bldCatName}"`);
        let bldTypesOfCat: Array<IConstructable> = globalThis.CONSTRUCTABLE.getByCategory(bldCatNum);
        this.log(`${bldTypesOfCat.length} building types in cat`);
        for (const bldType of bldTypesOfCat) {
            this.addConstructableListItem(hideNonProducable, bldType);
        }
    }

    private addConstructableListItem(hideNonProducable: boolean, bldType: IConstructable) {
        // when building is of production cat & cities cant produce this resource
        let hideType: boolean = false;
        if (hideNonProducable == true && bldType.category.isProd) {
            hideType = this.prodBldTypeIsHidden(bldType.id);
        }
        if (!hideType) { this.addConstructableItem(bldType); } // show if not hidden
    }

    private prodBldTypeIsHidden(bldId: string): boolean {
        const resId = bldId.replace('prod-', '');
        // const resId = ProductionBuildingType.ResFromTypeId(bldId).id;
        const canBuild = this.city.canProduce(resId);
        if (! canBuild) {
            this.log(`building ${bldId} cant be build here; no neighboring ${resId} tiles`);
            return true;
        }
        return false;
    }

    private addConstructableItem(bldType: IConstructable) {
        const elemId = `lbl-build-${bldType.id}`;
        const constrRow = this.addNewDataRow(bldType.name, "Costs: 0", "building", true, elemId, this.innerLayout);
        constrRow.name = elemId;
    }
}

/* == CONSTANTS == */
export const DIAG_CONSTR_SWITCHER = GameDialogKeys.DIAG_CONSTRUCTION + "-bld-cat";
export const DIAG_CONSTR_SWITCHER_OPTIONS_NAMES = ["Civic", "Resources", "Military"];
export const DIAG_CONSTR_SWITCHER_OPTIONS_ID = [ BuildingCategory.CIVIC, BuildingCategory.PROD, BuildingCategory.MIL ];
export const DIAG_CONSTR_CONTENT = GameDialogKeys.DIAG_CONSTRUCTION + "-content";
export const DIAG_CONSTR_CONTENT_OUTTER = DIAG_CONSTR_CONTENT + "-bg";
export const DIAG_CONSTR_CONTENT_HEIGHT_PX = 400;
export const DIAG_CONSTR_BLD_PREFIX = GameDialogKeys.DIAG_CONSTRUCTION + "-bld-";