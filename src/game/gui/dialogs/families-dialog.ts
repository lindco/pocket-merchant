import { EmptyDialog } from "./common/base-dialog";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";
import { FamilyDialog } from "./family-dialog";

export class FamiliesDialog extends EmptyDialog {

    constructor() {
        super(GameDialogKeys.DIAG_FAMILIES, false, "Families");
    }

    registerToObservables() {
        super.registerToObservables();
        this.onChildClicked$.add(
            (elemId) => {
                const familyName = elemId.replace(FAMILY_ROW_PREFIX, "");
                const family = globalThis.FACTIONS.getFamilyByName(familyName);
                this.switchToDialog( new FamilyDialog(family) );
            }
        )
    }

    addCustomContent(): void {
        super.addCustomContent();
        this.addFamilyList();
    }

    addFamilyList() {
        for (const family of globalThis.FACTIONS.families) {
            const elemId: string = FAMILY_ROW_PREFIX + family.name;
            this.addNewDataRow("Family", family.name, "trader", true, elemId);
        }
    }


}

const FAMILY_ROW_PREFIX = "datarow-Family";