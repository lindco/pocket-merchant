import { City } from "../../models/city/city";
import { EmptyDialog } from "./common/base-dialog";
import { CityConstructionDialog } from "./city-construction-dialog";
import { BuildingRowElement } from "../elements/entities/building-row-element";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == CITY BUILDINGS DIALOG == */
export class CityBuildingsDialog extends EmptyDialog {
    city: City;

    constructor(city: City) {
        super(GameDialogKeys.DIAG_BUILDINGS, false, `${city.name} Buildings`);
        this.city = city;
        this.registerToObservables();
    }

    registerToObservables() {
        super.registerToObservables();
        this.onDialogBtnClicked$.add(
            (btnId) => {
                if (btnId == DIAG_CITYBLDS_BTN_CONSTRUCT) {
                    this.log('show City Construction Dialog');
                    this.close();
                    const diagCityConstr = new CityConstructionDialog(this.city);
                    globalThis.GUI.showDialog(diagCityConstr, true);
                }
        });
    }

    addCustomContent(): void {
        for (const building of this.city.buildings) {
            const elemId: string = `bldrow-${building.bldType.id}`;
            const bldElem: BuildingRowElement = new BuildingRowElement(elemId, building);
            this.addControl(bldElem, true);
            // add row for building in city TODO: replace with BuildingRowElement
            // this.addNewDataRow(building.name, "" + building.level, "building");
        }
        this.addNewDivider();
        this.addNewButton(DIAG_CITYBLDS_BTN_CONSTRUCT, "Construct");
    }
}


/* == CONSTANTS == */
export const DIAG_CITYBLDS_ID_PREFIX = GameDialogKeys.DIAG_BUILDINGS + "-";
export const DIAG_CITYBLDS_BTN_CONSTRUCT = DIAG_CITYBLDS_ID_PREFIX + "btn-construct";
