import { EmptyDialog } from "./common/base-dialog";
import { IDialog } from "./common/dialog.interfaces";

export class EndTurnBalanceDialog extends EmptyDialog implements IDialog {
    titleText: string = "Ending Turn: Balance";

    constructor() {
        super("endturn-1", true);
    }
}

export class EndTurnMovementDialog extends EmptyDialog implements IDialog {
    titleText: string = "Ending Turn: Enemies";

    constructor() {
        super("endturn-2", true, "Ending Turn", ["skip"]);
    }
}