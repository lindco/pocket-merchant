import { ITrade } from "../../models/trade/market-trade";
import { EmptyDialog } from "./common/base-dialog";
import { ITradeLocation } from "../../world/interfaces/location.interfaces";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == TRADE HISTORY DIALOG == */
export class CityTradeHistoryDialog extends EmptyDialog {
    tradeLocation: ITradeLocation;

    constructor(city: ITradeLocation) {
        super(GameDialogKeys.DIAG_TRADEHIST, false, `${city.name}: Trade History`);
        this.tradeLocation = city;
    }

    build(): void {
        super.build();
    }

    addCustomContent(): void {
        let last16 = this.tradeLocation.tradeHistory.trades.slice(-16);
        for (const trade of last16) {
            // default values are for imports
            this.addTradeHistoryItem(trade);
        }
    }

    addTradeHistoryItem(trade: ITrade) {
        const resName = globalThis.TRADE.getResName(trade.resId);
        const absProfitStr = "" + trade.absProfit;
        const tradeStr = `3 ${resName} ${trade.fromToStr} ${trade.otherLocationName}`;
        // add data row, p.e. "[X] -125:  1 Fish from London"
        this.addNewDataRow(absProfitStr, tradeStr, trade.iconName);
    }
}