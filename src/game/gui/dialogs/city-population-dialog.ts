import { City } from "../../models/city/city";
import { CityPopulation, POPS_UPDATE_GROWTH } from "../../models/city/city-population";
import { POP_CLASSES, PopulationClass } from '../../models/city/population-class';
import * as GUI_CONST from '../common/constants';
import { EmptyDialog } from "./common/base-dialog";
import { CityConsumptionDialog } from "./city-consumption-dialog";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == CITY POP DIALOG == */
/* opened on city population row click; shows the class composition and resource requirements of this city */
export class CityPopulationDialog extends EmptyDialog {
    city: City;
    population: CityPopulation = null;

    constructor(city: City) {
        super(GameDialogKeys.DIAG_POPS, false, `${city.name} Population`);
        this.city = city;
        this.population = city.population;
    }

    registerToObservables() {
        super.registerToObservables();
        this.onChildClicked$.add(
            elemId => {
                if(elemId.startsWith(GUI_CONST.ELEM_PREFIX_DATAROW)) {
                    const datarowKey = elemId.replace(GUI_CONST.ELEM_PREFIX_DATAROW, "");
                    if (datarowKey.startsWith("Delivered")) {
                        globalThis.GUI.showDialog( new CityConsumptionDialog(this.city) );
                        return true;
                    }
                }
                return false;
       });
    }

    /* ADD CHILD ELEMENTS */
    addCustomContent(): void {
        // class pops
        this.addPopClassesList();
        // consumption
        const resDeliveredRowId = ID_ROW_DELIVERED;
        const consumptionPercent = this.population.lastConsumptionSuccess * 100;
        const consumptionSuccessStr = `${consumptionPercent.toFixed(1)}%`
        this.addNewDataRow("Delivered", consumptionSuccessStr, "happiness", true, resDeliveredRowId);
        // total growth
        const growthStr = this.city.growth.toFixed(1) + "/" + POPS_UPDATE_GROWTH;
        this.addNewDataRow("Growth", growthStr, "growth");
        // last turn taxes
        this.addNewDataRow("Taxes", "" + this.city.taxesLastTurn, "money");
        // show "Talk To Mayor" button
        this.addNewButton(ID_DIAG_MAYOR, "Talk To Mayor");
    }

    addPopClassesList() {
        for (const cityClass of POP_CLASSES) {
            this.addSinglePopClassRow(cityClass);
        }
        this.addNewDivider(); // add divider after pop class list
    }

    private addSinglePopClassRow(cityClass: PopulationClass) {
        const classIcon = cityClass.iconName;
        const classPopsAmount = this.population.getAmount(cityClass.level);
        this.addNewDataRow(cityClass.name, `${classPopsAmount}`, classIcon);
    }
}

/* == CONSTANTS == */
export const ID_ROW_DELIVERED = GameDialogKeys.DIAG_POPS + "-datarow-Delivered";
export const ID_DIAG_MAYOR = GameDialogKeys.DIAG_POPS + "-btn-talktomayor";
