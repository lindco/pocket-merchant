import { Player } from "../../models/player/player";
import { StoredResource } from "../../models/trade/resource/stored-resource";
import { TradeResource } from "../../models/trade/resource/resource";
import { GuiTools } from "../common/gui-tools";
import * as GUI_CONST from '../common/constants';
import { EmptyDialog } from "./common/base-dialog";
import { GuiRectangle } from "../common/rectangle";
import { SingleDataRow } from "../elements/data-row";
import * as GUI from 'babylonjs-gui';
import { ITradeNode } from "../../models/trade/interfaces/trade-node.interface";
import { ITradeLocation } from "../../world/interfaces/location.interfaces";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == CITY TRADE TRANSFER == */
export class CityTransferDialog extends EmptyDialog {
    location: ITradeLocation;
    tradeNode: ITradeNode;
    resId: string;
    // child elements
    cityStorageRow: SingleDataRow;
    playerStorageRow: SingleDataRow;
    resImgContainer: GuiRectangle;
    resImgBackground: GUI.Image;
    resImg: GUI.Image;

    constructor(location: ITradeLocation, resId: string) {
        super(GameDialogKeys.DIAG_TRANSFER, true, "Transfer");
        this.resId = resId;
        // set location & trade node property
        this.location = location;
        this.tradeNode = location.tradeNode;
    }

    registerToObservables() {
        super.registerToObservables();
        this.onDialogBtnClicked$.add(
            (btnId) => {
                if (btnId == DIAG_TRANSFER_BTN_BUY) { this.buyOnce() }
                else if (btnId == DIAG_TRANSFER_BTN_SELL) { this.sellOnce(); }
                this.updateCustomContent();
            }
        )
    }

    build(): void {
        super.build();
    }

    get player(): Player {
        return globalThis.PLAYER;
    }

    addCustomContent() {
        const resource = CityTransferDialog.GetResById(this.resId);
        // res image
        this.addResourceImage();
        // res name
        this.addNewDataRow("name", resource.name, "name");
        // storage
        let inCity: number = this.location.tradeNode.amountOf(this.resId);
        const cityStorageText = inCity.toFixed(1);
        this.cityStorageRow = this.addNewDataRow("City", cityStorageText, "storage")
        this.playerStorageRow = this.addNewDataRow("Player", "" + this.player.amountInStorage(this.resId), "trader");
        // buttons
        this.addBuySellButtons();
    }

    addResourceImage() {
        // make container
        const resImgContainerId = DIAG_TRANSFER_RESIMG_ID + "-container";
        this.resImgContainer = new GuiRectangle(resImgContainerId);
        this.resImgContainer.setSize(GUI_CONST.DEFAULT_INNER_WIDTH, DIAG_TRANSFER_RESIMG_SIZE)
        // make container bg
        const resImgBgId = DIAG_TRANSFER_RESIMG_ID + "-bg";
        this.resImgBackground = new GUI.Image(resImgBgId, GUI_CONST.BG_PAPER);
        GuiTools.setSizeFrom(this.resImgBackground, this.resImgContainer);
        // make image
        const resImgUrl = CityTransferDialog.GetResImgPath(this.resId);
        this.resImg  = new GUI.Image(DIAG_TRANSFER_RESIMG_ID, resImgUrl);
        this.resImg.width = DIAG_TRANSFER_RESIMG_SIZE;
        this.resImg.height = DIAG_TRANSFER_RESIMG_SIZE;
        // add to dialog
        this.resImgContainer.addControl ( this.resImgBackground );
        this.resImgContainer.addControl( this.resImg );
        this.addControl( this.resImgContainer, true );
    }
    
    addBuySellButtons() {
        // sett button
        let storedByPlayer: StoredResource = this.player.getStoredRes(this.resId);
        const isInPlayerStorage = storedByPlayer && storedByPlayer.amount >= 1;
        let cityPrice: number = this.tradeNode.priceOf(this.resId);
        if ( isInPlayerStorage ) { 
            const boughtPrice = storedByPlayer.price
            const playerSellProfit = cityPrice - boughtPrice;
            this.addNewButton(DIAG_TRANSFER_BTN_SELL, `-1 (Prf.: ${ playerSellProfit }o)`);
        }
        // buy button
        const isInCityStorage = this.tradeNode.hasEnough(this.resId, 1);
        if (isInCityStorage) {
            this.addNewButton(DIAG_TRANSFER_BTN_BUY, `+1 (Price: ${ cityPrice.toFixed(1) }o)`);
        }
    }

    updateCustomContent() {
        // city storage
        const cityStorageAmount = this.location.tradeNode.amountOf(this.resId).toFixed(1);
        this.cityStorageRow.valueLabel.text = ""+ cityStorageAmount;
        // player storage
        const playerStorageAmount = globalThis.PLAYER.storage.amountOf(this.resId).toFixed(1);
        this.playerStorageRow.valueLabel.text = ""+ playerStorageAmount;
    }

    static GetResById(resId: string): TradeResource {
        let resource: TradeResource = globalThis.RESOURCE_TYPES.get(resId);
        if (! resource ) {
            console.error(`<Dialog> no resObj for ${resId}`); return;
        }
        if (globalThis.TRADE && globalThis.TRADE.hasSpecialResLoaded) {
            resource = globalThis.TRADE.getRes(resId);
        }
        return resource;
    }

    static GetResImgPath(resId: string): string {
        return `res/img/icons/res/big/shadowed_${resId}.png`;
    }

    private buyOnce() {
        const storedRes = this.location.tradeNode.getStoredResource(this.resId);
        const success = this.location.tradeNode.remove(this.resId, 1);
        if ( storedRes && success ) {
            this._buyResource( this.resId, storedRes.price );
        }
    }

    private _buyResource( resId: string, price: number) {
        // exchange coins
        const coinChangeCount = -price;
        globalThis.PLAYER.onCoinsChange$.notifyObservers(coinChangeCount);
        globalThis.PLAYER.coins -= price;
        // add to storage
        globalThis.PLAYER.storage.add(this.resId, 1, price);
        const storedRes = new StoredResource( resId, 1, price );
        globalThis.PLAYER.army.resourceHandler.storedResources.push( storedRes );
        // log & inform trade manager
        this.log(`bought 1 ${resId} from ${this.location.name} for ${price}c`);
        globalThis.TRADE.onPlayerCoinsChange();
    }

    private sellOnce() {
        const enoughInStorage = globalThis.PLAYER.storage.hasEnough(this.resId, 1);
        if (! enoughInStorage) {
            this.warn(`cant sell more "${this.resId}": not enough in player storage`); return;
        }
        const storedRes = this.player.getStoredRes(this.resId);
        let canRemove: boolean = false;
        if (storedRes) {
            canRemove = globalThis.PLAYER.storage.remove(this.resId, 1);
        }
        if (canRemove) {
            this._sellResource( this.resId, storedRes.price );
        }
    }

    private _sellResource( resId: string, playerPrice: number ) {
        const resAmount = 1;
        // remove from player storage & add to city market
        const player = globalThis.PLAYER;
        player.army.resourceHandler.removeSingleStoredResource(resId);
        this.location.tradeNode.add( this.resId, resAmount);
        // exchange coins
        const localPrice = this.location.tradeNode.priceOf( resId ) * resAmount;
        player.coins += localPrice;
        player.onCoinsChange$.notifyObservers(localPrice);
        this.location.money -= localPrice;
        // log & inform trade manager
        this.log(`sold ${resAmount} ${resId} bought for ${playerPrice}c for${localPrice}c`);
        globalThis.TRADE.onPlayerCoinsChange();
    }
}

/* == CONSTANTS == */
export const DIAG_TRANSFER_BTN_BUY = "btn-transfer-buy";
export const DIAG_TRANSFER_BTN_SELL = "btn-transfer-sell";
export const DIAG_TRANSFER_RESIMG_ID = GameDialogKeys.DIAG_TRANSFER
    + "-img-res";
export const DIAG_TRANSFER_RESIMG_SIZE = "200px";