import { EmptyDialog } from "./common/base-dialog";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class RecruitmentDialog extends EmptyDialog {
  army: IUnitArmy;

  constructor(army: IUnitArmy) {
    super(GameDialogKeys.DIAG_RECRUIT, true, "Recruit");
    this.army = army;
  }
}