import { EmptyDialog } from "./common/base-dialog";
import { IFamily } from "../../models/faction/faction-interfaces";

export class FamilyDialog extends EmptyDialog {
    family: IFamily;

    constructor(family: IFamily) {
        const title = `${family.name} Family`
        super(DIAG_FAMILY, false, title);
        this.family = family;
    }

    registerToObservables() {
        super.registerToObservables();
    }

    addCustomContent() {
        super.addCustomContent();
        // Name
        const name = this.family.name;
        this.addNewDataRow("Name", name, "name");
        // Cities
        const cities = this.family.ownedCities.length + "";
        this.addNewDataRow("Cities", cities, "city");
    }

}

const DIAG_FAMILY = 'diag-family';