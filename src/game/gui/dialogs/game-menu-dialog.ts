import { EmptyDialog } from "./common/base-dialog";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class GameMenuDialog extends EmptyDialog {

    constructor() {
        super(GameDialogKeys.DIAG_GAME_MENU, false, "Game Menu");
    }

    addCustomContent(): void {
        
    }
}