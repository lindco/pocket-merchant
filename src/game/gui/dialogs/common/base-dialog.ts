import { FactionFlag } from '../../../models/faction/faction-flag';
import * as GUI_CONST from '../../common/constants';
import { FlexGuiLayout } from '../../common/layouts';
import { DialogButton } from './dialog-button';
import { GuiRectangle } from '../../common/rectangle';
import { FactionFlagImage } from "../../elements/babylon/flags";
import { GuiScrollView } from "../../elements/scroller";
import { IDialog } from "./dialog.interfaces";
import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';
import { SwitcherChangeEvent } from "../../elements/switcher";
import { IFaction } from "../../../models/faction/faction-interfaces";

/**
 * <p>parent of every function Dialog in the game GUI system</p>
 * <p>Do not forget to implement the <i>addCustomContent()</i> for bootstrapping GUI creation!</p>
 */
export class EmptyDialog extends FlexGuiLayout implements IDialog {
    diagKey: string;
    titleText: string = "Unnamed Dialog";
    // flags
    hasCustomBackground: boolean = false;
    isBlocking: boolean;
    showFactionFlag: IFaction = null;
    // child elements
    inner: GuiRectangle;
    buttonIds: Array<string> = ["cancel", "ok"];
    noButtonDivider: boolean = false;
    // observables
    readonly onDialogBtnClicked$ = new BABYLON.Observable<string>();
    readonly onChildClicked$ = new BABYLON.Observable<string>();
    readonly onSwitcherChanged$ = new BABYLON.Observable<SwitcherChangeEvent>();

    constructor(id: string, isBlocking: boolean, headerText?: string, extraButtons?: Array<string>, 
        customBackground?: boolean, noButtonDivider?: boolean, showFactionFlag?: IFaction) {
        super(id, GUI_CONST.DIR_VERT, headerText);
        this.diagKey = id;
        this.isBlocking = isBlocking;
        // optionals
        if (customBackground) { this.hasCustomBackground = true; }
        if (noButtonDivider) { this.noButtonDivider = true; }
        if (showFactionFlag) { this.showFactionFlag = showFactionFlag; }
        // add buttons
        this.addButtons(extraButtons);
        this.registerToObservables();
    }

    registerToObservables(): void {
        this.onSwitcherChanged$.add(
            (event) => {
                this.log(`switcher "${event.switcherElemId}" changed to ${event.optionText}`);
            }
        )
        this.onDialogBtnClicked$.add(
            (btnId) => {
                if (btnId == ID_BUTTON_CLOSE) { this.close(); }
        });
    }

    switchToDialog(dialog: IDialog) {
        this.close();
        globalThis.GUI.showDialog(dialog, true);
    }

    build(): void {
        // build FlexGuiLayout & set style & sizing
        super.build( false );
        this.container.height = "70%";
        GuiScrollView.setDefaultStyle(this.container);
        // build optional children: background image & faction flag
        if (this.hasCustomBackground) { this.buildBackground(); }
        if (this.showFactionFlag) { this.addFactionFlag(); }
        // build button bar
        this.buildButtonBar();
    }

    private addFactionFlag() {
        const factionFlag: FactionFlag = this.showFactionFlag.flag;
        if (factionFlag) {
            const flag = new FactionFlagImage(factionFlag);
            flag.left = "-20px";
            this.container.addControl(flag);
        }
    }

    private addButtons(newButtonIds: Array<string>): void {
        this.buttonIds = this.buttonIds.concat(newButtonIds);
    }

    addNewDivider(empty?: boolean, setParent?: GUI.Container): GUI.TextBlock {
        const divider = super.addNewDivider(empty, setParent);
        divider.widthInPixels = 400;
        return divider;
    }

    private buildBackground() {
        this.inner = new GuiRectangle(`${this.id}-inner`);
        this.inner.setSize(GUI_CONST.DIALOG_OUTTER_WIDTH, GUI_CONST.DIALOG_HEIGHT)
        this.addControl(this.inner);
    }

    buildButtonBar() {
        if (! this.noButtonDivider) { this.addNewDivider(true); }
        // add close button
        this.addNewButton(ID_BUTTON_CLOSE, "Close");
    }

    close() {
        this.log(`closing ..`);
        this.container.dispose();
        this.babylon.dispose();
        globalThis.DIALOG = null;
    }

    addNewButton(btnId: string, text: string): GUI.Button { 
        return DialogButton.CreateNew(btnId, text, this); 
    }

    /* TO STRING */
    toString(): string { 
        const directChildCount = this.children.length;
        return `<Dialog id="${this.id}" title="${this.title}" children=${directChildCount}>`;
    }
}
export const ID_BUTTON_CLOSE = "diag-close";
export const BUTTON_TEXT_COLOR = "ATLANTIS";