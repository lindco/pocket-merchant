import { IGuiContainer } from "../../common/layouts";
import * as BABYLON from 'babylonjs';
import { IFaction } from "../../../models/faction/faction-interfaces";

export interface IDialog extends IGuiContainer {
    diagKey: string;
    onSwitcherChanged$: any;
    // basic properties
    isBlocking: boolean;
    // dialog flags
    hasCustomBackground: boolean;
    buttonIds: Array<string>;
    showFactionFlag: IFaction;
    // dialog methods
    close(): void;
    switchToDialog(dialog: IDialog): void;
    // observables
    onDialogBtnClicked$: BABYLON.Observable<string>;


}