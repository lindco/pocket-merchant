// common
import { ColorPalette } from "../../../../helpers/colorpalette";
// gui
import * as GUI_CONST from '../../common/constants';
// babylon
import * as GUI from 'babylonjs-gui';
import { StrProperty } from "../../../../common/models/properties/properties";
import { IDialog } from "./dialog.interfaces";

/* == DIALOG BUTTON CLASS == */
export class DialogButton extends GUI.Button {
    dialog: IDialog;
    btnId: string;
    text: string;

    static CreateNew(btnId: string, text: string, parentDialog: IDialog): GUI.Button {
        // add button
        const btn = GUI.Button.CreateImageWithCenterTextButton(btnId, text, GUI_CONST.BG_BUTTON1);
        // set default style
        DialogButton.AdjustContainer(btn);
        DialogButton.SetFontStyle(btn);
        // add on click listener
        parentDialog.addControl(btn);

        btn.onPointerDownObservable.add(
            () => {
                let btnId: string = "" + btn.uniqueId;
                if (btn.name) { btnId = btn.name; }
                parentDialog.onDialogBtnClicked$.notifyObservers(btnId);
            }
        );
        return btn;
    }

    static CreateFromProperty(strProp: StrProperty, parentDialog: IDialog): GUI.Button {
        const btnId = strProp.id;
        const btnText = strProp.value;
        return DialogButton.CreateNew(btnId, btnText, parentDialog);
    }

    static AdjustContainer(btn: GUI.Button) {
        btn.thickness = 0;
        btn.width = GUI_CONST.DEFAULT_INNER_WIDTH;
    }

    static SetFontStyle(btn: GUI.Button) {
        btn.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
        btn.fontSize = GUI_CONST.TEXTSIZE_BIGGER;
        btn.color = ColorPalette.NameAsHexStr(BUTTON_TEXT_COLOR);
    }

    
}

/* == DIALOG BUTTON CONSTANTS == */
export const BUTTON_TEXT_COLOR = "ATLANTIS";