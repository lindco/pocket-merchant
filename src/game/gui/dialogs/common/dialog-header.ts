import { GuiRectangle } from '../../common/rectangle';
import { GuiLabel } from '../../elements/babylon/labels';
import * as GUI from 'babylonjs-gui';
import { IDialog } from "./dialog.interfaces";

export interface IDialogHeader {
    dialog: IDialog;
    titleStr: string;
    // child elements
    headerBg: GUI.Image;
    headerLabel: GuiLabel;
}

export class DialogHeader extends GuiRectangle implements IDialogHeader {
    dialog: IDialog;
    titleStr: string;
    // child elements
    headerBg: GUI.Image;
    headerLabel: GuiLabel;
    
}