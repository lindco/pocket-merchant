import { IDialog } from "./dialog.interfaces";
import { GameDialogKeys } from "../../../../common/constants/game-dialog-keys";
import { FamiliesDialog } from "../families-dialog";

export class DialogFactory {


    public static createDialogFromKey(dialogKey: string): IDialog {
        if (dialogKey === GameDialogKeys.DIAG_FAMILIES) {
            return new FamiliesDialog();
        }
    }
}