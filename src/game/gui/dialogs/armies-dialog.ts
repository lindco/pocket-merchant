import { MapPos } from "../../world/pos";
import { EmptyDialog } from "./common/base-dialog";
import { SingleArmyDialog } from "./army-dialog";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class ArmiesDialog extends EmptyDialog {
    pos: MapPos;
    armiesAtPos: Array<IUnitArmy>;

    constructor(pos: MapPos) {
        super(GameDialogKeys.DIAG_ARMIES, true, "Armies");
        this.pos = pos;
        this.armiesAtPos = globalThis.ARMIES.getArmiesAtPos(pos);
    }

    registerToObservables() {
        super.registerToObservables();
        this.onChildClicked$.add(
            (elemId) => {
                const armyRowKey = elemId.replace("datarow-", "");
                const armyNum = this.getArmyNumByKeyStr(armyRowKey);
                if (armyNum) { this.showArmyDialog(armyNum); }
        });
    }

    public addCustomContent(): void {
        for (const army of this.armiesAtPos) {
            const keyStr = this.getArmyNameStr(army);
            const valueStr = `${army.units.length} Units`;
            const armyIcon = army.armyTypeIcon;
            this.addNewDataRow(keyStr, valueStr, armyIcon, true);
        }
    }

    private getArmyNameStr(army: IUnitArmy): string {
        return `${army.armyTypeName} (${army.num})`;
    }

    private getArmyNumByKeyStr(datarowKeyStr: string): number {
        const armyNumFullStr = datarowKeyStr.split(" ")[1];
        const armyNumStr = armyNumFullStr.replace("(", "")
            .replace(")", "");
        return parseInt(armyNumStr);
    }

    /* GUI INTERACTION */
    showArmyDialog(armyNum: number): boolean {
        // show single army dialog
        const singleArmyDiag = new SingleArmyDialog( globalThis.ARMIES.getArmyByNum(armyNum) );
        this.close();
        globalThis.GUI.showDialog(singleArmyDiag);
        return true;
    }
}