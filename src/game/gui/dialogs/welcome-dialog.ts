import { EmptyDialog } from "./common/base-dialog";
import { IDialog } from "./common/dialog.interfaces";

export class WelcomeDialog extends EmptyDialog implements IDialog {
    titleText: string = "Welcome";

    constructor() {
        super("welcome", true);
    }

    addCustomContent() {
        super.addCustomContent();
        this.addNewLabel("lbl-welcome", WELCOME_TEXT);
        this.addNewLabel("lbl-maptext", this.mapText);
    }

    private get mapText() {
        const mapName: string = globalThis.MAP.name
        return `Du spielst auf der Karte: ${mapName} \n`
        + `Deine Fraktion ist die Familie ${globalThis.PLAYER.family.name}`;
    }

}

/* == CONSTANTS == */
const WELCOME_TEXT = "Willkommen in Pocket Merchant!";