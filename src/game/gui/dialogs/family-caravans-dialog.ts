import { EmptyDialog } from "./common/base-dialog";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { IFamily } from "../../models/faction/faction-interfaces";

/* == DIALOG MODEL == */
export class FamilyCaravansDialog extends EmptyDialog {
    familyNum: number = 0;

    constructor(noPlayerFamilyNum?: number) {
        super(DIAG_FAMILY_CARAVANS, false, "Caravans");
        if (noPlayerFamilyNum) { this.familyNum = noPlayerFamilyNum; }
    }

    addCustomContent(): void {
        this.addCaravanArmyList();
    }

    addCaravanArmyList() {
        for (const familyCaravan of this.familyCaravanArmies) {
            const elemId: string = "datarow-Army" + familyCaravan.num;
            this.addNewDataRow("Army", familyCaravan.name, "army", false, elemId);
        }
    }

    get isForPlayer(): boolean {
        return this.familyNum == 0;
    }

    get family(): IFamily {
        return globalThis.FACTIONS.getFamilyByNum(this.familyNum);
    }

    get familyCaravanArmies(): Array<IUnitArmy> {
        const factionKey = "F" + this.familyNum;
        const factionArmies = globalThis.ARMIES.getArmiesByFactionKey(factionKey);
        const factionCaravans: Array<IUnitArmy> = [];
        for (const factionArmy of factionArmies) {
            if (factionArmy.isCaravan) { factionCaravans.push(factionArmy); }
        }
        return factionCaravans;
    }
}

/* == DIALOG CONSTANTS == */
export const DIAG_FAMILY_CARAVANS = "diag-caravans";