import { City} from "../../models/city/city";
import { TradeConnection } from "../../models/trade/connection/connection";
import { EmptyDialog } from "./common/base-dialog";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class CityConnectionsDialog extends EmptyDialog {
    city: City;
    connections: Array<TradeConnection> = [];

    constructor(city: City) {
        super(GameDialogKeys.DIAG_CONNECTIONS, false, `Connections: ${city.name}`);
        this.city = city;
        this.connections = city.connections;
    }

    addCustomContent(): void {
        for (const connection of this.connections) {
            const cityName = connection.target.name;
            const cityWorldDist = connection.worldDistance;
            const keyStr = cityName;
            const valueStr = `${cityWorldDist} (${connection.doneTrades} Trades for ${connection.tradedCoins}c)`;
            this.addNewDataRow(keyStr, valueStr, "connection");
        }
    }

    public build(): void {
        super.build();
    }
}