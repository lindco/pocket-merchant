import { City } from "../../models/city/city";
import { StoredResource } from "../../models/trade/resource/stored-resource";
import { StoredResourceTile } from "../elements/entities/stored-resource-tile";
import { EmptyDialog } from "./common/base-dialog";
import { ResourceListDialog } from "./reslist-dialog";
import { CityConnectionsDialog } from "./connections-dialog";
import { CityPopulationDialog } from "./city-population-dialog";
import { CityBuildingsDialog } from "./city-buildings-dialog";
import { ArmiesDialog } from "./armies-dialog";
import { RecruitmentDialog } from "./recruitment-dialog";

import * as GUI from 'babylonjs-gui';
import * as GUI_CONST from '../common/constants';
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

/* == CITY DIALOG == */
/* Main dialog that is opened on city click (or possibly entering);
   Shows name, owner, money plus clickable datarows for more information to population etc */
export class CityDialog extends EmptyDialog {
    city: City;
    onlyView: boolean;

    constructor(city: City) {
        super(GameDialogKeys.DIAG_CITY, false, city.name, null, false, false, city.ownerEmpire);
        this.city = city;
        this.onlyView = ! this.playerIsInCity;
        this.registerToObservables();
    }

    registerToObservables() {
        super.registerToObservables();
        this.onDialogBtnClicked$.add(
            (btnId) => {
                if (btnId == ID_BTN_TRADE) { this.switchToDialog( new ResourceListDialog(this.city.tradeNode) ); }
                else if ( btnId == ID_BTN_RECRUIT ) { // city recruit btn
                    globalThis.GUI.showDialog( new RecruitmentDialog(globalThis.PLAYER.army) );
                }
                else if ( btnId == ID_BTN_COMMAND ) { this.log("COMMAND"); }  // city command btn
        });
        this.onChildClicked$.add(
            (elemId) => {
                const city = this.city;
                if (elemId == 'datarow-Buildings') { this.switchToDialog( new CityBuildingsDialog(city) ); } // buildings
                else if (elemId == 'datarow-Connections') { this.switchToDialog( new CityConnectionsDialog(city) ); } // connections
                else if (elemId == 'datarow-Militia') { this.switchToDialog( new ArmiesDialog(city.pos) ); } // militia
                else if (elemId == 'datarow-Population') { this.switchToDialog( new CityPopulationDialog(city) ); } // pops
                else { this.log(`element ${elemId} clicked but no action is connected`); } // else
            }
        )
    }

    get playerIsInCity(): boolean {
        const playerPos = globalThis.PLAYER.pos
        return playerPos && this.city.pos.equals(playerPos);
    }

    get cityIsPlayerOwned(): boolean {
        const playerFamilyKey = globalThis.PLAYER.family.key;
        return this.city.ownerFamily && this.city.ownerFamily.key == playerFamilyKey;
    }

    build(): void {
        super.build();
    }

    addCustomContent() {
        // connections
        const connectionsText = `${this.city.connections.length}`;
        this.addNewDataRow("Connections", connectionsText, "connection", true, ID_ROW_CONNS);
        // properties
        this.addCityPropertiesBar();
        // owner
        this.addNewDataRow("Owner", this.city.ownerName, "empire", false);
        // leader
        let leaderName: string = "Unknown";
        if ( this.city.leader && this.city.leader.fullName ) { leaderName = this.city.leader.fullName; }
        this.addNewDataRow("Leader", leaderName, "trader", false);
        // money
        const moneyText = "" + Math.round(this.city.money);
        this.addNewDataRow("Money", moneyText, "money", false);
        // pops
        const growthIncreaseStr = ` (Grw.: +${this.lastGrowthIncrease.toFixed(1)})`;
        const popsText = Math.round(this.city.population.totalPops) + growthIncreaseStr;
        this.addNewDataRow("Population", popsText, "growth", true, ID_ROW_POPS);
        // buildings
        const buildingsText = "" + this.city.buildings.length;
        this.addNewDataRow("Buildings", buildingsText, "building", true, ID_ROW_BLDS);
        // milita
        const militiaText = this.getMiltiaUnitTypes();
        this.addNewDataRow("Militia", militiaText, "might", true, ID_ROW_MILITIA);
        this.addNewDivider( true );
        // add optional buttons
        this.addButtonsForPlayerInCity();
        this.addButtonsWhenPlayerOwned();
    }

    private addButtonsWhenPlayerOwned() {
        if (this.cityIsPlayerOwned) {
            // plus city command options if city is player owned
            this.addNewButton(ID_BTN_COMMAND, "Command");
        }
    }

    private addButtonsForPlayerInCity() {
        if (this.playerIsInCity) {
            // res info
            this.addResourceInformation();
            // add trade & recruitment button
            this.addNewButton(ID_BTN_TRADE, "Trade");
            this.addNewButton(ID_BTN_RECRUIT, "Recruit");
        }
    }

    private addCityPropertiesBar() {

    }

    get resHappinessStr(): string {
        const resHappinessPercent = Math.round(this.city.population.lastConsumptionSuccess * 100);
        return `${resHappinessPercent}%`;
    }

    get lastGrowthIncrease(): number { return this.city.population.totalGrowthIncrease; }

    private getMiltiaUnitTypes(): string {
        if (! this.city.militia) {
            return "empty";
        }
        let unitNames: Array<string> = [];
        for (const unit of this.city.militia.units) {
            unitNames.push(unit.type.id);
        }
        return unitNames.join(", ");
    }

    private addResourceInformation() {
        // "Resources:" label
        let resInfoTitle: GUI.TextBlock = this.addNewLabel(`${DIAG_CITY_RESINFO}-title`, "  Resources:", false, true);
        resInfoTitle.textHorizontalAlignment = GUI.TextBlock.HORIZONTAL_ALIGNMENT_LEFT;
        resInfoTitle.fontFamily = GUI_CONST.FONT_HIGHLIGHTED;
        // resource info containter
        const resInfoOutter = this.addResourceInfoContainer();
        // most resources
        const resInfoRow = new GUI.StackPanel(DIAG_CITY_RESINFO);
        resInfoRow.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        resInfoRow.isVertical = false;
        resInfoRow.horizontalAlignment
        resInfoRow.width = GUI_CONST.DEFAULT_INNER_WIDTH;
        // add row to panel
        resInfoOutter.addControl(resInfoRow);
        // well-stocked resources
        for (const storedRes of this.city.tradeNode.getMostStored()) {
            resInfoRow.addControl( this.makeResourceStorageElement(storedRes, true) );
        }
        this.addControl( resInfoOutter, false );
        this.addNewDivider(true);
    }

    private addResourceInfoContainer() {
        const resInfoOutter = new GUI.Rectangle(`${DIAG_CITY_RESINFO}-outter`);
        resInfoOutter.width = GUI_CONST.DEFAULT_INNER_WIDTH;
        resInfoOutter.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        resInfoOutter.thickness = 0;
        const resInfoBg = new GUI.Image(`${DIAG_CITY_RESINFO}-bg`, GUI_CONST.BG_PAPER);
        resInfoBg.width = GUI_CONST.DEFAULT_INNER_WIDTH;
        resInfoBg.height = GUI_CONST.DEFAULT_LABEL_HEIGHT;
        resInfoOutter.addControl(resInfoBg);
        return resInfoOutter;
    }

    private makeResourceStorageElement(storedRes: StoredResource, colorByAmount: boolean): StoredResourceTile {
        const elemId = `diag-res-max-${storedRes.resId}`;
        // create outter ractangle
        return new StoredResourceTile(storedRes, colorByAmount);
    }
}


/* == CONSTANTS == */
// city dialog

export const DIAG_CITY_RESINFO = "diag-city-resinfo";
export const ID_BTN_TRADE = "diag-city-btn-trade";
export const ID_BTN_RECRUIT = "diag-city-btn-recruit";
export const ID_BTN_COMMAND = "diag-city-btn-command";
export const DIAG_CITY_LBL_LEADER = "diag-city-leader";
export const ID_ROW_CONNS = "diag-city-connections";
export const ID_ROW_MILITIA = "diag-city-militia";
export const ID_ROW_BLDS = "diag-city-buildings";
export const ID_ROW_POPS = "diag-city-growth";
