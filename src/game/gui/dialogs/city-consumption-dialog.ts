import { City } from "../../models/city/city";
import { EmptyDialog } from "./common/base-dialog";
import { ColorPalette } from "../../../helpers/colorpalette";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";

export class CityConsumptionDialog extends EmptyDialog {
    city: City;
    consumedResIds: Array<string>;

    constructor(city: City) {
        super(GameDialogKeys.DIAG_CONSUMPTION, false, `${city.name} Consumption`);
        // set city & classes list
        this.city = city;
        this.consumedResIds = city.population.getTotalResConsumptionList();
    }

    addCustomContent(): void {
        this.log(`consumed resources: ${this.consumedResIds}`)
        for (const resId of this.consumedResIds) {
            const consumedResAmount = this.city.population
                .getTotalResConsumption(resId);
            if (consumedResAmount > 0) {
                const resName = globalThis.TRADE.getResName(resId);
                const amountFloat = consumedResAmount.toFixed(2);
                const consumedResRow = this.addNewDataRow(resName,
                    amountFloat, "storage");
                // text color
                if (! this.city.population.resIsFullySupplied(resId)) {
                    consumedResRow.valueLabel.color = ColorPalette
                        .NameAsHexStr(TEXT_COLOR_RED_NAME);
                }
            } // end of consumption amount heck
        } // end of consumed resId loop
    }
}

/* == CONSUMPTION DIALOG CONSTANTS == */
export const TEXT_COLOR_RED_NAME = "BROWN";
