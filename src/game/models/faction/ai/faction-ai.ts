import { BaseEventListener, IBaseEvent } from "../../../../common/models/events";
import { IFaction } from "../faction-interfaces";

export interface IFactionAi {
    faction: IFaction;
}

export class FactionAi extends BaseEventListener implements IFactionAi {
    faction: IFaction;

    constructor(forFaction: IFaction) {
        super("FactionAi");
        this.faction = forFaction;
    }
    
}