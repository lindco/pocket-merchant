// common
import { Loggable } from "../../../common/models/log";

export class FactionLanguage extends Loggable {
    id: string;

    constructor(languageId: string) {
        super("Language");
        this.id = languageId;
    }
}