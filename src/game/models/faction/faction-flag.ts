import { Loggable } from "../../../common/models/log";
import { ColorPalette } from "../../../helpers/colorpalette";
import { RandomElement } from "../../../helpers/random";
import { IFaction, IFactionFlag } from "./faction-interfaces";

export class FactionFlag extends Loggable implements IFactionFlag {
    autoGenerate: boolean = true;
    // example: "black tower on christi"
    strData: string;
    // colors && icon
    iconColorName: string;
    bgColorName: string;
    iconName: string;
    // parent
    faction: IFaction = null;
    // flags
    hasFlagStrOverride: boolean = false;

    constructor(parentFaction: IFaction, overwriteFlagStr?: string) {
        super("FactionFlag");
        if (parentFaction) { 
            this.faction = parentFaction;
            this.strData = parentFaction.flagData;
        }
        // set str data
        if (overwriteFlagStr) { 
            this.hasFlagStrOverride = true;
            this.strData = overwriteFlagStr;
        }
        this.loadOrGenerate();
    }

    loadOrGenerate() {
        if (! this.strData) this.generate();
        else this.load();
    }

    private generate() {
        this.iconColorName = ColorPalette.RandomColorName();
        this.bgColorName = ColorPalette.RandomColorName();
        this.iconName = RandomElement.of(FLAG_ICONS);
    }

    private load() {
        const splitted = this.strData.split(" ");
        if (splitted.length < 4) {
            this.error(`cant load flag from str: ${this.strData}`);
            return;
        }
        this.iconColorName = splitted[0].toUpperCase();
        this.iconName = splitted[1].toUpperCase();
        this.bgColorName = splitted[3].toUpperCase();
    }

    /* FACTION FLAG GETTERS */
    get factionKey(): string {
        if (this.faction) { return this.faction.key; }
        return "U";
    }
}

export const FLAG_ICONS = [ "axes", "bird", "circle", "crescent", "cross", "cup", "flower", "horse", 
    "lion", "none", "ship", "swords", "tower", "trident" ];