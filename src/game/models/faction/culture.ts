// common
import { Loggable } from "../../../common/models/log";

/* == INTERFACES == */
export interface ICulture {
    id: string;
    name: string;
}

/* == CULTURE MODEL == */
export class Culture implements ICulture {
    id: string;
    name: string;

    constructor(id: string, name: string) {
        this.id = id;
        this.name = name;
    }

    get cityAssetName(): string {
        return null;
    }
}

/* == CULTURES LIST == */
export class Cultures extends Loggable {
    _ids: Array<string> = [];
    cultures: Array<Culture> = [];

    constructor() {
        super("Cultures");
        this.addAll();
    }

    addAll() {

    }

    add(cultureId: string, name: string) {
        const newCulture = new Culture(cultureId, name);
        this._add(newCulture);
    }

    get(cultureId: string): Culture {
        const index = this._ids.indexOf(cultureId);
        return this.cultures[index];
    }

    private _add(cultureObj: Culture) {
        this._ids.push(cultureObj.id);
        this.cultures.push(cultureObj);
        this.log(`added ${cultureObj.id}`);
    }

    get length(): number {
        return this.cultures.length;
    }
}