// common
import { RandomWorldPos } from "../../../helpers/random";
import { WorldEventEntity } from "../../../common/models/entity/entity";
// world
import { MapPos } from "../../world/pos";
// models
import { City } from "../city/city";
import { UnitArmy } from "../military/army/army";
import { ArmyCategory } from "../military/army/army-category";
import { FactionAi } from "./ai/faction-ai";
import { FactionDiplomacy } from "./diplomacy/diplomacy";
import { FactionFlag } from "./faction-flag";
import { FactionType, FACTION_TYPES, TYPE_REBEL } from "./types/faction-type";
import { IFaction } from "./faction-interfaces";

/* == FACTION MODEL == */
/* Base Faction which families and empires inherit from; owns towns and armies & can handle
   diplomatic deals with other factions */
export class AbstractFaction extends WorldEventEntity implements IFaction {
    key: string;
    num: number;
    factionType: FactionType = FACTION_TYPES[TYPE_REBEL];
    name: string;
    prestige: number = 0;
    // cities
    home: City;
    // armies
    factionArmies: Array<UnitArmy> = [];
    // ai & diplomacy
    ai: FactionAi = null;
    diplomat: FactionDiplomacy;
    // flag
    flagData: string;
    flag: FactionFlag;

    constructor(key: string, name: string, home?: City, flagStr?: string) {
        super(-1, null);
        this.key = key;
        // assign faction num
        if (! globalThis.FACTIONS) { this.num = -1; }
        else { this.num = globalThis.FACTIONS.nextFactionNum(); }
        // set base properties
        this.name = name;
        this.flag = new FactionFlag(this, flagStr);
        // optional properties
        this.initializeHome(home);
        this.initializeDiplomacyAndAi();
    }

    private initializeHome(home: City) {
        if (home) {
            this.home = home;
            this.pos = this.home.pos;
        }
    }

    private initializeDiplomacyAndAi() {
        this.diplomat = new FactionDiplomacy(this);
        this.ai = new FactionAi(this);
    }

    updateFlag() {
        this.flag = new FactionFlag(this, this.flagData);
    }

    createNewArmy(isPatrol: boolean) {
        if (! this.home) {
            this.error(`cant create army; has no home`); return;
        }
        let armyCatNum: number = ArmyCategory.FACTION_ARMY;
        if ( isPatrol ) { armyCatNum = ArmyCategory.FACTION_PATROL; }
        // find random position around faction home
        let centerPos: MapPos = this.home.pos;
        let rndPos: MapPos = RandomWorldPos.around(centerPos, ARMY_SPAWNING_RADIUS);
        // add new UnitArmy model
        let newArmy: UnitArmy = new UnitArmy(armyCatNum, rndPos);
        newArmy.parentFaction = this;
        // add army to this factions army array
        this.factionArmies.push(newArmy);
    }

    /* sets home if first city & adds this city to ownedCity list */
    giveCityOwnership(ownedCity: City, beforeStart?: boolean) {
        ownedCity.owner = this;
        const isFirstCity = this.ownedCities.length == 0;
        this.ownedCities.push( ownedCity );
        // when city is initial city (not conquered) -> set as home
        if ( beforeStart && isFirstCity ) {
            this.home = ownedCity
        }
    }

    /* FACTION GETTERS */
    get ownedCities(): Array<City> {
        const factionAndArmyMgrLoaded = globalThis.CITIES && globalThis.FACTIONS;
        if (! factionAndArmyMgrLoaded) {
            this.warn(`cant get owned Cities of "${this.name}": CityManager or FactionManager not found`); return;
        }
        return globalThis.CITIES.getCitiesByFactionKey(this.key);
    }

    get attackArmies(): Array<UnitArmy> {
        const attackArmies: Array<UnitArmy> = [];
        for (const factionArmy of this.factionArmies) {
            if (factionArmy.armyCat.num == ArmyCategory.FACTION_ARMY) {
                attackArmies.push(factionArmy);
            }
        }
        return attackArmies;
    }

    get patrolArmies(): Array<UnitArmy> {
        const patrolArmies: Array<UnitArmy> = [];
        for (const factionArmy of this.factionArmies) {
            if (factionArmy.armyCat.num == ArmyCategory.FACTION_PATROL) {
                patrolArmies.push(factionArmy);
            }
        }
        return patrolArmies;
    }

    /* EVENT HANDLERS */
    onTurn() {
        super.onTurn();
    }
}

/* == CONSTANTS == */
const ARMY_SPAWNING_RADIUS = 3;
