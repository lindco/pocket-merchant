import { FactionType } from "./types/faction-type";
import { City } from "../city/city";
import { IUnitArmy } from "../military/interfaces/unit-army.interface";
import { UnitArmy } from "../military/army/army";
import { FactionAi } from "./ai/faction-ai";
import { FactionDiplomacy } from "./diplomacy/diplomacy";
import { FactionFlag } from "./faction-flag";

export interface IFaction {
    // basic properties
    key: string;
    num: number;
    name: string;
    prestige: number;
    factionType: FactionType;
    // cities
    home: City;
    ownedCities: Array<City>;
    // units & armies
    factionArmies: Array<IUnitArmy>;
    attackArmies: Array<UnitArmy>;
    patrolArmies: Array<UnitArmy>;
    createNewArmy(isPatrol: boolean): void;
    // diplomacy
    ai: FactionAi;
    diplomat: FactionDiplomacy;
    // flag
    flagData: string;
    flag: FactionFlag;
    // methods
    updateFlag(): void;
}

export interface IFamily extends IFaction {
    hasRandomHome: boolean;
    isAi: boolean;
    manorCityNums: Array<number>;
    // methods
    setNum(familyNum: number): void;
}

export interface IEmpire extends IFaction {
    diplTrait: string;
    capitalCity: City;
    // cache
    _ownedCityNums: Array<number>;
    capitalKey: string;
    // flags
    isDefault: boolean;
    isIndie: boolean;
}

export interface IFactionFlag {
    // parent faction
    faction: IFaction;
    factionKey: string;
    // icon & colors
    iconName: string;
    bgColorName: string;
    iconColorName: string;
    // flags
    autoGenerate: boolean;
}