
/* == INTERFACES == */
export interface IReligion {
    key: string;
    name: string;
    groupKey: string;
}

/* == RELIGION MODEL == */
export class Religion implements IReligion {
    key: string;
    name: string;
    groupKey: string = null;

    constructor(key: string, name: string, groupKey?: string) {
        this.key = key;
        this.name = name;
        if (groupKey) { this.groupKey = groupKey; }
    }
}

/* == CONSTANTS == */
export const RELIGIONS = [
    // world religions
    new Religion("catholic", "Catholic", "abrahamist"),
    new Religion("protestant", "Protestant", "reformist"),
    new Religion("muslim", "Muslim", "abrahamist"),
    new Religion("jewish", "Jewish", "abrahamist"),
    new Religion("pantheon", "Pantheon", "hellenic"),
    new Religion("hindu", "Hindu"),
    new Religion("pagan", "Pagan"),
    new Religion("mayan", "mayan")
];