// common
import { Loggable } from "../../../../common/models/log";


export class FactionType extends Loggable {
  factionTypeNum: number = -1;
  name: string;

  constructor(factionTypeNum: number, name:string) {
    super("FactionType");
    this.factionTypeNum = factionTypeNum;
    this.name = name;
  }

  static GetByNum(factionTypeNum: number): FactionType {
    return FACTION_TYPES[factionTypeNum];
  }
}

/* == CONSTANTS == */
export const TYPE_REBEL = 0;
export const TYPE_MINOR = 1;
export const TYPE_EMPIRE = 2;
export const TYPE_LOCALS = 3;
export const TYPE_FAMILY = 4;

// faction type list
export const FACTION_TYPES = [
  new FactionType(0, "Rebel"),
  new FactionType(1, "Minor"),
  new FactionType(2, "Empire"),
  new FactionType(3, "Locals"),
  new FactionType(4, "Family")
]
