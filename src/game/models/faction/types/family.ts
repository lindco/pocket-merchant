import { Unit } from "../../military/unit";
import { City } from "../../city/city";
import { AbstractFaction } from "../faction";
import { RandomElement } from "../../../../helpers/random";
import { MapPos } from "../../../world/pos";
import { FACTION_TYPES, TYPE_FAMILY } from "./faction-type";
import { IFamily } from "../faction-interfaces";

/* == FAMILY MODEL == */
export class Family extends AbstractFaction implements IFamily {
    num: number;
    name: string;
    units: Array<Unit> = [];
    // cities
    _homeCity: City;
    /** three-letter city key for this families starting city */
    _cachedHomeCityKey: string;
    manorCityNums: Array<number> = [];
    // flags
    hasRandomHome = false;
    isAi: boolean = true;
    customLanguage: string;
    customStartPos: MapPos;

    constructor(name: string, home: City, isPlayer?: boolean) {
        super(null, name, home);
        this.factionType = FACTION_TYPES[TYPE_FAMILY];
        // optional
        if (! name) { this.name = Family.randomName(); }
        if ( isPlayer ) { this.isAi = false; }
    }

    setNum(familyNum: number) {
        this.num = familyNum;
        this.key = "F" + familyNum;
    }

    setHome(city: City) {
        this._homeCity = city;
        // this.home = city;
        this.setRandomNameByHome();
    }

    /* RANDOM GENERATION */
    static randomName(): string {
        return RandomElement.of(Family.SURNAMES) as string;
    }

    setRandomNameByHome() {
        if (! this.homeCity) { return; }
        if (! globalThis.CHARS.hasNameListLoaded ) {
            this.error(`cant set random name from home; Char name list not found`); return;
        }
        this.name = globalThis.CHARS.getRandomFamilyName(this.homeCity.language);
    }

    /* returns a new randomly generated Family with random name & home */
    static random(): Family {
        let rndName: string = Family.randomName();
        let rndHome: City = globalThis.CITIES.getRandom();
        return new Family(rndName, rndHome);
    }

    buildManorIn(city: City) {
        this.manorCityNums.push( city.num );
    }

    /* GETTERS */
    get homeCity(): City {
        if (this._homeCity) { return this._homeCity; }
        else if (this._cachedHomeCityKey && globalThis.CITIES) {
            return globalThis.CITIES.getByKey(this._cachedHomeCityKey);
        }
    }

    /* CONSTANTS */
    static get PREFIX() { return "House"; }
    static get SURNAMES() {
        return ["Valecci", "Du Chapel", "Fugger", "Habsburg", "Rurik", "Qing", "of Cornwall", "Di Bordia"]
    }

    /* LOG */
    toString(): string {
        return `<Family ${this.num} "${this.name}">`;
    }
}

/* == FAMILY CONSTANTS == */
