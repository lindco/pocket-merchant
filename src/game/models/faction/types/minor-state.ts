import { IUniqueInstance } from "../../../../common/models/entity/entity";
import { AbstractFaction } from "../faction";
import { IFaction } from "../faction-interfaces";

/* == MINOR INTERFACE == */
export interface IMinorState extends IFaction, IUniqueInstance {
    
}

/* == FAMILY MODEL == */
export class MinorState extends AbstractFaction implements IMinorState {
    

}