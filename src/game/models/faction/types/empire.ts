import { AbstractFaction } from "../faction";
import { City } from "../../city/city";
import { Unit } from "../../military/unit";
import { FACTION_TYPES, TYPE_EMPIRE } from "./faction-type";
import { IEmpire } from "../faction-interfaces";

/* == EMPIRE MODEL */
export class Empire extends AbstractFaction implements IEmpire {
    num: number;
    name: string;
    units: Unit[];
    // cities
    _ownedCityNums: Array<number> = [];
    home: City;
    // diplomacy
    diplTrait: string = null;

    constructor(key: string, name: string, colors?: Array<string>, home?: City) {
        super(key, name, home);
        this.factionType = FACTION_TYPES[TYPE_EMPIRE];
        // retrieve a faction id
        this.num = this.getFactionNum();
    }

    private getFactionNum(): number {
        if (! globalThis.FACTIONS ) {
            this.error(`cant get a faction number; Faction Manager not loaded yet`);
            return;
        }
        return globalThis.FACTIONS.nextFactionNum();
    }

    getOwnedCities(): Array<City> {
        if (! globalThis.CITIES ) {
            this.error(`cant get owned city of ${this.name}: CityManager not loaded yet`);
        }
        let ownedCities: Array<City> = [];
        for (const cityNum of this._ownedCityNums) {
            let cityObj: City = globalThis.CITIES.getByNum(cityNum);
            ownedCities.push( cityObj );
        }
        return ownedCities;
    }

    _giveCity(cityObj: City) {
        cityObj._ownerFactionNum = this.num;
        this.log(`city ${cityObj.name} is now given to empire ${this.name}`);
        let isFirstCity: boolean = this._ownedCityNums.length <= 0;
        this._ownedCityNums.push( cityObj.num );
        if ( isFirstCity ) {
            this.home = cityObj;
        }
    }

    get capitalKey(): string {
        if (! globalThis.FACTIONS) { return; }
        return globalThis.FACTIONS.getEmpireCapitalKey(this.key);
    }

    get capitalCity(): City {
        if (globalThis.CITIES && this.capitalKey) {
            return globalThis.CITIES.getByKey(this.capitalKey);
        }
        return null;
    }

    /* == EMPIRE FLAGS == */
    get isDefault(): boolean {
        return this.key && this.key.length == 3;
    }

    get isIndie(): boolean {
        const indieKeys = ["I", "_I"];
        return indieKeys.includes(this.key);
    }

}
