import { Loggable } from "../../../../common/models/log";
import { IDiplomaticTreaty } from "./treaty";
import { IFaction } from "../faction-interfaces";

export interface IFactionDiplomacy {
    parentFaction: IFaction;
    treaties: Array<IDiplomaticTreaty>;
}

export class FactionDiplomacy extends Loggable implements IFactionDiplomacy {
    parentFaction: IFaction;
    treaties: Array<IDiplomaticTreaty> = [];

    constructor(parentFaction: IFaction) {
        super("Diplomacy");
        this.parentFaction = parentFaction;
    }

    toString(): string {
        return `<FactionDiplomacy "${this.parentFaction.name}">`;
    }
}

