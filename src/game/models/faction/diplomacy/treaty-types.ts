import {DiplomaticTreatyType} from "./treaty";


export const TREATY_TYPES = [
    new DiplomaticTreatyType("TRADE"),
    new DiplomaticTreatyType("ALLIANCE"),
    new DiplomaticTreatyType("WAR"),
    new DiplomaticTreatyType("NON-AGRESSION")

]