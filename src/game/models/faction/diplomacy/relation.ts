import {Loggable} from "../../../../common/models/log";
import { IFaction } from "../faction-interfaces";

export interface IFactionRelation {
    faction1: IFaction;
    faction2: IFaction;
    relationship: number;
    treaties: Array<string>;
}

export class FactionRelation extends Loggable implements IFactionRelation {
    faction1: IFaction;
    faction2: IFaction;
    relationship: number = null;
    treaties: Array<string> = [];

    constructor(faction1: IFaction, faction2: IFaction, relationship?: number, treaties?: Array<string>) {
        super("FactionRelation");
        this.faction1 = faction1;
        this.faction2 = faction2;
        // optionals
        if ( relationship ) { this.relationship = relationship; }
        if ( treaties ) { this.treaties = treaties; }
    }

    getMirrored(): FactionRelation {
        let mirrored: FactionRelation = new FactionRelation(this.faction2, this.faction1);
        if ( this.relationship ) { mirrored.relationship = this.relationship; }
        if ( this.treaties ) { mirrored.treaties = this.treaties; }
        return mirrored;
    }
}