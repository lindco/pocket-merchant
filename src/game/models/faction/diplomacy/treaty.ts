import { Loggable } from "../../../../common/models/log";
import { IFaction } from "../faction-interfaces";


/* == INTERFACES == */
export interface IDiplomaticTreaty {
    treatyType: IDiplomaticTreatyType;
    factions: Array<IFaction>;
}

export interface IDiplomaticTreatyType {
    treatyKey: string;
}

export class DiplomaticTreatyType extends Loggable implements IDiplomaticTreatyType {
    treatyKey: string;

    constructor(key: string) {
        super("DiplomaticTreatyType");
        this.treatyKey = key;
    }
}