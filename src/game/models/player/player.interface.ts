import { IUniqueInstance } from "../../../common/models/entity/entity";
import { MapPos } from "../../world/pos";
import { City } from "../city/city";
import { Family } from "../faction/types/family";
import { MapTile } from "../../world/tile/tile";
import * as BABYLON from "babylonjs";
import { IUnitArmy } from "../military/interfaces/unit-army.interface";

export interface IPlayer extends IUniqueInstance {
    num: number;
    pos: MapPos;
    coins: number;
    localCity: City;
    army: IUnitArmy;
    family: Family;
    tile: MapTile;
    // observables
    onBeforePlayerMove$: BABYLON.Observable<boolean>;
    onAfterPlayerMove$: BABYLON.Observable<MapPos>;
    onCoinsChange$: BABYLON.Observable<number>;
    // mesh
    mesh: BABYLON.AbstractMesh;
    // methods
    showDummy(): void;
}
