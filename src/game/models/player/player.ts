import { WorldEventEntity } from "../../../common/models/entity/entity";
import { MapTile } from "../../world/tile/tile";
import { TerrainTypes } from "../../world/tile/terrain-types";
import { HexDirection, MapPos } from "../../world/pos";
import { WorldTools } from "../../../helpers/world-tools";
import { City } from "../city/city";
import { Family } from "../faction/types/family";
import { StoredResource } from "../trade/resource/stored-resource";
import { TradeNode } from "../trade/trade-node/trade-node";
import * as BABYLON from 'babylonjs';
import { PlayerInfoDialog } from "../../gui/dialogs/player-dialog";
import { IPlayer } from "./player.interface";
import { IUnitArmy } from "../military/interfaces/unit-army.interface";

export class Player extends WorldEventEntity implements IPlayer {
    num: number = -1;
    pos: MapPos = new MapPos(100, 50);
    coins: number = STARTING_MONEY;
    mesh: BABYLON.AbstractMesh;
    army: IUnitArmy;
    family: Family = null;
    // observables
    onCoinsChange$: BABYLON.Observable<number> = new BABYLON.Observable<number>();
    onBeforePlayerMove$: BABYLON.Observable<boolean> = new BABYLON.Observable<boolean>();
    onAfterPlayerMove$: BABYLON.Observable<MapPos> = new BABYLON.Observable<MapPos>();

    constructor() {
        super(0, null);
        this.listenToObservables();
        // this.onWorldCreated();
    }

    private listenToObservables() {
        this.onAfterPlayerMove$.add(
            () => {
                this.updatePlayerYPos();
                if (globalThis.TURNS) { globalThis.TURNS.onEndTurnButton(); }
            }
        )
        this.onBeforePlayerMove$.add(
            () => {
                if (globalThis.SELECTION) { globalThis.SELECTION.setSourceToPlayer(); }
            }
        );
        this.onClicked$.add(
            () => { globalThis.GUI.showDialog( new PlayerInfoDialog() ); }
        );
    }

    public onWorldCreated() {
        this.getFamily();
        this.setStartPos();
        this.showDummy();
    }

    // PLAYER START POSITION
    setStartPos() {
        const mapStartPos = globalThis.MAP.playerStartingPos;
        this.log(`setting starting pos selected from map at ${mapStartPos.asString()}`);
        this.pos = mapStartPos;
        if (this.family && this.family.customStartPos) {
            this.pos = this.family.customStartPos;
        }  
    }

    setStartPosFromHome() {
        const playerHome = this.family.homeCity;
        if ((! this.family) || (! this.family.homeCity ) ) {
            this.error(`cant get player starting pos; player family home is not set`);
            return;
        }
        this.log(`setting starting pos to ${this.family.homeCity.name} at ${this.family.homeCity.pos.asString()}`);
        this.pos = this.family.homeCity.pos;
    }

    getFamily(): Family {
        if(! globalThis.FACTIONS ) {
            this.error(`cant get player family; FactionManager hasnt loaded yet`);
            return null;
        }
        this.family = globalThis.FACTIONS.playerFamily;
        return this.family;
    }

    // PLAYER 3D MODEL
    /* shows a sphere on player position before models are loaded */
    showDummy(): void {
        if (! globalThis.SCENE || ! globalThis.WORLD) {
            this.error(`cant create; scene or world isnt loaded yet`);
        }
        const sphereOptions = {
            diameter: 2
        };
        // create sphere dummy mesh
        this.mesh = BABYLON.MeshBuilder.CreateSphere("player-mesh", sphereOptions, globalThis.SCENE);
        // with red standard mat
        let mat: BABYLON.StandardMaterial = new BABYLON.StandardMaterial("player-mat", globalThis.SCENE);
        mat.diffuseColor = new BABYLON.Color3(1, 0, 0);
        this.mesh.material = mat;
        // at player position
        const scenePos = this.scenePos;
        scenePos.y += .5;
        this.mesh.position = scenePos;
        // inform managers
        if (globalThis.SELECTION) {
            globalThis.SELECTION.setSourceToPlayer();
        }
    }

    /* updates player model to horseman on land or small ship on water */
    updateMesh() {
        // save pos & remove old mesh
        const meshPos = this.mesh.position;
        this.mesh.dispose();
        const tile: MapTile = globalThis.MAP.tileAtPos(this.pos);
        const isOnLand = tile.isOnLand;
        // get new mesh depending whether player is on land or water
        if (isOnLand) {
            this.mesh = globalThis.ASSETS.getAsset(ASSET_LAND).getMesh().clone("player", null);
        } else {
            this.mesh = globalThis.ASSETS.getAsset(ASSET_WATER).getMesh().clone("player", null) as BABYLON.Mesh;
        }
        this.mesh.position = meshPos;
        this.mesh.scaling = new BABYLON.Vector3(2, 2, 2);
        // inform managers
        if (globalThis.SELECTION) {
            globalThis.SELECTION.setSourceToPlayer();
        }
    }

    setArmy(army: IUnitArmy) {
        this.army = army;
    }

    // PLAYER MOVEMENT
    moveInDir(dirStr: string) {
        // 1. save latest position in constant
        const startPos: MapPos = this.pos;
        // 2. calculate new relative and absolute target position
        const relPos = new HexDirection(dirStr).relPos;
        const targetPos = new MapPos(this.pos.x + relPos.x, this.pos.z + relPos.z);
        this.setPos(targetPos, dirStr, startPos);
    }

    setPos(targetPos: MapPos, dirStr?: string, startPos?: MapPos) {
        if (! globalThis.PLAYER_M) { return; }
        // check possibility
        const canMove = globalThis.PLAYER_M.canMoveTo(targetPos);
        if (! canMove) {
            this.onBeforePlayerMove$.notifyObservers(false);
            return;
        }
        this.onBeforePlayerMove$.notifyObservers(true);
        // update pos
        this.pos = targetPos;
        this.onAfterPlayerMove$.notifyObservers(targetPos);
    }

    updatePlayerYPos() {
        this.mesh.position = this.scenePos;
        this.mesh.position.y += this.groundOffset;
    }

    /* returns height between world 0 and terrain top as number in world space */
    get groundOffset(): number {
        let heightOffset = Player.HEIGHT_OFFSET;
        if (this.tile) {
            // terrain offset: half of tile height
            heightOffset += ( TerrainTypes.heightOf(this.tile.terrainChar) /2 );
        }
        return heightOffset;
    }

    get localCity(): City {
        if(! globalThis.CITIES ) {
            this.error(`cant get local city if player; CityManager not found`);
            return;
        }
        return globalThis.CITIES.AtPos(this.pos);
    }

    /* PLAYER STORAGE */
    get storage(): TradeNode {
        if (! this.army) { return null; }
        return this.army.tradeNode;
    }

    getStoredRes(resId: string): StoredResource {
        if (! this.storage) { return null; }
        return this.army.resourceHandler.getStoredRes(resId);
    }

    amountInStorage(resId: string): number {
        if (! this.storage) { return 0; }
        const fixedStr = this.storage.amountOf(resId).toFixed(1);
        return parseFloat(fixedStr);
    }

    toString(): string {
        return `<Player ${this.pos.x}|${this.pos.z}>`;
    }

    get tile(): MapTile {
        return globalThis.MAP.tileAtPos(this.pos);
    }

    get scenePos(): BABYLON.Vector3 {
        return WorldTools.worldToScenePos(this.pos);
    }

    static get HEIGHT_OFFSET() { return 0; }
}

/* == CONSTANTS == */
// default properties
export const STARTING_MONEY = 500;
// 3d properties
export const ASSET_WATER = "ship_smallest";
export const ASSET_LAND = "rider";