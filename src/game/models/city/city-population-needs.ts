import { City } from "./city";
import { PopulationClass } from "./population-class";
import { IPopulationClass } from "./interfaces/population.interfaces";
import { NeededResource } from "../trade/resource/needed-resource";

export class CityPopulationNeeds {
    city: City;
    classNeeds: Map<number, CityPopulationClassNeeds> = new Map<number, CityPopulationClassNeeds>();

    constructor(city: City) {
        this.city = city;
    }
}

export class CityPopulationClassNeeds {
    city: City;
    popClass: IPopulationClass;
    neededResources: Array<NeededResource> = [];

    constructor(city: City, popClass: PopulationClass) {
        this.city = city;
        this.popClass = popClass;
        this.neededResources = popClass.neededResources;
        // add to city needed resources
        const cityNeededRes = city.population.popNeeds;
        const popClassNum = popClass.level;
        // add to city needs map
        const cityPopNeeds = new CityPopulationClassNeeds(city, popClass);
        cityPopNeeds.neededResources = this.neededResources;
        cityNeededRes.classNeeds.set(popClassNum, cityPopNeeds);
    }
}