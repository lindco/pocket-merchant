// common
import { DawnbringerPalette } from "../../../common/models/colors";
import { RandomDice, RandomElement, RandomName } from "../../../helpers/random";
import { IBaseEvent } from "../../../common/models/events";
import { MapPos } from "../../world/pos";
import { Road } from "../../world/road";
import { TradeLocation } from "../../world/location";
import { Player } from "../player/player";
import { Unit } from "../military/unit";
import { UnitArmy } from "../military/army/army";
import { Character, ICharacter } from "../character/character";
import { BuildingCategory } from "../building/building-category";
import { AbstractBuildingType } from "../building/type/building-type";
import { TradeConnection } from "../trade/connection/connection";
import { IPopulation } from "./interfaces/population.interfaces";
import { CityPopulation, POPS_UPDATE_GROWTH } from "./city-population";
import { POP_PEASANTS_NUM } from './population-class';
import { CityCulture, CityCultureKeys } from "./city-culture";
import { TradeNode } from "../trade/trade-node/trade-node";
import { MarketTrade } from "../trade/market-trade";
import { TradeResource } from "../trade/resource/resource";
import * as BABYLON from 'babylonjs';
import { CityDialog } from "../../gui/dialogs/city-dialog";
import { TradeDelivery } from "../trade/trade-delivery";
import { IUnitArmy } from "../military/interfaces/unit-army.interface";
import { ITradeLocation } from "../../world/interfaces/location.interfaces";
import { IFaction } from "../faction/faction-interfaces";

/* CITY INTERFACES */
export interface ICity extends ITradeLocation {
    // basic properties
    key: string;
    cityCulture: CityCulture;
    // optional
    _cultureKey: string;
    featureStr: string;
    isColony: boolean;
    // population
    population: IPopulation;
    leader: ICharacter;
    // resources
    connections: Array<TradeConnection>;
    primaryResource: string;
    makeBestDelivery(): boolean;
    getProfitableDeliveries(): Array<MarketTrade>;
    deliverResourcesOnce(target: ITradeLocation, resId: string, amount?: number): void;
    // units
    militia: IUnitArmy;
    // mesh
    mesh: BABYLON.AbstractMesh;
    meshId: string;
    // methods
    // on event
    onEnter(playerOrUnit: Player | Unit);
    onLeave(playerOrUnit: Player | Unit);
}

/* CITY INSTANCE */

export class City extends TradeLocation implements ICity {
    cityCulture: CityCulture;
    population: CityPopulation;
    taxesLastTurn: number = 0;
    // optional properties
    owner: IFaction = null;
    _cultureKey: string = "weur";
    featureStr: string = null;
    isColony: boolean = false;
    // factions & characters
    leader: ICharacter = null;
    localCharacters: Array<Character> = [];
    // roads
    roads: Road[] = [];
    // trade
    tradeNode = new TradeNode(999, false, this);
    primaryResource: string;
    // units
    militia: IUnitArmy = null;
    patrols: Array<UnitArmy> = [];
    // mesh
    mesh: BABYLON.AbstractMesh = null;

    constructor(num: number, name: string, pos: MapPos, cityCultureKey: string, primaryResource: string,
                empireKey?: string, featureStr?: string) {
        super(num, pos, name);
        this._entityType = "city";
        // city culture
        this._cultureKey = cityCultureKey;
        this.cityCulture = new CityCulture(this, cityCultureKey);
        // optional city properties
        if ( empireKey ) { this._factionKey = empireKey; }
        if ( featureStr ) { this.featureStr = featureStr; }
        // city population
        this.population = new CityPopulation(this);
        // city properties
        this.setRndStartingProperties();
        this.primaryResource = primaryResource;

        this.registerToObservables();
    }

    registerToObservables(): void {

    }

    public static randomName(): string {
        return RandomName.from(CITY_NAME_PREFIXES, CITY_NAME_POSTFIXES);
    }

    addStartingBuildings() {
        // initialize resource production
        this._generateProductionBuildings();
        // give a random civic building
        this._generateStartingCivicBuildings();
    }

    public toString(): string {
        const factionNum = this._ownerFactionNum;
        const posStr = `${this.pos.x} ${this.pos.z}`;
        let ownerStr: string = "";
        if ( globalThis.FACTIONS && factionNum ) {
            const ownerFactionName = globalThis.FACTIONS.getFactionByNum(factionNum);
            ownerStr = ` owner="${ownerFactionName}"`;
        }
        return `<City num=${this.num} name="${this.name}" owner=${ownerStr} pos="${posStr}>"`;
    }

    /* FACTION */
    getOwner(): IFaction {
        if (! this._ownerFactionNum ) { this.log(`cant get owner`); }
        else {
            return globalThis.FACTIONS.getFactionByNum(this._ownerFactionNum);
        }
    }

    /* POPULATION */
    increaseCityPops() {
        this.growth = 0;
        this.population.increaseRandom();
        this.log("City population increased");
    }

    private onGrowthIncrease(growth: number) {
        if(this.growth >= POPS_UPDATE_GROWTH) {
            this.increaseCityPops();
        }
    }

    get language(): string {
        return this.cityCulture.languageId;
    }


    /* TRADE */
    public checkMakingBestDelivery(): boolean {
        const rndChance = RandomDice.roll(10);
        if (rndChance) {
            return this.makeBestDelivery();
        }
        return false;
    }

    public makeBestDelivery(): boolean {
        // this.log(`starting to make best delivery ..`);
        const profitableDeliveries = this.getProfitableDeliveries();
        const resAmount = CITY_DELIVERY_AMOUNT;
        if ( profitableDeliveries ) {
            let rndDelivery: MarketTrade = RandomElement.of( profitableDeliveries );
            // delivery is invalid
            if (! rndDelivery) {
                this.log(`cant make delivery; no priftable delivery found in:`);
                this.log(profitableDeliveries.toString());
                return false;
            }
            // city cant afford buying
            if (! this.canAfford(rndDelivery.resId, resAmount)) {
                const totalPrice = this.tradeNode.priceOf(rndDelivery.resId) * resAmount;
                this.warn(`cant deliver ${rndDelivery.resId} to ${rndDelivery.target.name}; city cant afford ${totalPrice} coins`);
                return false;
            }
            this.deliverResourcesOnce( rndDelivery.target, rndDelivery.resId );
            return true;
        }
        this.log(`cant find a profitable delivery in ${this.connections.length} connected cities`);
        return false;
    }



    public deliverResourcesOnce(target: ITradeLocation, resId: string, amount?: number) {
        if(! amount) { amount = CITY_DELIVERY_AMOUNT; }
        const targetMarket = target.tradeNode;
        // calculate cities profit of trading resources
        const localPrice = this.tradeNode.priceOf( resId );
        const targetPrice = targetMarket.priceOf( resId );
        const delivery = new TradeDelivery(resId, amount, this.tradeNode, localPrice,
            target.tradeNode, targetPrice);
        // exchange money from trading target to delivering city
        target.money -= amount*targetPrice;
        this.money += delivery.totalProfit;
        // exchange resources
        this.exchangeResourcesTo(resId, amount, targetMarket);
        globalThis.TRADE.onAfterDelivery(delivery);
    }

    private exchangeResourcesTo(resId: string, amount: number, targetMarket: TradeNode) {
        this.tradeNode.remove(resId, amount);
        targetMarket.add(resId, amount);
    }

    /* BUILDINGS */
    private _generateProductionBuildings() {
        // primary production
        const bldTypeId = `prod-${this.primaryResource}`;
        this.addNewBuilding(bldTypeId, 2);
        // secondary production
        for (let secProductions = 0; secProductions < 2; secProductions++) {
            const rndRes = RandomElement.of(RESOURCE_TYPES.getIds());
            this.addNewBuilding(`prod-${rndRes}`, 1);
        }
    }

    private _generateStartingCivicBuildings() {
        let startingCivicBlds = 1;
        if (this.featureStr == CITY_FEATURE_DEVELOPED) {
            startingCivicBlds += 1;
        }
        for (let index = 0; index < startingCivicBlds; index++) {
            this.buildRandomCivicBuilding();
        }
    }

    buildRandomCivicBuilding() {
        const civicBldTypes = globalThis.CITIES.getBuildingTypesByCat(BuildingCategory.CIVIC);
        const randomType = RandomElement.of(civicBldTypes) as AbstractBuildingType;
        this.addNewBuilding(randomType.id);
    }

    private setRndStartingProperties() {
        this.properties.addInteger(CITY_PROP_GROWTH, -1, DawnbringerPalette.CHRISTI, 10);
        this.properties.addInteger(CITY_PROP_MIGHT, 0, DawnbringerPalette.MANDY);
        this.properties.addInteger(CITY_PROP_HAPPINESS, -1, DawnbringerPalette.CLAIRVOYANT, 10);
        this.properties.addInteger(CITY_PROP_FAITH, -1, DawnbringerPalette.CORNFLOWER, 6);
    }

    /* CITY FEAUTURES */
    initializeCityFeaturesFromStr() {
        if (! this.featureStr) { return; }
        // additional connections
        if (this.featureStr.startsWith("conn")) {
            const targetCityKey: string = this.featureStr.replace("conn ", "");
            const targetCity = globalThis.CITIES.getByKey(targetCityKey);
            this.addConnectionTo(targetCity, true);
        }
    }

    /* RESOURCE PRODUCTION & CONSUMPTION */
    private getProducableResources(): TradeResource[] {
        let terrainResTypes: TradeResource[] = [];
        for (const resObj of globalThis.RESOURCE_TYPES.getAll()) {
            if (this.canProduce(resObj.id)) {
                terrainResTypes.push(resObj);
            }
        }
        return terrainResTypes;
    }

    private _processProduction() {
        for (const bld of this.buildings) {
            if(! bld.bldType) {
                this.log(`cant produce resources in ${bld.name}; no bldtype of building`); continue;
            }
            if (bld.bldType.category.isProd) {
                const resId = bld.bldType.id.replace("prod-", "");
                const amount = bld.level *0.2 *2;
                if (this.tradeNode.hasRequirementsFor(resId, amount))
                    this.produceResource(resId, amount);
            }
        }
    }

    private _collectTaxes() {
        const income = this.population.getTotalTaxes();
        this.money += income;
        this.taxesLastTurn = income;
    }

    /* returns an Array of TradeResourceProfit Models with connected towns when no losses are made */
    getProfitableDeliveries(): Array<MarketTrade> {
        let profitable: Array<MarketTrade> = []
        for (const storedRes of this.getStoredResources()) {
            const resId = storedRes.resId;
            const connectedTradeLocations = this.getConnectedTradeLocations();
            if (connectedTradeLocations.length <= 0)
                this.error(`cant find most profitable delivery; no connected trade locations`);
            for (const connected of connectedTradeLocations)
                this.addDeliveryOptionIfProfitable(connected, resId, profitable);
        }
        return profitable;
    }

    private addDeliveryOptionIfProfitable(connected: ITradeLocation, resId: string, profitable: Array<MarketTrade>) {
        let profit: number = connected.tradeNode.priceOf(resId) - this.tradeNode.priceOf(resId);
        if (profit > 0) {
            const turnNum = globalThis.TURNS.turnNum();
            let profitObj: MarketTrade = new MarketTrade(turnNum, resId, profit, connected, this);
            profitable.push(profitObj);
        }
    }

    /* ASSETS */
    public getAssetName() {
        if (! this._cultureKey) { return CITY_MODEL_DEFAULT; }
        return `city_${this._cultureKey}`;
    }

    /* CONNECTIONS */
    public addConnections(maxDist: number): Array<TradeConnection> {
        let connections: Array<TradeConnection> = [];
        // city connection models
        for (const otherCity of globalThis.CITIES.getAll()) {
            if (! this.equals(otherCity)) {
                const distance = this.pos.distanceTo(otherCity.pos);
                if (distance <= maxDist)
                    connections.push( this.addTradeConnectionTo(otherCity, false) );
            }
        }
        // find simple path
        for (const conn of this.connections) {
            // connection starts here?
            if (conn.source.num == this.num)
                conn.path.setDirectPath();
        }
        // return created connection
        return connections;
    }

    /* ROADS */
    getRoads(): Array<Road> {
        return this.roads;
    }

    /* WORLD */
    get meshId(): string {
        return "city-" + this.key + "-mesh";
    }

    static GetKeyByMeshName(cityMeshName: string): string {
        return cityMeshName.split("-")[1];
    }

    private hideIdTooFar() {
        if (! this.mesh ) {
            return;
        }
        const playerPos = globalThis.PLAYER.pos;
        const distToPlayer = this.pos.distanceTo(playerPos);
        if (distToPlayer > CITY_RENDER_MAX_DIST) {
            // this.log(`hiding city ${this.name}: ${distToPlayer} is too far`);
            this.mesh.setEnabled(false);
        } else { this.mesh.setEnabled(true); }
    }

    public onEnter(playerOrUnit: Player | Unit) {
        this.log(`is entered by ${playerOrUnit}`);
        const diag = new CityDialog(this);
        globalThis.GUI.showDialog(diag);
    }

    public onLeave(playerOrUnit: Player | Unit) {
        this.log(`is left by player`);
    }

    public onTurn() {
        super.onTurn();
        this._collectTaxes();
        this._processProduction();
    }

    /* LOAD OR GENERATE */
    static FromValueArray(cityValueArray: string[]): City {
        const name = cityValueArray[1];
        const pos = MapPos.FromString(cityValueArray[2]);
        const culture = cityValueArray[3];
        const empireKey = cityValueArray[4];
        const primRes = cityValueArray[5];
        // city features?
        let featuresStr: string = null;
        if (cityValueArray.length > 6) {featuresStr = cityValueArray[6]; }
        let cityIsColony: boolean = cityValueArray.length > 7 && cityValueArray[7] == "true";
        // create city object
        const cityNum = globalThis.CITIES.getAll().length;
        let newCity: City = new City(cityNum, name, pos, culture, primRes, empireKey);
        newCity.featureStr = featuresStr;
        // including city key ..
        newCity.key = cityValueArray[0];
        // .. and possibly as colony
        if (cityIsColony) { newCity.isColony = true; }
        // return city with key and colony flag
        return newCity;
    }

    static MakeRandom(): City {
        // let cityNum: number = globalThis.CITIES.getAll().length;
        let rndName: string = City.randomName();
        let rndPos: MapPos = MapPos.random();
        let rndCulture: string = CityCultureKeys.GetRandom();
        let rndPrimRes: string = RandomElement.of(globalThis.RESOURCE_TYPES.getIds())
        return new City(null, rndName, rndPos, rndCulture, rndPrimRes);
    }

    /* GETTERS & SETTERS */
    // growth
    get growth(): number {
      let baseGrowth = this.properties.getNum(CITY_PROP_GROWTH);
      // add 0.5 growth per peasant
      const peasantPopsAmount = this.population.getPopsOf(POP_PEASANTS_NUM)
      const peasantGrowth = peasantPopsAmount * 0.25;
      return baseGrowth + peasantGrowth;
    }

    set growth(value: number) {
        const cityGrowthProperty = this.properties.getProperty(CITY_PROP_GROWTH);
        cityGrowthProperty.value = value;
        this.properties.onIncrease$.notifyObservers(cityGrowthProperty);
        this.onGrowthIncrease(value);
    }
    // property getters
    get might(): number { return this.properties.getNum(CITY_PROP_MIGHT); }
    get happiness(): number { return this.properties.getNum(CITY_PROP_HAPPINESS); }
    get faith(): number { return this.properties.getNum(CITY_PROP_FAITH); }
    // property setters
    set might(value: number) { this.properties.getProperty(CITY_PROP_MIGHT).value = value; }
    set happiness(value: number) { this.properties.getProperty(CITY_PROP_HAPPINESS).value = value; }
    set faith(value: number) { this.properties.getProperty(CITY_PROP_FAITH).value = value; }
}

/* == CITY PROPERTIES == */
export const CITY_PROP_GROWTH = "growth";
export const CITY_PROP_MIGHT = "might";
export const CITY_PROP_HAPPINESS = "happiness";
export const CITY_PROP_FAITH = "faith";

/* == CITY ASSETS == */
export const CITY_MODEL_DEFAULT = "rural_townhall";

/* == CITY CONSTANTS == */
export const CITY_CONNECTION_MAX_DIST = 10;
export const CITY_RENDER_MAX_DIST = 35;
// random names
export const CITY_NAME_PREFIXES = ["Santa", "Port", "New", "", "Cap", "Fort", "Old", "Point"];
export const CITY_NAME_POSTFIXES = [" Royale", " London", " Marino", " Brugges", " Mecca", " Marble", "green", "Cross", "cross", "rock", "stadt"];
// city features
export const CITY_FEATURE_DEVELOPED = "developed";
export const CITY_FEATURE_INDUSTRIAL = "industrial";
export const CITY_FEATURE_CONN_PREFIX = "conn ";
export const CITY_DELIVERY_AMOUNT = 1;
