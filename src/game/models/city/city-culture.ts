// common
import { Loggable } from "../../../common/models/log";
import { RandomElement } from "../../../helpers/random";
// models
import { City } from "./city";

/* == INTERFACE == */
export interface ICityCulture {
    key: string;
    city: City;
    meshName: string;
    languageId: string;
}

/* == CITY CULTRE MODEL == */
export class CityCulture extends Loggable implements ICityCulture {
    key: string = null;
    city: City = null;

    constructor(city: City, cultureKey?: string) {
        super("CityCulture");
        this.city = city;
        const cultureKeyIsValid: boolean = cultureKey && CityCultureKeys.ALL.includes(cultureKey);
        // undefined? set to weur
        if (! cultureKeyIsValid) {
            this.warn(`cant find city mesh for culture "${cultureKey}"; set to "weur"`);
            this.key = CityCultureKeys.W_EUROPEAN;
        } else {
            this.key = cultureKey; // or set cultureKey str
        }
    }

    get meshName(): string {
        return "city_" + this.key + ".obj";
    }

    get languageId(): string {
        switch (this.key) {
            case CityCultureKeys.DEFAULT: return "western";
            case CityCultureKeys.HELLENIC: return "hellenic";
            case CityCultureKeys.ROMAN: return "italian";
            case CityCultureKeys.W_EUROPEAN: return "western";
            case CityCultureKeys.SLAVIC: return "slavic";
            case CityCultureKeys.ASIAN: return "easian";
            case CityCultureKeys.TRIBAL: return "african";
            case CityCultureKeys.COLONIAL: return "italian";
            case CityCultureKeys.MIDEAST: return "arabic";
            case CityCultureKeys.BARBARIAN: return "germanic";
            case CityCultureKeys.CLOISTER: return "western";
            case CityCultureKeys.ELVEN: return "elven";
            case CityCultureKeys.VENETIAN: return "italian";
            case CityCultureKeys.HANSEATIC: return "germanic";
            case CityCultureKeys.VIVEC: return "elven";
            case CityCultureKeys.DWARVEN: return "germanic";
            default: return "western";
        }
    }
}

/* == CITY CULTURES LIST == */
export class CityCultureKeys {
    static get DEFAULT() { return "weur"; }
    static get MIDEAST() { return "mideast"; }
    static get ASIAN() { return "asian"; }
    static get TRIBAL() { return "tribal"; }
    static get SLAVIC() { return "slavic"; }
    static get W_EUROPEAN() { return "weur"; }
    static get COLONIAL() { return "colonial"; }
    static get CLOISTER() { return "cloister"; }
    static get VENETIAN() { return "venetian"; }
    static get HANSEATIC() { return "hanseatic"; }
    // ancient
    static get ROMAN() { return "roman"; }
    static get HELLENIC() { return "hellenic"; }
    static get BARBARIAN() { return "barbarian"; }
    // fantasy
    static get ELVEN() { return "elven"; }
    static get DWARVEN() { return "dwarven"; }
    static get EVIL() { return "evil"; }
    // unique cities
    static get VIVEC() { return "vivec"; }

    // list
    static get ALL(): Array<string> {
        return [ CityCultureKeys.W_EUROPEAN, CityCultureKeys.MIDEAST, CityCultureKeys.ASIAN, CityCultureKeys.TRIBAL,
        CityCultureKeys.SLAVIC, CityCultureKeys.COLONIAL, CityCultureKeys.CLOISTER, CityCultureKeys.CLOISTER, CityCultureKeys.VENETIAN,
        CityCultureKeys.HANSEATIC, CityCultureKeys.ROMAN, CityCultureKeys.HELLENIC, CityCultureKeys.BARBARIAN, CityCultureKeys.ELVEN, 
        CityCultureKeys.DWARVEN, CityCultureKeys.EVIL, CityCultureKeys.VIVEC ];
    }

    // random
    static GetRandom(): string {
        return RandomElement.of(CityCultureKeys.ALL);
    }
}