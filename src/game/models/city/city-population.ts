import { RandomInt } from "../../../helpers/random";
import { StoredResource } from "../trade/resource/stored-resource";
import { TradeNode } from "../trade/trade-node/trade-node";
import { City } from "./city";
import { POP_CLASSES, PopulationClass } from './population-class';
import { IPopulation } from "./interfaces/population.interfaces";
import * as BABYLON from 'babylonjs';
import { CityPopulationNeeds } from "./city-population-needs";

export class CityPopulation implements IPopulation {
    // parent city
    city: City;
    // population fields
    classPops: Map<number, number> = new Map<number, number>();
    // pop growth
    totalGrowthIncrease: number = 0;
    // supply & consumtion
    popNeeds: CityPopulationNeeds;
    lastConsumptionSuccess: number = 0;
    lastSuppliedResIds: Array<string> = [];
    // observables
    onPopIncrease$ = new BABYLON.Observable<number>();

    constructor(city: City) {
        this.city = city;
        this.generateClassPops();
    }

    private generateClassPops() {
        for (let classNum = 0; classNum < POP_CLASSES.length; classNum++) {
            // retrieve max starting pops and pick a random amount below
            const maxPopsForClass = POP_CLASSES_MAX_START[classNum];
            let classPops: number = RandomInt.below(maxPopsForClass);
            // add to class map
            this.classPops.set(classNum, classPops);
        }
        // create pop needs class
        this.popNeeds = new CityPopulationNeeds(this.city);
    }

    getPopsOf(popClassLevel: number): number {
        return this.classPops.get(popClassLevel);
    }

    get totalPops(): number {
        let totalPops: number = 0;
        this.classPops.forEach(value => totalPops += value);
        return totalPops;
    }

    increaseRandom() {
        const rndClassLevel = RandomInt.below(POP_CLASSES.length);
        const populationBefore = this.classPops.get(rndClassLevel);
        this.classPops.set(rndClassLevel, populationBefore + 1);
        // fire pop increased event
        this.onPopIncrease$.notifyObservers(rndClassLevel);
    }

    get upgradePercent(): number {
        const currentGrowth = this.city.growth;
        return currentGrowth /POPS_UPDATE_GROWTH *100;
    }

    /* POPULATION CONSUMPTION */
    consumeResources(): number {
        // clear last consumed resource array
        this.lastSuppliedResIds = [];
        const consumed = this.getConsumedResources();
        const totalResCount = consumed.length;
        let successfulResCount: number = 0;
        for (const storedRes of consumed) {
            const consumedResAmount = storedRes.amount;
            const consumedResId = storedRes.resId;
            // market has resources
            if(this.market.hasEnough(consumedResId, consumedResAmount)) {
                successfulResCount++;
                this.market.remove(consumedResId, consumedResAmount);
                // add to successfully consumed respurces array
                this.lastSuppliedResIds.push(consumedResId);
            } else {
                // increase price anyway
                this.market.increasePrice(consumedResId, 1);
            }
        }
        const successful = successfulResCount / totalResCount;
        this.onConsumptionFinished(successful);
        return successful;
    }

    resIsFullySupplied(resId: string): boolean {
        return this.lastSuppliedResIds.includes(resId);
    }

    onConsumptionFinished(successful: number) {
        this.lastConsumptionSuccess = successful;
        this.city.growth += successful;
        this.totalGrowthIncrease += successful;
    }

    /* returns the amount of resources of "resId" that the popluation class "classNum" consumes in one turn */
    getClassResConsumption(classLevel: number, resId: string): number {
        const popClass = POP_CLASSES[classLevel];
        const consumesPer10 = popClass.getResourceNeed(resId);
        const consumesSingle = consumesPer10 /10;
        return this.getAmount(classLevel) * consumesSingle;
    }

    getTotalResConsumption(resId: string): number {
        let consumesTotal: number = 0;
        for (const popClass of this.getClasses()) {
            const classConsumes = this.getClassResConsumption(popClass.level, resId);
            consumesTotal += classConsumes;
        }
        return consumesTotal;
    }

    getTotalResConsumptionList(): Array<string> {
        let consumedResIds: Array<string> = [];
        for (const popClass of this.getClasses()) {
            const classConsumed = popClass.getConsumedResIds();
            for (const classResId of classConsumed) {
                // add resId when not already in array (no doubles)
                const isNewResId = !consumedResIds.includes(classResId) && classResId;
                if (isNewResId) { consumedResIds.push(classResId); }
            }
        }
        return consumedResIds;
    }

    getConsumedResources(): Array<StoredResource> {
        let consumedResources: Array<StoredResource> = [];
        for (const consumedResId of this.getTotalResConsumptionList()) {
            const consumedAmount = this.getTotalResConsumption(consumedResId);
            const consumedRes = new StoredResource(consumedResId, consumedAmount);
            consumedResources.push(consumedRes);
        }
        return consumedResources;
    }

    /* returns the amount of taxes generated per turn from class pops */
    getTotalTaxes(): number {
        let totalTaxes: number = 0;
        for (const popClass of this.getClasses()) {
            let classPops: number = this.getPopsOf(popClass.level);
            let classTaxes = classPops * popClass.taxRate * TAXES_MODIFIER;
            totalTaxes += classTaxes;
        }
        return Math.round(totalTaxes);
    }

    /* POPULATION GETTERS & SETTERS */
    get market(): TradeNode { return this.city.tradeNode; }

    getAmount(classLevel: number): number {return this.classPops.get(classLevel); }

    getClasses(): Array<PopulationClass> {
        return POP_CLASSES;
    }

    getClassName(classLevel: number): string {
        return POP_CLASSES[classLevel].name;
    }
}

export const POP_CLASSES_MAX_START = [4, 6, 6, 4, 2, 2];

/* == CONSTANTS == */
export const RES_CONSUMPTION_TURNS = 7;
export const POPS_UPDATE_GROWTH = 15;
export const TAXES_MODIFIER = 1;
