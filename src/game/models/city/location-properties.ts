import { IProperty, Properties } from "../../../common/models/properties/properties";
// babylon
import * as BABYLON from 'babylonjs';
import { ILocation } from "../../world/interfaces/location.interfaces";

export class LocationProperties extends Properties {
    parentCity: ILocation;
    // observables
    onIncrease$: BABYLON.Observable<IProperty> = new BABYLON.Observable<IProperty>();

    constructor(city: ILocation) {
        super();
        this.parentCity = city;
        this.addDefaultProperties();
    }

    addDefaultProperties() {
        this.addFloat("growth", 0);
    }
}