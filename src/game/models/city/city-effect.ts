import { Loggable } from "../../../common/models/log";
import { EntityPropertyEffect } from "../../../common/models/entity/entity-effect";
import { ILocation } from "../../world/interfaces/location.interfaces";

export class LocationEffects extends Loggable {
    location: ILocation;
    effects: Array<EntityPropertyEffect> = [];

    constructor(location: ILocation) {
        super("LocationEffects");
        this.location = location;
    }

}