import { City } from "../city";
import { PopulationClass } from "../population-class";
import { StoredResource } from "../../trade/resource/stored-resource";
import { NeededResource } from "../../trade/resource/needed-resource";

export interface IPopulation {
    city: City;
    classPops: Map<number, number>;
    lastConsumptionSuccess: number;
    lastSuppliedResIds: Array<string>;
    totalPops: number;
    upgradePercent: number;
    // methods
    consumeResources(): number;
    resIsFullySupplied(resId: string): boolean;
    getTotalResConsumption(resId: string): number;
    getTotalResConsumptionList(): Array<string>;
}

export interface IPopulationClass {
    // basic properties
    level: number;
    name: string;
    taxRate: number;
    iconName: string;
    // consumtion & upgrade resources
    neededResources: Array<NeededResource>;
    upgradeResource: StoredResource;
    // methods
    getConsumedResIds(): Array<string>;
    getResourceNeed(resId: string): number;
}