// trade
import { StoredResource } from '../trade/resource/stored-resource';
import { IPopulationClass } from "./interfaces/population.interfaces";
import { NeededResource } from "../trade/resource/needed-resource";

/* SINGLE POPULATION CLASS */
export class PopulationClass implements IPopulationClass {
    // basic properties
    level: number;
    name: string;
    taxRate: number = .2;
    iconName: string = "trader";
    // consumption & upgrade resources
    neededResources: Array<NeededResource> = [];
    upgradeResource: StoredResource = null;

    constructor(level: number, name: string, iconName: string, consumesResourcesStr: string, taxes?: number, upgradeResStr?: string) {
        this.level = level;
        this.name = name;
        if ( taxes ) { this.taxRate = taxes; }
        // icon
        this.iconName = iconName;
        this.setConsumedResourcesFromString(consumesResourcesStr);
    }

    /* consumption is for every 10 pops, so it will usually by lower than the given amount */
    private setConsumedResourcesFromString(consumesResourcesStr: string) {
        for (const storedResStr of consumesResourcesStr.split(" ")) {
            const storedRes = StoredResource.FromString(storedResStr);
            const neededRes = NeededResource.FromStoredRes(storedRes)
            this.neededResources.push(neededRes);
        }
    }

    private setUpdateResourceFromString(upgradeResStr: string) {
        this.upgradeResource = StoredResource.FromString(upgradeResStr);
    }

    getConsumedResIds(): Array<string> {
        let consumedResIds: Array<string> = [];
        for (const neededRes of this.neededResources) {
            consumedResIds.push(neededRes.resId);
        }
        return consumedResIds;
    }

    getResourceNeed(resId: string): number {
        for (const consumedRes of this.neededResources)
            if (consumedRes.resId == resId) { return consumedRes.needed; }
        return 0;
    }
}

/* CONSTANTS */
// popuulation class ids
export const POP_BEGGARS_NUM = 0;
export const POP_PEASANTS_NUM = 1;
export const POP_CITIZEN_NUM = 2;
export const POP_CLERICAL_NUM = 3;
export const POP_MERCHANTS_NUM = 4;
export const POP_NOBLES_NUM = 5;

// SinglePopulationClass object definitions
export const POP_CLASSES = [
    new PopulationClass(POP_BEGGARS_NUM, "Beggars", "beggar", "0.3xale 0.3xfish 1xwood", .5, "0.5xtools"),
    new PopulationClass(POP_PEASANTS_NUM, "Peasants", "peasant", "0.4xgrain 0.3xfish 0.2xale", 2, "0.4xtools"),
    new PopulationClass(POP_CITIZEN_NUM, "Citizen", "citizen", "0.5xgrain 0.3xpelts 0.2xale", 1.5, "0.2xclothes"),
    new PopulationClass(POP_CLERICAL_NUM, "Clerical", "clerical", "0.4xale 0.2xclothes 0.1xpearls", 2, "0.2xgold"),
    new PopulationClass(POP_MERCHANTS_NUM, "Merchants", "trader", "0.7xgrain 0.2xclothes 0.2xtools 0.2xpearls", 3.3, "0.4xgold"),
    new PopulationClass(POP_NOBLES_NUM, "Nobles", "noble", "1xgrain 0.3xgold 0.5xclothes 0.1xweapons", 4)
];
