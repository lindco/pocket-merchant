import { Character } from "../character";
import { ICharacterAction, AbstractCharacterAction, CharacterActionScope } from "./action";
import * as BABYLON from 'babylonjs';

/* BASIC CHAR ACTIONS */
export class JoinArmyAction extends AbstractCharacterAction {


    constructor(armyNum: number, character: Character) {
        super("join-army", CharacterActionScope.MOVEMENT, armyNum, null, character);
    }

    onExecute(): boolean {

        return true;
    }
}


/* ACTIONS LIST */
export const BASIC_ACTIONS = [
    new AbstractCharacterAction("joinArmy", CharacterActionScope.MOVEMENT)
];