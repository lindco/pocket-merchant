import { Character, ICharacter } from "../character";
import * as BABYLON from 'babylonjs';

export interface ICharacterAction {
    character: Character;
    scopeNum: number;
    actionKey: string;
    actionValue: number;
    actionExtraData: string;
    // flags
    canBeExecuted: boolean;
    // observables
    onAdded$: BABYLON.Observable<ICharacter>;
    onBeforeExecute$: BABYLON.Observable<void>;
    onExecute$: BABYLON.Observable<void>;
    // methods
    onExecute(charAction: ICharacterAction): boolean;
}

/**
 * A possible action to take for a character for one turn. An example could be taking a rest to heal or
 * attacking an enemy character that is residing in the same tile.
 *
  */

export class AbstractCharacterAction implements ICharacterAction {
    character: Character = null;
    scopeNum: number;
    actionKey: string;
    actionValue: number = -1;
    actionExtraData: string = null;
    // observables
    onAdded$ = new BABYLON.Observable<ICharacter>();
    onBeforeExecute$ = new BABYLON.Observable<void>();
    onExecute$ = new BABYLON.Observable<void>();

    constructor(actionKey: string, typeNum: number, actionValue?: number, actionData?: string, character?: Character) {
        this.scopeNum = typeNum;
        this.actionKey = actionKey;
        // optional
        if (actionValue && actionValue >0) { this.actionValue = actionValue; }
        if (actionData && actionData.length > 0) { this.actionExtraData = actionData; }
        if (character) { this.character = character; }
    }

    get canBeExecuted(): boolean {
        return false;
    }

    onExecute(this): boolean {
        return false;
    }
}

export class CharacterActionScope {

    static get MOVEMENT() { return 1; }
    static get COURT() { return 2; }
    static get FIGHT() { return 3; }
    static get TRADE() { return 4; }
    static get PLOT() { return 5; }
    static get OTHER() { return 6; }
}