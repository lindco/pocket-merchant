// common
import { Loggable } from "../../../common/models/log";
// world
import { MapPos } from "../../world/pos";
// models
import { City } from "../city/city";

export interface ICharacter {
    // base properties
    num: number;
    fullName: string;
    isMale: boolean;
    pos: MapPos;
    // faction & language
    home: City;
    factionNum: number;
    language: string;
    // optional
    customTitle: string;

}

export class Character extends Loggable implements ICharacter {
    num: number = -1;
    fullName: string = null;
    isMale: boolean = true;
    pos: MapPos = null;
    // faction & language
    home: City = null;
    factionNum: number = -1;
    language: string = "western";
    // optional
    customTitle: string = null;

    constructor(num: number, factionNum: number, language?: string, fullName?: string, isFemale?: boolean) {
        super("Character");
        this.num = num;
        this.factionNum = factionNum;
        // language
        if ( language ) { this.language = language; }
        // female?
        if ( isFemale ) { this.isMale = false; }
        // fixed or random name?
        if ( fullName ) { this.fullName = fullName; }
        else { this.assignRandomName(); }
    }

    assignRandomName() {
        // check whether name list has successfully loaded
        const nameListLoaded = globalThis.CHARS && globalThis.CHARS.hasNameListLoaded;
        if (! nameListLoaded ) {
            this.warn(`cant assign random name; name list not found`);
            return;
        }
        const rndNameFull = globalThis.CHARS.getRandomName(this.language, this.isFemale);
        this.fullName = rndNameFull;
    }

    /* GETTERS & SETTERS */
    get isFemale(): boolean { return ! this.isMale }

}

/* == CHARACTER CONSTANTS == */