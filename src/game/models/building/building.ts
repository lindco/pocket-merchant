import { IWorldEntity, WorldEntity } from "../../../common/models/entity/entity";
import { MapPos } from "../../world/pos";
import { AbilityList } from "../../../common/models/ability";
import {BuildingEffect} from "./building-effect";
import { IBuildingType } from "./interfaces/constructable.interfaces";

/* == INTERFACES == */
export interface IBuildingInstance extends IWorldEntity {
    // basic properties
    bldTypeStr: string;
    bldType: IBuildingType;
    level: number;
    // optional properties
    name: string;
    isSecondary: boolean;
    customName: string;
    abilities: AbilityList;
    // methods
    setBldType(bldTypeStr: string): void;
    onTick(): void;
}

/* == BUILDING INSTANCES == */
export class Building extends WorldEntity implements IBuildingInstance  {
    // basic properties
    level: number;
    bldTypeStr: string;
    _bldType: IBuildingType = null;
    // optional
    isSecondary: boolean = false;
    customName: string;
    abilities: AbilityList = new AbilityList();

    constructor(bldTypeStr: string, customName: string, level: number, isSecondary?: boolean, customPos?: MapPos) {
        super(-1, customPos);
        this._entityType = "building";
        this.level = level;
        this.setBldType(bldTypeStr);
        // optional properties
        if ( customName ) { this.customName = customName; }
        if ( isSecondary ) { this.isSecondary = isSecondary; }
    }

    /* BUILDING TYPE */
    setBldType(bldTypeStr: string) {
        this.bldTypeStr = bldTypeStr;
        this._bldType = globalThis.CONSTRUCTABLE.get(bldTypeStr);
    }

    get bldType(): IBuildingType {
        if(! this._bldType) {
            this.error(`cant get BuildingType object for "${this.bldTypeStr}"`); return;
        }
        return this._bldType;
    }

    /* ABILITIES */
    _addAbility(bldAbilityObj: BuildingEffect) {
        this.abilities._addAbility(bldAbilityObj);
    }

    addAbility(key: string, value?: number, extra?: string) {
        const newBldAbility = new BuildingEffect(key, value, extra);
        this._addAbility(newBldAbility);
    }

    /* EVENT HANDLING */
    onTick() {

    }

    /* HELPERS */
    get name(): string {
        if (this.bldType.names.length == 1) {
            return this.bldType.names[0];
        }
        return this.bldType.names[this.level];
    }
 }