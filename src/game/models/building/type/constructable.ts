import {Loggable} from "../../../../common/models/log";
import { IBuildingType, IConstructable } from "../interfaces/constructable.interfaces";

export class ConstructableTypes extends Loggable {
    private _bldTypeIds: Array<string> = [];
    private _bldTypeCategories: Array<number> = [];
    bldTypes: Array<IBuildingType> = [];

    constructor() {
        super("CityBuildingTypes");
    }

    public add(bldType: IBuildingType) {
        this.bldTypes.push(bldType);
        // add ids and categories into private arrays
        this._bldTypeIds.push(bldType.id);
        this._bldTypeCategories.push(bldType.category.num);
    }

    public get(bldTypeId: string): IBuildingType {
        if(! this._bldTypeIds.includes(bldTypeId)) {
            this.warn(`cant get building of type "${bldTypeId}"; returns null`); return;
        }
        const bldTypeIndex = this._bldTypeIds.indexOf(bldTypeId);
        return this.bldTypes[bldTypeIndex];
    }

    /* CUSTOM BLD TYPE GETTERS */
    get count(): number {
        return this._bldTypeIds.length;
    }

    getAll(): Array<IConstructable> {
        return this.bldTypes;
    }

    getByCategory(bldCatNum: number): Array<IConstructable> {
        let bldTypesOfCat: Array<IConstructable> = [];
        for (let bldTypeNum = 0; bldTypeNum < this.count; bldTypeNum++) {
            const bldCat = this._bldTypeCategories[bldTypeNum];
            if(bldCat == bldCatNum) {
                const bldTypeObj = this.bldTypes[bldTypeNum];
                bldTypesOfCat.push(bldTypeObj);
            }
        }
        return bldTypesOfCat;
    }
}