// common
import { TradeResource } from '../../trade/resource/resource';
import { CATEGORIES, BuildingCategory } from '../building-category';
import { AbilityList, BaseAbility } from "../../../../common/models/ability";
import { BuildingEffect } from "../building-effect";
import { Loggable } from '../../../../common/models/log';
import { IBuildingType } from "../interfaces/constructable.interfaces";

/* == INTERFACES == */


/* == BUILDING TYPES == */
export class AbstractBuildingType extends Loggable implements IBuildingType {
    id: string;
    maxLevel: number;
    names: string[];
    // category
    category: BuildingCategory;
    // abilities
    _abilitiesStr: string;
    abilities: AbilityList = new AbilityList();

    constructor(id: string, names: string[], abilities?: string) {
        super("BldType");
        this.id = id;
        this.maxLevel = names.length + 1;
        this.names = names;
        if (abilities) {
            this._abilitiesStr = abilities;
            this.addAbilitiesFromStr();
        }
    }

    addAbilitiesFromStr() {
        if (this._abilitiesStr && this._abilitiesStr.length > 0) {
            const splittedStr = this._abilitiesStr.split(", ");
            for (const abilityStr of splittedStr) {
                let ability: BuildingEffect = BuildingEffect.FromString(abilityStr);
                this.addAbility(ability);
            }
        }
    }

    public onTick() {

    }

    get name(): string {
        return this.names[0];
    }

    /* ABILITIES */
    addAbility(bldAbility: BuildingEffect) {
        this.abilities._addAbility(bldAbility);
    }
}

export class CivicBuildingType extends AbstractBuildingType implements IBuildingType {
    category: BuildingCategory = CATEGORIES[BuildingCategory.CIVIC];

    constructor(id: string, names: Array<string>, abilities?: string) {
        super(id, names, abilities);
    }
}

export class ProductionBuildingType extends AbstractBuildingType implements IBuildingType {
    category: BuildingCategory = CATEGORIES[BuildingCategory.PROD];
    id: string;
    maxLevel: number = 10;
    names: Array<string>;

    constructor(resId: string) {
        super(`prod-${resId}`, []);
        const tradeRes = globalThis.RESOURCE_TYPES.get(resId);
        const prodBldName = tradeRes.prodBldName;
        this.names = [`${tradeRes.name} ${prodBldName}`];
    }

    public onTick() {
        super.onTick();
    }

    get name(): string {
        return this.names[0];
    }
}