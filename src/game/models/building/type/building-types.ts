import { CivicBuildingType } from "./building-type";

export class CivicBuildingTypes {
    static get CHURCH() {
        return new CivicBuildingType(
            "church",
            ["Chapel", "Abbey", "Church", "Large Church", "Cathedral"],
            "faith-bonus 2"
        );
    }

    static get TOWNHALL() {
        return new CivicBuildingType(
            "townhall",
            ["Gathering Spot", "Mayor's Hut", "Townhall", "City Hall", "Palace"],
            "administration 2"
        );
    }

    static get MARKET() {
        return new CivicBuildingType(
            "market",
            ["Market Spot", "Local Market", "City Market", "Central Market"],
            "trade-profit-bonus 0.3"
        );
    }

    static get GUILDHALL() {
        return new CivicBuildingType("guildhall",
            ["Guild Meeting Room", "Guild House", "Guild Hall"] );
    }

    static get PALACE() {
        return new CivicBuildingType("palace",
            ["Mayor Hut", "Town Manor", "Lord\'s Residence", "Great Palace"] );
    }

    static get IRRIGATION() {
        return new CivicBuildingType(
            "irrigation",
            ["Village Pond", "Field Canals", "Underground Irrigation"],
            "growth-bonus 1"
        );
    }

    static get LIST() {
        return [
            CivicBuildingTypes.CHURCH,
            CivicBuildingTypes.TOWNHALL,
            CivicBuildingTypes.MARKET,
            CivicBuildingTypes.GUILDHALL,
            CivicBuildingTypes.PALACE,
            CivicBuildingTypes.IRRIGATION
        ];
    }
}