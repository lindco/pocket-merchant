import { BaseAbility, IAbility } from "../../../common/models/ability";


export interface IBuildingAbility extends IAbility {

}

export class BuildingEffect extends BaseAbility implements IBuildingAbility {

    constructor(key: string, value: number, extra?: string) {
        super(key, value, extra);
    }
}