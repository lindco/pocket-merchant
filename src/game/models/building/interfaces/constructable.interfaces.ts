import { ILoggable } from "../../../../common/models/log";
import { AbilityList } from "../../../../common/models/ability";
import { BuildingCategory } from "../building-category";

export interface IBuildingType extends IConstructable, ILoggable {
    names: Array<string>;
    // abilities
    abilities: AbilityList;

    onTick();
}

export interface IConstructable {
    category: BuildingCategory;
    id: string;
    maxLevel: number;
    name: string;
}