import { IConstructable } from "./interfaces/constructable.interfaces";

export interface IBuildingCategory {
    num: number;
    name: string;
    icon: string;
    bldTypes: Array<IConstructable>;
    canBeBuiltOutside: boolean;
    // methods
    addBldType(bldType: IConstructable);
}

export class BuildingCategory implements IBuildingCategory{
    num: number;
    icon: string = "building";
    bldTypes: Array<IConstructable> = [];
    _name: string = null;
    canBeBuiltOutside: boolean = false;

    constructor(bldCatNum: number, name?: string, icon?: string, canBeBuiltOutside?: boolean) {
        this.num = bldCatNum;
        if (name) { this._name = name; }
        if (icon) { this.icon = icon; }
        if (canBeBuiltOutside) { this.canBeBuiltOutside = true; }
    }

    get name(): string {
        if(this._name && this._name.length > 0) { return this._name; }
        return "";
    }

    addBldType(bldType: IConstructable) {
        this.bldTypes.push(bldType);
    }

    get bldTypeIds(): Array<string> {
        let bldTypeIds: Array<string> = [];
        for (const catBldType of this.bldTypes) {
            bldTypeIds.push(catBldType.id);
        }
        return bldTypeIds;
    }

    // flags
    get isNone(): boolean { return this.num == BuildingCategory.NONE; }
    get isCivic(): boolean { return this.num == BuildingCategory.CIVIC; }
    get isProd(): boolean { return this.num == BuildingCategory.PROD; }
    get isMil(): boolean { return this.num == BuildingCategory.MIL; }
    get isWorld(): boolean { return this.num == BuildingCategory.WORLD; }

    // static bld type numbers
    static get NONE(): number { return 5; }
    static get CIVIC(): number { return 0; }
    static get PROD(): number { return 1; }
    static get MIL(): number { return 2; }
    static get TRADER(): number { return 4; }
    static get WORLD(): number { return 3; }
}

export const CATEGORIES = [
    new BuildingCategory(BuildingCategory.CIVIC, "Civic"),
    new BuildingCategory(BuildingCategory.PROD, "Production", "production"),
    new BuildingCategory(BuildingCategory.MIL, "Military", "attack"),
    new BuildingCategory(BuildingCategory.WORLD, "World", "connection", true),
    new BuildingCategory(BuildingCategory.TRADER, "Trader"),
    new BuildingCategory(BuildingCategory.NONE, "None", null, true),

];