import { Loggable } from "../../../../common/models/log";
// babylon
import * as BABYLON from 'babylonjs';

export class GameLoadingState extends Loggable {
    public currentState: number = 0;
    // observables
    onStateInitialization$: BABYLON.Observable<number> = new BABYLON.Observable<number>();
    private onAfterStateInit$: BABYLON.Observable<number> = new BABYLON.Observable<number>();

    constructor() {
        super("GameLoadingState");
        this.onStateInitialization$.notifyObservers(0);
    }

    public nextStage(): void {
        this.currentState += 1;
        this._onStateChanged(this.currentState);
    }

    private _onStateChanged(stateNum: number): void {
        this.onStateInitialization$.notifyObservers(stateNum);
        this.onAfterStateInit$.notifyObservers(stateNum);
    }

    public setStateTo(stateNum: number) {
        this.currentState = stateNum;
        this._onStateChanged(stateNum);
    }

}

export class GameLoadingStateNums {
    public static get PRE_EVENTS() { return 0; }
    public static get INFRASTRUCTURE() { return 1; }
    public static get ENTITY_HOLDERS() { return 2; }
    public static get ENTITY_MANAGERS() { return 3; }
    public static get MAP_CREATION() { return 4; }
    public static get SCENE_CREATIONS() { return 5; }
}