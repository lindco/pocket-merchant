export class JsonDataFile {
    key: string;
    fileNameWithoutEnding: string;
    data: any | undefined = undefined;
    isLoaded: boolean = false;

    constructor(key: string, fileNameWithoutEnding: string) {
        this.fileNameWithoutEnding = fileNameWithoutEnding;
        this.key = key;
    }

    get fileName(): string {
        return `default-${this.fileNameWithoutEnding}.json`;
    }

    public setData(data: any): void {
        this.data = data;
        this.isLoaded = true;
    }

    toString(): string {
        const keysCount = Object.keys(this.data).length;
        return `<JsonDataFile \"${this.key}\" keys=${keysCount}>`;
    }
}

export class JsonDataFiles {
    public static get BUILDING_TYPES() { return "BUILDINGS"; }
    public static get MOUNTS() { return "MOUNTS"; }
    public static get GEAR_TYPES() { return "GEAR_TYPES"; }
    public static get CHARACTER_NAMES() { return "CHARACTER_NAMES"; }
    public static get ALL() { return [
        JsonDataFiles.BUILDING_TYPES, JsonDataFiles.MOUNTS, JsonDataFiles.GEAR_TYPES, JsonDataFiles.CHARACTER_NAMES
    ]; }
}