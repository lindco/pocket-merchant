// common
import { Loggable } from "../../../../common/models/log";

export class ArmyCategory extends Loggable {
    num: number;

    constructor(armyCatNum: number)  {
        super("ArmyCategory");
        this.num = armyCatNum;
    }

    get iconName(): string {
        switch (this.num) {
            case ArmyCategory.CITY_MILITIA || ArmyCategory.CITY_PATROL: return "defense";
            case ArmyCategory.FACTION_ARMY: return "attack";
            case ArmyCategory.CARAVAN || ArmyCategory.CARAVAN_PLAYER: return "trader";
            default: return "army";
        }
    }

    get name(): string {
        switch(this.num) {
            case ArmyCategory.CITY_MILITIA: return "Militia";
            case ArmyCategory.CITY_PATROL: return "Patrol";
            case ArmyCategory.FACTION_ARMY: return "Expedition";
            case ArmyCategory.CARAVAN: return "Caravan";
            case ArmyCategory.CARAVAN_PLAYER: return "Caravan";
            default: return "Army";
        }
    }

    /* FLAG GETTERS */
    get isMilitia(): boolean { return this.num == ArmyCategory.CITY_MILITIA; }
    get isCaravan(): boolean { return this.num == ArmyCategory.CARAVAN || this.num == ArmyCategory.CARAVAN_PLAYER; }
    get isWild(): boolean { return this.num == ArmyCategory.WILD; }

    static get CITY_MILITIA(): number { return 0; }
    static get CITY_PATROL(): number { return 1; }
    static get FACTION_PATROL(): number { return 2; }
    static get FACTION_ARMY(): number { return 3; }
    static get CARAVAN(): number { return 4; }
    static get CARAVAN_PLAYER(): number { return 5; }
    static get WILD(): number { return 6; }
}