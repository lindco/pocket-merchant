import { IBaseEvent } from '../../../../common/models/events';
import { MapPos } from "../../../world/pos";
import { Unit } from "../unit";
import { TradeNode } from "../../trade/trade-node/trade-node";
import { City } from "../../city/city";
import { RandomElement } from "../../../../helpers/random";
import { IBaseCommand } from '../../../../common/models/command';
import { OwnedLocation } from '../../../world/location';
import { ArmyCategory } from './army-category';
import { IUnitType } from "../interfaces/unit-type.interface";
import { IUnitArmy } from "../interfaces/unit-army.interface";
import { IUnitInstance } from "../interfaces/unit.interfaces";
import * as BABYLON from "babylonjs";
import { ArmyResourceHandler } from "./army-resource-handler";
import { IFaction } from "../../faction/faction-interfaces";


/* == ARMY MODEL == */
export class UnitArmy extends OwnedLocation implements IUnitArmy {
    home: City;
    armyCat: ArmyCategory;
    // flags
    isFortified: boolean = false;
    // parent
    _ownerKey: string;
    parentCity: City;
    parentFaction: IFaction;
    parentIsPlayer: boolean = false;
    // commands
    commands: IBaseCommand[];
    onCommand$ = new BABYLON.Observable<IBaseCommand>();
    // military list
    units: Array<Unit> = [];
    // trade & resources
    resourceHandler: ArmyResourceHandler = new ArmyResourceHandler(this);
    tradeNode: TradeNode;

    // observables
    onUnitAdded$ = new BABYLON.Observable<IUnitInstance>();

    constructor(armyCatNum: number, pos: MapPos, overwriteStorageSpace?: number, targetPos?: MapPos) {
        super(-1, pos, "Unnamed Army", null);
        this.armyCat = new ArmyCategory(armyCatNum);
        // optional parameters
        if ( overwriteStorageSpace ) { this.resourceHandler.storageSpace = overwriteStorageSpace; }
        this.tradeNode = new TradeNode( this.resourceHandler.storageSpace, true , this);
        // give random name
        this.name = this.getRandomName();
        // register
        this.registerToObservables();
    }

    private registerToObservables(): void {
        globalThis.CITIES.onCitiesSpawned$.add(
            () => { }
        );
        this.onUnitAdded$.add(
            (unit) => {
                if ( this.parentCity ) { this.parentCity.might += 1; }
            }
        )
    }

    addUnit(unitObj: Unit): void {
        // set army in Unit model
        unitObj.army = this;
        // add to military list
        this.units.push(unitObj);
        this.onUnitAdded$.notifyObservers(unitObj);
    }

    addUnitOfType(typeId: string) {
        const newUnit = globalThis.UNITS.createUnit(typeId, this.pos);
        this.addUnit(newUnit);
    }

    /* ARMY GETTERS */
    get unitsTypeKeys(): Array<string> {
        const unitKeys: Array<string> = [];
        for (const unit of this.units) {
            if (unit) { unitKeys.push(unit.type.key) }
        }
        return unitKeys
    }

    /* SIMPLIFIED GETTERS FOR COMMONLY USED PROPERTIES */
    get armyTypeIcon(): string { return this.armyCat.iconName }
    get count(): number { return this.units.length; }
    get length(): number { return this.units.length; }
    get isCaravan(): boolean { return this.armyCat.isCaravan; }

    get isInField(): boolean {
        if (this.armyCat.isMilitia) { return false; } 
        const isInCity = globalThis.CITIES && globalThis.CITIES.getCityAt(this.pos);
        return ! isInCity;

    }

    /* return whether this army is of given ARMY_TYPE number */
    isOfType(armyType: number): boolean {
        return this.armyCat.num == armyType;
    }

    get unitTypes(): Array<IUnitType> {
        const unitTypes: Array<IUnitType> = [];
        for (const unit of this.units) {
            if (! unitTypes.includes(unit.type)) {
                unitTypes.push(unit.type);
            }
        }
        return unitTypes;
    }

    get ownerFaction(): IFaction {
        if (! globalThis.FACTIONS) {
            this.warn(`cant get army owner faction: FactionManager not found`); return;
        }
        return globalThis.FACTIONS.getFactionByKey(this._ownerKey);
    }

    private getRandomName(): string {
        // get prefix
        const randomPrefixes = ["Strong", "Zelous", "Loyal", "Free", "Bold", "Quick", "Victorious", "Vigilant", "Blessed"];
        // use city or empire name as prefix for militias, patrols or attack armies
        let prefix: string = RandomElement.of(randomPrefixes);
        // change prefix for player or city owned armies
        if (this.parentIsPlayer) { prefix = "Player"; }
        if (this.parentCity) { prefix = this.parentCity.name; }
        // postfix
        const randomNouns = ["Army", "Force", "Expedition", "Soldiers", "Troops", "Banners", "Spears", "Swords"];
        let postfix: string = RandomElement.of(randomNouns);
        return prefix + " " + postfix;
    }

    /* AVERAGE UNIT PROPERTIES */
    get averageDamage(): number {
        let totalDmg: number = 0;
        for (const unit of this.units) {
            const unitBaseDmg = unit.type.gear.weapon.baseAtk;
            totalDmg += unitBaseDmg;
        }
        // average military damage
        return totalDmg / this.units.length;
    }

    get averageMovement(): number {
        let totalMovement: number = 0;
        for (const unit of this.units) {
            const unitMovement = unit.type.movement;
            totalMovement += unitMovement;
        }
        return totalMovement / this.units.length;
    }

    get averageHealth(): number {
        let totalHp: number = 0;
        for (const unit of this.units) {
            const unitHp = unit.hp;
            totalHp += unitHp;
        }
        return totalHp / this.units.length;
    }

    get armyTypeName(): string { return this.armyCat.name; }

    /* EVEN HANDLING */
    onEvent(ev: IBaseEvent) {
        super.onEvent( ev );
    }

    /* COMMANDS */
    onTurn() {
        
    }

    addCommand(cmd: IBaseCommand): void {
        
    }

    onCommand(cmd: IBaseCommand): void {
        
    }

    /* MOVEMENT */
    moveInDir(dirStr: string) {
        
    }

    /* STATIC & TOSTRING */
    toString(): string {
        const unitsStr = this.unitsTypeKeys.join(", ");
        return `<Army "${this.name}" cat=${this.armyCat.name} units=[${unitsStr}]>`;
    }

    static get DEFAULT_MODEL() { return "army_camp"; }
}

/* == ARMY CONSTANTS == */