import { UnitArmy } from "./army";
import { StoredResource } from "../../trade/resource/stored-resource";
import { Loggable } from "../../../../common/models/log";
import { IStorageEntity } from "../../trade/interfaces/resource.interfaces";

/**
 * gets all resource & trade related functions delegated from {@link UnitArmy}
 */
export class ArmyResourceHandler extends Loggable implements IStorageEntity {
    army: UnitArmy;
    storedResources: Array<StoredResource> = [];
    storageSpace: number = 4;

    public constructor(army: UnitArmy) {
        super("ArmyResourceHandler");
        this.army = army;
    }

    removeSingleStoredResource(resId: string): void {
        let lastStored: StoredResource = null;
        let lastStoredIndex: number = -1;
        let _index: number = 0;
        for (const storedRes of this.storedResources) {
            if( storedRes.resId == resId ) {
                lastStored = storedRes;
                lastStoredIndex = _index;
            }
            _index += 1;
        }
        if ( lastStored ) {
            this.storedResources.splice(lastStoredIndex, 1);
            this.log(`1 ${resId} removed from storage`);
        }
    }

    getStoredRes( resId: string, cheapest?: boolean ): StoredResource {
        let ofResType: Array<StoredResource> = [];
        for ( const stored of this.storedResources ) {
            if (stored.resId == resId) { ofResType.push( stored ); }
        }
        if ( ofResType.length < 1 ) {
            this.error(`cant get stored resource ${resId}: none in storage`); return;
        }
        if (! cheapest ) { return ofResType[0]; }
        else {
            let sortedByPrice: Array<StoredResource> = ofResType.sort( function(stored1, stored2) {
                if ( stored1.price > stored2.price ) { return 1; }
                else if ( stored1.price < stored2.price ) { return -1; }
                return 0;
            });
        }
    }

    get freeStorageSpace(): number {
        return this.storageSpace - this.storedResources.length;
    }

}