// world
import { MapPos } from "../../world/pos";
// models
import { CommandableMapEntity, IUniqueInstance } from "../../../common/models/entity/entity";
import { UnitArmy } from "./army/army";
import { IUnitInstance } from "./interfaces/unit.interfaces";
import { IUnitType } from "./interfaces/unit-type.interface";

/* == UNIT MODEL == */
export class Unit extends CommandableMapEntity implements IUnitInstance {
    num: number;
    type: IUnitType;
    pos: MapPos;
    hp: number;
    mp: number;
    army: UnitArmy = null;

    constructor(typeId: string, pos: MapPos, army?: UnitArmy) {
        super(globalThis.UNITS.nextUnitId(), pos);
        this.type = globalThis.UNIT_TYPES.get(typeId);
        // assign to army when given
        if (army) { this.army = army; }
        this.hp = this.type.health;
        this.mp = this.type.movement;
    }

    toString(): string {
        let unitPos: MapPos = this.pos;
        if (! unitPos ) {
            unitPos = this.army.pos;
        }
        return `<Unit ${this.num} "${this.type.id}" pos=${unitPos.asString()} hp=${this.hp} mp=${this.mp}>`;
    }
}

/* == UNIT MOVEMENT == */