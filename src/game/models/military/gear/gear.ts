import { BaseAbility } from "../../../../common/models/ability";
import { Loggable } from "../../../../common/models/log";
import { Properties } from "../../../../common/models/properties/properties";
import { IUnitArmour, IUnitGear, IUnitWeapon } from "../interfaces/unit-gear.interfaces";

/* == INTERFACES == */


/* WEAPON MODEL */
export class UnitWeapon implements IUnitWeapon {
    id: string;
    properties: Properties = new Properties();
    abilities: Array<BaseAbility>;

    constructor(id: string) {
        this.abilities = [];
    }

    /* PROPERTY GETTERS */
    get name(): string { return this.properties.getStr("name"); }
    get baseAtk(): number { return this.properties.getNum("baseAtk"); }
    get criticalChance(): number { return this.properties.getNum("criticalChance"); }
    get distance(): number { return this.properties.getNum("distance"); }
    get weaponCat(): number { return this.properties.getNum("weaponCat"); }

    private atkModVsArmourCat(armourCat: number): number {
        // magic attack
        if (this.weaponCat == WEAPON_CAT_MAGIC) { return 1;}
        // pierce vs heavy
        const pierceVsHeavy: boolean = this.weaponCat == WEAPON_CAT_PIERCE && armourCat == ARMOUR_CAT_HEAVY;
        if ( pierceVsHeavy ) { return 2; }
        // bash vs light
        const bashVsLight: boolean = this.weaponCat == WEAPON_CAT_BASH && armourCat == ARMOUR_CAT_LIGHT;
        if ( bashVsLight ) { return 2; }
        // bash vs heavy 
        const bashVsHeavy: boolean = this.weaponCat == WEAPON_CAT_BASH && armourCat == ARMOUR_CAT_HEAVY;
        if ( bashVsHeavy ) { return .6; }
        // fire vs vehicles
        const fireVsVehicle: boolean = this.weaponCat == WEAPON_CAT_FIRE && armourCat == ARMOUR_CAT_VEHICLE;
        if ( fireVsVehicle ) { return 3; }
        return 1;
    }

    damageVsArmour(armourId: string): number {
        const baseDmg = this.baseAtk;
        const enemyArmourCat = new UnitArmour(armourId).armourCat;
        const dmgMod = this.atkModVsArmourCat(enemyArmourCat);
        return baseDmg * dmgMod;
    }

    static FromJson(jsonObj: {}): UnitWeapon {
        const weaponObj = new UnitWeapon(jsonObj["id"]);
        weaponObj.properties.addString("name", jsonObj["name"]);
        weaponObj.properties.addFloat("baseAtk", jsonObj["baseAtk"]);
        weaponObj.properties.addFloat("criticalChance", jsonObj["criticalChance"]);
        weaponObj.properties.addInteger("distance", jsonObj["distance"]);
        weaponObj.properties.addInteger("dmgType", jsonObj["dmgType"]);
        // add weapon abilities
        for (const abilityStr of jsonObj["abilities"]) {
            const ability = BaseAbility.FromString(abilityStr);
            weaponObj.abilities.push(ability);
        }
        return weaponObj;
    }
}

export class UnitArmour implements IUnitArmour {
    id: string;
    properties: Properties = new Properties();

    constructor(id: string) {
        this.id = id;
    }

    /* GETTERS & SETTERS */
    get name(): string { return this.properties.getStr("name"); }
    get category(): number { return this.properties.getNum("category"); }
    get armourCat(): number { return this.category; }
    get extraDefense(): number { return this.properties.getNum("extraDefense"); }

    static FromJson(jsonObj: {}): UnitArmour {
        const armorObj: UnitArmour = new UnitArmour(jsonObj["id"]);
        armorObj.properties.addString("name", jsonObj["name"]);
        armorObj.properties.addFloat("category", jsonObj["category"]);
        armorObj.properties.addFloat("extraDefense", jsonObj["extraDefense"]);
        armorObj.properties.addFloat("rangedMod", jsonObj["rangedMod"]);
        return armorObj;
    }
}

/* WEAPON & ARMOUR SET */
export class UnitGear extends Loggable implements IUnitGear {
    str: string;
    hasLoaded: boolean = false;
    _weaponId: string;
    _armourId: string;

    constructor(str: string, autoInitialize?: boolean) {
        super("UnitGear");
        this.str = str;
        const weaponArmourIds = str.split(" ");
        this._weaponId = weaponArmourIds[0];
        this._armourId = weaponArmourIds[1];
        if (autoInitialize && autoInitialize == true) {
            this.fillGearTypeObjects();
        }
    }

    fillGearTypeObjects() {
        if(! globalThis.GEAR ) {
            this.warn(`cant fill gear type objects; UnitGearManager not found`);
            return;
        }
        // this.weapon = globalThis.GEAR.getWeaponById(this._weaponId);
        // this.armour = globalThis.GEAR.getArmorById(this._armourId);
        this.hasLoaded = true;
    }

    /* GETTERS */
    get weapon(): UnitWeapon {
        if (!globalThis.GEAR) {
            this.error(`cant load weapon "${this._weaponId}"; UnitGearManager not found`);
            return;
        }
        return globalThis.GEAR.getWeaponById(this._weaponId);
    }

    get armour(): UnitArmour {
        if (!globalThis.GEAR) {
            this.error(`cant load armor "${this._armourId}"; UnitGearManager not found`);
            return;
        }
        return globalThis.GEAR.getArmorById(this._armourId);
    }
}

/* == GEAR CONSTANTS */
// weapons
export const WEAPON_CRITICAL_DMG_MOD = 2;
// damage categories
export const WEAPON_CAT_CUT = 0;
export const WEAPON_CAT_PIERCE = 1;
export const WEAPON_CAT_BASH = 2;
export const WEAPON_CAT_FIRE = 3;
export const WEAPON_CAT_MAGIC = 4;
// armour categories
export const ARMOUR_CAT_LIGHT = 1;
export const ARMOUR_CAT_MEDIUM = 2;
export const ARMOUR_CAT_HEAVY = 3;
export const ARMOUR_CAT_VEHICLE = 8;