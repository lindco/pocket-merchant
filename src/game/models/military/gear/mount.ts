// common 
import { Loggable } from "../../../../common/models/log";
// world 
import { TerrainTypes } from "../../../world/tile/terrain-types";

/* == INTERFACES == */


/* == MOUNT MODEL == */
export class UnitMount extends Loggable implements IUnitMount {
    id: string;
    mountCatNum: number;
    isLand: boolean;
    baseMovement: number = 2;
    baseCargoSpace: number = 0;
    baseHealth: number = 4;
    // modifiers
    private modifiersStr: string;
    moveModifiers: Array<IMovementModifier> = [];

    constructor(isLand: boolean, baseHealth: number, baseMovement: number, modifiersStr: string, baseCargoSpace?: number) {
        super("UnitMount");
        this.isLand = isLand;
        this.baseHealth = baseHealth;
        this.baseMovement = baseMovement;
        this.modifiersStr = modifiersStr;
        this.moveModifiers = UnitMount.TerrainModsFromString(modifiersStr);
        if (baseCargoSpace) { this.baseCargoSpace = baseCargoSpace; }
        // assign cat
        this.assignMountCategory();
    }

    private assignMountCategory() {
        this.mountCatNum = MOUNT_CAT_INF;
        if (! this.id) { return; }
        // vehicle?
        if (this.id.includes("ship")) { this.mountCatNum = MOUNT_CAT_VEH; } 
        else if (this.id.includes("vehicle")) { this.mountCatNum = MOUNT_CAT_VEH; }
        // or cavalry?
        else if (this.baseMovement > 3) { this.mountCatNum = MOUNT_CAT_CAV; }
    }

    static TerrainModsFromString(modifiersStr: string): Array<IMovementModifier> {
        let modifiers: Array<IMovementModifier> = [];
        const splitted = modifiersStr.split(" ");
        for (const modifierStr of splitted) {
            let singleModifier: UnitMovementModifier = UnitMovementModifier.FromString( modifierStr );
            modifiers.push( singleModifier );
        }
        return modifiers;
    }
}

/* == MOVEMENT MODIFIER MODEL == */
export class UnitMovementModifier implements IMovementModifier {
    terrainId: string;
    movementPercent: number;
    private modifier: number;

    constructor(terrainId: string, percent: number) {
        this.terrainId = terrainId;
        this.movementPercent = percent;
        this.modifier = percent / 100;
    }

    static FromString(singleModifierStr: string): UnitMovementModifier {

        return null;
    }
}

/* == MOVE TYPE LIST == */
export class UnitMovementTypes {

}

function getTerrainTypes(): Array<string> {
    return TerrainTypes.LIST;
}

/* == CONSTANTS == */
// categories
export const MOUNT_CAT_INF = 0;
export const MOUNT_CAT_CAV = 1;
export const MOUNT_CAT_VEH = 2;
export const MOUNT_CAT_NAMES = ["Infantry", "Cavalry", "Vehicle"];
// movement modifier strings
export const MOVEMOD_DECR1 = "-";
export const MOVEMOD_DECR2 = "--";
export const MOVEMOD_ZERO = "0";
export const MOVEMOD_INCR1 = "+";
export const MOVEMOD_INCR2 = "++";
// mount movement modifiers
export const MOVEMOD_STR_DEFAULT = "M- F- D- S- W0";
export const MOVEMOD_STR_FOOT = MOVEMOD_STR_DEFAULT;
export const MOVEMOD_STR_HORSE = "M-- F-- D- S- W0";
export const MOVEMOD_STR_CAMEL = "M- F-- D+ S- G- W0";
export const MOVEMOD_STR_VEHICLE = "M0 F-- S- W0";
export const MOVEMOD_STR_SHIP = "W+ G0 F0 D0 S0 M0";
// mount json fields
export const JSON_FIELD_MP = "mp";
export const JSON_FIELD_TERRMOD = "terrainModifiers";