import { ICommandableMovableEntity } from "../../../../common/models/entity/entity";
import { ArmyCategory } from "../army/army-category";
import { City } from "../../city/city";
import { Unit } from "../unit";
import { TradeNode } from "../../trade/trade-node/trade-node";
import * as BABYLON from 'babylonjs';
import { IUnitInstance } from "./unit.interfaces";
import { ArmyResourceHandler } from "../army/army-resource-handler";
import { IFaction } from "../../faction/faction-interfaces";

export interface IUnitArmy extends ICommandableMovableEntity {
    name: string;
    armyCat: ArmyCategory;
    home: City;
    armyTypeName: string;
    // parent
    _ownerKey: string;
    ownerFaction: IFaction;
    parentCity: City;
    parentFaction: IFaction;
    parentIsPlayer: boolean;
    // army type
    armyTypeIcon: string;
    // flags
    isInField: boolean;
    isFortified: boolean;
    isCaravan: boolean;
    // units
    units: Array<Unit>;
    addUnit(unitObj: Unit);
    addUnitOfType(typeId: string);
    // storage
    resourceHandler: ArmyResourceHandler;
    tradeNode: TradeNode;
    // observables
    onUnitAdded$: BABYLON.Observable<IUnitInstance>;
}