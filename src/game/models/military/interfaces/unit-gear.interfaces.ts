import { Properties } from "../../../../common/models/properties/properties";
import { BaseAbility } from "../../../../common/models/ability";

export interface IUnitWeapon {
    id: string;
    properties: Properties;
    baseAtk: number;
    distance: number;
    weaponCat: number;
    abilities: Array<BaseAbility>;
    damageVsArmour(armourId: string): number;
}

export interface IUnitArmour {
    id: string;
    armourCat: number;
}

export interface IUnitGear {
    str: string;
    weapon: IUnitWeapon;
    armour: IUnitArmour;
}