interface IUnitMount {
    // basic properties
    id: string;
    mountCatNum: number;
    // health & cargo
    baseHealth: number;
    baseCargoSpace: number;
    // movement
    baseMovement: number;
    moveModifiers: Array<IMovementModifier>;
}

interface IMovementModifier {
    terrainId: string;
    movementPercent: number;
}