import { IUniqueInstance } from "../../../../common/models/entity/entity";
import { MapPos } from "../../../world/pos";
import { IUnitType } from "./unit-type.interface";

export interface IUnitInstance extends IUniqueInstance  {
    num: number;
    type: IUnitType;
    pos: MapPos;
    hp: number;
    mp: number;
}

export interface IUnitMover {
    maxMovement: number;
    currentMovement: number;
    canMoveInDir(dirStr: string): boolean;
    moveInDir(dirStr: string, force?: boolean): boolean;
}