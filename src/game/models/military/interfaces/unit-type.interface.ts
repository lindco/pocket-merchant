import { Timespan } from "../../../../common/models/timespan";
import { UnitGear } from "../gear/gear";
import { AbilityList } from "../../../../common/models/ability";


export interface IUnitType {
    // basic properties
    id: string;
    key: string;
    name: string;
    // flags
    isLand: boolean;
    timespan: Timespan;
    restrictedToCultures: Array<string>;
    // inherited properties
    health: number;
    movement: number;
    cargoSpace: number;
    gear: UnitGear;
    abilities: AbilityList;
}