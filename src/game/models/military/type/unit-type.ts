import { AbilityList } from "../../../../common/models/ability";
import { Loggable } from "../../../../common/models/log";
import { Timespan } from "../../../../common/models/timespan";
import { UnitGear, UnitWeapon, UnitArmour } from "../gear/gear";
import { UnitMount } from "../gear/mount";
import { IUnitType } from "../interfaces/unit-type.interface";

export class UnitType extends Loggable implements IUnitType {
    // basic properties
    id: string;
    name: string;
    // gear && mount
    _gearStr: string;
    gear: UnitGear;
    _mountId: string;
    _mountObj: UnitMount;
    // flags
    isTrader: boolean = false;
    isWild: boolean = false;
    restrictedToCultures: Array<string> = null;
    // available between years
    timespan: Timespan = null;
    _timespanStr: string = null;
    // abilities
    abilities: AbilityList = null;
    // restricted cultures


    constructor(id: string, name: string, mountId: string, gearStr: string, isTrader?: boolean, 
        abilitiesStr?: string, isWild?: boolean, timespanStr?: string, restrictedToCultures?: Array<string>) {
        super("UnitType");
        // set basic properties
        this.id = id;
        this.name = name;
        // set military mount, gear & abilities
        this.setMountAndGear(mountId, gearStr);
        this.abilities = AbilityList.FromString(abilitiesStr);
        // set optional flags
        if (isWild) { this.isWild = true; }
        if (isTrader && isTrader == true) { this.isTrader = true; } 
        this.timespan = Timespan.FromString(timespanStr);
        if (restrictedToCultures && restrictedToCultures.length > 0) {
            this.restrictedToCultures = restrictedToCultures;
        }
    }

    /* UNIT TYPE INITIALIZING */
    setMountAndGear(mountId: string, gearStr: string) {
        this.setMountById(mountId);
        this.setGearFromStr(gearStr);
    }

    setGearFromStr(gearStr: string) {
        this._gearStr = gearStr;
        this.gear = new UnitGear(gearStr);
    }

    setMountById(mountId: string) {
        this._mountId = mountId;
    }

    /* GEAR & ARMOR */
    get weapon(): UnitWeapon {
        if (! globalThis.GEAR) { this.warn(`cannot get weapon; UnitGearManager not found`); return; }
        return globalThis.GEAR.getWeaponById(this.gear._weaponId);
    }

    get armour(): UnitArmour {
        if (! globalThis.GEAR) { this.warn(`cannot get armor; UnitGearManager not found`); return; }
        return globalThis.GEAR.getArmorById(this.gear._armourId);
    }

    get mount(): UnitMount {
        if (! globalThis.GEAR) { this.warn(`cannot get mount; UnitGearManager not found`); return; }
        return globalThis.GEAR.getMountById(this._mountId);
    }

    /* UNIT TYPE GETTERS */
    toString(): string {
        return `<UnitType "${this.id}" isLand=${this.isLand} health=${this.health} mp=${this.movement} 
            mount="${this._mountId}" gear="${this._gearStr}" abilities=${this.abilities.length}>`;
    }

    get isLand(): boolean {
        if (this.mount) { return this.mount.isLand; }
        return true;
    }

    get movement(): number {
        if (this.mount) {
            let totalMovement: number = this.mount.baseMovement;
            // military type has movement point bonus or malus?
            totalMovement += this.abilities.getValue(ABILITY_BONUS_MOVEMENT);
            totalMovement -= this.abilities.getValue(ABILITY_MINUS_MOVEMENT);
            return totalMovement;
        }
        return 2;
    }

    get health(): number {
        if (this.mount) {
            let totalHealth: number = this.mount.baseHealth;
            // military type has health bonus or malus?
            totalHealth += this.abilities.getValue(ABILITY_BONUS_HEALTH);
            totalHealth -= this.abilities.getValue(ABILITY_MINUS_HEALTH);
            return totalHealth;
        }
        return 3;
    }

    get cargoSpace(): number {
        if (this.mount) { return this.mount.baseCargoSpace; }
        return 0;
    }

    get key(): string { return this.id; }

}

/* == UNIT TYPE CONSTANTS == */
export const ABILITY_BONUS_HEALTH = "bonus-health";
export const ABILITY_MINUS_HEALTH = "minus-health";
export const ABILITY_BONUS_MOVEMENT = "bonus-move";
export const ABILITY_MINUS_MOVEMENT = "minus-move";