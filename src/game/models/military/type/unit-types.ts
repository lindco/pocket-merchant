// common
import { Loggable } from "../../../../common/models/log";
// military models
import { UnitGear } from "../gear/gear";
import { UnitType } from "./unit-type";


/* == UNIT TYPES == */
export class UnitTypes {
    private _ids: Array<string> = [];
    unitTypes: Array<UnitType> = [];

    constructor() {
        /* ID_CHARS, NAME, MOUNT_ID GEAR_STR, IS_TRADER?, ABILITIES_STR?, IS_WILD?, TIMESPAN_STR */
        // traders
        this.add(new UnitType( ID_TRADER_DONKEY, "Donkey", "horse", "lance leather", true, "minus-move 1") );
        this.add(new UnitType( ID_TRADER_CAMEL, "Camel", "camel", "bow leather", true, "bonus-vs-type 45 heavy-horse") );
        this.add(new UnitType( ID_TRADER_COG_SMALL, "Small Cog", "small-ship", "bow vehicle", true) );
        this.add(new UnitType( ID_TRADER_COG_LARGE, "Large Cog", "large-ship", "crossbow heavy_vehicle", true) );
        this.add(new UnitType( ID_TRADER_GALLEY_SMALL, "Small Galley", "small-ship", "javelin vehicle", true) );
        this.add(new UnitType( ID_TRADER_GALLEY_LARGE, "Large Galley", "large-ship", "bow heavy_vehicle", true) );
        // military
        this.add(new UnitType( ID_MIL_BANDIT, "Bandit", "foot", "dagger cloth", false));
        this.add(new UnitType( ID_MIL_PEASANT, "Peasant", "foot", "spear cloth", false, "miss-chance 33, bonus-vs-type 33 horse") );
        this.add(new UnitType( ID_MIL_SOLDIER, "Soldier", "foot", "mace leather", false, "small-shield, fort-attack 20") );
        this.add(new UnitType( ID_MIL_SLINGER, "Slinger", "foot", "sling cloth", false, "miss-chance 50", false, "0-1100") );
        this.add(new UnitType( ID_MIL_ARCHER, "Archer", "foot", "bow cloth", false, "fort-defense 33, miss-chance 33") );
        this.add(new UnitType( ID_MIL_RAIDER, "Raider", "horse", "javelin bronze", false, "miss-chance 40") );
        this.add(new UnitType( ID_MIL_GUARD, "Guard", "heavy-foot", "helbard leather", false, "large-shield, bonus-morale 2") );
        this.add(new UnitType( ID_MIL_CROSSBOW, "Crossbowman", "foot", "crossbow bronze", false, "bonus-vs-type 33 heavy-foot, reload-turns 1") );
        this.add(new UnitType( ID_MIL_MERC, "Mercenary", "heavy-foot", "longsword iron", false, "minus-morale 1, fort-attack 33,") );
        this.add(new UnitType( ID_MIL_KNIGHT, "Knight", "heavy-horse", "sword iron", false, "bonus-morale 2", false, "900-1550") );
        // unique units
        this.add(new UnitType( ID_MIL_CONQUISTADOR, "Conquistador", "horse", "pistol bronze", false, "", false, "1400-1800", ["italian", "venetian"]) );
        this.add(new UnitType( ID_MIL_RANGER, "Ranger", "foot", "bow bronze", false, "terrain-knowledge 50 F, miss-chance 20, bonus-range 1", false, "0-1300", ["elven"]));
        this.add(new UnitType( ID_MIL_BERSERKER, "Beserker", "heavy-foot", "sword leather", false, "small-shield, bonus-morale 2, crush-shield 2, cause-fear 1", false, "0-1200", ["dwarven", "germanic"]) );
        // wild
        this.add(new UnitType( ID_WILD_WOLF, "Wolf", "horse", "bite cloth", false, "is-animal, minus-morale 1, minus-move 2", true));
        this.add(new UnitType( ID_WILD_BEAR, "Bear", "foot", "crush leather", false, "is-animal", true));
        this.add(new UnitType( ID_WILD_DEMON, "Demon", "heavy-foot", "bomb leather", false, "is-evil 2", true));
    }

    add(unitType: UnitType): void {
        this._ids.push(unitType.id);
        this.unitTypes.push(unitType);
    }

    get(typeId: string): UnitType {
        const typeIndex = this._ids.indexOf(typeId);
        return this.unitTypes[typeIndex];
    }

    /* returns all military types that are able to trade/form caravans */
    getTraderTypes(): Array<UnitType> {
        let traderTypes: Array<UnitType> = [];
        for (const unitType of this.getAll()) {
            if (unitType.isTrader == true) {
                traderTypes.push(unitType);
            }
        }
        return traderTypes;
    }

    getMilitaryTypes(): Array<UnitType> {
        let milTypes: Array<UnitType> = [];
        for (const unitType of this.getAll()) {
            if (unitType.isTrader == false) {
                milTypes.push(unitType);
            }
        }
        return milTypes;
    }

    getAll(): Array<UnitType> {
        return this.unitTypes;
    }

    getAllIds(): Array<string> {
        return ALL_IDS;
    }

    isValidUnitTypeId(id: string): boolean {
        return this.getAllIds().includes(id);
    }
}

/* == CONSTANTS == */
// trader units
export const ID_TRADER_DONKEY = "DN";
export const ID_TRADER_CAMEL = "CM";
export const ID_TRADER_COG_SMALL = "CS";
export const ID_TRADER_COG_LARGE = "CL";
export const ID_TRADER_GALLEY_SMALL = "GS";
export const ID_TRADER_GALLEY_LARGE = "GL";
export const ID_TRADER_TYPE_IDS = [
    ID_TRADER_DONKEY, ID_TRADER_CAMEL, ID_TRADER_COG_SMALL, ID_TRADER_COG_LARGE,
    ID_TRADER_GALLEY_SMALL, ID_TRADER_GALLEY_SMALL
];
// military units
export const ID_MIL_BANDIT = "BD";
export const ID_MIL_PEASANT = "PS";
export const ID_MIL_SOLDIER = "SO";
export const ID_MIL_SLINGER = "SL";
export const ID_MIL_ARCHER = "RC";
export const ID_MIL_RAIDER = "RD";
export const ID_MIL_GUARD = "GD";
export const ID_MIL_CROSSBOW = "XB";
export const ID_MIL_MERC = "MC";
export const ID_MIL_KNIGHT = "KN";
// unique military units
export const ID_MIL_CONQUISTADOR = "CQ";
export const ID_MIL_RANGER = "RN";
export const ID_MIL_BERSERKER = "BS";
export const ID_UNIQUE = [ ID_MIL_RANGER, ID_MIL_CONQUISTADOR, ID_MIL_BERSERKER ];
export const ID_STD_MILITARY_TYPE_IDS = [
    ID_MIL_BANDIT, ID_MIL_PEASANT, ID_MIL_SLINGER, ID_MIL_ARCHER, ID_MIL_RAIDER, 
    ID_MIL_GUARD, ID_MIL_CROSSBOW, ID_MIL_MERC, ID_MIL_KNIGHT
];
export const ID_MILITARY_TYPE_IDS = ID_STD_MILITARY_TYPE_IDS.concat(ID_UNIQUE);
// all military types
export const RECRUITABLE_IDS = ID_TRADER_TYPE_IDS.concat(ID_STD_MILITARY_TYPE_IDS);
// wild
export const ID_WILD_WOLF = "WL";
export const ID_WILD_BEAR = "BR";
export const ID_WILD_DEMON = "DN";
export const WILD_IDS = [ ID_WILD_WOLF, ID_WILD_BEAR, ID_WILD_DEMON ];
// complete military type id list
export const ALL_IDS = RECRUITABLE_IDS.concat(WILD_IDS)
// military categories
export const UNIT_CAT_INFANTRY = 1;
export const UNIT_CAT_CAVALRY = 2;
export const UNIT_CAT_HEAVY_CAV = 3;
export const UNIT_CAT_VEHICLE = 4;