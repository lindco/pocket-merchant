import { Properties } from "../../../../common/models/properties/properties";
import { MapPos } from "../../../world/pos";

export class GameOptions extends Properties {
    constructor() {
        super();
        this.loadDefaultGameOptions();
    }

    private loadDefaultGameOptions() {
        console.log(`<GameOptions> loading default options ..`);
        // map options
        this.addWorldPos(GameOptions.START_POS, new MapPos(100, 100));
        this.addInteger(GameOptions.MAP_VIEW_DISTANCE, 40);
        // map generation
        this.addBoolean(GameOptions.GENERATE_LANDSCAPE, false);
        this.addBoolean(GameOptions.GENERATE_LOCATIONS, false);
        this.addInteger(GameOptions.MAP_MAXCITIES, 30);
        // players
        this.addInteger(GameOptions.AI_FAMILIES, 5);
        console.log(`<GameOptions> ${this.props.length} options loaded`);
    }

    /* OPTION KEYS */
    // Map
    public static get GENERATE_LANDSCAPE() { return "generate-map"; }
    public static get GENERATE_LOCATIONS() { return "generate-locations"; }
    public static get MAP_MAXCITIES() { return "max-cities"; }
    public static get MAP_VIEW_DISTANCE() { return "view-distance"; }
    // Players
    public static get START_POS() { return "start-pos"; }
    public static get AI_FAMILIES() { return "max-families"; }
}