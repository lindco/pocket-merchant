import { ResourceStorage } from "../resource/resource-storage";
import { ITradeNode } from "../interfaces/trade-node.interface";
import { MarketTrade } from "../market-trade";
import { City } from "../../city/city";
import { TradeResource } from "../resource/resource";
import { StoredResource } from "../resource/stored-resource";
import { ITradeLocation } from "../../../world/interfaces/location.interfaces";

/* == MARKET MODEL == */
export class TradeNode extends ResourceStorage implements ITradeNode {
    /* markets have no maximal storage amount */
    /* justFor: dont create storage for all resources but for specified resource ids (in array) */
    fixedPrices: boolean = false;
    linked: Array<TradeNode> = [];
    parentLocation: ITradeLocation = null;
    history: Array<MarketTrade> = [];

    constructor(maxSpace: number, fixedPrices: boolean, parent: any, justFor?: Array<string>) {
        super(maxSpace, parent, justFor);
        if (parent instanceof City) {
            this.parentLocation = parent as ITradeLocation;
        }
        this.fixedPrices = fixedPrices;
    }

    asStoresResArr(): Array<StoredResource> {
        let storedResources: Array<StoredResource> = [];
        for (const resId of this.resIds) {
            storedResources.push( this.getStoredResource(resId) );
        }
        return storedResources;
    }

    add(resId: string, amount: number, price?: number): boolean {
        const success = super.add(resId, amount);
        if ( success ) {
            if (! this.fixedPrices) { this.reducePrice(resId, amount);
            } else if (price) { this._resPrice = price; }
            return true }
        return false;
    }

    remove(resId: string, amount: number, force?: boolean): boolean {
        const success = super.remove( resId, amount, force );
        if (! success) { this.increasePrice( resId, amount ); }
        return success;
    }

    /* TRADE NODE GETTERS */
    tradeIsExport(marketTrade: MarketTrade): boolean {
        return this.parentLocation.equals(marketTrade.source);
    }

    priceOf(resId: string): number {
        return Math.round(this._resPrice[resId]);
    }

    /* returns the difference to a resource (ids) default/start price */
    defaultPriceDiffOf(resId: string): number {
        let tradeRes: TradeResource = globalThis.RESOURCE_TYPES.get(resId);
        // replace with special resource when loaded
        if (globalThis.TRADE && globalThis.TRADE.hasSpecialResLoaded) {
            tradeRes = globalThis.TRADE.getRes(resId);
        }
        return tradeRes.defaultPrice - this.priceOf(resId);
    }

    /* returns whether a resource has minAmount left in storage */
    hasEnough(resId: string, minAmount: number): boolean {
        return this.amountOf(resId) >= minAmount;
    }

    /* returns whether the requirements to produce resId in given quantity are met */
    hasRequirementsFor(resId: string, toProduce: number): boolean {
        let resource: TradeResource = globalThis.RESOURCE_TYPES.get(resId);
        if (globalThis.TRADE && globalThis.TRADE.hasSpecialResLoaded) {
            resource = globalThis.TRADE.getRes(resId);
        }
        if (resource.hasRequirement) {
            for (const requiredResId of resource.requires) {
                if (! this.hasEnough(requiredResId, toProduce)) {
                    // make more expensive
                    this.increasePrice(resId, toProduce);
                    return false;
                }
            }
        }
        return true;
    }

    increasePrice(resId: string, unmetAmount: number) {
        const increaseAmount = Math.round(this.priceOf(resId) /20);
        this._resPrice[resId] += increaseAmount;
    }

    reducePrice(resId: string, unmetAmount: number) {
        const decreaseAmount = Math.round(this.priceOf(resId) /20);
        this._resPrice[resId] -= decreaseAmount;
    }

    /* TRADE NOTE GETTER METHODS */
    /* returns an array of 3 resources that this storage has the most of */
    getMostStored(): Array<StoredResource> {
        let sortedByAmount: Array<StoredResource> = this.asStoresResArr().sort( function(stored1, stored2) {
            if (stored1.amount < stored2.amount) { return 1; }
            if (stored1.amount > stored2.amount) { return -1; }
            return 0;
        });
        //  console.log(sortedByAmount);
        return [ sortedByAmount[0], sortedByAmount[1], sortedByAmount[2] ];
    }

    compareResourceProfitWith( resId: string, otherMarket: TradeNode ): number {
        const otherMarketStoredRes = otherMarket.getStoredResource( resId );
        return this.priceOf( resId ) - otherMarketStoredRes.price;
    }

    getProfitFor(storedRes: StoredResource): number {
        if (! storedRes || ! storedRes.price) {
            this.error(`cant get profit for resource ${storedRes.resId}; no price given`); return;
        }
        const storedPrice = storedRes.price;
        const marketPrice = this.priceOf(storedRes.resId);
        return storedPrice - marketPrice;
    }

    getTradeProfitsBetween( otherMarket: TradeNode ): Array<MarketTrade> {
        let profits: Array<MarketTrade> = [];
        for (const resId of globalThis.RESOURCE_TYPES.getIds()) {
            if ( this.amountOf( resId ) > 0 || otherMarket.amountOf( resId ) > 0 ) {
                const otherRes = otherMarket.getStoredResource( resId );
                const profit = this.getProfitFor( otherRes );
                profits.push( new MarketTrade( null, resId, profit ));
            }
        }
        return profits;
    }

    getBestToBuy(amount?: number): Array<StoredResource> {
        const storage = this;
        // sort StoredResources by price difference
        let sortedByPriceDiff: Array<StoredResource> = this.asStoresResArr().sort( function(stored1, stored2) {
            const priceDiff1 = storage.defaultPriceDiffOf( stored1.resId );
            const priceDiff2 = storage.defaultPriceDiffOf( stored2.resId );
            if (priceDiff1 < priceDiff2) { return 1; }
            if (priceDiff1 > priceDiff2) { return -1; }
            return 0;
        });
        if (amount) { return sortedByPriceDiff.slice(0, amount);
        } else { return sortedByPriceDiff; }
    }

    getStoredResource( resId: string ): StoredResource {
        let stored: StoredResource = super.getStoredResource( resId ); 
        stored.price = this.priceOf( resId );
        return stored;
    }

    getEmptyStoredResArr(): Array<StoredResource> {
        const maxAmount = 3;
        let emptyResources: Array<StoredResource> = [];
        for (const storedRes of this.asStoresResArr()) {
            if (storedRes.amount <= 0 && emptyResources.length < maxAmount) {
                emptyResources.push(storedRes)
            }
        }
        return emptyResources;
    }

    toString(): string {
        return `<TradeNode city=\"${this.parentLocation.name}\" connections=${this.linked.length}>`;
    }
}