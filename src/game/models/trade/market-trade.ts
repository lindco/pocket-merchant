import { ITradeNode } from "./interfaces/trade-node.interface";
import { ITradeLocation } from "../../world/interfaces/location.interfaces";

/* == TRADE INTERFACES == */
export interface ITrade {
    turn: number;
    profit: number;
    absProfit: number;
    resId: string;
    market: ITradeNode;
    // optional
    target: ITradeLocation;
    source: ITradeLocation;
    isExport: boolean;
    // target
    otherLocation: ITradeLocation;
    otherLocationName: string;
    // string getters
    fromToStr: string;
    iconName: string;
    plusMinusStr: string;
}

/* == MARKET TRADE MODEL == */
export class MarketTrade implements ITrade {
    turn: number;
    profit: number;
    resId: string;
    market: ITradeNode;
    // cities
    target: ITradeLocation;
    source: ITradeLocation;
    isExport: boolean = false;

    constructor(turn: number, resId: string, profit: number, target?: ITradeLocation,
        source?: ITradeLocation, marketOverride?: ITradeNode) {
        this.turn = turn;
        this.resId = resId;
        this.profit = profit;
        // source & target cities
        if ( source ) {
            this.source = source;
            this.market = source.tradeNode;
            if ( target ) { this.target = target; }
        } 
        if ( marketOverride ) { this.market = marketOverride; }
    }

    /* GETTER METHODS */
    get fromToStr(): string {
        if (this.isExport) { return "t."; }
        return "f.";
    }

    get iconName(): string {
        if (this.isExport) { return "export"; }
        return "import";
    }

    get plusMinusStr(): string {
        if (this.isExport) { return "+"; }
        return "-";
    }

    get otherLocation(): ITradeLocation {
        if (this.isExport) { return this.target; }
        return this.source;
    }

    get otherLocationName(): string {
        if (this.otherLocation) { return this.otherLocation.name; }
        return "nowhere";
    }

    private get absProfitModifier(): number {
        if (! this.isExport) { return -1; }
        return 1;
    }

    get absProfit(): number {
        return this.profit * this.absProfitModifier;
    }
}