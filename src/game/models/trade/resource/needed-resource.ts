import { StoredResource } from "./stored-resource";

/**
 * Used wherever an amount if a resource is required for something, per example to build a house or supply a class.
 * The needed-number is the absolute amount of this resource already delivered.
 */
export class NeededResource {
    resId: string;
    supplied: number;
    needed: number;

    constructor(resId: string, supplied: number, needed: number) {
        this.resId = resId;
        this.supplied = supplied;
        this.needed = needed;
    }

    public static FromStoredRes(storedRes: StoredResource): NeededResource {
        const resId = storedRes.resId;
        return new NeededResource(resId, storedRes.amount, 0);
    }
}