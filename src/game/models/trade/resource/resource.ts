import { ColorPalette } from "../../../../helpers/colorpalette";
import { Strings } from "../../../../helpers/strings";
import { ITradeResource } from "../interfaces/resource.interfaces";

export class TradeResource implements ITradeResource {
    id: string;
    name: string;
    color: Array<number>;
    defaultPrice: number;
    requires: Array<string> = [];
    prodBldType: string = PROD_BLD_DEFAULT;
    restrictedProdBldTerrains: Array<string>;
    _prodBldTerrainStr: string;
    _prodRequiredBldId: string;

    constructor(id: string, name: string, color: Array<number>, defaultPrice: number, 
            requires?: Array<string>, prodBldType?: string, bldTerrainStr?: string, prodRequiredBld?: string) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.defaultPrice = defaultPrice;
        // optional properties
        if (requires && requires.length > 0) { this.requires = requires; }
        if (prodBldType ) { this.prodBldType = prodBldType; }
        if (bldTerrainStr) { this.setProdBldTerrain(bldTerrainStr); }
        if (prodRequiredBld) { this._prodRequiredBldId = prodRequiredBld; }
    }

    static FromJson(tradeResJson: {}): TradeResource {
        const resId = tradeResJson["id"];
        const resName = tradeResJson["name"];
        const colorName = tradeResJson["colorName"];
        const defaultPrice = tradeResJson["price"];
        const color = ColorPalette.NameAsNumArray(colorName);
        const requires = tradeResJson["requires"];
        const bldType = tradeResJson["bldType"];
        return new TradeResource(resId, resName, color, defaultPrice, requires, bldType);
    }

    setProdBldTerrain(bldTerrainStr: string) {
        if (bldTerrainStr && bldTerrainStr.length > 0) {
            this._prodBldTerrainStr = bldTerrainStr;
            this.restrictedProdBldTerrains = bldTerrainStr.split(" ");
        }
    }

    /* TRADE RES FLAG GETTERS */

    canBuildProdBldOn(terrainChar: string): boolean {
        if (! this.hasProdBldTerrainRequirement) { return true; }
        return this.restrictedProdBldTerrains.includes(terrainChar);

    }

    get hasResRequirement(): boolean {
        return this.requires && this.requires.length > 0;
    }

    get hasProdBldTerrainRequirement(): boolean {
        return this._prodBldTerrainStr && this._prodBldTerrainStr.length > 0;
    }

    get hasRequirement(): boolean {
        return this.requires.length > 0;
    }

    equals(otherResource: TradeResource): boolean {
        return this.id == otherResource.id;
    }

    /* returns the name of this resource's producing building as capitalized string */
    get prodBldName(): string {
        return Strings.capitalizeFirst(this.prodBldType);
    }
}

export const PROD_BLD_DEFAULT = "hut";