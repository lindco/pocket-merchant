// common
import { Loggable } from "../../../../common/models/log";
// trade
import { TradeResource } from "./resource";
import { ResourceTypesList } from "./resource-types-list";
import { IResourceStorage } from "../interfaces/resource.interfaces";
import { StoredResource } from "./stored-resource";

/**
 * An entity or object that store & take, but not delivery any {@link TradeResource}s
 */
export class ResourceStorage extends Loggable implements IResourceStorage {
    maxSpace: number;
    spaceTaken: number = 0;
    parent: any;
    resIds: Array<string> = [];
    protected _resAmount = {};
    protected _resPrice = {};

    constructor(maxSpace: number, parent?: any, justFor?: Array<string>) {
        super("ResourceStorage");
        this.maxSpace = maxSpace;
        if ( parent ) { this.parent = parent; }
        if ( justFor && justFor.length > 0 ) {
            this.initializeForTypes(justFor);
        }
        this.initializeEmpty();
    }
    
    /* add "amount" of resources */
    add(resId: string, amount: number): boolean {
        if (this.spaceTaken < this.maxSpace) {
            this._resAmount[resId] += amount;
            this.spaceTaken += amount;
            return true;
        }
        return false;
    }

    /* remove "amount" resources without checking whether there are enough left or changing prices */
    remove(resId: string, amount: number, force?: boolean): boolean {
        const storedAmount = this._resAmount[resId];
        if ( storedAmount >= amount) {
            this._resAmount[resId] -= amount;
            return true;
        } else { return false; }
    }

    /* returns how many units of a resource is stored here */
    amountOf(resId: string): number { return this._resAmount[resId]; }

    private initializeEmpty() {
        const allResIds = new ResourceTypesList()._resourceIds;
        for (const resId of allResIds) {
            this.resIds.push(resId);
            this._resAmount[resId] = 0;
            this._resPrice[resId] = globalThis.RESOURCE_TYPES.get(resId).defaultPrice;
        }
    }

    private initializeForTypes(forIds: Array<string>) {
        for (const resId of forIds) {
            this.resIds.push(resId);
            this._resAmount[resId] = 0;
            this._resPrice[resId] = globalThis.RESOURCE_TYPES.get(resId).defaultPrice;
        }
    }

    getStoredResource(resId: string): StoredResource {
        const amount = this.amountOf( resId );
        return new StoredResource(resId, amount);
    }
    
}