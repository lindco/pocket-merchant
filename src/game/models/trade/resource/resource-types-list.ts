// common
import { DawnbringerPalette } from "../../../../common/models/colors";
// models
import { TradeResource } from "./resource";

export class ResourceTypesList {
    public _resourceIds: Array<string> = [];
    public resources: Array<TradeResource> = [];

    constructor() {
        /* == 1 - 9 == */
        // ores
        this.add( new TradeResource( "ore", "Ores", DawnbringerPalette.LOULOU, 
            25, null, "mine", "M H" ) );
        // gold
        this.add( new TradeResource( "gold", "Gold", DawnbringerPalette.GOLDENFIZZ, 
            80, null, "mine", "M" ) );
        // gems
        this.add( new TradeResource( "gems", "Gems", DawnbringerPalette.BROWN, 
            240, null, "mine", "M" ) );
        // pottery
        this.add( new TradeResource( "pottery", "Pottery", DawnbringerPalette.TAHITIGOLD, 
            60, ["stone"], "maker", "W", "guildhall" ) );
        // paper
        this.add( new TradeResource( "paper", "Paper", DawnbringerPalette.WHITE, 
            66, ["wood"], "maker", "G H", "church" ) );
        // pearls
        this.add( new TradeResource( "pearls", "Pearls", DawnbringerPalette.VENICEBLUE, 
            80, null, "farm", "W" ) );
        // ivory
        this.add( new TradeResource( "ivory", "Ivory", DawnbringerPalette.WHITE, 
            75, null, "camp", "S", "market" ) );
        // pelts
        this.add( new TradeResource( "pelts", "Pelts", DawnbringerPalette.VERDIGRIS, 
            65, null, "camp", "H F" ) );
        // spices
        this.add( new TradeResource( "spices", "Spices", DawnbringerPalette.CHRISTI, 
            45, null, "farm", "D" ) );
        /* == 9 - 19 == */
        // wool
        this.add( new TradeResource( "wool", "Wool", DawnbringerPalette.LIGHTSTEEL, 
            18, null, "farm", "H G" ) );
        // wood
        this.add( new TradeResource( "wood", "Wood", DawnbringerPalette.OILEDCEDAR, 
            14, null, "camp", "F" ) );
        // ale
        this.add( new TradeResource( "ale", "Ale", DawnbringerPalette.ROPE, 
            40, ["grain"], "house", "G H" ) );
        // clothes
        this.add( new TradeResource( "clothes", "Clothes", DawnbringerPalette.DELL, 
            80, ["wool"], "farm", "H" ) );
        // silk
        this.add( new TradeResource( "silk", "Silk", DawnbringerPalette.LIGHTSTEEL, 
            120, null, "farm", "S" ) );
        // glass
        this.add( new TradeResource( "glass", "Glass", DawnbringerPalette.DEEPKOAMARU, 
            95, ["stone", "tools"] ) );
        // tools
        this.add( new TradeResource( "tools", "Tools", DawnbringerPalette.DIMGRAY, 
            65, ["ore"], "forge", null, "guildhall" ) );
        // grain
        this.add( new TradeResource( "grain", "Grain", DawnbringerPalette.GOLDENFIZZ, 
            22, null, "farm", "G S" ) );
        // fish
        this.add( new TradeResource( "fish", "Fish", DawnbringerPalette.ROYALBLUE, 
            26, null, "docks", "W" ) );
        // weapons
        this.add( new TradeResource( "weapons", "Weapons", DawnbringerPalette.HEATHER, 
            135, ["ore", "tools"], "smith", "guildhall" ) );
        /* == SPECIAL == */
        // special1
        this.add( new TradeResource( "special1", "X1", DawnbringerPalette.ROYALBLUE, 
            100 ) );
        // special2
        this.add( new TradeResource( "special2", "X2", DawnbringerPalette.RAINFOREST, 
            100 ) );
        // special3
        this.add( new TradeResource( "special3", "X3", DawnbringerPalette.CLAIRVOYANT, 
            100 ) );
    }

    add(resource: TradeResource): void {
        this._resourceIds.push(resource.id);
        this.resources.push(resource);
    }

    get(resId: string): TradeResource {
        const resIndex = this._resourceIds.indexOf(resId);
        return this.resources[resIndex];
    }

    getIds(): Array<string> {
        return this._resourceIds;
    }

    getAll(): Array<TradeResource> {
        return this.resources;
    }

    overwriteSingleResource(resId: string, resObj: TradeResource) {
        const resIndex = this._resourceIds.indexOf(resId);
        this.resources[resIndex] = resObj;
    }
}