import { Loggable } from "../../../../common/models/log";
import { IStoredResource } from "../interfaces/resource.interfaces";
import { TradeDelivery } from "../trade-delivery";
import { TradeResource } from "./resource";

export class StoredResource extends Loggable implements IStoredResource {
    resId: string;
    amount: number;
    // optional
    price: number = null;
    profit: number = null;

    constructor(resId: string, amount: number, price?: number, profit?: number) {
        super("StoredRes");
        this.resId = resId;
        this.amount = amount;
        if ( price ) { this.price = price; }
        if ( profit ) { this.profit = profit; }
    }

    public static FromTradeDelivery(delivery: TradeDelivery): StoredResource {
        return new StoredResource(delivery.resId, delivery.resAmount, delivery.priceRange.targetPrice,
            delivery.totalProfit);
    }

    get type(): TradeResource {
        if ( ! globalThis.RESOURCE_TYPES ) {
            this.log("cant get resource type; resource types not loaded"); return;
        } else if (! globalThis.TRADE) {
            this.log("cant get resource type; TradeManager not loaded"); return;
        }
        return globalThis.TRADE.getRes(this.resId);
    }

    get tradeRes(): TradeResource { return this.type; }

    toString(): string {
        return `<StoredRes ${this.amount}x${this.resId} price=${this.price}>`
    }

    static FromString(storedResStr): StoredResource {
        const splittedStr = storedResStr.split("x");
        const amount = parseFloat(splittedStr[0]);
        const resId = splittedStr[1];
        return new StoredResource(resId, amount);
    }
}