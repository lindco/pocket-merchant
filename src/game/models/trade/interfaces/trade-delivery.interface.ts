import { ITradeNode } from "./trade-node.interface";
import { PriceRange } from "../price-range";

export interface ITradeDelivery {
    sourceNode: ITradeNode;
    targetNode: ITradeNode;
    // resource
    resId: string;
    resAmount: number;
    // prices
    priceRange: PriceRange;
    totalProfit: number | null;
}