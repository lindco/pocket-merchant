import { LocationConnectionPath } from "../connection/connection-path";
import { TilePath } from "../../../world/tile/tile-path";
import { MapPos } from "../../../world/pos";
import { IConnectedLocation } from "../../../world/interfaces/location.interfaces";

export interface IConnection {
    // base properties
    source: IConnectedLocation;
    target: IConnectedLocation;
    level: number;
    worldDistance: number;
    tileDistance: number;
    // optional properties
    path: LocationConnectionPath;
    tilePath: TilePath;
    doneTrades: number;
    // averages
    averageWealth: number;
    // methods
    hasTarget(target: IConnectedLocation): boolean;
}

export interface IConnectionPath {
    parent: IConnection;
    start: MapPos;
    target: MapPos;
    path: MapPos[];
}