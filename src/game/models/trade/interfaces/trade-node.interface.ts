import { StoredResource } from "../resource/stored-resource";
import { MarketTrade } from "../market-trade";
import { IResourceStorage } from "./resource.interfaces";
import { ITradeLocation } from "../../../world/interfaces/location.interfaces";

export interface ITradeNode extends IResourceStorage {
    fixedPrices: boolean;
    parentLocation: ITradeLocation;
    // methods
    asStoresResArr(): Array<StoredResource>;
    add(resId: string, amount: number, price?: number): boolean;
    remove( resId, amount, force? ): boolean;
    tradeIsExport(marketTrade: MarketTrade): boolean;
    priceOf(resId: string): number;
    defaultPriceDiffOf(resId: string): number;
    hasEnough(resId: string, minAmount: number): boolean;
    hasRequirementsFor(resId: string, toProduce: number): boolean;
    increasePrice(resId: string, unmetAmount: number);
    reducePrice(resId: string, unmetAmount: number);
    getProfitFor(storedRes: StoredResource): number;
    getStoredResource( resId: string ): StoredResource;
}

