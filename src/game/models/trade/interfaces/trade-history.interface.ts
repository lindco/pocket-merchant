import { MarketTrade } from "../market-trade";

export interface ITradeHistoryKeeper {
    tradeHistory: ITradeHistory;
    // methods
    tradeIsExport(trade: MarketTrade): boolean;
}

export interface ITradeHistory {
    parent: ITradeHistoryKeeper;
    trades: MarketTrade[];
    length: number;
    // methods
    add(trade: MarketTrade, isExport?: boolean): void;
    getLastTrades(amount: number): void;
}