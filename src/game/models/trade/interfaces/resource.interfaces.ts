import { TradeResource } from "../resource/resource";
import { StoredResource } from "../resource/stored-resource";

export interface ITradeResource {
    id: string;
    name: string;
    color: Array<number>;
    defaultPrice: number;
    requires: Array<string>;
    prodBldType: string;
    // flags
    hasResRequirement: boolean;
    hasProdBldTerrainRequirement: boolean;
    // lethods
    canBuildProdBldOn(terrainChar: string): boolean;
}

export interface IStoredResource {
    resId: string;
    amount: number;
    price: number;
    tradeRes: TradeResource;
}

export interface IStorageEntity {
    storedResources: Array<StoredResource>;
    getStoredRes( resId, cheapest?: boolean ): StoredResource;
}

export interface IResourceStorage {
    maxSpace: number;
    add(resId: string, amount: number): boolean;
    remove(resId: string, amount: number, force?: boolean): boolean;
}