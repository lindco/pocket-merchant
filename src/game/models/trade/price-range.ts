
export interface IPriceRange {
    sourcePrice: number;
    targetPrice: number;
    profit: number;
}

export class PriceRange implements IPriceRange {
    sourcePrice: number;
    targetPrice: number;

    constructor(sourcePrice: number, targetPrice: number) {
        this.sourcePrice = sourcePrice;
        this.targetPrice = targetPrice;
    }

    public get profit(): number {
        return this.targetPrice - this.sourcePrice;
    }

}