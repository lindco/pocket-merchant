import { Loggable } from "../../../common/models/log";
import { MarketTrade } from "./market-trade";
import { ITradeHistory, ITradeHistoryKeeper } from "./interfaces/trade-history.interface";

/* == TRADE HISTORY == */
export class TradeHistory extends Loggable implements ITradeHistory {
    parent: ITradeHistoryKeeper;
    isForConnection: boolean = false;
    trades: MarketTrade[] = [];

    constructor(parent: ITradeHistoryKeeper, isForConnection?: boolean) {
        super("TradeHistory");
        this.parent = parent;
        if (isForConnection) { this.isForConnection = true; }
    }

    add(trade: MarketTrade, isExport?: boolean) {
        if (isExport) { trade.isExport = true; }
        this.trades.push(trade);
    }

    get length(): number {
        return this.trades.length;
    }

    getLastTrades(amount: number) {
        return this.trades.slice(-1 * amount);
    }
}