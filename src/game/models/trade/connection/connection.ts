import { Loggable } from "../../../../common/models/log";
import { TilePath } from "../../../world/tile/tile-path";
import { LocationConnectionPath } from "./connection-path";
import { MarketTrade } from "../market-trade";
import * as BABYLON from 'babylonjs';
import { IConnection } from "../interfaces/connection.interfaces";
import { IConnectedLocation } from "../../../world/interfaces/location.interfaces";

export class TradeConnection extends Loggable implements IConnection {
    // basic properties
    source: IConnectedLocation;
    target: IConnectedLocation;
    level: number = 2;
    // trade related properties
    doneTrades: number = 0;
    tradedCoins: number = 0;
    // tiles & path
    tileDistance: number;
    path: LocationConnectionPath;
    tilePath: TilePath = null;
    // observables
    onRouteTrade$: BABYLON.Observable<MarketTrade> = new BABYLON.Observable<MarketTrade>();

    constructor(source: IConnectedLocation, target: IConnectedLocation, findPath?: boolean) {
        super("CityConnection");
        this.source = source;
        this.target = target;
        // find distances
        this.tileDistance = this._findTileDistance();
        // find path if required
        this.path = new LocationConnectionPath(this, source, target);
    }

    private _findTileDistance(): number {
        const distX = Math.abs(this.source.pos.x - this.target.pos.x);
        const distZ = Math.abs(this.source.pos.z - this.target.pos.z);
        return distX + distZ;
    }

    get worldDistance(): number {
        const sourceMesh = this.source.mesh;
        const targetMesh = this.target.mesh;
        if(! (sourceMesh && targetMesh)) {
            this.warn(`cant find distance between city meshes ${sourceMesh} and ${targetMesh}`);
            return -1;
        }
        const worldDist =  BABYLON.Vector3.Distance(sourceMesh.position, targetMesh.position);
        return Math.round(worldDist);
    }

    get locations(): IConnectedLocation[] {
        return [this.source, this.target];
    }

    getDistance(distanceType: number): number {
        if (distanceType == DISTANCE_TYPE_TILES) {
            return this.tileDistance;
        } else {
            return this.worldDistance;
        }
    }

    get mirrored(): TradeConnection {
        if(globalThis.CONN) {
            return globalThis.CONN.getMirrored(this);
        }
        return;
    }

    get averageWealth(): number {
        return (this.source.money + this.target.money) /2;
    }

    addToTradeHistory(marketTrade: MarketTrade, updateMirrored?: boolean): void {
        this.doneTrades += 1;
        this.tradedCoins += marketTrade.profit;
        if (updateMirrored && updateMirrored == true) {
            this.mirrored.doneTrades += 1;
            this.mirrored.tradedCoins += marketTrade.profit;
        }
    }

    hasTarget(target: IConnectedLocation): boolean {
        return this.target.key === target.key;

    }

    /* CONSTANTS */
    static get MAX_DISTANCE() { return 12; }

}

/* == CONNECTION CONSTANTS == */
export const DISTANCE_TYPE_TILES = 0;
export const DISTANCE_TYPE_WORLD = 1;
