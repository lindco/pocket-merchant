import { MapPos } from "../../../world/pos";
import { IConnection, IConnectionPath } from "../interfaces/connection.interfaces";
import { IConnectedLocation } from "../../../world/interfaces/location.interfaces";

/* == CONNECTION PATH INTERFACE == */


/* == CONNECTION PATH MODEL == */
export class AbstractConnectionPath implements IConnectionPath {
    parent: IConnection;
    start: MapPos;
    target: MapPos;
    path: MapPos[] = [];

    constructor(parent: IConnection, start: MapPos, target: MapPos) {
        this.parent = parent;
        this.start = start;
        this.target = target;
    }

    setDirectPath() {
        // add start pos
        this.addNextPos(this.start, true);
        while(! this.isAtTarget()) {
            let nextPos: MapPos = this.getNextPos();
            this.addNextPos(nextPos, true);
            length += 1;
        }
    }

    /* adds WorldPosition into array of positions to target */
    addNextPos(pos: MapPos, addToTile?: boolean) {
        this.path.push(pos);
        if (addToTile == true) {
            const tile = globalThis.MAP.tileAtPos(pos);
            tile.connections.push(this.parent);
        }
    }

    /* finds next WorldPos towards target */
    getNextPos(): MapPos {
        let lastPos: MapPos = this.getLastPos();
        let relDist = [this.target.x - lastPos.x, this.target.z - lastPos.z];
        // get relative position towards next position (example: [-1, 0] = one to the west)
        let relXZ = [relDist[0] / Math.abs(relDist[0]), relDist[1] / Math.abs(relDist[1])];
        // start from absolute pos ..
        let absX: number = lastPos.x;
        let absZ: number = lastPos.z;
        // .. and move horizontally ..
        if ( relXZ[0] ) { absX = lastPos.x + relXZ[0]; } 
        // .. or vertically
        if ( relXZ[1] ) { absZ = lastPos.z + relXZ[1]; }
        return new MapPos(absX, absZ);
    }

    getLastPos(): MapPos {
        return this.path[this.path.length - 1];
    }

    isAtTarget(): boolean {
        let lastPos = this.getLastPos();
        return (lastPos.x == this.target.x) && (lastPos.z == this.target.z);
    }
}

/* == CITY CONN PATH MODEL == */
export class LocationConnectionPath extends AbstractConnectionPath implements AbstractConnectionPath {
    startLocation: IConnectedLocation;
    targetLocation: IConnectedLocation;
    path: MapPos[] = [];

    constructor(parent: IConnection, startLocation: IConnectedLocation, targetLocation: IConnectedLocation) {
        super(parent, startLocation.pos, targetLocation.pos);
        this.startLocation = startLocation;
        this.targetLocation = targetLocation;
    }

}