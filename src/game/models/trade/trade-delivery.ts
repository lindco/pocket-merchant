import { PriceRange } from "./price-range";
import { ITradeNode } from "./interfaces/trade-node.interface";
import { ITradeDelivery } from "./interfaces/trade-delivery.interface";

export class TradeDelivery implements ITradeDelivery {
    sourceNode: ITradeNode;
    targetNode: ITradeNode;
    // resource
    resId: string;
    resAmount: number;
    // prices
    priceRange: PriceRange;
    totalProfit: number | null = null;

    constructor(resId: string, amount: number, sourceNode: ITradeNode, sourcePrice: number,
                targetNode: ITradeNode, targetPrice: number) {
        this.resId = resId;
        this.resAmount = amount;
        this.sourceNode = sourceNode;
        this.priceRange = new PriceRange(sourcePrice, targetPrice);
        this.targetNode = targetNode;
        this.calculateTotalProfit();
    }

    private calculateTotalProfit(): void {
        this.totalProfit = this.resAmount * this.priceRange.profit;
    }

}