import {MapPos, Size} from '../world/pos';
import { GameOptions } from '../models/common/game/game-options';
import { WorldMap } from '../world/map';
import { TerrainTypes } from '../world/tile/terrain-types';
import { WorldTileBuilder } from "./builders/world-tile-builder";
import { WorldCityBuilder } from "./builders/world-city-builder";
import { WorldArmyBuilder } from "./builders/world-army-builder";
import { TileBlueprintBuilder } from "./builders/tile-blueprint-builder";
import { WorldEnvironmentBuilder } from "./builders/world-environment-builder";
import { City } from '../models/city/city';
import { AbstractWorldScene, IAbstractWorldScene } from "./world-scene.interface";
import { CityGenerator, CityLoader } from '../managers/world/world-loaders';
import { WorldTools } from '../../helpers/world-tools';
import * as BABYLON from 'babylonjs';
import { PlayerCamBuilder } from "./builders/player-cam-builder";
import {MapTile} from "../world/tile/tile";


export default class WorldMapScene extends AbstractWorldScene implements IAbstractWorldScene {
    // basic properties
    readonly _canvas: HTMLCanvasElement;
    engine: BABYLON.Engine;
    // assets
    tileInstances: Array<BABYLON.InstancedMesh> = [];
    featureMeshes: Array<BABYLON.AbstractMesh> = [];
    armyMeshes: Array<BABYLON.AbstractMesh> = [];
    blueprintFactory: TileBlueprintBuilder = new TileBlueprintBuilder()
    // observables
    onCameraTargetChange$ = new BABYLON.Observable<BABYLON.AbstractMesh>();
    entitiesPlaced$ = new BABYLON.Observable<void>();

    constructor(canvasHtmlElementId: string) {
        super("WorldScene");
        // Create canvas and engine.
        this._canvas = document.getElementById(canvasHtmlElementId) as HTMLCanvasElement;
        this.engine = new BABYLON.Engine(this._canvas, true);
        this.registerToObservables();
    }

    private registerToObservables() {
        this.entitiesPlaced$.add(() => {
            this.onEntitiesPlaced$.notifyObservers();
            globalThis.PLAYER.updateMesh();
            this.onCameraTargetChanged(globalThis.PLAYER.mesh);
        });
    }

    public beforeCreation(): void {
        // add observer to asset loading
        globalThis.ASSETS.onAllAssetsLoaded$.add(() => this.placeWorldEntities(globalThis.MAP));
        // add observer to camera target change
        this.onCameraTargetChange$.add(targetMesh => this.onCameraTargetChanged(targetMesh));
        this.createMapObject(); // make tilemap model
        // build Scene Object & notify observers
        globalThis.SCENE = new BABYLON.Scene(this.engine);
        this.onSceneCreated$.notifyObservers(this);
    }

    spawnPlayerAndLocations() {
        this.createPlayerAndEnvironment()
        // init tiles
        this.blueprintFactory.makeTileBlueprints(TerrainTypes.LIST);
        // spawn cities
        this.loadOrGenerateLocations();
    }

    createPlayerAndEnvironment(): void {
        // Create player object
        globalThis.PLAYER.onWorldCreated();
        // And camera
        PlayerCamBuilder.createPlayerCam();
        // And sun
        const sunId = WorldMapScene.NODE_ID_SUN;
        const sunLight = new BABYLON.HemisphericLight(
            sunId, new BABYLON.Vector3(0, 1, 0), globalThis.SCENE);
    }

    /**
     * renew camera target with standard distance & angle and set center to given mesh
     * @param cameraTargetMesh mesh the camera should be facing towards
     */
    onCameraTargetChanged(cameraTargetMesh: BABYLON.AbstractMesh) {
        globalThis.CAMERA.setTarget(cameraTargetMesh);
        // turn to right angle 
        globalThis.CAMERA.radius = CAMERA_RADIUS;
        globalThis.CAMERA.alpha = CAMERA_ALPHA;
        globalThis.CAMERA.beta = CAMERA_BETA;
    }

    private createMapObject(): WorldMap {
        // get map name and default size
        const mapName = globalThis.MAP_NAME;
        const worldSize = new Size(100, 100);
        this.log(`creating WorldMap object \"${mapName}\" with size \"${worldSize.toString()}\"`);
        // generate and return WorldMap model
        globalThis.MAP = new WorldMap(mapName, worldSize);
        console.log(globalThis.MAP);
        return globalThis.MAP;
    }

    placeWorldEntities(worldMap: WorldMap): void {
        super.placeWorldEntities(worldMap);
        WorldTileBuilder.placeMapTiles(worldMap, this);
        WorldEnvironmentBuilder.placeTargetCursor(this);
        // loading citied & other locations
        WorldCityBuilder.placeLocations();
        // place landcape features
        WorldEnvironmentBuilder.placeLandscapeFeatures(this);
        // place armies & traders
        WorldArmyBuilder.placeOutsideArmies(this);
        // finished
        this.entitiesPlaced$.notifyObservers();
    }

    get topleft(): MapPos {
        const map = globalThis.MAP;
        return new MapPos(-map.size.x * MapTile.HEXSIZE / 2, map.size.z * MapTile.HEXSIZE / 2);
    }

    getOffsetAt(z: number): number {
        return (z % 2 === 0) ? MapTile.HEXSIZE / 2 : 0;
    }

    private loadOrGenerateLocations() {
        this.loadOrGenerateCities();
    }

    private loadOrGenerateCities(): void {
        let cities: Array<City>;
        const generateLocations = globalThis.OPTIONS.getBool(GameOptions.GENERATE_LOCATIONS);
        // generate or load cities
        if (generateLocations) cities = new CityGenerator().generate();
        else cities = CityLoader.load();
        globalThis.CITIES.onCitiesLoaded$.notifyObservers(cities)
    }

    showCityConnections(city: City) {
        if (!SHOW_CONNECTION_LINES)  return;
        const sourceVec3 = WorldTools.worldToScenePos(city.pos);
        sourceVec3.y = 0.9;
        let targetVec3Arr: Array<BABYLON.Vector3> = [];
        for (const connectedTradeLocation of city.getConnectedTradeLocations()) {
            const targetPos = WorldTools.worldToScenePos(connectedTradeLocation.pos);
            targetPos.y = .9;
            targetVec3Arr.push(targetPos);
        }
        let connectionNum: number = 0;
        for (const targetVec3 of targetVec3Arr) {
            this.createNewTileConnectionLine(sourceVec3, targetVec3, city, connectionNum);
            connectionNum += 1;
        }
    }

    private createNewTileConnectionLine(sourceVec3: BABYLON.Vector3, targetVec3: BABYLON.Vector3, city: City, connectionNum: number) {
        const lineOptions = {points: [sourceVec3, targetVec3]};
        // create line
        const connectionLineId = "conn-" + city.key + "-" + connectionNum;
        let line: BABYLON.LinesMesh = BABYLON.MeshBuilder.CreateLines(connectionLineId, lineOptions, globalThis.SCENE);
        line.alpha = 0.3;
        // line.edgesRenderer = new BABYLON.LineEdgesRenderer(line);
        line.edgesWidth = 3;
        line.freezeWorldMatrix();
    }

    doRender(): void {
        super.doRender();
        if (globalThis.CURSOR && globalThis.CURSOR.mesh) {
            const rotation = globalThis.CURSOR.mesh.rotation;
            rotation.set(rotation.x, rotation.y, rotation.z + .01);
        }
        // Run the render loop.
        this.engine.runRenderLoop( () => globalThis.SCENE.render() );
        // The canvas/window resize event handler.
        window.addEventListener( 'resize', () => this.engine.resize() );
    }

    static get NODE_ID_CAMERA(): string { return "cam-main"; }
    private static get NODE_ID_SUN(): string { return "sun"; }

    toString(): string {
        let mapName = "no-map";
        let mapSize = "no-size";
        if (globalThis.MAP) {
            mapName = globalThis.MAP.name;
            mapSize = `${globalThis.MAP.tiles[0].length}x${globalThis.MAP.tiles.length}`;
        }
        return `<WorldScene "${mapName}" tiles=${mapSize}> `;
    }
}

/* == SCENE CONSTANTS == */
export const USE_MESHES_FOR_TILES = true;
export const USE_FOG = false;
// camera
export const CAMERA_RADIUS = 25;
export const CAMERA_ALPHA = -1 * Math.PI / 2;
export const CAMERA_BETA = 0.84;
export const SHOW_CONNECTION_LINES = false;