import { UnitArmy } from "../../models/military/army/army";
import { WorldTools } from "../../../helpers/world-tools";
import WorldMapScene from "../world-scene";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";

export class WorldArmyBuilder {
    public static placeSingleArmy(army: IUnitArmy, world: WorldMapScene) {
        const assetName = UnitArmy.DEFAULT_MODEL;
        const meshId = "army-" + army.num;
        const instance = WorldTools.placeAssetInstance(assetName, meshId, army.pos, true, true);
        // add to army meshes holder
        world.armyMeshes.push(instance);
    }

    public static placeOutsideArmies(scene: WorldMapScene) {
        const outsideArmies = globalThis.ARMIES.getOutsideArmies();
        for (const army of outsideArmies) {
            WorldArmyBuilder.placeSingleArmy(army, scene);
        }
    }
}