import * as BABYLON from "babylonjs";
import WorldMapScene from "../world-scene";

export class PlayerCamBuilder {
    public static createPlayerCam(): void {
        // check for player
        const player = globalThis.PLAYER;
        if (!player) { this._error(`cant create camera; player not set`); }
        else { this._log(`camera follows player at ${player.pos}`); }
        // check for scene
        if (!globalThis.SCENE) {
            this._error("cannot create player cam: Scene not found");
            return;
        }
        // create babylon cam
        const camViewProperties = [-Math.PI / 2, Math.PI / 4, 15]
        globalThis.CAMERA = new BABYLON.ArcRotateCamera( WorldMapScene.NODE_ID_CAMERA,
            camViewProperties[0], camViewProperties[1], camViewProperties[2],
            BABYLON.Vector3.Zero(), globalThis.SCENE);
        const worldScene = globalThis.SCENE as unknown as WorldMapScene;
        globalThis.CAMERA.attachControl(worldScene._canvas, true);
    }

    public static _log(logMsg: string): void {
        console.log("<PlayerCamBuilder> " + logMsg);
    }

    public static _error(logMsg: string): void {
        console.error("<PlayerCamBuilder> " + logMsg);
    }
}