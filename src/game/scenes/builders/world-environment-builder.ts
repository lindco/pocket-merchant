import { WorldMap } from "../../world/map";
import { LandscapeFeature } from "../../world/feature";
import { WorldTools } from "../../../helpers/world-tools";
import WorldMapScene, { USE_FOG } from "../world-scene";
import { TargetCursor } from "../../world/cursor";
import { MapPos } from "../../world/pos";
import * as BABYLON from 'babylonjs';

export const SKYBOX_TEXTURE_FOLDER = './res/img/sky/skybox';

export class WorldEnvironmentBuilder {
    public static createSkyBoxMaterial(scene) {
        const skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
        skyboxMaterial.backFaceCulling = false;
        skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture(SKYBOX_TEXTURE_FOLDER, scene);
        skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
        skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
        skyboxMaterial.fogEnabled = false;
        return skyboxMaterial;
    }

    public static createFogAndSky(): void {
        this.createFog();
        this.createSky();
    }

    public static placeLandscapeFeatures(scene: WorldMapScene): void {
        const mapObj: WorldMap = globalThis.MAP;
        let placedFeatureCount: number = 0;
        for (const feature of mapObj.features) {
            const successfullyPlaced = this._placeSingleLandscapeFeature(feature, scene);
            if ( successfullyPlaced ) { placedFeatureCount += 1; }
        }
        console.log(`<WorldEnvFactory> placed ${placedFeatureCount} landscape features on map`);
    }

    private static _placeSingleLandscapeFeature(feature: LandscapeFeature, scene: WorldMapScene): boolean {
        const asset = globalThis.ASSETS.getAsset(feature.key);
        if (! asset) {
            console.warn(`<WorldEnvFactory> cant place landscape feature "${feature.key}"; asst not found`); return false;
        }
        const meshId = `lds-feature-${scene.featureMeshes.length}`;
        const meshInstance = WorldTools.placeAssetInstance(feature.key, meshId, feature.pos, true, true);
        scene.featureMeshes.push( meshInstance );
        return meshInstance != null;
    }

    public static createFog() {
        if (!USE_FOG) {
            return;
        }
        globalThis.SCENE.fogMode = BABYLON.Scene.FOGMODE_EXP2;
        globalThis.SCENE.fogDensity = 0.011;
        globalThis.SCENE.fogColor = new BABYLON.Color3(0.7, 0.7, 0.7);
    }

    public static createSky() {
        const scene = globalThis.SCENE;
        const skybox = BABYLON.MeshBuilder.CreateBox("skyBox", {size: 1000.0}, scene);
        skybox.material = WorldEnvironmentBuilder.createSkyBoxMaterial(scene);
    }

    public static placeTargetCursor(scene: WorldMapScene) {
        // create cursor object
        globalThis.CURSOR = new TargetCursor();
        // set to start pos
        const cursorDefaultAssetName = TargetCursor.DEFAULT_ASSET_NAME;
        const cursorStartPos: MapPos = new MapPos(20, 20);
        globalThis.CURSOR.mesh = WorldTools.placeAssetInstance(cursorDefaultAssetName,
            TargetCursor.NODE_ID, cursorStartPos, true, true);
        globalThis.CURSOR.mesh.unfreezeWorldMatrix();
        // create animation
        this.createCursorAnimation(globalThis.CURSOR.mesh);
    }

    private static createCursorAnimation(cursorMesh: BABYLON.AbstractMesh) {
        // create rotation animation
        const cursorRotationAnim = new BABYLON.Animation("cursorRotationAnim",
            "rotation.z", 30, BABYLON.Animation.ANIMATIONTYPE_FLOAT,
            BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);

        const wheelKeys = [];
        // at the animation key 0, the value of rotation.y is 0
        wheelKeys.push({frame: 0, value: 0});
        // at the animation key 30, (after 1 sec since animation fps = 30) the value of
        // rotation.y is 2PI for a complete rotation
        wheelKeys.push({frame: 30, value: 2 * Math.PI});
        //set the keys
        cursorRotationAnim.setKeys(wheelKeys);
        //Link this animation to the right back wheel
        cursorMesh.animations = [];
        cursorMesh.animations.push(cursorRotationAnim);
        //Begin animation - object to animate, first frame, last frame and loop if true
        globalThis.SCENE.beginAnimation(cursorMesh, 0, 30, true);
    }
}