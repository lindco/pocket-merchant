import { City } from "../../models/city/city";
import { WorldTools } from "../../../helpers/world-tools";
import WorldMapScene from "../world-scene";

export class WorldCityBuilder {
    public static placeSingleCity(city: City) {
        const cityAssetName = city.getAssetName();
        city.mesh = globalThis.ASSETS.getAsset(cityAssetName).getMeshInstance(city.meshId, true);
        WorldTools.placeMesh(city.mesh, city.pos, true, true);
        // inform city manager observable
        globalThis.CITIES.onCitySpawned$.notifyObservers(city);
    }

    public static placeCities(scene: WorldMapScene) {
        const cities = globalThis.CITIES.cities;
        let placedCitiesCount: number = 0;
        scene.log(`placing ${cities.length} cities starting with ${cities[0].name} ..`);
        for (const city of cities) {
            WorldCityBuilder.placeSingleCity(city);
            scene.showCityConnections(city);
            placedCitiesCount += 1;
        }
        // fire CitiesPlayedEvent
        globalThis.CITIES.onCitiesSpawned$.notifyObservers(cities);
    }

    public static placeLocations(): void {
        // this.loadOrGenerateCities();
        WorldCityBuilder.placeCities(globalThis.WORLD);
    }
}