import { Loggable } from "../../../common/models/log";
import { MapTile } from "../../world/tile/tile";
import { WorldTools } from "../../../helpers/world-tools";
import * as BABYLON from 'babylonjs';
import { TerrainTypes } from "../../world/tile/terrain-types";

export class TileBlueprintBuilder extends Loggable {
    private _blueprints = {};

    constructor() {
        super("TileBlueprintBuilder");
    }

    makeTileBlueprints(terrainTypes: Array<string>): void {
        this.log(`making blueprints ..`);
        for (const terrainId of terrainTypes) {
            this._blueprints[terrainId] = this.makeTileBlueprint(terrainId);
        }
    }

    /* creates Babylon hexagon Mesh for every terrain tile, that will be used
       to make mesh instances of */
    private makeTileBlueprint(terrainType: string): BABYLON.Mesh {
        var diameterBottom = MapTile.HEXSIZE;
        const tileName = `base-tile-${terrainType}`;
        let mesh = WorldTools.createTileHexMesh(tileName, terrainType, diameterBottom);
        // set rotation & y offset
        WorldTools.adjustTileMexHex(mesh, terrainType);
        return mesh;
    }

    public getBlueprint(terrainChar: string) {
        if (! terrainChar) { terrainChar = TerrainTypes.UNKNOWN; }
        const blueprint =  this._blueprints[terrainChar];
        if (! blueprint) {
            this.error(`cant create tile with terrain ${terrainChar} in TileBlueprintFactory. Abort.`); return;
        }
        return blueprint;
    }
}