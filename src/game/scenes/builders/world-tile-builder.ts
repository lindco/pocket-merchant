import * as BABYLON from 'babylonjs';
import { TerrainTypes } from "../../world/tile/terrain-types";
import { RandomInt } from "../../../helpers/random";
import { MapTile } from "../../world/tile/tile";
import { WorldTools } from "../../../helpers/world-tools";
import WorldMapScene, { USE_MESHES_FOR_TILES } from "../world-scene";
import { TileBlueprintBuilder } from "./tile-blueprint-builder";
import { WorldMap } from "../../world/map";
import { TerainTypeNames } from "../../world/tile/tile-terrain-names";

export class WorldTileBuilder {
    public static applyRndRotationToTile(terrainChar: string, meshInstance: BABYLON.InstancedMesh) {
        if (terrainChar !== TerrainTypes.DESERT && terrainChar !== TerrainTypes.WATER) {
            const rndRot = RandomInt.below(6) * 1.0471975512;
            const zAxis = new BABYLON.Vector3(0, 1, 0);
            meshInstance.rotate(zAxis, rndRot, BABYLON.Space.WORLD);
        }
        meshInstance.freezeWorldMatrix();
    }

    public static createTileInstanceFromBlueprint(blueprint: BABYLON.Mesh, tile: MapTile, offset: number,
                                                  scene: WorldMapScene) {
        let meshInstance: BABYLON.InstancedMesh = blueprint.createInstance(tile.meshName);
        if (USE_MESHES_FOR_TILES && TerrainTypes.TILES_WITH_MESHES.includes(tile.terrainChar)) {
            meshInstance = this.getNewTileMeshInstance(tile);
        }
        meshInstance.position = WorldTools.worldToScenePos(tile.pos);
        tile.offset = offset;
        // meshInstance.freezeWorldMatrix();
        meshInstance.checkCollisions = false;
        return meshInstance;
    }

    public static createTileInstance(tile: MapTile, offset: number): BABYLON.InstancedMesh {
        const worldScene = globalThis.WORLD;
        // add random mini-offset
        let rndAdditionalOffset = RandomInt.below(100) *.001;
        let finalOffset = offset + rndAdditionalOffset;
        const blueprint = worldScene.blueprintFactory.getBlueprint(tile.terrainChar);
        if (! blueprint) {
            worldScene.warn(`cant create tile instance of "${tile.terrainChar}" at ${tile.pos}`); return;
        }
        // create new instance from terrain blueprint
        let meshInstance = this.createTileInstanceFromBlueprint(blueprint, tile, finalOffset,
            worldScene);
        // apply random rotation (in rad!)
        WorldTileBuilder.applyRndRotationToTile(tile.terrainChar, meshInstance);
        // link mesh to tile model
        tile.meshInstance = meshInstance;
        return meshInstance;
    }

    public static placeSingleMapTile(tile: MapTile,
                               blueprintFactory: TileBlueprintBuilder): BABYLON.InstancedMesh {
        const worldScene = globalThis.WORLD;
        if (! tile) { return; }
        // offset every second row
        const offset = (tile.pos.z%2 === 0) ? MapTile.HEXSIZE/2 : 0;
        // show tile mesh
        const tileInstance = this.createTileInstance(tile, offset);
        worldScene.tileInstances.push(tileInstance);
        return tileInstance;
    }

    public static placeMapTiles(worldMap: WorldMap, scene: WorldMapScene) {
        for (let y = 0; y < worldMap.size.z; y++) {
            for (let x = 0; x < worldMap.size.x; x++) {
                let tile: MapTile = worldMap.tileAt(x, y);
                this.placeSingleMapTile(tile, scene.blueprintFactory);
            }
        }
        scene.log(`placed ${scene.tileInstances.length} MapTiles`);
    }

    public static createTileMeshInstance(tile: MapTile, terrainNameStr: string): BABYLON.InstancedMesh {
        return globalThis.ASSETS.getAsset(`tile_${terrainNameStr}`)
            .getMesh().createInstance(tile.meshName);
    }

    private static getNewTileMeshInstance(tile: MapTile): BABYLON.InstancedMesh {
        const terrainName = TerainTypeNames.getName(tile.terrainChar);
        return WorldTileBuilder.createTileMeshInstance(tile, terrainName);
    }
}