import { BaseEventListener } from "../../common/models/events";
import { WorldMap } from "../world/map";
import { Observable } from "@babylonjs/core";
import WorldMapScene from "./world-scene";

/**
 * Engine independent interface to world scene creation: at best its abstract enough to allow a change to
 * a 2D engine as well as per example to Web3D
 */
export interface IAbstractWorldScene {
    // Observables
    onSceneCreated$: Observable<WorldMapScene>;
    onEntitiesPlaced$: Observable<void>;
    // methods
    beforeCreation(): void;
    createSceneObject(): void;
    createScene(): void;
    placeWorldEntities(worldMap: WorldMap): void;
    doRender(): void;
}

export class AbstractWorldScene extends BaseEventListener implements IAbstractWorldScene {
    // Observables
    onSceneCreated$: Observable<WorldMapScene> = new Observable<WorldMapScene>();
    public onEntitiesPlaced$: Observable<void> = new Observable<void>();

    constructor(canvasHtmlElementId: string) {
        super("WorldScene");
    }

    beforeCreation(): void {
    }

    createSceneObject(): void {
    }

    createScene(): void {
    }

    placeWorldEntities(worldMap: WorldMap): void {
        this.log(`showing map`);
    }

    doRender(): void {}
}