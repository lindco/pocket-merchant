// events
import { BaseEventListener, IBaseEvent } from "../../../common/models/events";
// babylon
import * as BABYLON from 'babylonjs';


export interface IGameManager {
    scopeStr: string;
    // observables
    onCreation$: BABYLON.Observable<void>;
    // methods
    registerToObservables(): void;
    run(): void;
}

export abstract class AbstractManager extends BaseEventListener implements IGameManager {
    scopeStr: string = "";
    // observables
    onCreation$: BABYLON.Observable<void> = new BABYLON.Observable<void>();

    protected constructor(scopeStr: string) {
        super(scopeStr + "Manager");
        this.scopeStr = scopeStr;
        // register to observables
        // this.registerToObservables();
        // call onCreate observable
        this.onCreation$.notifyObservers();
    }

    onEvent(ev: IBaseEvent) {
        super.onEvent(ev);
    }

    abstract run(
    );

    registerToObservables(): void {
    }

}