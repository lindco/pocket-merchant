import { BaseEventListener } from '../../../common/models/events';
import * as BABYLON from 'babylonjs';
import 'babylonjs-loaders';
import { Asset, IAsset } from '../../../common/models/world/asset';
import { GameOptions } from "../../models/common/game/game-options";
import { Observable } from "@babylonjs/core";

/* INTERFACES */

interface IAssetManager {
    assets: Array<IAsset>;
    loadAssets(): void;
}

/* ASSET MANAGER */
export class AssetsLoader extends BaseEventListener implements IAssetManager {
    private assetNames: Array<string> = [];
    public assets: Array<Asset> = [];
    private assetsToLoad: number = 0;
    private assetsLoaded: number = 0;
    // observable
    onAllAssetsLoaded$: Observable<Array<Asset>> = new Observable<Array<Asset>>();

    constructor() {
        super("AssetManager");
        this.log("AssetsLoader created");
    }

    run() {

    }

    loadAssets() {
        const assetsCount: number = this.assets.length;
        this.log(`initializing loading of ${assetsCount} assets ..`);
        this.createEmptyAssets();
        // start model loading
        let loadingAssetCount: number = 0;
        for (const asset of this.assets) {
            const loadingStarted = this.loadSingleAsset(asset);
            if ( loadingStarted ) { loadingAssetCount += 1; }
        }
        this.log(`loading of ${loadingAssetCount} assets started`);
    }

    private createEmptyAssets(): void {
        for (const modelFileName of MODELS_TO_IMPORT) {
            this.addAsset(new Asset(modelFileName));
        }
    }

    private addAsset(asset: Asset): Asset {
        this.assets.push(asset);
        this.assetNames.push(asset.name);
        this.assetsToLoad += 1;
        return asset;
    }

    loadSingleAsset(asset: Asset): boolean {
        const modelFileName = asset.fileName;
        // this.log(`start loading of ${modelFileName} ..`);
        if (! globalThis.SCENE) {
            this.error(`cant load ${modelFileName}; scene not yet initialized`);
            return false;
        }
        const rootDir = MODEL_DIRECTORY;
        const assetName = modelFileName.split(".")[0];
        BABYLON.SceneLoader.ImportMesh(assetName, rootDir, modelFileName, globalThis.SCENE,
            (newMeshes: Array<BABYLON.Mesh>) => {
                this.addSingleMesh(newMeshes, assetName);
            }),
            (error: any) => {
                console.error(error);
            }
        return true;
    }

    private addSingleMesh(newMeshes: Array<BABYLON.Mesh>, assetName: string) {
        // add lod
        for (const newMesh of newMeshes) {
            const lodDist = globalThis.OPTIONS.getNum(GameOptions.MAP_VIEW_DISTANCE);
            newMesh.addLODLevel(lodDist, null);
        }
        // console.log(`LOADED MESH ${assetName} meshes "${newMeshes.length}`);
        globalThis.ASSETS.onSingleAssetLoaded(assetName, newMeshes);
    }

    onSingleAssetLoaded(assetName: string, meshes: Array<BABYLON.AbstractMesh>) {
        const loadedAsset = this.getAsset(assetName);
        const assetsMgr = this;
        meshes[0].name = "asset-" + assetName;
        if (meshes.length > 1) { meshes[1].id = assetName + "-1"; }
        loadedAsset.setMeshes(meshes);
        // renew loading counters
        this.assetsToLoad -= 1;
        if (this.allAssetsLoaded) {
            globalThis.GLOOBS.onAllAssetsLoaded$.notifyObservers(undefined);
            assetsMgr.onAllAssetsLoaded();
        }
    }

    onAllAssetsLoaded() {
        this.log("all assets loaded!");
        // fire observable
        this.onAllAssetsLoaded$.notifyObservers(this.assets);
    }

    get allAssetsLoaded(): boolean {
        return this.assetsToLoad == 0;
    }

    getAsset(assetName: string, autoscale?: boolean): Asset {
        const index = this.assetNames.indexOf(assetName);
        const asset = this.assets[index];
        if (!asset) {
            this.error(`cant get asset ${assetName}: is unknown`); return;
        } else if (asset.errorMessage && asset.errorMessage.length > 0) {
            this.error(`cant get asset ${assetName}: loading error: ${asset.errorMessage}`);
            return;
        }
        return asset;
    }
}

/* == MODEL NAME CONSTANTS == */
export const TILE_MODELS = [ "tile_grass.gltf", "tile_hills.gltf", "tile_mountain.gltf", "tile_water.gltf",
    "tile_desert.gltf", "tile_forest.gltf", "tile_savannah.gltf", "tile_bridge.gltf", "tile_arctic.gltf" ]

export const CURSOR_MODELS = [ "cursor_highlighted.obj", "cursor_move_green.obj", "cursor_move_red.obj",
    "selection_circle.gltf"];

export const CITY_MODELS = [
    "rural_townhall.obj", "city_asian.obj", "city_tribal.obj", "city_venetian.obj", "city_hanseatic.obj" , "city_hellenic.obj",
    "city_mideast.obj", "city_slavic.obj", "city_weur.gltf", "city_elven.obj", "city_colonial.obj",
    "city_dwarven.obj", "city_barbarian.obj", "city_cloister.obj", "city_roman.obj", "city_evil.obj",
    // unique citiy models
    "city_vivec.obj"
];

export const ARMY_MODELS = [
    "rider.obj", "ship_smallest.obj", "army_camp.gltf", "army_fort.obj"
];

export const FEATURE_MODELS = [
    "forest.obj", "treasure.obj", "volcano.obj", "temple.obj", "tipis.obj", "shipwreck.obj",
    "village.gltf", "oasis.obj", "arctic_peak.obj", "shrooms.obj", "ruins.obj", "watchtower.obj",
    "watchtower_white.obj", "farmstead.obj", "ice_floe.obj", "lighttower.obj", "pyramids.obj", "island.obj",
    "fields.gltf"
];

export const MODELS_TO_IMPORT = TILE_MODELS.concat(CURSOR_MODELS, FEATURE_MODELS, CITY_MODELS, ARMY_MODELS);

/* == ASSET LOADER CONSTANTS == */
export const MODEL_DIRECTORY = "./res/models/";