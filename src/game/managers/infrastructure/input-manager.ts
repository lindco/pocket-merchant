import { HexDirection, MapPos } from "../../world/pos";
import * as BABYLON from 'babylonjs';
import { AbstractManager } from "./abstract-manager";
import { FamiliesDialog } from "../../gui/dialogs/families-dialog";
import { GameMenuDialog } from "../../gui/dialogs/game-menu-dialog";
import { GameDialogKeys } from "../../../common/constants/game-dialog-keys";
import { GameMapClick } from "../../../common/models/input/game-map-click";

const moveKeyCodes = {
    "A": new HexDirection(HexDirection.WEST),
    "D": new HexDirection(HexDirection.EAST),
    "W": new HexDirection(HexDirection.NORTHWEST),
    "E": new HexDirection(HexDirection.NORTHEAST),
    "Z": new HexDirection(HexDirection.SOUTHWEST),
    "X": new HexDirection(HexDirection.SOUTHEAST)
}

const KEY_NEXT_TURN_BTN = "N";
const KEY_TOGGLE_INSPECTOR = "I";
const KEY_GAME_MENU = "G";
const KEY_LIST_FAMILIES = "F";

export class InputManager extends AbstractManager {
    public readonly onMapClick$ = new BABYLON.Observable<GameMapClick>();

    constructor() {
        super("Input");
        this.onMapClick$.add(
            mapClickObj => {
                if (mapClickObj.pick.pickedMesh && globalThis.SELECTION)
                    globalThis.SELECTION.onMeshClicked(mapClickObj.pick.pickedMesh);
        });
    }

    run() {

    }

    movePlayer(keyCode: string): boolean {
        const hexDir = moveKeyCodes[keyCode] as HexDirection;
        globalThis.PLAYER.moveInDir(hexDir.dirStr);
        return true;
    }

    private endTurn(): boolean {
        if (! globalThis.TURNS) {
            this.error("turn manager not loaded");
            return false;
        }
        globalThis.TURNS.onEndTurnButton();
        return true;
    }

    onKeyUp(keyCode: string): boolean {
        if (! globalThis.MAP || ! globalThis.PLAYER) {
            this.error("map not loaded");
            return false;
        }
        if (keyCode in moveKeyCodes) // move player
            return this.movePlayer(keyCode);
        else if (keyCode === KEY_NEXT_TURN_BTN) // next turn
            return this.endTurn();
        else if (keyCode === KEY_TOGGLE_INSPECTOR) // toggle inspector
            return globalThis.GUI.toggleInspector();
        else if (keyCode === KEY_LIST_FAMILIES) // Families menu
            this.closeAndShowFamiliesDiag();
        else if (keyCode === KEY_GAME_MENU) // Game Menu
            this.closeAndShowGameMenuDiag();
    }

    /* == DIALOG OPENING METHODS */
    private closeAndShowGameMenuDiag() {
        if (globalThis.DIALOG?.diagKey === GameDialogKeys.DIAG_GAME_MENU)
            globalThis.GUI.closeCurrentDialog();
        else globalThis.GUI.showDialog(new GameMenuDialog(), true);
    }

    private closeAndShowFamiliesDiag() {
        if (globalThis.DIALOG?.diagKey === GameDialogKeys.DIAG_FAMILIES)
            globalThis.GUI.closeCurrentDialog();
        else globalThis.GUI.showDialog(new FamiliesDialog(), true);
    }

    toString(): string {
        return "<Input>";
    }
}