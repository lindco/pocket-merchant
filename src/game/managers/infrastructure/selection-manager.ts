import { WorldSelection } from "../../world/selection";
import { MapTile } from "../../world/tile/tile";
import { MapPos } from "../../world/pos";
import { TerrainTypes } from "../../world/tile/terrain-types";
import { City } from "../../models/city/city";
import { SingleArmyDialog } from "../../gui/dialogs/army-dialog";
import { AbstractManager } from "./abstract-manager";
import { WorldTools } from "../../../helpers/world-tools";
import { CityDialog } from "../../gui/dialogs/city-dialog";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import * as BABYLON from 'babylonjs';

export class SelectionManager extends AbstractManager {
    source: WorldSelection = new WorldSelection(null);
    _clickedPos: MapPos;
    _cursorPos: MapPos = new MapPos(20, 20);
    // observables
    onCityClicked$: BABYLON.Observable<City> = new BABYLON.Observable<City>();
    onBeforeCursorMove$: BABYLON.Observable<MapPos> = new BABYLON.Observable<MapPos>();

    constructor() {
        super("Selection");
    }

    registerToObservables() {
        super.registerToObservables();
        this.onBeforeCursorMove$.add(
            (worldPos) => this.moveCursorTo(worldPos)
        );
    }

    run() {

    }

    setSourceToPlayer() {
        if (globalThis.PLAYER) {
            this.source.setToPlayer();
            const playerPosStr = globalThis.PLAYER.pos.asString();
            return;
        }
        this.warn(`cant set source to player: Player not found`);
    }

    moveCursorTo(worldPos: MapPos) {
        this._cursorPos = worldPos;
        const tile = globalThis.MAP.tileAtPos(worldPos);
        const tilePosVec3 = WorldTools.worldToScenePos(tile.pos);
        // update cursor pos
        if (tile && globalThis.CURSOR.mesh) {
            const offsetY = TerrainTypes.heightOf(tile.terrainChar) /2;
            globalThis.CURSOR.mesh.position.set(tilePosVec3.x, offsetY, tilePosVec3.z);
        }
    }

    movePlayerToCursor() {
        globalThis.PLAYER_M.movePlayerTo(this._cursorPos)
    }

    /* ON ENTITY CLICK */
    onCityClicked(city: City) {
        this.log(`click on city ${city.name}`);
        this.onCityClicked$.notifyObservers(city);
        // set as selection source
        this.source.setToCity(city);
        this.onPosClick(city.pos);
        // show city dialog
        const cityDiag = new CityDialog(city);
        globalThis.GUI.showDialog(cityDiag);
    }

    onPlayerClicked() {
        this.onPosClick(globalThis.PLAYER.pos);
        globalThis.PLAYER.onClicked$.notifyObservers();
    }

    onTileClicked(tile: MapTile) {
        this.log(`clicked on tile ${tile.terrainChar} at ${tile.pos.asString()}`);
        this.onPosClick(tile.pos);
        this.moveCursorTo(tile.pos);
    }

    onCursorClicked() {
        const cursorPosStr = this._cursorPos.asString();
        this.log(`clicked on cursor at ${cursorPosStr}: initialize movement check`);
        // check for possible movement
        this.movePlayerToCursor();
    }

    onPosClick(clickedPos: MapPos) {
        this._clickedPos = clickedPos;
    }

    onPlayerMeshPlaced() {
        this.setSourceToPlayer();
    }

    onArmyClicked(army: IUnitArmy) {
        this.log(`click on army \"${army.name}\" of ${army._ownerKey}`);
        army.onClicked$.notifyObservers();
        // show army dialog
        const armyDiag = new SingleArmyDialog(army);
        globalThis.GUI.showDialog(armyDiag);
    }

    /* EVENT HANDLERS FROM OTHER MANAGERS */
    onMeshClicked(mesh: BABYLON.AbstractMesh) {

        if (mesh.id.startsWith("city")) { // CITY click
            this.onCityClicked(SelectionManager.GetCityByMesh(mesh));

        } else if (mesh.id.startsWith("army")) { // ARMY click
            this.onArmyClicked(SelectionManager.GetArmyByMesh(mesh));
        } else if ( mesh.id.startsWith("player")) { // PLAYER caravan click
            this.onPlayerClicked();

        } else if ( mesh.id.startsWith("tile")) { // TILE click on map
            const tile = SelectionManager.GetTileByMesh(mesh);
            if (tile) { this.onTileClicked(tile); }
        } else if( mesh.id.startsWith("cursor") ) { // CURSOR click
            this.onCursorClicked();
        }
    }

    /* STATIC METHODS */
    static GetTileByMesh(tileMesh: BABYLON.AbstractMesh): MapTile {
        let tilePosStr: string = tileMesh.id.split("-")[2];
        return globalThis.MAP.tileAtPos( MapPos.FromString(tilePosStr, true) );
    }

    static GetCityByMesh(cityMesh: BABYLON.AbstractMesh): City {
        const cityKey= cityMesh.name.split("-")[1];
        return globalThis.CITIES.getByKey(cityKey);
    }

    static GetArmyByMesh(armyMesh: BABYLON.AbstractMesh): IUnitArmy {
        const armyNum = parseInt(armyMesh.name.split("-")[1]);
        return globalThis.ARMIES.ByNum(armyNum);
    }
}