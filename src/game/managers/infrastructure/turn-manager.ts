import { Turn } from "../../../common/models/turn";
import { AbstractManager } from "./abstract-manager";
import * as BABYLON from 'babylonjs';

export class TurnManager extends AbstractManager {
    public pastTurns: Array<Turn> = [];
    public active: Turn = null;
    // observables
    onBeforeTurnStarted$ = new BABYLON.Observable<number>();
    onTurnStarted$ = new BABYLON.Observable<number>();

    constructor() { 
        super("Turn");
        this.active = new Turn(0);
    }

    run() {

    }

    turnNum(): number {
        return this.active.num;
    }

    public onEndTurnButton(): void {
        // this.log(`End Turn button clicked`);
        // processing & fire turn finished event
        this.active.nextStage();
        globalThis.GLOOBS.onTurnFinishing$.notifyObservers(this.active.num);
        // finishing
        this.active.nextStage();
    }

    public onTurnFinished(turn: Turn): void {
        // finishing
        this.startNextTurn();
    }

    public startNextTurn(): void {
        const oldTurn: Turn = this.active;
        const nextTurnNum = oldTurn.num + 1;
        this.pastTurns.push(oldTurn);
        this.active = new Turn(nextTurnNum);
        // fire before turn started event
        this.onBeforeTurnStarted$.notifyObservers(this.active.num);
        this.onTurnStarted$.notifyObservers(this.active.num);
    }
}