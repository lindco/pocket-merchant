import * as $ from "jquery";
import { JsonDataFile, JsonDataFiles } from "../../models/infrastructure/loading/json-data-file";
import { AbstractManager, IGameManager } from "./abstract-manager";
import * as BABYLON from 'babylonjs';
import { FileManager } from "./file-manager";

export class JsonDataLoader extends AbstractManager implements IGameManager {
    public jsonDataFiles: JsonDataFile[] = [];
    // observables
    public onAllLoaded$: BABYLON.Observable<boolean> = new BABYLON.Observable<boolean>();
    public onOneLoaded$: BABYLON.Observable<JsonDataFile> = new BABYLON.Observable<JsonDataFile>();
    public onLoadingFailed$: BABYLON.Observable<string> = new BABYLON.Observable<string>();
    // single observables
    public onBldTypesLoaded$ = new BABYLON.Observable<any>();
    public onUnitMountsLoaded$ = new BABYLON.Observable<any>();
    public onUnitGearLoaded$ = new BABYLON.Observable<any>();
    public onLanguagesLoaded$ = new BABYLON.Observable<any>();


    constructor() {
        super("JsonDataLoader");
        this.setupFilesToLoad();
        // subscribe to single loaded
        this.onOneLoaded$.add(jsonData => {
            loadingStates[jsonData.key] = true;
            this.fireSingleLoadedObservable(jsonData.key, jsonData.data);
            this.checkAllLoaded(); // check whether all are loaded
        });
        this.onLoadingFailed$.add(
            fullPath => {
                const fileName = FileManager.FileNameFromPath(fullPath);
                this.warn(`error while loading file "${fileName}"; aborted`);
                globalThis.FILES._loadingErrors.push(fileName);
            }
        )
    }

    public getJsonData(jsonDataFileKey: string): {} {
        for (const jsonDataFile of this.jsonDataFiles) {
            if (jsonDataFile.key === jsonDataFileKey) { return jsonDataFile.data; }
        }
        return null;
    }

    private setupFilesToLoad() {
        this.jsonDataFiles.push( new JsonDataFile(JsonDataFiles.BUILDING_TYPES, "building-types") );
        this.jsonDataFiles.push( new JsonDataFile(JsonDataFiles.MOUNTS, "unit-mounts") );
        this.jsonDataFiles.push( new JsonDataFile(JsonDataFiles.GEAR_TYPES, "unit-gear") );
        this.jsonDataFiles.push( new JsonDataFile(JsonDataFiles.CHARACTER_NAMES, "languages") );
    }

    private fireSingleLoadedObservable(key: string, data: object) {
        switch (key) {
            case JsonDataFiles.BUILDING_TYPES: this.onBldTypesLoaded$.notifyObservers(data); return;
            case JsonDataFiles.MOUNTS: this.onUnitMountsLoaded$.notifyObservers(data); return;
            case JsonDataFiles.GEAR_TYPES: this.onUnitGearLoaded$.notifyObservers(data); return;
            case JsonDataFiles.CHARACTER_NAMES: this.onLanguagesLoaded$.notifyObservers(data); return;
        }
    }

    run() {
        this.startLoadingAll();
    }

    private startLoadingAll() {
        for (const jsonDataFile of this.jsonDataFiles) {
            this.startLoading(jsonDataFile);
        }
    }

    private startLoading(jsonDataFile: JsonDataFile): void {
        const dataFolder = "./res/data/default/";
        const fullPath = dataFolder + jsonDataFile.fileName;
        $.getJSON(fullPath).done(data => {
            jsonDataFile.data = data;
            this.onOneLoaded$.notifyObservers(jsonDataFile)
        })
        .fail( () => {
            this.onLoadingFailed$.notifyObservers(fullPath)
        });
    }

    private checkAllLoaded(): boolean {
        for (const jsonDataFileKey of JsonDataFiles.ALL) {
            const isLoaded: boolean = loadingStates[jsonDataFileKey];
            if (! isLoaded) { return false; }
        }
        this.log("ALL JSONS LOADED!!");
        this.onAllLoaded$.notifyObservers(true);
        return true;
    }

}

/* == CONSTANTS == */
let loadingStates: { BUILDINGS: boolean, MOUNTS: boolean, GEAR_TYPES: boolean,
        CHARACTER_NAMES: boolean } = {
    BUILDINGS: false,
    MOUNTS: false,
    GEAR_TYPES: false,
    CHARACTER_NAMES: false
}