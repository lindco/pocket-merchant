import * as $ from "jquery";
import { AbstractManager } from "./abstract-manager";

export class FileManager extends AbstractManager {
    jsonContent: {} = {};
    _loadingErrors: Array<string> = [];

    constructor() {
        super("File");
    }

    run() {

    }

    /* INTERNAL ON EVENT */
    static FileNameFromPath(fullPath: string, withoutEnding?: boolean): string {
        const splittedPath = fullPath.split("/");
        const filePath = splittedPath[splittedPath.length -1];
        if (withoutEnding) {
            const nameAndEnding = filePath.split(".");
            return nameAndEnding[0];
        }
        return filePath;
    }
}