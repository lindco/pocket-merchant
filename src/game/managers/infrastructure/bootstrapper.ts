import { AbstractManager } from "./abstract-manager";
import { FileManager } from "./file-manager";
import { AssetsLoader } from "./asset-loader";
import { JsonDataLoader } from "./json-data-loader";
import { PlayerManager } from "../diplomacy/player-manager";
import { SelectionManager } from "./selection-manager";
import { InputManager } from "./input-manager";
import { GlobalObservables } from "../../../common/game-observables";
import { CharacterManager } from "../diplomacy/character-manager";
import { BuildingManager } from "../city/building-manager";
import { CityManager } from "../city/city-manager";
import { TradeManager } from "../trade/trade-manager";
import { ConnectionManager } from "../trade/connection-manager";
import { FactionManager } from "../diplomacy/faction-manager";
import { UnitManager } from "../military/unit-manager";
import { UnitGearManager } from "../military/unit-gear-manager";
import { ArmyManager } from "../military/army-manager";

export class Bootstrapper extends AbstractManager {

    constructor() {
        super("Bootstrapper");
        globalThis.GLOOBS = new GlobalObservables();
    }

    run() {
        this.loadFilesAndAssets();
        this.startPlayerAndSelectionManaging();
        globalThis.GLOOBS.onBootstrappingFinished$.notifyObservers();
    }

    private startPlayerAndSelectionManaging() {
        globalThis.PLAYER_M = new PlayerManager();
        globalThis.SELECTION = new SelectionManager();
        globalThis.INPUT = new InputManager();
    }

    loadFilesAndAssets() {
        globalThis.FILES = new FileManager();
        globalThis.JSON_LOADER = new JsonDataLoader();
        globalThis.ASSETS = new AssetsLoader();
        // globalThis.ASSETS.loadAssets();
    }

    createEntityManagers(): void {
        this.log("creating entity managers ..");
        globalThis.CHARS = new CharacterManager();
        globalThis.BLD = new BuildingManager();
        globalThis.CITIES = new CityManager();
        // after CityManager
        globalThis.TRADE = new TradeManager();
        globalThis.CONN = new ConnectionManager();
        globalThis.FACTIONS = new FactionManager();
        // units & gear
        globalThis.UNITS = new UnitManager();
        globalThis.GEAR = new UnitGearManager();
        globalThis.ARMIES = new ArmyManager();
    }
}