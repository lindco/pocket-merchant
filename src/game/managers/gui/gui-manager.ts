import { ControllerButtons } from "../../gui/menus/controller-buttons";
import { AbstractGuiContainer } from "../../gui/common/layouts";
import { DomNode, DomNodes } from "../../gui/elements/controls-dom";
import { TopBarMenu } from '../../gui/menus/top-bar';
import { AbstractMenu } from '../../gui/menus/common/abstract-menu';
import { ActionButtonsBar } from "../../gui/menus/action-buttons";
import { StartMenuBuilder } from "../../gui/menus/start-menu-builder";
import { IGameManager } from "../infrastructure/abstract-manager";
import { IDialog } from "../../gui/dialogs/common/dialog.interfaces";
import * as GUI from 'babylonjs-gui';
import * as BABYLON from 'babylonjs';

const GUI_TREE_DEPTH = 5;

export class GuiManager extends AbstractGuiContainer implements IGameManager {
    advTexture: GUI.AdvancedDynamicTexture;
    private showingInspector: boolean = false;
    // menu controls
    menues: Array<AbstractMenu> = [];
    topBar: TopBarMenu;
    actionButtonsBar: ActionButtonsBar;
    controllerBtns: ControllerButtons;
    // observables
    onCreation$: BABYLON.Observable<void> = new BABYLON.Observable<void>();
    onChildClicked$ = new BABYLON.Observable<string>();
    onShowIngameMenues$ = new BABYLON.Observable<void>();

    constructor() {
        super("GuiManager");
        // this.registerToObservables();
        this._createDomRoot();
        this.log(`created`);
        this.registerToObservables();
    }

    registerToObservables(): void {
        // Player
        globalThis.GLOOBS.onPlayerObjCreated$.add(
            () => {
                globalThis.PLAYER.onAfterPlayerMove$.add(
                    () => { this.updateTopBar(); }
                );
        });
        // Scenario start
        this.onShowIngameMenues$.add(
            () => { StartMenuBuilder.showStartingMenues(); }
        );
        this.onChildClicked$.add(
            (elemId) => {
                this.log(`element "${elemId}" clicked!`);
                for (const menu of this.menues) { menu.onElementClicked(elemId); }
                if (globalThis.DIALOG) { globalThis.DIALOG.onChildClicked$.notifyObservers(elemId); }
        });
    }

    private _createDomRoot() {
        // create global DOM_NODES array
        globalThis.DOM_NODES = new DomNodes();
        // create root node
        const domRoot = new DomNode("root");
        globalThis.DOM_NODES.registerNode(domRoot);
        this.log(`DOM Root created: ${domRoot}`);
    }

    public init(): void {
        this.log("building; doing nothing yet");
    }

    private buildAll(): void {

    }

    getControlById(controlId: string): GUI.Control {
        if (! this.advTexture) {
            this.error(`cant get control "${controlId}"; AdvTexture not found`); return;
        }
        for (const control of this.advTexture.getDescendants()) {
            if (control.name === controlId) { return control; }
        }
        return;
    }

    public showDialog(dialog: IDialog, forceClose?: boolean) {
        this.log(`showing dialog ${dialog.id} ..`);
        this.closeCurrentDialog(forceClose);
        globalThis.DIALOG = dialog;
        globalThis.DIALOG.build();
    }

    public closeCurrentDialog(force?: boolean) {
        if (globalThis.DIALOG) {
            if (globalThis.DIALOG.isBlocking && ! force) {
                this.warn(`cant close current dialog ${globalThis.DIALOG.id}; is blocked`); return;
            }
            globalThis.DIALOG.close();
        }
    }

    /* called from after app assets are loaded */
    public build(): GUI.AdvancedDynamicTexture {
        this.log(`creating babylon adv texture with scene ${globalThis.SCENE}..`);
        this.advTexture = GUI.AdvancedDynamicTexture
            .CreateFullscreenUI("UI_ROOT", true, globalThis.SCENE);
        return this.advTexture;
    }

    public toggleInspector(): boolean {
        this.showingInspector = ! this.showingInspector;
        if (this.showingInspector) globalThis.SCENE.debugLayer.show();
        else globalThis.SCENE.debugLayer.hide();
        return true;
    }

    updateTopBar(): void {
        this.topBar.updateAll();
    }

    playSound(soundName: string): void {

    }

    scopeStr: string;

    run() {
    }
}

// == CONSTANTS == */
// MENUES
export const MENU_TOP_ITEM_WIDTH = "150px";