import { AbstractManager } from "../infrastructure/abstract-manager";
import WorldMapScene from "../../scenes/world-scene";
import { TurnManager } from "../infrastructure/turn-manager";
import { WorldEnvironmentBuilder } from "../../scenes/builders/world-environment-builder";
import { EntityPlacer } from "./entity-placer";


export class SceneManager extends AbstractManager {
    worldScene: WorldMapScene;
    placer: EntityPlacer;
    // observables
    worldScenePrecreated$ = new BABYLON.Observable<WorldMapScene>();

    constructor() {
        super("Scene");
    }

    registerToObservables() {
        super.registerToObservables();
        this.worldScenePrecreated$.add(
            (worldScene) => {

        });
    }

    run(): void {

    }

    public loadWorld() {
        this.log("creating scene ..");
        // load world
        globalThis.MAP_NAME = "world_large";
        this.worldScene = new WorldMapScene('renderCanvas');
        globalThis.WORLD = this.worldScene;
        this.placer = new EntityPlacer(this.worldScene);
        // show the scene.
        globalThis.TURNS = new TurnManager();
        this.createScene();
    }

    /** Called by App.ts */
    public createScene(): void {
        this.worldScene.beforeCreation();
        WorldEnvironmentBuilder.createFogAndSky();
        // create materials & load models
        globalThis.MAP.createMaterials();
        globalThis.ASSETS.loadAssets();
        this.worldScene.spawnPlayerAndLocations();
    }
}