import { AbstractManager } from "../infrastructure/abstract-manager";
import * as BABYLON from 'babylonjs';
import WorldMapScene from "../../scenes/world-scene";

export class EntityPlacer extends AbstractManager {
    scene: WorldMapScene;

    constructor(scene: WorldMapScene) {
        super("EntityPlacer");
        this.scene = scene;
    }

    run(): void {

    }

}