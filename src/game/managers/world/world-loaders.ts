import { BaseEventListener } from "../../../common/models/events";
import { GameOptions } from "../../models/common/game/game-options";
import { Loggable } from "../../../common/models/log";
import { IDataGenerator, IDataLoader } from "../../../common/models/world/loader";
import { RandomElement } from "../../../helpers/random";
/* CHANGE USED MAP HERE */
import * as MAPDATA from "../../../../res/worlds/hansa_104_1150";
import { MapPos } from "../../world/pos";
import { MapTile } from "../../world/tile/tile";
import { LandscapeFeature } from "../../world/feature";
import { City } from "../../models/city/city";
import { Empire } from "../../models/faction/types/empire";
import { Properties } from "../../../common/models/properties/properties";
import { TradeResource } from "../../models/trade/resource/resource";
import { Family } from "../../models/faction/types/family";
import { IEmpire, IFamily } from "../../models/faction/faction-interfaces";

export class WorldTilesLoader extends BaseEventListener implements IDataLoader {
    constructor() {
        super("WorldTileLoader");
    }



    load(): MapTile[][] {
        let tileRows: MapTile[][] = [];
        let createdTilesCount: number = 0;
        // for row
        for (let z = 0; z < MAPDATA.TILE_ROWS.length; z++) {
            const terrRow = MAPDATA.TILE_ROWS[z];
            let tileRow: MapTile[] = [];
            for (let x = 0; x < terrRow.length; x++) {
                // find terrain char in array
                createdTilesCount = this.loadSingleTile(x, z, terrRow, createdTilesCount, tileRow);
            } // for x end
            tileRows.push(tileRow);
        } // for z end
        this.log(`${createdTilesCount} WorldTile objects loaded`);
        return tileRows;
    }

    private loadSingleTile(x: number, z: number, terrRow: string, createdTilesCount: number, tileRow: MapTile[]) {
        const pos = new MapPos(x, z);
        const terrainChar = terrRow[pos.x];
        // create WorldTile & increase counter
        const worldTile = this.makeWorldTile(terrainChar, pos);
        createdTilesCount += 1;
        tileRow.push(worldTile);
        return createdTilesCount;
    }

    /* returns WorldTile object with given pos & terrain */
    makeWorldTile(terrainChar: string, pos: MapPos): MapTile {
        const tile: MapTile = new MapTile(pos, terrainChar);
        // check whether tile is overwritten
        const terrainOverride = this.getTerrainOverwriteAtPos(pos);
        if (terrainOverride) {
            tile.terrainChar = terrainOverride;
        }
        return tile;
    }

    getTerrainOverwriteAtPos(pos: MapPos): string {
        for (const tileOverwriteStr of MAPDATA.TILE_OVERRIDE) {
            const tileOverrideData = tileOverwriteStr.split(" ");
            const tileOverridePos = MapPos.FromString(tileOverwriteStr);
            // return terrain key
            if (tileOverridePos.equals(pos)) { return tileOverrideData[2]; }
        }
        return null;
    }
}

/* == LOADER: LANDSCAPE FEATURE == */
export class WorldLandscapeLoader extends BaseEventListener implements IDataLoader {
    constructor() {
        super("LandscapeLoader");
    }

    load(): LandscapeFeature[] {
        let features: LandscapeFeature[] = [];
        for (const featureStr of MAPDATA.LANDSCAPE) {
            features.push( LandscapeFeature.FromString(featureStr) );
        }
        return features;
    }
}

/* == GENERATOR: CITIES == */
export class CityGenerator extends BaseEventListener implements IDataGenerator {
    constructor() {
        super("CityGenerator");
    }

    generate(): Array<City> {
        const maxCities = globalThis.OPTIONS.get(GameOptions.MAP_MAXCITIES);
        let generatedCities: Array<City> = [];
        for (let cityNum = 0; cityNum < maxCities; cityNum++) {
            generatedCities.push( this.generateSingleCity(cityNum) );
        }
        return generatedCities;
    }

    private generateSingleCity(cityNum: number): City {
        const generatedCity: City = City.MakeRandom();
        generatedCity.num = cityNum;
        return generatedCity;
    }
}

/**
 * parses CITIES from json and returns an array of {@link City}
 */
export class CityLoader extends Loggable {
    static load(): Array<City> {
        let cities: Array<City> = [];
        for (const cityValues of MAPDATA.CITIES) {
            cities.push( City.FromValueArray(cityValues) );
        }
        return cities;
    }
}

/* == LOADER: RND START POS == */
export class RandomStartPosLoader extends Loggable {
    static load(): MapPos {
        const rndPosStr = RandomElement.of(MAPDATA.RND_START_POS);
        return MapPos.FromString(rndPosStr);
    }
}

/**
 * parses CUSTOM_FAMILIES from mapdata json and returns an array of {@link IEmpire}
 */
export class WorldEmpireLoader extends BaseEventListener implements IDataLoader {
    constructor() {
        super('EmpireLoader');
    }

    load(): IEmpire[] {
        let empires: IEmpire[] = [];
        for (const empireJson of MAPDATA.CUSTOM_EMPIRES) {
            if (!empireJson["key"]) {
                this.warn(`cant load empire ${empireJson.toString()} has no key property}`);
                continue;
            }
            const empireKey = empireJson["key"];
            const empireName = empireJson["name"];
            const empire = new Empire(empireKey, empireName);
            // set empire flag data
            empire.flagData = empireJson["flag"];
            empire.updateFlag();
            empires.push(empire);
        }
        return empires;
    }
}

/**
 * takes CUSTOM_FAMILIES from mapdata & returns a parsed array of trade families as {@link IFamily}
 */
export class CustomFamiliesLoader extends BaseEventListener implements IDataLoader {
    constructor() {
        super("CustomFamilyLoader");
    }

    load(): Array<IFamily> {
        if (!Object.keys(MAPDATA).includes("CUSTOM_FAMILIES")) {
            this.warn(`cant load custom families; none in map file`);
            return;
        }
        let customFamilies: Array<IFamily> = [];
        for (const familyJson of MAPDATA["CUSTOM_FAMILIES"]) {
            const name = familyJson["name"];
            const family = new Family(name, null, false);
            // set home key but load city later
            family._cachedHomeCityKey = familyJson["home"];
            // set optional properties
            family.customLanguage = familyJson["lang"];
            family.customStartPos = MapPos.FromString(familyJson["customStartPos"])
            customFamilies.push(family);
        }
        return customFamilies;
    }
}


/* LOADER: RESOURCES */
export class SpecialResLoader extends BaseEventListener implements IDataLoader {
    constructor() {
        super('SpecialResLoader');
    }

    load(): Array<TradeResource> {
        let specialResources: Array<TradeResource> = [];
        if (MAPDATA["SPECIAL_RESOURCES"]) {
            const specialResJson = MAPDATA["SPECIAL_RESOURCES"];
            for (const singleResJson of specialResJson) {
                specialResources.push(TradeResource.FromJson(singleResJson));
            }
        }
        return specialResources;
    }
}

/* LOADER: MAP PROPERTIES == */
export class WorldPropertiesLoader extends BaseEventListener implements IDataLoader {
    static load(): Properties {
        let mapProperties = new Properties("map");
        // start & end year
        mapProperties.addInteger("startYear", MAPDATA.STARTING_YEAR);
        mapProperties.addInteger("endYear", MAPDATA.END_YEAR);
        // random start family?
        if (MAPDATA.PLAY_RANDOM_FAMILY == true) {
            mapProperties.addBoolean("randomStartFamily", true);
        }
        // seasons per turn
        mapProperties.addInteger("seasonsPerYear", MAPDATA.SEASONS_PER_YEAR);
        return mapProperties;
    }

}
