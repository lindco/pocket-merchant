import { RandomElement } from "../../../helpers/random";
import { MapPos } from "../../world/pos";
import { MapTile } from "../../world/tile/tile";
import { City } from "../../models/city/city";
import { AbstractBuildingType } from "../../models/building/type/building-type";
import { AbstractManager, IGameManager } from "../infrastructure/abstract-manager";
import { IConstructable } from "../../models/building/interfaces/constructable.interfaces";
import * as BABYLON from 'babylonjs';

export class CityManager extends AbstractManager implements IGameManager {
    _names: Array<string> = [];
    cities: Array<City> = [];
    // observables
    onCityLoaded$ = new BABYLON.Observable<City>();
    onCitiesLoaded$ = new BABYLON.Observable<Array<City>>();
    oAfterCitiesLoaded$ = new BABYLON.Observable<Array<City>>();
    onCitySpawned$ = new BABYLON.Observable<City>;
    onCitiesSpawned$ = new BABYLON.Observable<Array<City>>();

    constructor(generate?: boolean) { 
        super("City");
        this.registerToObservables();
    }

    registerToObservables() {
        this.onCitiesLoaded$.add(
            (cities: City[]) => {
                // add new cities to CityList holder & inform CityManager
                for (const city of cities) { this.addCity(city); }
                this.onCitiesLoaded(cities); // start processing
        });
        globalThis.GLOOBS.onTurnFinishing$.add(
            () => { // consume city resources
                for (const city of this.cities) city.onTurn();
        });
        this.onCitiesSpawned$.add( () => this.linkCityTiles() );
    }

    run() {

    }

    /* GET & SET CITIES */
    addCity(city: City): void {
        // assign citynum
        if (! city.num) { city.num = this.cities.length; }
        this._names.push(city.name);
        this.cities.push(city);
        // inform observable
        this.onCityLoaded$.notifyObservers(city);
    }

    createCity(name: string, pos: MapPos, culture: string, primaryResource: string) {
        let newCity: City = new City(null, name, pos, culture, primaryResource);
        this.addCity(newCity);
    }

    getAll(): Array<City> {
        return this.cities;
    }

    get citiesCount(): number {
        if (this.cities && this.cities.length > 0) return this.cities.length;
        return 0;
    }

    /* GET BY PROPERTY */
    getByNum(num: number): City {
        return this.cities[num];
    }

    getByKey(key: string): City {
        for (const city of this.cities)
            if (city.key == key) { return city; }
        return null;
    }

    getByName(name: string): City {
        let cityIndex: number = this._names.indexOf(name);
        return this.cities[cityIndex];
    }

    getCityAt(worldPos: MapPos): City {
        for (const city of this.cities)
            if (city.pos.equals(worldPos)) return city;
        return null;
    }

    getCitiesByFactionKey(factionKey: string): Array<City> {
        const factionCities: Array<City> = [];
        for (const city of this.getAll()) {
            if (city._factionKey && city._factionKey == factionKey) factionCities.push(city);
        }
        return factionCities;
    }

    // shortforms
    ByNum(num: number): City { return this.getByNum(num); }
    ByKey(key: string): City { return this.getByKey(key); }
    ByName(name: string): City { return this.getByName(name); }
    AtPos(pos: MapPos): City { return this.getCityAt(pos); }
    OfOwner(factionKey: string): Array<City> { return this.getCitiesByFactionKey(factionKey); }

    /* CITY MESH */
    getMeshes(): Array<BABYLON.AbstractMesh> {
        let cityMeshes: Array<BABYLON.AbstractMesh> = [];
        for (const city of this.cities) {
            if (city.mesh) cityMeshes.push(city.mesh);
        }
        return cityMeshes;
    }

    getRandom(): City {
        return RandomElement.of(this.cities) as City;
    }

    /* notifies a tile that a city is standing on it */
    linkCityTiles() {
        for (const city of this.getAll()) {
            let tileAt: MapTile = globalThis.MAP.tileAtPos(city.pos);
            tileAt.city = city;
        }
        this.log("flagged city tiles");
    }

    makeCityRegions() {
        // link neighboring tiles
        this.log('TODO generate city regions');
    }

    initializeCityEmpire() {
        let ownedCitiesAmount: number = 0;
        for (const city of this.getAll()) {
            if ( city._factionKey ) { // when city has an owner..
                const empireObj = globalThis.FACTIONS.getEmpireByKey( city._factionKey );
                if (empireObj) { // .. that is an empire
                    empireObj.giveCityOwnership(city, true);
                    ownedCitiesAmount += 1;
                }
            }
        }
        this.log(`given empire ownership to ${ownedCitiesAmount} cities`);
    }

    /* CITY FEATURES */
    initializeCityFeatures() {
        for (const city of this.getAll())
            city.initializeCityFeaturesFromStr();
    }

    /* BUILDINGS */
    getAllBuildingTypes(): IConstructable[] {
        return globalThis.CONSTRUCTABLE.getAll();
    }

    getBuildingTypesByCat(bldCatNum: number): AbstractBuildingType[] {
        let catBuildings = [];
        for (const bldType of this.getAllBuildingTypes())
            if (bldCatNum == bldType.category.num) catBuildings.push(bldType);
        return catBuildings;
    }

    /* ON EVENTS */

    /* fired by world scene after all cities have been added */
    onCitiesLoaded(cities: Array<City>) {
        this.initializeCityFeatures();
        // initialize cities ownership
        this.initializeCityEmpire();
        globalThis.GLOOBS.onCitiesLoaded$.notifyObservers(cities);
        // initialize city regions
        this.makeCityRegions();
        this.oAfterCitiesLoaded$.notifyObservers(this.cities);
    }

}