import {AbstractBuildingType, ProductionBuildingType} from "../../models/building/type/building-type";
import { AbstractManager } from "../infrastructure/abstract-manager";
import { CivicBuildingTypes } from "../../models/building/type/building-types";
import { JsonDataFiles } from "../../models/infrastructure/loading/json-data-file";
import { IBuildingType } from "../../models/building/interfaces/constructable.interfaces";

export class BuildingManager extends AbstractManager {
    _fileJson: {} = null;

    constructor() {
        super("Building");
        // subscribe to json loader
        globalThis.JSON_LOADER.onAllLoaded$.add( () => {
            const bldData = globalThis.JSON_LOADER.getJsonData(JsonDataFiles.BUILDING_TYPES);
            this._onJsonLoaded(bldData);
        });
    }

    run() {

    }

    initializeBuildingTypes() {
        this.log(`initializing building types`);
        // when resource types not yet loaded -> abort
        if (! globalThis.RESOURCE_TYPES) {
            this.error(`cant initialize bld types; resource types not loaded`); return;
        }
        // create production & civic bld types
        const resTypesCount = this.createProductionBldTypes();
        const civicTypesCount = this.createCivicBldTypes();
        this.log(`${resTypesCount} production and ${civicTypesCount} civic building types loased`);
        this.onBldTypesInitialized();
    }

    createStartingBldInCities() {
        for (const city of globalThis.CITIES.getAll())
            city.addStartingBuildings();
    }

    /* BUILDING TYPE CREATION */
    createProductionBldTypes(): number {
        let resTypesCount = 0;
        for (const res of globalThis.TRADE.getResources()) {
            const prodBldType = new ProductionBuildingType(res.id);
            this.addAsConstructable(prodBldType);
            resTypesCount += 1;
        }
        // returns the amount of created resource production bld types
        return resTypesCount;
    }

    createCivicBldTypes(): number {
        let civicTypesCount = 0;
        for (const civicBldType of CivicBuildingTypes.LIST) {
            this.addAsConstructable(civicBldType);
            civicTypesCount += 1;
        }
        return civicTypesCount;
    }

    addAsConstructable(bldType: AbstractBuildingType) {
        globalThis.CONSTRUCTABLE.add(bldType);
    }

    /* EVENT LISTENING */
    _onJsonLoaded(bldTypeJson: {}) {
        this._fileJson = bldTypeJson;
        this.log("Building Types received");
        // initialize bld types
        this.initializeBuildingTypes();
    }

    onBldTypesInitialized() {
        this.createStartingBldInCities();
    }

    getBuildingTypeById(bldTypeId: string): IBuildingType {
        return globalThis.CONSTRUCTABLE.get(bldTypeId);
    }
}