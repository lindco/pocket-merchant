import { TradeConnection } from "../../models/trade/connection/connection";
import { IConnectedLocation } from "../../world/interfaces/location.interfaces";
import { AbstractManager } from "../infrastructure/abstract-manager";


export class ConnectionManager extends AbstractManager {
    connections: Array<TradeConnection> = [];

    constructor() {
        super("Connection");
        globalThis.CITIES.oAfterCitiesLoaded$.add(
            () => this.makeCityConnections()
        );
    }

    run() {

    }

    makeCityConnections() {
        if (! globalThis.CITIES) {
            this.warn(`cant create connections; CityManager not found`); return;
        }
        this.log("creating city connections ..");
        const createdConnections: Array<TradeConnection> = [];
        for ( const city of globalThis.CITIES.getAll() ) {
            const cityConnections =  city.addConnections( TradeConnection.MAX_DISTANCE );
            createdConnections.concat(cityConnections);
        }
        // add city connections to manager connection list
        this.connections.concat(createdConnections);
    }

    fromTo(source: IConnectedLocation, target: IConnectedLocation): TradeConnection {
        for (const conn of source.connections) {
            if ( conn.hasTarget(target) ) { return conn; }
        }
        return;
    }

    getMirrored(connection: TradeConnection): TradeConnection {
        return this.fromTo(connection.target, connection.source);
    }
}