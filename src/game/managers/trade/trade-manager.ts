import { StoredResource } from '../../models/trade/resource/stored-resource';
import { TradeNode } from '../../models/trade/trade-node/trade-node';
import { MarketTrade } from '../../models/trade/market-trade';
import { ConnectedLocation } from '../../world/location';
import { TradeConnection } from '../../models/trade/connection/connection';
import { Road } from '../../world/road';
import { TradeResource } from '../../models/trade/resource/resource';
import { AbstractManager } from "../infrastructure/abstract-manager";
import { TradeDelivery } from "../../models/trade/trade-delivery";
import { IRoad } from "../../world/interfaces/road.interface";
import { IConnectedLocation, ITradeLocation } from "../../world/interfaces/location.interfaces";
import { RandomDice } from "../../../helpers/random";

/* == TRADE MANAGER == */
export class TradeManager extends AbstractManager {
    worldTradeHistory: Array<MarketTrade> = [];
    // roads
    roads: Array<Road> = [];
    lastRoadNum: number = -1;

    constructor() {
        super("Trade");
        this.registerToObservables();
    }

    registerToObservables() {
        super.registerToObservables();
        globalThis.CITIES.oAfterCitiesLoaded$.add(() => this.onCitiesLoaded());
        globalThis.GLOOBS.onTurnFinishing$.add(() => this.makeCityDeliveries());
    }

    run() {

    }
    
    onCitiesLoaded(): void {
        this.createRoadNetwork();
    }

    overrideSpecialResources() {
        
    }

    makeCityDeliveries(): void {
        for (const city of globalThis.CITIES.getAll())
            if (RandomDice.roll(10)) city.makeBestDelivery();
    }

    /* called when city delivery event is fired */
    onAfterDelivery(delivery: TradeDelivery) {
        // get locations & log
        const sourceLocation = delivery.sourceNode.parentLocation;
        const targetLocation = delivery.targetNode.parentLocation;
        /* this.log(`delivered ${delivery.resAmount} ${delivery.resId} for ${delivery.totalProfit} coins
            profit from ${sourceLocation.name} to ${targetLocation.name}`); */
        const turnNum = globalThis.TURNS.turnNum();
        const marketTrade = new MarketTrade(turnNum, delivery.resId, delivery.totalProfit,
            targetLocation, sourceLocation );
        // add to world trade history
        this.worldTradeHistory.push(marketTrade);
        // increase city connection counter
        const connection = globalThis.CONN.fromTo(sourceLocation, targetLocation);
        connection.addToTradeHistory(marketTrade, true);
        // add to trade history
        sourceLocation.tradeHistory.add(marketTrade, true);
        targetLocation.tradeHistory.add(marketTrade, false);
    }

    onPlayerTrade(isSell: boolean, city: ITradeLocation, storedRes: StoredResource, profit: number) {

    }

    get hasSpecialResLoaded(): boolean {
        return globalThis.SPECIAL_RES && globalThis.SPECIAL_RES.length > 0;
    }

    /* inform gui over changed player monex */
    onPlayerCoinsChange(): void {
        globalThis.GUI.updateTopBar();
    }

    getRes(resId: string): TradeResource {
        if (resId.startsWith("X")) {
            const xResNum = parseInt( resId.replace("X", "") );
            return globalThis.SPECIAL_RES[xResNum];
        } else return globalThis.RESOURCE_TYPES.get(resId);
    }

    getResources(): Array<TradeResource> {
        let allResTypes: Array<TradeResource> = [];
        for (const resId of RESOURCE_TYPES.getIds())
            allResTypes.push(this.getRes(resId));
        return allResTypes;
    }

    getResByName(resName: string): TradeResource {
        for (const tradeRes of globalThis.RESOURCE_TYPES.getAll())
            if (tradeRes.name == resName) return tradeRes;
        return;
    }

    /* returns the resource name string ("Grain") of a resource with given resId */
    getResName(resId: string): string {
        return this.getRes(resId).name;
    }

    getMarkets(): TradeNode[] {
        let markets: TradeNode[] = [];
        // city markets
        const cities = globalThis.CITIES.getAll();
        if (cities.length >= 0) this.error("cant get markets; cities not loaded yet");
        // add market to list
        for (const city of cities) markets.push(city.tradeNode);
        return markets;
    }

    /* CITY CONNECTIONS */
    private getCityConnection(from: IConnectedLocation, to: IConnectedLocation): TradeConnection {
        const connection = from.getConnectionTo(to);
        if (! connection) {
            this.warn(`cant get city connection: ${from.name} to ${to.name}`);
            return;
        }
        return connection;
    }

    /* ROADS */
    createRoadNetwork(): number {
        let roadCounter: number = 0;
        // create road objects
        for (const sourceCity of globalThis.CITIES.cities) {
            let sourceLocation: ConnectedLocation = sourceCity as ConnectedLocation;
            for (const connection of sourceLocation.connections) {
                let targetLocation: ConnectedLocation = connection.target as ConnectedLocation;
                this.addRoad(sourceLocation, targetLocation);
                roadCounter += 1;
            }
        }
        this.log(`${roadCounter} roads created`);
        // find road paths
        for (const road of this.roads) road.findRoadPath();
        this.log("created road paths");
        return roadCounter;
    }

    nextRoadNum(): number {
        const nextRoadNum: number = this.lastRoadNum +1;
        this.lastRoadNum = nextRoadNum;
        return nextRoadNum;
    }

    getRoad(roadNum: number): Road {
        return this.roads[roadNum];
    }

    getRoadFromTo(fromCity: IConnectedLocation, toCity: IConnectedLocation): IRoad {
        let roadsFromTarget: Array<IRoad> = fromCity.roads;
        for (const startCityRoad of roadsFromTarget) {
            const isToTargetCity: boolean = startCityRoad.target.equals(toCity);
            if(isToTargetCity) return startCityRoad;
        }
        this.warn(`cant find road between ${fromCity.name} & ${toCity.name}`);
        return null;
    }

    getReturnRoad(roadObj: IRoad): IRoad {
        return this.getRoadFromTo(roadObj.target, roadObj.start);
    }

    addRoad(startCity: IConnectedLocation, targetCity:IConnectedLocation): Road {
        let newRoad: Road = new Road(startCity, targetCity, this.nextRoadNum());
        startCity.roads.push(newRoad);
        targetCity.roads.push(newRoad);
        this.roads.push(newRoad);
        return newRoad;
    }

    /* GETTER SHORTFORMS */
    GetRes(resId: string): TradeResource { return this.getRes(resId); }
    GetResName(resId: string): string { return this.getRes(resId).name; }
}