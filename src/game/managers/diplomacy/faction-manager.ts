import { RandomDice, RandomInt } from "../../../helpers/random";
import { Empire } from "../../models/faction/types/empire";
import { Family } from "../../models/faction/types/family";
import { Cultures } from "../../models/faction/culture";
import { City, ICity } from "../../models/city/city";
import { CustomFamiliesLoader, WorldEmpireLoader } from "../world/world-loaders";
import { AbstractManager } from "../infrastructure/abstract-manager";
import { ArmyManager } from "../military/army-manager";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { IEmpire, IFaction, IFamily } from "../../models/faction/faction-interfaces";
import * as BABYLON from 'babylonjs' ;

/* == FACTION MANAGER == */
export class FactionManager extends AbstractManager {
    // families
    families: Array<Family> = [];
    playerFamily: Family = null;
    // empires
    empires: Array<Empire> = [];
    _empireKeys: Array<string> = [];
    cultures: Cultures = null;
    // flags
    hasFamilyHomesLoaded: boolean = false;
    hasStartingArmiesCreated: boolean = false;
    hasEmpireCapitalsAssigned: boolean = false;
    // observables
    public onCapitalsAssigned$ = new BABYLON.Observable<Array<ICity>>();
    // private
    private lastFactionNum: number = -1;
    private _empireCapitalKeys: {} = {};

    constructor() {
        super("Faction");
        this.loadCultures();
        this.createDefaultEmpires();
        this.loadEmpiresFromTsFile();
        this.loadOrGenerateFamilies();
    }

    registerToObservables() {
        super.registerToObservables();
        globalThis.JSON_LOADER.onLanguagesLoaded$.add(
            () => {
                this.assignFamilyHomes();
            }
        )
        globalThis.GLOOBS.onCitiesLoaded$.add(
            () => {
                this.assignFamilyHomes();
                this.assignEmpireCapitals();
                this.createStartingArmies();
            }
        )
    }

    run() {

    }

    loadOrGenerateFamilies(): Array<IFamily> {
        const familiesFromMap = new CustomFamiliesLoader().load();
        if (familiesFromMap && familiesFromMap.length > 0) {
            this.loadMapFamilies(familiesFromMap);
        } else {
            this.createRandomFamilies();
        }
        return null;
    }

    private loadMapFamilies(customFamilies: Array<IFamily>) {
        let isFirstFamily: boolean = true;
        for (const family of customFamilies) {
            if (isFirstFamily) family.isAi = false;
            this.addFamily(family as Family);
            isFirstFamily = false;
        }
    }

    /* FACTION GETTERS */
    getFactionByNum(factionNum: number): IFaction {
        if (factionNum > this.lastFactionNum) {
            this.error(`cant get faction with num ${factionNum}; only ${this.lastFactionNum} factions loaded`);
            return;
        }
        for (const faction of this.allFactions) {
            if (faction.num == factionNum) return faction;
        }
        return null;
    }

    getEmpireByKey(empireKey: string): Empire {
        for (const empire of this.empires)
            if (empire.key == empireKey) return empire;
        return null;
    }

    get allFactions(): Array<IFaction> {
        let allFactions: Array<IFaction> = [];
        for (const family of this.families) allFactions.push(family); // collect families
        for (const empire of this.empires) allFactions.push(empire); // .. & empires
        return allFactions; // and return all as IFaction array
    }

    getFactionByKey(factionKey: string): IFaction {
        // family?
        if (factionKey.startsWith("F") && factionKey.length == 2) {
            const familyNum = parseInt(factionKey.replace("F", ""));
            return this.getFamilyByNum(familyNum);
        }
        // empire?
        return this.getEmpireByKey(factionKey);
    }

    /* shortforms */
    ByKey(key: string) {
        return this.getFactionByKey(key);
    }

    /* FAMILY MANAGEMENT */
    addFamily(family: Family): void {
        const familyNum = this.families.length;
        family.setNum(familyNum);
        this.families.push(family);
        if (!family.isAi) {
            this.playerFamily = family;
        }
    }

    addRandomFamily(name?: string, randomHome?: boolean): Family {
        const playersAmount = this.families.length
        const isPlayer = playersAmount == 0;
        let rndFamily: Family = Family.random();
        if (randomHome) rndFamily.hasRandomHome = true;
        if (isPlayer) {
            rndFamily.isAi = false;
            this.playerFamily = rndFamily;
        }
        this.addFamily(rndFamily);
        return rndFamily;
    }

    createRandomFamilies(): Array<Family> {
        let families: Array<Family> = [];
        for (let familyNum = 0; familyNum < RND_FAMILY_COUNT; familyNum++) {
            this.addRandomFamily(null, true);
        }
        return families;
    }

    getPlayerFamily(): Family {
        if (this.families.length == 0) {
            this.error(`cant get player faction; families not generated`);
            return;
        }
        return this.families[0];
    }

    getFamilyByNum(num: number): Family {
        const foundFamily = this.families[num];
        if (!foundFamily) {
            this.error(`no family ${num} found in ${this.getFamilyNums()}`);
            return;
        }
        return foundFamily;
    }

    getFamilyByName(name: string): Family {
        for (const family of this.families) {
            if (family.name === name) return family;
        }
        return null;
    }

    getFamilyNums(): Array<number> {
        let nums: Array<number> = [];
        for (const family of this.families) {
            nums.push(family.num);
        }
        return nums;
    }

    getFactionArmies(faction: IFaction): Array<IUnitArmy> {
        if (!globalThis.ARMIES) {
            this.warn(`cant get armies of ${faction.name}: ArmyManager not found`);
            return;
        }
        return globalThis.ARMIES.getArmiesByFaction(faction);
    }

    getFactionArmiesByCat(faction: IFaction, armyCatNum: number) {
        const factionArmies = this.getFactionArmies(faction);
        return ArmyManager.FilterByArmyCatNum(factionArmies, armyCatNum);
    }

    /* EMPIRE MANAGEMENT */
    addEmpire(empire: Empire): void {
        this._empireKeys.push(empire.key);
        this.empires.push(empire);
    }

    getEmpireCities(empireKey: string): Array<City> {
        let empireCities: Array<City> = [];
        for (const city of globalThis.CITIES.getAll()) {
            if (city._factionKey && city._factionKey == empireKey) {
                empireCities.push(city);
            }
        }
        return empireCities;
    }

    getEmpires(hideDefault?: boolean): Array<IEmpire> {
        const empires: Array<IEmpire> = [];
        for (const empire of this.empires) {
            let hideEmpire: boolean = false;
            if (hideDefault == true && empire.isDefault) {
                hideEmpire = true;
            }
            if (!hideEmpire) empires.push(empire);
        }
        return empires;
    }

    getEmpireKeys(hideDefault?: boolean): Array<string> {
        const empireKeys: Array<string> = [];
        for (const nonHiddenEmpire of this.getEmpires(hideDefault)) {
            empireKeys.push(nonHiddenEmpire.key)
        }
        return empireKeys;
    }

    /* LOADING */
    private loadCultures() {
        this.cultures = new Cultures();
        this.log(`loaded ${this.cultures.length} cultures`);
    }

    private createDefaultEmpires(): void {
        // indie empire
        const independent = new Empire("I", "Independent", null, null);
        this.addEmpire(independent);
    }

    private assignFamilyHomes() {
        if (this.hasFamilyHomesLoaded) {
            return;
        }
        for (const family of this.families) {
            const familyHasNoHome: boolean = !family.home;
            if (familyHasNoHome && family.hasRandomHome)
                family.setHome(globalThis.CITIES.getRandom());
        }
        this.hasFamilyHomesLoaded = true;
    }

    private loadEmpiresFromTsFile() {
        const mapEmpires = new WorldEmpireLoader().load();
        const empiresCount = mapEmpires.length;
        let empireNames: Array<string> = [];
        for (const empire of mapEmpires) {
            this.addEmpire(empire as Empire);
            empireNames.push(empire.name);
        }
        const empiresStr = empireNames.join(", ");
        this.log(`${empiresCount} empires were loaded: ${empiresStr}`);
    }

    assignEmpireCapitals(loadOnly?: boolean) {
        if (this.hasEmpireCapitalsAssigned) return;
        const capitals: Array<ICity> = [];
        this.log(`start empire capital assignment`);
        // check for FactionManager
        if (!(globalThis.FACTIONS && globalThis.FACTIONS.empires && globalThis.FACTIONS.empires.length > 1)) {
            this.warn(`cant assign empire capitals: FactionManager not initialized`);
            return;
        }
        let empireCapitalsCount: number = 0;
        for (const city of globalThis.CITIES.getAll()) {
            const ownerKey = city._factionKey;
            const empiresWithCapital = Object.keys(this._empireCapitalKeys);
            const ownerHasCapital = empiresWithCapital.includes(ownerKey);
            if (!ownerHasCapital) {
                this.assignSingleEmpireCapital(ownerKey, city.key, true);
                capitals.push(city);
            }
        }
        // when all capitals are assigned
        const empireListStr = Object.keys(this._empireCapitalKeys).join(", ");
        this.log(`capitals for ${empireCapitalsCount} empires assigned: ${empireListStr}`);
        this.hasEmpireCapitalsAssigned = true;
        // inform other managers
        this.onCapitalsAssigned$.notifyObservers(capitals);
    }

    assignSingleEmpireCapital(empireKey: string, capitalCityKey: string, loadOnly?: boolean): boolean {
        this._empireCapitalKeys[empireKey] = capitalCityKey;
        const empireObj: IEmpire = this.getEmpireByKey(empireKey);
        const setToFaction = empireObj && !loadOnly;
        if (setToFaction) {
            const cityObj = globalThis.CITIES.getByKey(capitalCityKey);
            if (cityObj) {
                empireObj.home = cityObj;
                return true;
            }
        }
        return false;
    }

    getEmpireCapitalKey(empireKey: string): string {
        const empiresWithCapitalKeys = Object.keys(this._empireCapitalKeys);
        if (! empiresWithCapitalKeys.includes(empireKey) ) { return; }
        return this._empireCapitalKeys[empireKey];
    }

    /* STARTING ARMIES */
    private createStartingArmies() {
        if (this.hasStartingArmiesCreated) { return; }
        this.log(`placing starting armies for ${this.allFactions.length} factions..`);
        let armiesCreated: number = 0;
        for (const faction of this.allFactions) {
            let amount: number = RandomInt.below( MAX_STARTING_ARMIES );
            for (let index = 0; index < amount; index++) {
                armiesCreated = this.createRandomFactionArmy(faction, armiesCreated);
            }
        }
        this.hasStartingArmiesCreated = true;
        this.log(`created ${armiesCreated} starting armies`);
    }

    private createRandomFactionArmy(faction: IFaction, armiesCreated: number) {
        let rndIsPatrol: boolean = RandomDice.roll(2);
        faction.createNewArmy(rndIsPatrol)
        armiesCreated += 1;
        return armiesCreated;
    }

    nextFactionNum(): number {
        this.lastFactionNum += 1;
        return this.lastFactionNum;
    }
}

/* == CONSTANTS == */
const MAX_STARTING_ARMIES = 4;
const RND_FAMILY_COUNT = 5;