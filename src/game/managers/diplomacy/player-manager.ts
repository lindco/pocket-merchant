import { MapPos } from "../../world/pos";
import { WorldTools } from "../../../helpers/world-tools";
import { AbstractManager } from "../infrastructure/abstract-manager";
import * as BABYLON from 'babylonjs';
import { UnitArmy } from "../../models/military/army/army";
import { IPlayer } from "../../models/player/player.interface";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";

export class PlayerManager extends AbstractManager {
    playerArmy: IUnitArmy;
    // observables
    onBeforePlayerMove$ = new BABYLON.Observable<MapPos>();
    onPlayerObjCreated$ = new BABYLON.Observable<IPlayer>();
    onPlayerArmyCreated$= new BABYLON.Observable<IUnitArmy>();
    // flags
    public hasPlayerArmyCreated: boolean = false;

    constructor() {
        super("Player");
        this.onPlayerObjCreated$.add(
            () => {
                globalThis.GLOOBS.onPlayerObjCreated$.notifyObservers();
        });
    }

    run() { }

    movePlayerTo(newPos: MapPos) {
        const playerStartPos = globalThis.PLAYER.pos;
        globalThis.PLAYER.setPos(newPos, null, playerStartPos);
    }

    canMoveTo(targetPos: MapPos): boolean {
        const playerPos = globalThis.PLAYER.pos;
        const sceneDist = WorldTools.getWorldPosDistance(targetPos, playerPos);
        return true;
    }
}
