import { RandomElement } from "../../../helpers/random";
import { City } from "../../models/city/city";
import { Character, ICharacter } from "../../models/character/character";
import { AbstractManager } from "../infrastructure/abstract-manager";
import * as BABYLON from "babylonjs";
import { JsonDataFiles } from "../../models/infrastructure/loading/json-data-file";

export class CharacterManager extends AbstractManager {
    nameList: any = null;
    characters: Array<Character> = [];
    hasCreatedLeaders: boolean = false;
    // observables
    onLanguagesLoaded$ = new BABYLON.Observable<{}>();

    constructor() {
        super("CharManager");
        this.registerToObservables();
    }

    registerToObservables() {
        super.registerToObservables();
        globalThis.JSON_LOADER.onLanguagesLoaded$.add((languageData) => {
            this.onLanguagesLoaded$.notifyObservers(languageData);
        });
        this.onLanguagesLoaded$.add(
            (languageData) => {
                this.log("Character name list initialized");
                this.log(languageData.toString());
                this.nameList = globalThis.JSON_LOADER.getJsonData(JsonDataFiles.CHARACTER_NAMES);
                // create leaders
                this.createCityLeaders();
            }
        )
        globalThis.GLOOBS.onCitiesLoaded$.add(
            () => this.createCityLeaders()
        );
    }

    get hasNameListLoaded(): boolean {
        this.nameList = globalThis.JSON_LOADER.getJsonData(JsonDataFiles.CHARACTER_NAMES);
        if ( this.nameList ) {
            const languagesCount = Object.keys(this.nameList).length;
            if ( languagesCount > 1 ) { return true; }
        }
        return false;
    }

    createPlayerCharacter() {

    }

    createCityLeaders() {
        const canCreateLeaders = this.checkCityLeadersCreation();
        if (canCreateLeaders) {
            let createdCount: number = 0;
            for (const city of globalThis.CITIES.getAll()) {
                this.createCharacterIn(city, true);
                createdCount += 1;
            }
            this.log(`created ${createdCount} character leaders in cities`);
            this.hasCreatedLeaders = true;
        }
    }

    private checkCityLeadersCreation(): boolean {
        if(this.hasCreatedLeaders) { return; }
        if(! this.hasNameListLoaded) {
            const jsonFiles = globalThis.JSON_LOADER.jsonDataFiles;
            this.warn(`cant create city leaders; name list not loaded yet in JsonFiles ${jsonFiles}`);
            return false;
        }
        const citiesCount = globalThis.CITIES.cities.length;
        if (citiesCount <= 3) {
            this.warn(`createCityLeaders: City list is not loaded yet`);
            return false;
        }
        return true;
    }

    /* CHARACTER GETTERS */
    getByNum(charNum: number): Character {
        return this.characters[charNum];
    }

    getRandom(): Character {
        return RandomElement.of(this.characters) as Character;
    }

    getRandomName(language: string, isFemale?: boolean): string {
        if (! this.hasNameListLoaded) {
            this.warn(`cant generaate random name; name list not found`);
            return;
        }
        const languageNameList = this.nameList[language];
        // family name
        let rndFamilyName = "";
        if (languageNameList[2].length > 0) { rndFamilyName = RandomElement.of(languageNameList[2]); }
        // own name
        let rndCharName: string = RandomElement.of(languageNameList[0]);
        if (isFemale) { rndCharName = RandomElement.of(languageNameList[1]); }
        return rndCharName + " " + rndFamilyName;
    }

    getCityLeaders(): Array<ICharacter> {
        const citiesAreLoaded = globalThis.CITIES && globalThis.CITIES.citiesCount > 0;
        if (! citiesAreLoaded) {
            this.warn(`cant get city leaders: CityManager not initialized`); return;
        }
        const cityLeaders: Array<ICharacter> = [];
        for (const city of globalThis.CITIES.getAll()) {
            const cityLeader: ICharacter = city.leader as Character;
            if (cityLeader) { cityLeaders.push(cityLeader); }
        }
        return cityLeaders;
    }

    /* NAME LIST */
    getRandomFamilyName(langId: string): string {
        if(! this.hasNameListLoaded) {
            this.error("NAME LIST NOT LOADED");
            return;
        }
        const familyNames = this.nameList[langId][2];
        return RandomElement.of(familyNames);
    }

    /* CHARACTER CREATION */
    addCharacter(characterObj: Character): Character {
        this.characters.push(characterObj);
        return characterObj;
    }

    createCharacterIn(city: City, isLeader?: boolean): Character {
        const charNum = this.characters.length;
        const isFemale = city.num % 2 == 0;
        const cityLanguage = city.language;
        // city is owned by a faction?
        let cityOwnerNum = -1;
        if ( city.owner && city.owner.num >= 0) { cityOwnerNum = city.owner.num; }
        // create new Character object
        let rndChar: Character = new Character(charNum, cityOwnerNum, cityLanguage, null, isFemale);
        // assign home & pos
        rndChar.home = city;
        rndChar.pos = city.pos;
        // add to city characters and make leader when required
        if ( isLeader ) { city.leader = rndChar; }
        city.localCharacters.push(rndChar);
        // add to character list
        this.addCharacter(rndChar);
        return rndChar;
    }

    run() {
    }

}
