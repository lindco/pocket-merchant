import { UnitWeapon, UnitArmour } from "../../models/military/gear/gear";
import { UnitMount } from "../../models/military/gear/mount";
import { AbstractManager } from "../infrastructure/abstract-manager";
import { JsonDataFiles } from "../../models/infrastructure/loading/json-data-file";
import * as BABYLON from "babylonjs";

export class UnitGearManager extends AbstractManager {
    // mounts
    _mountsJson: {} = null;
    _mountIds: Array<string> = [];
    mountTypes: Array<UnitMount> = [];
    // weapons
    _weaponsJson: {} = null;
    _weaponIds: Array<string> = [];
    weaponTypes: Array<UnitWeapon> = [];
    // armor
    _armorJson: {} = null;
    _armorIds: Array<string> = [];
    armorTypes: Array<UnitArmour> = [];
    // observables
    mountListLoaded$ = new BABYLON.Observable<any>();
    gearListLoaded$ = new BABYLON.Observable<any>();

    constructor() {
        super("UnitGear");
        this.subscribeToJsonLoader();
    }

    private subscribeToJsonLoader(): void {
        // initialize internal observables
        globalThis.JSON_LOADER.onOneLoaded$.add(
            jsonDataFile => {
                if (jsonDataFile.key === JsonDataFiles.MOUNTS) {
                    this.onMountListLoaded(jsonDataFile.data);
                }
                else if(jsonDataFile.key === JsonDataFiles.GEAR_TYPES) { this.onGearListLoaded(jsonDataFile.data); }
        });
        // mounts loaded
        this.mountListLoaded$.add(() => {

        })
        // gear loaded

    }

    run() {

    }

    /* MOUNT & GEAR BY ID */
    getWeaponById(weaponId: string): UnitWeapon {
        const weaponNum = this._weaponIds.indexOf(weaponId);
        return this.weaponTypes[weaponNum];
    }

    getArmorById(armorId: string): UnitArmour {
        const armorNum = this._armorIds.indexOf(armorId);
        return this.armorTypes[armorNum];
    }

    getMountById(mountId: string) {
        const mountNum = this._mountIds.indexOf(mountId);
        return this.mountTypes[mountNum];
    }

    /* MOUNT & GEAR CREATION */
    addMountType(mount: UnitMount, overwriteId?: string): UnitMount {
        if (overwriteId) { mount.id = overwriteId; }
        this._mountIds.push(mount.id);
        this.mountTypes.push(mount);
        return mount;
    }

    addWeaponType(weapon: UnitWeapon, overwriteId?: string): UnitWeapon {
        if (overwriteId) { weapon.id = overwriteId; }
        this._weaponIds.push(weapon.id);
        this.weaponTypes.push(weapon);
        return weapon;
    }

    addArmorType(armor: UnitArmour, overwriteId?: string): UnitArmour {
        if (overwriteId) { armor.id = overwriteId; }
        this._armorIds.push(armor.id);
        this.armorTypes.push(armor);
        return armor;
    }

    /* MOUNT & GEAR LOADING */
    onMountListLoaded(data: {}) {
        this.log(`mount list ${data} loaded!!`);
        this._mountsJson = data;
        for (const mountId of Object.keys(data)) {
            const jsonValues = data[mountId];
            const baseHealth = parseInt(jsonValues["health"]);
            const baseMove = parseInt(jsonValues["mp"]);
            const baseCargo = parseInt(jsonValues["cargo"]);
            const isLand = jsonValues["isLand"];
            const terrModsStr = jsonValues["terrainModifiers"];
            const mount = new UnitMount(isLand, baseHealth, baseMove, terrModsStr, baseCargo);
            this.addMountType(mount, mountId);
        }
        this.checkMountsAndGearLoaded();
    }

    onGearListLoaded( gearListObj: {} ) {
        // weapons
        this._weaponsJson = gearListObj["weapons"];
        for (const weaponId of Object.keys(this._weaponsJson)) {
            const jsonValues = this._weaponsJson[weaponId];
            const weaponObj = UnitWeapon.FromJson(jsonValues);
            this.addWeaponType(weaponObj, weaponId);
        }
        // armor
        this._armorJson = gearListObj["armor"];
        for (const armorId of Object.keys(this._armorJson)) {
            const jsonValues = this._armorJson[armorId];
            const armorObj = UnitArmour.FromJson(jsonValues);
            this.addArmorType(armorObj, armorId);
        }
        // finish gear loading
        this.log(`gear data loaded; initializing ${this.weaponsCount} weapons & ${this.armorCount} armor types`);
        this.checkMountsAndGearLoaded();
    }

    checkMountsAndGearLoaded() {
        if (this.mountTypes.length > 2 && this.weaponTypes.length > 2 && this.armorTypes.length > 2) {
            this.log(`all mount, weapon & armour types loaded!`);
            // inform faction manager
            globalThis.UNITS.onGearTypesLoaded();
            globalThis.FACTIONS.assignEmpireCapitals();
            // fire event
        }
    }

    get mountAndGearCount(): number {
        return this.armorCount + this.weaponsCount + this.mountsCount;
    }

    get weaponsCount(): number { return this.weaponTypes.length; }
    get armorCount(): number { return this.armorTypes.length; }
    get mountsCount(): number { return this.mountTypes.length; }
}