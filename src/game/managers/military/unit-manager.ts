import { IBaseEvent } from '../../../common/models/events';
import { MapPos } from "../../world/pos";
import { Unit } from "../../models/military/unit";
import { UnitTypes } from "../../models/military/type/unit-types";
import { UnitType } from "../../models/military/type/unit-type";
import { AbstractManager } from "../infrastructure/abstract-manager";
import * as BABYLON from 'babylonjs';
import { IUnitType } from "../../models/military/interfaces/unit-type.interface";

export class UnitManager extends AbstractManager {
    // lastArmyNum: number = -1;
    units: Array<Unit> = [];
    // observables
    onUnitTypesLoaded$ = new BABYLON.Observable<Array<IUnitType>>();

    constructor() {
        super("Unit");
        globalThis.CITIES.oAfterCitiesLoaded$.add(
            () => this.onCitiesLoaded()
        );
    }

    registerToObservables() {
        super.registerToObservables();
    }

    run() {

    }

    createUnit(typeId: string, pos: MapPos): Unit {
        const instance = new Unit(typeId, pos);
        this.units.push(instance);
        this.onUnitSpawned(instance);
        return instance;
    }

    /* UNIT TYPES */
    initializeUnitTypes() {
        this.log(`start unit type initialization`);
        globalThis.UNIT_TYPES = new UnitTypes();
        // fire event
        const unitTypesArr = globalThis.UNIT_TYPES.getAll();
        this.onUnitTypesLoaded$.notifyObservers(unitTypesArr);
    }

    isValidUnitTypeId(unitTypeId: string): boolean {
        if(! globalThis.UNIT_TYPES) {
            this.warn(`cant check whether "${unitTypeId}" is valid; UnitType list not found`);
            return false;
        }
        return globalThis.UNIT_TYPES.isValidUnitTypeId(unitTypeId);
    }

    getUnitTypeById(unitTypeId: string): UnitType {
        return globalThis.UNIT_TYPES.get(unitTypeId);
    }

    /* UNIT AND ARMY NUMBERS */
    nextUnitId(): number {
        return this.units.length;
    }

    /* ON EVENTS */
    onEvent(ev: IBaseEvent): boolean {
        super.onEvent(ev);
        return true;
    }

    onUnitSpawned(newUnit: Unit) {

    }

    onCitiesLoaded() {
        // globalThis.ARMIES.createStartingArmies();
    }

    onGearTypesLoaded() {
        this.initializeUnitTypes();
    }
}