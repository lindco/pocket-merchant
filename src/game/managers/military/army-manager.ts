import { RandomDice, RandomInt } from "../../../helpers/random";
import { ArmyCategory } from "../../models/military/army/army-category";
import { City } from "../../models/city/city";
import { AbstractManager, IGameManager } from "../infrastructure/abstract-manager";
import { MapPos } from "../../world/pos";
import { ArmyFactory } from "./builders/army-factory";
import { IUnitArmy } from "../../models/military/interfaces/unit-army.interface";
import { IEmpire, IFaction } from "../../models/faction/faction-interfaces";

export class ArmyManager extends AbstractManager implements IGameManager {
    armies: Array<IUnitArmy> = [];
    _armyTypes: Array<number> = [];
    _playerArmy: IUnitArmy;
    // flags
    hasPlayerArmyCreated: boolean = false;
    // observables
    playerArmyCreated$ = new BABYLON.Observable<void>();
    miltiasCreated$ = new BABYLON.Observable<void>();
    empireArmiesCreated$ = new BABYLON.Observable<void>();
    allArmiesCreated$ = new BABYLON.Observable<void>();

    constructor() {
        super("Army");
        this.registerToObservables();
    }

    registerToObservables() {
        super.registerToObservables();
        globalThis.CITIES.onCitiesLoaded$.add(
            () => {
                ArmyFactory.createPlayerArmy();
        });
        globalThis.FACTIONS.onCapitalsAssigned$.add(
            () => this.onEmpireCapitalsAssigned()
        );
        globalThis.CITIES.oAfterCitiesLoaded$.add(
            (cities) => {
                this._createCityMilitias(cities);
        });
        this.allArmiesCreated$.add(
            () => {
                this.log(`finished creation of ${this.armies.length} armies for player, cities & faction`);
        });
    }

    run() {
    }

    _addArmy(armyObj: IUnitArmy): IUnitArmy {
        this.armies.push(armyObj);
        this._armyTypes.push(armyObj.armyCat.num);
        return armyObj;
    }

    /* ARMY GETTERS */
    getArmyByNum(armyNum: number): IUnitArmy {
        const armyWithNum = this.armies[armyNum];
        if (! armyWithNum) { this.warn(`cant find army with num ${armyNum}`); }
        return armyWithNum;
    }

    getArmiesAtPos(worldPos: MapPos): Array<IUnitArmy> {
        let armiesAtPos: Array<IUnitArmy> = [];
        for (const army of this.armies) {
            if (army.pos.equals(worldPos)) { armiesAtPos.push(army); }
        }
        return armiesAtPos;
    }

    getArmiesByFactionKey(factionKey: string): Array<IUnitArmy> {
        const factionArmies: Array<IUnitArmy> = [];
        for (const army of this.armies) {
            if (army._ownerKey == factionKey) {
                factionArmies.push(army);
            }
        }
        return factionArmies;
    }

    getArmiesByFaction(faction: IFaction): Array<IUnitArmy> {
        return this.getArmiesByFactionKey(faction.key);
    }

    getOutsideArmies(): Array<IUnitArmy> {
        const outsideArmies: Array<IUnitArmy> = [];
        for (const army of this.armies) {
            if (army.isInField) { outsideArmies.push(army); }
        }
        return outsideArmies;
    }

    static FilterByArmyCatNum(armies: Array<IUnitArmy>, armyCatNum: number): Array<IUnitArmy> {
        const armiesOfCat: Array<IUnitArmy> = [];
        for (const army of armies) {
            if (army.armyCat.num == armyCatNum) { armiesOfCat.push(army); }
        }
        return armiesOfCat;
    }

    filterByArmyCatNum(armies: Array<IUnitArmy>, armyCatNum: number): Array<IUnitArmy> {
        return ArmyManager.FilterByArmyCatNum(armies, armyCatNum);
    }

    /* shortforms */
    ByNum(num: number): IUnitArmy { return this.getArmyByNum(num); }
    AtPos(pos: MapPos): Array<IUnitArmy> { return this.getArmiesAtPos(pos); }

    /* CREATE NEW ARMIES */

    /* create a new army in a city (can be militia or patrol) */
    createNewCityArmy(city: City, isPatrol?: boolean): IUnitArmy {
        const ownerFactionKey = city._factionKey;
        let armyCatNum: number = ArmyCategory.CITY_MILITIA;
        if (isPatrol && isPatrol == true) { armyCatNum = ArmyCategory.CITY_PATROL; }
        const army = ArmyFactory.createNewArmy(armyCatNum, city.pos, ownerFactionKey);
        army.parentCity = city;
        army.home = city;
        if (! isPatrol) { city.militia = army;}
        return army;
    }

    /* STARTING ARMY CREATION */
    createStartingArmies() {
        // already created all starting armies? abort
        if ( this.checkAllArmiesCreated ) { return; }
        this.log(`creating starting armies..`);
        // create city miltia armies
        if(! ArmyFactory.hasMilitiasCreated)
            this._createCityMilitias(null);
        // create empire armies
        if (! ArmyFactory.hasEmpireArmiesCreated)
            this._createEmpireArmies();
    }

    private _createCityMilitias(cities: Array<City>): Array<IUnitArmy> {
        if (ArmyFactory.hasMilitiasCreated) return;
        const militiaArmies: Array<IUnitArmy> = [];
        const citiesCount = globalThis.CITIES.getAll().length;
        const firstMilitiaArmyNum = this.armies.length;
        for (const city of globalThis.CITIES.getAll()) {
            const militiaArmy = ArmyFactory.createRandomCityMilitia(city);
            militiaArmies.push(militiaArmy);
        }
        const militiaArmiesCount = this.armies.length - firstMilitiaArmyNum;
        this.log(`${militiaArmiesCount} for ${citiesCount} cities created`);
        ArmyFactory.hasMilitiasCreated = true;
        return militiaArmies;
    }

    private _createEmpireArmies(): boolean {
        // check for FactionManager
        if (! globalThis.FACTIONS) {
            this.warn(`cant create empire armies: FactionManager not found`);
            return false;
        }
        const firstEmpireArmyNum = this.armies.length;
        for (const empire of globalThis.FACTIONS.empires)
            this._createAllEmpireStartingArmies(empire);
        const empireArmiesCount = this.armies.length - firstEmpireArmyNum;
        this.log(`created ${empireArmiesCount} empire armies`);
        ArmyFactory.hasEmpireArmiesCreated = true;
        return true;
    }

    private _createAllEmpireStartingArmies(empire: IEmpire): Array<IUnitArmy> {
        let startingArmiesCount: number = RandomInt.below(3);
        const startingArmies: Array<IUnitArmy> = [];
        for (let empireArmyNum = 0; empireArmyNum <= startingArmiesCount; empireArmyNum++) {
            let isPatrol: boolean = RandomDice.roll(2);
            const empireArmy = ArmyFactory._createSingleEmpireArmy(empire, isPatrol);
            startingArmies.push(empireArmy);
        }
        return startingArmies;
    }

    /* ARMY MANAGER GETTERS */
    get checkAllArmiesCreated(): boolean {
        const hasPlayerArmyCreated = globalThis.PLAYER_M.hasPlayerArmyCreated;
        const allArmiesCreated = hasPlayerArmyCreated && ArmyFactory.hasMilitiasCreated
            && ArmyFactory.hasEmpireArmiesCreated;
        if (allArmiesCreated) {
            this.allArmiesCreated$.notifyObservers();
            return true;
        }
        return false;
    }

    /* INTERNAL EVENTS */
    onCityListCreated() {

    }

    onEmpireCapitalsAssigned() {
        this.createStartingArmies();
    }

    get hasLoaded(): boolean { return this.armies.length > 1; }
}