import { Loggable } from "../../../../common/models/log";
import { City } from "../../../models/city/city";
import { IUnitArmy } from "../../../models/military/interfaces/unit-army.interface";
import { RandomElement, RandomInt, RandomWorldPos } from "../../../../helpers/random";
import { UnitArmy } from "../../../models/military/army/army";
import { ArmyCategory } from "../../../models/military/army/army-category";
import { MapPos } from "../../../world/pos";
import { IEmpire, IFaction } from "../../../models/faction/faction-interfaces";

export class ArmyFactory extends Loggable {
    static hasMilitiasCreated: boolean = false;
    static hasEmpireArmiesCreated: boolean = false;
    // counters
    static _militiasCount: number = 0;
    static _cityPatrolsCount: number = 0;
    static _empireArmiesCount: number = 0;

    public static createRandomCityMilitia(city: City): IUnitArmy {
        if (ArmyFactory.hasMilitiasCreated) { return; }
        const army = this.createNewCityArmy(city, false);
        const startingUnitCount = RandomElement.of([1, 2, 3]);
        for (let unitNum = 0; unitNum < startingUnitCount; unitNum++) {
            this.addRndMilitaryUnitTo(army);
        }
        return army;
    }

    public static createNewCityArmy(city: City, isPatrol?: boolean): IUnitArmy {
        const ownerFactionKey = city._factionKey;
        let armyCatNum: number = ArmyCategory.CITY_MILITIA;
        if (isPatrol && isPatrol == true) {
            armyCatNum = ArmyCategory.CITY_PATROL;
            ArmyFactory._cityPatrolsCount += 1;
        }
        const army = this.createNewArmy(armyCatNum, city.pos, ownerFactionKey);
        army.parentCity = city;
        army.home = city;
        if (! isPatrol) {
            city.militia = army;
            ArmyFactory._militiasCount += 1;
        }
        return army;
    }

    static _createSingleEmpireArmy(empire: IEmpire, isPatrol: boolean): IUnitArmy {
        const army = ArmyFactory.createNewFactionArmy(empire, null, isPatrol);
        // empire has capital? set as home
        const empireCapital = empire.capitalCity;
        if (! empireCapital) {
            console.log(`<ArmyFactory> creating empire "${empire.key}" army: no home assignable`); }
        else {
            army.home = empireCapital;
            army.pos = this.findStartingPosAround(empireCapital.pos, 4);
        }
        // add 1 or 2 random military units
        let startingUnitsCount: number = RandomInt.below(3);
        for (let empireUnitNum = 0; empireUnitNum < startingUnitsCount; empireUnitNum++) {
            ArmyFactory.addRndMilitaryUnitTo(army);
        }
        return army;
    }

    public static findStartingPosAround(centerPos: MapPos, radius: number): MapPos {
        let isValid: boolean = false;
        while (! isValid) {
            let pos: MapPos = RandomWorldPos.around(centerPos, radius);
            const tile = globalThis.MAP.tileAtPos(pos);
            if (tile && tile.isOnLand) {
                if (globalThis.MAP.isInsideOfMap(pos)) { isValid = false; return pos; }
            }
        }
        return
    }

    public static createNewFactionArmy(faction: IFaction, startPos?: MapPos, isPatrol?: boolean): IUnitArmy {
        let armyCatNum: number = ArmyCategory.FACTION_ARMY;
        if (isPatrol) { armyCatNum = ArmyCategory.FACTION_PATROL; }
        const factionKey = faction.key;
        const factionArmy = ArmyFactory.createNewArmy(armyCatNum, startPos, factionKey);
        // add to faction
        factionArmy.parentFaction = faction;
        faction.factionArmies.push(factionArmy);
        ArmyFactory._empireArmiesCount += 1;
        return factionArmy;
    }

    public static createPlayerArmy(): IUnitArmy {
        console.log(`<ArmyFactory> creating player starting army`);
        // abort when player has already an army created
        if (globalThis.PLAYER_M.hasPlayerArmyCreated) { return; }
        // create new UnitArmy object
        const playerFamilyKey = globalThis.PLAYER.family.key;
        this.createNewPlayerArmy(playerFamilyKey);
        // flag player army creation as done
        globalThis.PLAYER_M.hasPlayerArmyCreated = true;
        return globalThis.PLAYER_M.playerArmy;
    }

    private static createNewPlayerArmy(playerFamilyKey: string) {
        globalThis.PLAYER_M.playerArmy = ArmyFactory.createNewArmy(ArmyCategory.CARAVAN_PLAYER, globalThis.PLAYER.pos, playerFamilyKey);
        // add to player
        globalThis.PLAYER.setArmy(globalThis.PLAYER_M.playerArmy);
        globalThis.PLAYER_M.playerArmy.parentIsPlayer = true;
        // add donkey starting military
        globalThis.PLAYER_M.playerArmy.addUnitOfType("DN");
        globalThis.PLAYER_M.onPlayerArmyCreated$.notifyObservers(globalThis.PLAYER_M.playerArmy);
    }

    public static addRndMilitaryUnitTo(army: IUnitArmy) {
        const unitTypeId = this.getRandomMilityUnitTypeId();
        army.addUnitOfType(unitTypeId);
    }
    private static getRandomMilityUnitTypeId(): string {
        const militaryTypes = globalThis.UNIT_TYPES.getMilitaryTypes();
        return RandomElement.of(militaryTypes).id;
    }

    public static createNewArmy(armyType: number, pos: MapPos, armyOwnerKey?: string): IUnitArmy {
        const army: UnitArmy = new UnitArmy(armyType, pos);
        army.num = globalThis.ARMIES.armies.length;
        if (armyOwnerKey) { army._ownerKey = armyOwnerKey; }
        return globalThis.ARMIES._addArmy(army);
    }
}