import os, sys
import json
import argparse
from PIL import Image

def make_terrainmap(imgName):
    withoutExtension = imgName.split(".")[0]
    img = Image.open(imgName)
    rows = []
    for y in range(0, img.height): # loop through pixel row
        rowCharStr = ""
        for x in range(0, img.width): # loop through point in row
            pos = (x, y)
            rowCharStr += get_color_key_at(img, pos)
        rows.append(rowCharStr)
    arrJson = json.dumps(rows)
    print("saving biome file")
    fullFileName = withoutExtension + ".json"
    save_in_json(fullFileName, arrJson)

def save_in_json(fullFileName, jsonRows):
    file = open(fullFileName, 'w')
    file.write(jsonRows)
    file.close()
    print("saved :)")

def get_color_key_at(img, pos):
    color = img.getpixel(pos)
    colHex = rgb2hex(color[0], color[1], color[2])
    if colHex in color_keys:
        return color_keys[colHex]
    else:
        print("no color key for {} at pos {}".format(colHex, pos))
        return "U"

def rgb2hex(r, g, b):
    return '{:02x}{:02x}{:02x}'.format(r, g, b)

color_keys = {
    "4082ca": "W", # water
    "28a82d": "G", # grass
    "587e24": "F", # forest
    "7aa650": "H", # hills
    "5b5a56": "M", # mountains
    "e4da8d": "D", # desert
    "89d352": "S", # savannah
    "d7d7d7": "A", # arctic
    "938966": "B", # bridge
    "9e9b8f": "R", # barren
    "d64c2b": "C", # city
    "ffffff": "1",
    "000000": "0"
}

imgName = sys.argv[1]
print("starting world maker for file " + imgName)
make_terrainmap(imgName)
