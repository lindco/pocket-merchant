const path = require('path')

module.exports = {
    mode: "development",
    entry: {
        app: "./src/app.ts"
    },
    devServer: {
        static: {
            directory: path.join(__dirname, '.'),
        },
        compress: true,
        port: 9000,
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
    resolve: {
        extensions: ['.ts', 'tsx', '.js']
    },
    devtool: 'source-map',
    plugins: [],
    module: {
        rules: 
        [
            {   test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: /node_modules/ 
            },
            {   test: /\.(png|jpe?g|gif|jp2|webp)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                }, 
            }
        ]
    }
}