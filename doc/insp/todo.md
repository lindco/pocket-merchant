# TODO LIST

## Loading
o Starting Characters (world/scenario)
o Map selection

## GUI
o Top Menu
  o Horizontal Layout
  o make bg image (GIMP)
  o Player storage
o Additional Dialogs
  o City Recruitment
  o Player owned city command
  x City Trade History
  x City Construction

## Map
x Map Random Start Pos

## Factions
o Remove ts empire loading and replace with json
o Add Independent Merchants

## 3D
o babylon node DOM for the world map
o Dialog Models
  o delete when dialog is closed
o 3D City Labels

## Trade
o DirectPathFinder
o SimpleTilePathFinder
o City Gates (road-city connections)