## Compile
* rpm run build

## Map Change
* map files are in res/worlds
* change import * as MAPDATA url in src/game/managers/world-loader.ts
* set tile terrain colors in WorldMap.createMaterials()