# Pocket Merchant   

A trading game on hexagonal world maps, based on Merchant Prince on DOS  

Play an uprising merchant in a world full of rip-off trades and corrupt
mayors, serving ruthless empires.  

Will you stand the test of time, or be robbed of your last belongings
by your hometowns pawnbroker?

## Architecture

* NodeJS & BABYLONJS 5
* Webpack to build into self-contained js
* in future: Electron for native applications

## Current functionality:   

* play on multiple maps from our world in colonial age to middleearth from LOTR  
* Cities generate and trade resources that they need between each other  
* Population system with different classes that have different needs  
* (Up to) 8 families that send caravans to generate money from price differences  
* own UI elements that add to BabylonJS but deliver integration into the 3D game graphics  
* built in a clean MVC system with JSON files as dataholders. This may be replaced by a DB later  

## Defaults & Options

* the starting map can not yet be chosen in the GUI, insteadgo into ``world-loads.ts``
  * in the imports at ``import * as MAPDATA from {...}`` you can chose the name of the map (as ts)
    from the ``res/worlds``-Folder
* Default Options like mesh distance before hidden can be changed in the ``GameOptions``-Class

## Screenshots   

![Europe](https://bitbucket.org/lindco/pocket-merchant/raw/354dc6de3308b0739fe232cafa4c31d0e8ef4cfd/doc/pres/europe_perspective.png "Europe")

## Code
![Europe](https://bitbucket.org/lindco/pocket-merchant/raw/354dc6de3308b0739fe232cafa4c31d0e8ef4cfd/doc/pres/code_map_germany.png "Code")

## Controls

* __WEADYX__: move player in a hexagonal fashion 
* __F__: Show Family Dialog
* __I__: Babylon.Js Inspector
* __N__: Next Turn
* __G__: Game Menu

## TODO

X use global observables for main events instead of Event objects
O replace the buttons left with some hoi-like nation-main-menu
O finally write a shader that allows coloring the faction flag icons