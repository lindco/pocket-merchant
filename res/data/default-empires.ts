export const DEFAULT_EMPIRES = {
    // ID: NAME COLOR BG_COLOR DIPL_TRAIT
    // european
    "hre": ["Holy Roman Empire", "GOLDENFIZZ", "SMOKEYASH", "defensive"],
    "france": ["France", "ROYALBLUE", "GOLDENFIZZ", "colonial"],
    "england": ["England", "MANDY", "WHITE", "colonial"],
    "sweden": ["Sweden", "VENICEBLUE", "GOLDENFIZZ", "defensive"],
    // mideast
    "arabs": ["Arabia", "ELFGREEN", "ATLANTIS", "raiders"],
    "persia": ["Persia", "PANCHO", "OILEDCEDAR", "defensive"],
    // asian
    "ming": ["Ming Dynasty", "GOLDENFIZZ", "OPAL", "defensive"],
    "japan": ["Shogunate", "WHITE", "BROWN", "defensive"],
    // tribal
    "mali": ["Mali", "STINGER", "ATLANTIS", "defensive"]
}