/* == MAP PROPERTIES == */
export const STARTING_YEAR = 0;
export const END_YEAR = 200;
export const SEASONS_PER_YEAR = 2;

/* == MAP TILES == */
export const TILE_ROWS = ["WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGAAAGGWWWWWWWWWWWWWWWWWWWWWWWWWWSSSAAAAAAAAAAAAAAAAASSS", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGAAGGWWWWWWWWWWWWWWWWWWWWWWWWWSSSSAAAAAAAAAAAAAAAAASSSS", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGAGGGWGAAWWWWWWWWWWWWWWWWWWFFFASSAAAAASSAAAAAAAAASSSSSS", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGGWWWGWWWGWWWWWWWWWWWWFFFFFFFASSSSSSSSSSSSSSSSSSSSSSW", "WWWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGGWWWWWWWWWWWWWWWWWGGFFFFFFFFFSSSSSSSSSSSSSSSSSSSSSWWW", "WWWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGGGGGGGWWWWWWGGGGGGGFFFFFFFFFSSSSSSSSSSSSSSSSHHSSSWWWW", "WWWWWWWWWWWWWGWGGGGGGGWWGGWWGGGGGGGGGGGGGGGGGGGGGGGGFFFFFFFSSSSSSSSSSSHHHHHHHHHHHSWWW", "WWWWWWWWWGGGGGWGGGGGGGGGGGWWGGGGGGGGGGGGGGGGGGGGGGGGFFFFFGGSSHHHSHHSSHHHHHHHHHHHHSSWW", "WWWWWWWWWGGGGGWGGGGGGGGGGGGWWGGGGGGGFFGGGGGGFFGGGGGFFFGGGGHHHMHHHHHHHHHMMMMMMMMHHSSSS", "WWWWWGGGWGGGGGWWGGGGHHHHFGGGBWFFFFFFFFFFGGGGGFFGGGFFFGGGGHMMMMMMMMMMHHHMMMMMMMMHHSSSS", "WWWWWGGGWWGGGGGWGGGHHMMHFGGGGWFFFFFFFFFFFGGGGFFGGGFFGGGGGHMMMMMMMMMMMHHMMMMMMMMHHHSSS", "WWWWWGGGGGGGGGFWBWGHHMMHGGGGGWWFFFFFFFFFFGGGGMFGGGGGGGGGGHHMMMMMMMMMHHMMMMMMMMMHHHHSS", "WWWWWGGGGGGGGFGGGWGGHHMHGGGGGGWWFFFFFFFFGGGGHMHGGGGGFGGGGGHHHHHHHMMMHHMMMMMMMMMMMHHHH", "WWWWWGGGGFFFFFGGGWBWWHHHGGFGGGGWBWWWFFFFGGGGHMHGGGGGFFFFGGGGGGGGHMMMHHHMMMMMMMMMMMMMH", "WWWWWGGGFFFFFGGGGGGFWFFHGGFFGGGGGGGWWWWGGGGHHMHFGGGGFFFHHGGGGGGGHHMHHHHHHHMMMMHMMMMMM", "WWWWWGGGFFFGGGGGGGGFWGGGGGGFFGGHHGGGGGGGGGHHMHHFGGGGGFMMMHGFGGGGGHHHHGHHHHHHHHMMMMMMM", "WWWWWGGGFGGGGHHHGGGGWGGGGGGGFFGHHHGGGGGGGHHMHHFFGGGGGGHMMHFFFGGGGGGGGGGGGGGHHHHHMMMMM", "WGWWGGGFGGGGHHMHGGGGGHHHHHGGGGGGGFFHHHGGGHHHHGGGGGGGGGHHMMMFFFFGGGGGGGFFFFFGGHHHHMMMM", "WWWGGGGGFFGHHMMHGGHHHHMMMHHHHHGGGFFFHHHGFFFFGGGGGGGGGGGHHFFFFFFGGGGGGGGFFFFFGGGHHHMMM", "WWWGGFGFFGHHMMMHHHHMHHMMMHHMMMHHGGGFFHHGFFGGGGGGGGGGGGFFFFFFFFHGGGGGGGGGGGFFGGGGHHHHM", "WWWGGFFGHHHHHMMMMHMMHHMMMHHMMMMHHGGGGGGGGGGGGGWWGGGGGGFFFFFFHHHGGGGGGGGGGGGGGGGGGGGHH", "WWGFFFGHHMHMFFMMHHHMMHHHHHHHMMMMMHHHGGGGGGGGGGGWWWWGGGGFFFFHHHGGGWGGGFFGGGGGGGGGGGGGG", "WGFFFFGHMMMMFWHHHHHHHHHGGGGHHHMMMMMHHGGGGGGFFGGGGGWWGGGFFFFHHHGGWWGGGFFFGGGGGGGGGGGGG", "GGFFFGGHMMMMMHHHHHHGGGFFGGGGGHHHMMMMHHHHGGGGFGGGGGGWWGGGFFFHHGGWWWGGGFFFFGGFFGGGGGGGG", "GGFFGGGHMMMHHHHGGGGGGFFFGGFFFGGHMMMMHHMMHHGGGGGGFGGGWWGGFFGGGGGWWWGGGFFFFFFFFFFGGGGGG", "GGFFGGHHMMMMHHGGGGGGGFGGGGGFFGGHHHHHHHMMMHHHGGGGFFGGGWWGFGSSSSGWWWGGGFFFFFFFFFFGGGGGG", "GFFGGGHMMMMMMHGFFFFGGGGWWGGFFGGGGHHHHHMMMMMHHHHHGFFGGGBGGGSSSSGWWWGGGFFFFFFFFFGGGGGGG", "GGGGFHHMMMMMMHGFFFFFGGGWWGGGGGFFGGGHMMMMMMMMHHMHHHFFGGWWWSSSGGGGWGGGGGGGGGGGGGGGGFFFG", "GGGFGHMMMMMHHHGFFFFFGGGWWWGGGGFFFFGHMMMHHMMMHMMMMHHFGGWWWWWWWGGWWGGGGGHGGGGGGGGGGFFFG", "GGGFGHMMMMHHGGGGGGFGGGGWWWWWWGGFFFHHMMHHHHMMHMMMMHHFGGWWWWWWSGGGBGGGHHHHHHHHGGGGGFFFS", "GGGGGHHMMHHGGGGWGGGHHHGGWWWWWWGGFFGHHHHGHHHHHMMMMHGGGWWWWWWWSSSGWGGHMMMMMMMHHGGGGGGGS", "UGGGGGHHHHGGGWBWWWGHMMGGGGWWWWGGGFGHHHHGGGGGHHHHHHGGWWWWWWWWWWWWWGGHHHMMMMMMHGGGGGSSS", "UUGGFGGGGGGWWWGWWWWGGMMMGGWWWWWGGFHGGHGGGGGGFFFGGGGGWWWWWWWWWWWWWGGGGHHHHMMMMHGGGGSSS", "UUGFFGGGGGWWWWGGWWWWGGHMHGGWWWWGGGHHHGGWWWWGGFFGGGGGWWWWWWWWWWWWWSSGGGGGHHHHHHGGGSSSS", "UUGFFGGGWWWWWGGGWWWWWCHHHGSSWWWGGHHMHGGWWWWWWGGGGGWWWWWGGWWWWWWWWSSGGGGGGGGGGGGSSSSSS", "GGGFFGGWWWWWWBWWWWWWWWGGHHHSSWGGGGHHHHHGGGWWWWGGWGGGGGGGGGGWWWWSSSSGGGGGGHHGGSSSSHSSS", "GGWFFGWWWWWWWGGWWWWWWWWGHHHSSBGGGGGFFFGMHGWWWWBWGGGGGGFGGGGGGHHSSHHHMMMHHHHHHHHHHHSSS", "GGWWFGWWWWWWWGGGWWWWWWWWGMMSSWWWWWWGGGGMHGWWWWGGGGGHFFFFFFFFHHHHHHMMMMMMHHHHMMMHHHHHS", "GFFWWWWWWWWWWGGSWWWWWWWWGMMWWWWWWWWWWWWHHWWWWWGGGHHHHFFFFFMMMMHHMMMMMMMMHHHMMMMHHHHHH", "GFSSWWWWWWWWWGGSWWWWGGGWGGWWWWWWWWWWWGGGGWWWWWGGGHHHMMMMHHMMMMHHMMMMMMMMMHHMMMMHHMMHH", "GSSSWWWWWWWWWSSSWWDDGSSSGGWWWWWWWWWWGGMGGWWWWWWGSHMMMMMMMHFFFSSSHMMMMMMMHHHHMMMHHMMHH", "SSSSWWWWWWWWWWSSWWWSSSSSWWWWWWWWWWWWWGCGGWWWWWWGSHMMMMMMMFFFSSSSSHHHHHHHHHHHHHMMMMMMM", "SSWWWWWWWWWWWWWWWWWWSSSSWWWWWWWWWWWWWWWWBWWWWWWWSSSMSSSSFFSSSFFFFSSSHHSSSSSHHMMMMMMMM", "SWWWWWWWWWWWWWWWWWWWWSSWWWWWWWWWWWWWWWWGGGSSGWWSSSSSSSSSSWDSFFFFFSSSSSSSSSSSHMMMMMMMM", "WWWWWWWWWWWWWWWWWWWWWWBWWWWWWWWWWWWWWWWSGWGSGWWSSSWWWBWWSWSSSSFSSSSDDDDDDDDDMMMMMMMMM", "SSWWWWWWWWWWWWWWWWWWWWGWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSWWWGSSSSSSSSDDDDDDDDDDMMMMMMMMM", "DSSSSSWWWWWWWWWWWWWWGGGGWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSWWGDFDDDDDDDDDDDDDDDDMMMMMMMMM", "MDSSSSSSSWWWWWWWWWWWSSGGGCWWWWWWWWWWWWWWWWWWWWWWWWWWSSWWWSSFSDDDDDDDDWDDDSSDDDMMMFMMS", "MMDDSSSSSSSSDSSSSSSSSSSSSSSWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSFFSSDDDDDDWWDDDSSSSDDDDDDSS", "MMMDDDDDDDDDDDDDDDDDDDDDSSSWWWWWWWWWWWGGGWWWWWWWWWWWWWWSSSFSSSDDDDDDWWWDDSSSSSSDDDDDD", "MMMDDDDDDDDDDDDDDDDDDDDDDSSWWWWWWWWWWWSSGGWWWGGSSWWWWWSSSFFSSSDDDDDDDWWDDDSSSSSSSDDDD", "MMMDDDDDDDDDDDDDDDDDDDDDDSSSWWWWWWWWWSSSSSSWWBGGGWWWSSSCSSSDDDSSSDDDDWWDDDDSSSSSSSDDD", "MMMDSSDDDDDSSDDDDDDDDDDDDDDSSSSWWWSSSDDSSSSSSBWGBBGGSSSSDSSDDDDSSSSDDDWSSDDDSSSSSSDDD", "MMMSSDDDDDDDSDDDDDDDDDDDDDDDDSSSDDDSDDDDDSSSSGWWWGGSSSSSDDSSSDDDSSSDDSWWSDDDSSSSDDDDD", "MDDSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDSSSSGWGGDDDDDSSSDSSSDDDDDDDSSSSDDDDDSSDDDDD", "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDSSGWGSDDDWDDDSDDDDDDDDDDDDSSDDDWDDDDDDDDD", "DDDDDDDDDDDDDDDDDDDDDDDDDDDSDDDDDDDDDDDDDDDDDSGWGSDDDWDDDDDDDDDDDDDDDDDSDDDWWDDDDDDDD", "DDDDDDDDDDDDDDDDDDDDDSDDDDDSSDDDDDDDDDDDDDDDDSGBSSDDDWSSDDDDDDDDDDDDDDDSDDDWWWWWDDDSS", "DDDDDDDDDDDDDDDDDDDDSSSDDDDDDDDDDDSSDDDDDDDDDSSWSSSSDWSSDDDDDDDDDDDDDDDDDDDDDWWWWWWSS", "DDDDDDDDDDDDDDDDDDDDDSSDDDDDDDDDDDSSSDDDDDDDDSSWWSDSDWWSSDMDDDDDDDDDDDDDDDDDDDWWSSWWS", "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDSSSDDDDDSDDSSSWSDSSSBSSDMMDDDDDDDDDDDDDDDDDDDWWSGWW", "DDDDDDDDDDDDDDMMMMMMDDDDDDDDDDDDDDDDDDDDDSSDDDSSWSDSSSWWSSDMDDDDDDDDDDDDDDDDDDSSWWSSW", "DDDDDDDDDDHHHMMMMMMMMMMDDDDDDDDDDDDDDDDDDDDDDDSSWSDSSSWWSSSDDDDDDDDDDDDDDDDDDDDSSWWWW", "DDDDDDDDDDDSHHHHMMMMMMMDDDDDDDDDDDDDDDDDDDDDDDSSWDDDSSSWWSSDDDMDDDDDDDDDDDDDDDDDSGWWW", "DDDDDDDSSSSSGHHHHHHHMMDDDDDDDDDDDDDDDDDDDDDDDDSWWSDDSSSWWWSSDDMMDSSSDDDDDDDDDDDDDDSWW", "DDDDWWWWWWBWWGGSSSSDDDDDDDDDDDDDDDDDDDDDDDDDDDSWWSDDDSSSWWSSDDMMMDDSSDDDDDDDDDDDDDSWW", "DDDDSSSSGGGGWWWSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDSWSSDMMMSSWWWSSDMMMDMMDSSDDDDDDDDDDDSWW", "DDDSSSSSSSSSSSWSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDSWSDMMMMSSWWWSSSDDDDDMMDSDDDDDDDDDDDSWW"];
/* == TILE OVERRIDE == */

export const TILE_OVERRIDE: Array<string> = [
    "17 20 C", "25 21 C", "17 22 G", "18 23 G", "17 21 G", "17 19 G", // alpine pass
    "49 35 B", "49 34 W", "49 38 F", // byzantium
    "51 42 F", "51 41 H", "52 41 H", // anatolia
    "58 46 H", "58 47 H", "57 49 W" // palestine
];

/* == MAP LOCATIONS == */
export const CITIES: Array<Array<string>> = [
    /* KEY NAME POS_STR MESH_NAME EMPIRE_KEY PRIMARY_RES BONUS? IS_COLONY? */
    // GREECE
    ["ATH", "Athens", "41 38", "venetian", "ATH", "glass"],
    ["SPR", "Sparta", "38 41", "hellenic", "SPA", "wood"],
    ["KNS", "Knossos", "42 43", "hellenic", "I", "weapons"],
    ["EPR", "Epirus", "31 32", "hellenic", "EPR", "tools"],
    ["APL", "Apollonia", "35 37", "hellenic", "ATH", "grain"],
    ["TSL", "Tessalia", "40 32", "hellenic", "I", "ore"],
    ["THR", "Thracia", "51 31", "barbarian", "I", "pelts"],
    ["EPH", "Ephesos", "45 38", "venetian", "I", "paper"],
    ["PLL", "Pella", "45 34", "hellenic", "MCD", "ore"],
    ["NRS", "Narsis", "36 26", "hellenic", "MCD", "pelts"],
    // ITALY
    ["ROM", "Rome", "21 34", "roman", "ROM", "ore"],
    ["ETR", "Etruria", "18 31", "roman", "ETR", "grain"],
    ["MDL", "Mediolanum", "16 25", "barbarian", "LIG", "ale"],
    ["TAR", "Tarentum", "28 35", "roman", "ROM", "wood"],
    ["VEN", "Venetia", "23 26", "venetian", "LIG", "pearls"],
    ["SYR", "Syracuse", "22 40", "roman", "I", "fish"],
    ["ILL", "Illyria", "28 28", "barbarian", "I", "wood"],
    ["GEN", "Genua", "12 31", "barbarian", "I", "pearls"],
    ["KRL", "Karalis", "14 41", "barbarian", "I", "fish"],
    // ANATOLIA
    ["CYP", "Cyprus", "52 46", "hellenic", "I", "clothes"],
    ["BYZ", "Byzantion", "49 36", "hellenic", "I", "fish"],
    ["JER", "Jerusalem", "53 52", "mideast", "ZIO", "clothes"],
    ["TYR", "Tyre", "56 48", "mideast", "phoenicia", "glass"],
    ["ANT", "Antioch", "58 44", "hellenic", "SEL", "ore"],
    ["GLT", "Galatia", "51 38", "tribal", "I", "pelts"],
    ["PRG", "Pergamon", "47 42", "venetian", "pergamon", "pearls", "conn ATH"],
    ["PLM", "Palmyra", "61 51", "mideast", "I", "ivory"],
    ["PNT", "Pontus", "57 35", "tribal", "I", "fish"],
    ["CLC", "Colchis", "65 33", "tribal", "colchis", "pelts"],
    ["HTR", "Hatra", "65 48", "mideast", "I", "ivory"],
    ["ARM", "Armavir", "71 35", "mideast", "ARM", "ore"],
    // EGYPT
    ["MPH", "Memphis", "44 55", "mideast", "EGY", "grain", "conn CYP"],
    ["THB", "Thebes", "47 60", "mideast", "EGY", "gems"],
    ["NPT", "Nepata", "45 66", "mideast", "I", "wool"],
    ["ALX", "Alexandria", "46 50", "hellenic", "EGY", "fish"],
    // MAGHREB
    ["CAR", "Carthage", "25 47", "vivec", "CAR", "clothes"],
    ["CYR", "Cyrene", "38 49", "mideast", "I", "pearls"],
    ["DMD", "Dimmidi", "21 56", "mideast", "garamantes", "spices"],
    ["LBY", "Libya", "30 52", "mideast", "I", "fish"],
    ["BDN", "Beduin Camp", "35 56", "mideast", "BDN", "ivory"],
    ["MAU", "Mauria", "3 50", "mideast", "maurians", "gems", "conn CAR"],
    // GERMANIA
    ["RUG", "Rugia", "39 6", "barbarian", "RUG", "fish"],
    ["HLV", "Helvetia", "17 20", "cloister", "I", "wood"],
    ["ARH", "Arhus", "31 7", "barbarian", "DAN", "tools"],
    ["NOR", "Noria", "25 21", "cloister", "I", "pelts"],
    ["RTT", "Rottomagnum", "12 11", "barbarian", "I", "grain", "conn CNB"],
    ["FRS", "Frisia", "20 8", "barbarian", "P", "fish"],
    ["SAX", "Saxons", "26 12", "barbarian", "SAX", "tools"],
    ["BVR", "Bavarians", "31 17", "barbarian", "I", "pelts"],
    // GALLIA & IBERIA
    ["MSS", "Massalia", "7 34", "hellenic", "I", "gold"],
    ["LEM", "Lemonum", "4 28", "tribal", "GAU", "pelts"],
    ["CNB", "Cenabus", "7 20", "tribal", "GAU", "wood"],
    ["TRR", "Taracco", "3 39", "tribal", "IBR", "pearls"],
    ["PLM", "Palma", "8 39", "tribal", "CAR", "fish", "conn CAR"],
    // ARABIA & PERSIA
    ["URK", "Uruk", "74 56", "mideast", "BBL", "ale"],
    ["BAB", "Babylon", "67 52", "mideast", "BBL", "pottery"],
    ["ASR", "Assur", "73 50", "mideast", "I", "silk"],
    ["NBT", "Nabatea", "56 56", "mideast", "BDN", "wool"],
    ["MDN", "Medina", "58 61", "mideast", "BDN", "ivory"],
    ["SUS", "Susa", "80 54", "mideast", "PRS", "gems"],
    ["BDN", "Beduins", "63 58", "mideast", "BDN", "spices"]
];

/* == SPECIAL RESOURCES == */
export const SPECIAL_RESOURCES = [
    { "id": "X1", "name": "sacrifices", "colorName": "ROPE", "price": 55, "requires": [], "bldType": "spot" },
    { "id": "X2", "name": "marble", "colorName": "HEATHER", "price": 40, "requires": [], "bldType": "quarry" },
    { "id": "X3", "name": "mosaics", "colorName": "MANDY", "price": 110, "requires": ["X2"], "bldType": "manufacture" },
];

/* == DECORATION == */
export const LANDSCAPE = [
    "volcano 27 37", "temple 48 44", "treasure 8 47", "shipwreck 27 29", "treasure 14 33", "shipwreck 16 43", // all
    "village 22 35", "village 15 29", "village 26 50", "village 13 22", "treasure 55 43",
    "village 21 28", "village 40 36", "34 33 watchtower", // greece
    "temple 71 55", "treasure 75 49", "oasis 62 52", // babylon
    "lighttower 45 50", "oasis 42 59", "pyramids 42 56", // egpyt
    "farmstead 26 11", "village 33 20", "watchtower 19 17", // germania
    "ice_floe 42 1", // scandinavia
    "oasis 65 61" // arabia 
];

/* == FAMILIES == */
export const CUSTOM_FAMILIES = [
    { "name": "Brutus", "home": "ROM", "lang": "roman", "customStartPos": "39 40" },
    { "name": "Barqa", "home": "CAR", "lang": "arabic", "customStartPos": "22 50" },
    { "name": "Argos", "home": "TSL", "lang": "hellenic", "customStartPos": "38 33" },
    { "name": "Nibelinging", "home": "BVR", "lang": "germanic", "customStartPos": "23 15" },
    { "name": "Aliq", "home": "MDN", "lang": "arabic", "customStartPos": "54 55" }
];

/* == PLAYER DATA == */
export const RND_START_POS = ["39 40", "63 48", "20 24"];

/* == EMPIRES == */
export const CUSTOM_EMPIRES = [
    // greece
    { "key": "ATH", "name": "Athens", "lang": "hellenic", "flag": "lightsteel bird on royalblue", "traits": [ "is-colonial" ] },
    { "key": "SPR", "name": "Sparta", "lang": "hellenic", "flag": "goldenfizz trident on brown", "traits": [ ] },
    { "key": "MCD", "name": "Macedon", "lang": "hellenic", "flag": "tahitigold trident on smokeyash", "traits": [ ] },
    { "key": "EPR", "name": "Epirus", "lang": "hellenic", "flag": "brown ship on pancho", "traits": [ ] },
    // italy
    { "key": "ROM", "name": "Rome", "lang": "italian", "flag": "goldenfizz bird on brown", "traits": [ "is-colonial" ] },
    { "key": "ETR", "name": "Etruscans", "lang": "italian", "flag": "goldenfizz tower on clairvoyant", "traits": [ "is-defensive" ] },
    { "key": "LIG", "name": "Ligures", "lang": "germanic", "flag": "pancho tower on elfgreen", "traits": [ "is-forester" ] },
    // african
    { "key": "EGY", "name": "Egypt", "lang": "hellenic", "flag": "goldenfizz tower on royalblue", "traits": [ "is-monumental" ] },
    { "key": "CAR", "name": "Carthage", "lang": "arabic", "flag": "verdigris crescent on geather", "traits": [ "navy-expert" ] },
    // asia minor
    { "key": "ZIO", "name": "Egypt", "lang": "hellenic", "flag": "pancho tower on veniceblue", "traits": [ "is-monumental" ] },
    { "key": "SEL", "name": "Selecucids", "lang": "hellenic", "flag": "smokeyash trident on lightsteel", "traits": [ ] },
    { "key": "ARM", "name": "Armenia", "lang": "arabic", "flag": "plum horse on smokeyash", "traits": [ "can-build-CT" ] },
    // babylonia
    { "key": "BDN", "name": "Bedouins", "lang": "arabic", "flag": "oildcedar crescent on pancho", "traits": [ "is-nomadic" ] },
    { "key": "BBL", "name": "Babylonians", "lang": "arabic", "flag": "goldenfizz tower on veniceblue", "traits": [ ] },
    { "key": "PRS", "name": "Persians", "lang": "arabic", "flag": "goldenfizz lion on goldenfizz", "traits": [ ] },
    // germania
    { "key": "RUG", "name": "Rugians", "lang": "germanic", "flag": "opal trident on atlantis", "traits": [ ] },
    { "key": "SAX", "name": "Saxons", "lang": "germanic", "flag": "heather swords on royalcedar", "traits": [ "is-forester" ] },
    { "key": "DAN", "name": "Danes", "lang": "germanic", "flag": "lightsteel ship on rope", "traits": [ "can-build-CT" ] },
    // gallia & iberia
    { "key": "GAU", "name": "Gallic Tribes", "lang": "germanic", "flag": "rainforest bird on verdigris", "traits": [ "is-berserker" ] },
    { "key": "IBR", "name": "Iberians", "lang": "italian", "flag": "brown horse on pancho", "traits": [ ] }
];