/* == MAP PROPERTIES == */
export const STARTING_YEAR = 900;
export const END_YEAR = 1200;
export const SEASONS_PER_YEAR = 2;

/* == MAP TILES == */
export const TILE_ROWS = ["AWWWWMMMMMMAAAAAMMMMMMMHHHHHHAMMMMMMAAAAAAMMMMMMMMAAAAAAAAAAAAAAAAAAAAAAAAAA", "AAWWWAMMMMMHAAHAAMMMMMMMHHHHHAMMMMMMMAAMMMMMMMMMMMMMMMMMMDDDAAAAADDDAAAAAAAA", "WWWWAAMMMMHHHHHHAAAHHHHHGGGHHHAMMAMMMMAMMMMMMMMMMMMMMMMMMMDDDDDDDDDDDAAAAAAA", "WWWWAMMAMMMHHHHHHHHHHHHHGGGGGHAMMHAMMMAAAAHHHMMMMMMMMMMMMMGGGGGGGGGDDAAAAAAA", "WWAAGGAAMMMHHHHHHHHHHHHHGGGGGGHHMHMMMMAHHHHGHHHHHHHHHHHHHHHGGGGGGGGSDDAAAAAA", "WWAGGFFMMMHHHGGGHHHHGGGHGGGGGGHHHHHHMMMMHHHGGGGGGGGGHHHHHMMMGGGGGGSSDDDAAAAA", "WWGGGFFMMHHHGGGGGHGGGGGGHGGGGGHHHHHHHMMMMHGGFFFFFFGGHHHHHMMMGGGGGGSDDDDAAAAA", "WWGGGFFFFHHHGGGGGHGFFGGGGGGGGHHHHHHFHMMMMHGGFFFFFFGGMMHHHMMGGGGGSSSDDDDAAAAA", "WWWGGFFFFHMGGGGGGGGFFGGGGGGGFHHHHHHFHHMMHHGGFFFFFFGGMMMHHHHGGGGGSSSDDDDDDAAA", "WWWGGGGGGMMGGGGGGGGGFGGGGGFFFGGGGGFFFHHMHGGFFFFFFFGSMMHHHHGGGGGSSSSDDDDDDDDA", "WWWGGGGGGMMWWWGGGGGGGGGGGGFFFGGGGGFFFHHMHGGFFFFFGGGGHGGGGGGHHHHHHSSHHSDDSSGG", "WWWWGGGGGGGWGGGGGGGGGGGGGGGFGGGGGGGFFHMMHGGFGGGGGGGGHHGGGGHHHHHHHHHMMSSSSGGS", "WWWWGWWGGWWWGGGGGGGGGGGGGGGGGGGGGGHHHHMMMGGGGGGGFFFGGHGGGGHHMMMMHMMMMHSSGGSS", "WWWWBWWWBWWGGFFFHHGGFFGGGFFFGGGGGHHHMMMMMGGGGFFGFFFGGHHGGGHHMMMHHHMMMHGGGGSS", "WWWWGGWWGGGGFFFMMMHGFFGGGFFFGGGGGHHHMMMMHGGGGFFGGFGGGHHGGGGHHHHHHHHHHHFFSSSS", "WWWWWGGGGGMMMFFMHMHFFFGGFFFFGGGGHHHHHHHHHGGGGFGGGFGGHHHGGGGGFFGGGHHGGGGFFGGG", "WWWWWGFGGMMMHFFFGHHFFGGGFFFFGGHHHHMMMMHGGGGFFFGGGFGGHHHFGGGGFFFGGWWWGGGFFGGS", "WWWWWFFFGGMMMHFFGGGFFGGFFFFGGHHHHHMMMHHGGGGFFFFGFFGGHHFFGGGGGFFGGWWWWWGFFSSS", "WWWWWWWFGGGGMHGGGGGGGGGFFFGGHHHHFMMMFFGGGGGFFFFGFFGGHHFFGGGGGFFGGWWWWWGGSSSS", "WWWWWWWWWWGGFGGGGGGGGGGGGGGGHHFFMMMMFGGGGGGGGGGGFFGGHHFFGGGGGGGGGWWWWWGGSSSS", "WWWWWWWWWWWGFWGGGGGGFGWWBHGGHHFFMMMFFGGGMMMGGGGGGGGGGFFFGGGGGGGGGWWWWGGGSSSS", "WWWWWWWWWWWWWWWWGGGWBWWGGGGGHHFMMMFFFGMGMMMMGHGGGHHHHFFGGGGGGGGGGWGGGGGGSSSS", "WWWWWWWWWWWWWWWWGGGWFFFGGGGGHHHMMFFFGGMMMMFFFHGGHHGHHGGGGGGGGGGGGBGGGGGFSSSS", "WWWWWWWWWWWWWWWWGGWWGGGGGGGGGGHHHHFGGGGMMFFFFHHGGGGHGGGGGGGGGGGGWWGGGFFFFSSS", "WWWWWWWWWWWWWWWWWWWGGGGGGGGGGGHHHHGGGGGGGGFFHHHGGGHGGGGHHHHGGGGWWSSGGFFFFSSS", "WWWWWWWWWWWWWWWWWWWWGGGGGGGGFFFGGGGFFFGGGGGGHGWWGGHHHHHHHHHHHWWWSSSGGGFFFFSS", "WWWWWWWWWWWWWWWWWWWGGGGGGGFFFFFGGFFFFFGGGGGGGWWGHHHHHHHMMHHHHHSSSSHSSSSSSSSD", "WWWWWWWWWWWWWWWWWWWWGGGGGFFFFFFFFMMMFFGHGGGGGBGHMMMHMMMHMMMMMMHSSHMHHHHSSSSD", "WWWWWWWWWWWWWWWWWWWWGGFGGGFMMMFMMMMMMMHMHGGGGWGHMMMMMMMMMMMMMMMMMHMMMMMHSSSD", "WWWWWWWWWWWWWWWWWWWGGFFFGGMMMMFMMMMMMMHMMHGHGWGHMMMMMMSSSSRRRRSSSHHMMMHHSSDD", "WWWWWWWWWWWWWWWWWWWGFFFFFGMMFFFHHMMMHHHMMHHMHWWGRRRRRRRRSSSSRRRSSSHHHHHSSSDD", "WWWWWWWWWWWWWWWWWWWGGFFFFGFFFFGHGHHHGFFHHHHMMHWGRRRRSRRRRSSRDDRSSSSSSSSDSSDD", "WWWWWWWWWWWWWWWWWWWGGGGGGGFFGGGHGGHHGFFFFGHHHGWWHMMSSSSRRDDHDDDSDDDDDDDDDSSS", "WWWWWWWWWWWWWWWWWWWGGHGHFFFFGGHGGGGGFFFFFGGGGGBGHMMSSRRRRRHMHDDDDDDDDDDDDHSS", "WWWWWWWWWWWWWWWWWWWGMHHMGFFGGGGGGGGFFFFFFGGGGGWGMMMSRRSSSRHMHDDDMMMDDDDDHHHS", "WWWWWWWWWWWWWWWWWWWGMHMMGGGWWWWWWWGGFFFFFGGGWWWGHMMSRRSSSDHHDDDDMMDDDDDHHMMM", "WWWWWWWWWWWWWWWWWWGGHGHHGGWWWWWWWWGGGGFFGWWWWGGGRRRRRSSSSRDDDDDDMMDDDDDMMMMM", "WWWWWWWWWWWWWWWWWGGGGGGGGWWWWWWWWWGGGGWWWWWGGGGSRMMMMMSSSRRRRDDDDDDDDDDMMMMM", "WWWWWWWWWWWWWWWWGGGGGGGWWWWWWWWWWWWGGWWWWWGGGGSSHMMMMMMSRRMMMMMDDDDDDMMMMDDD", "WWWWWWWWWWWWWWWGGGGWWWWWWWWWWWWWWWWGGWWWWWGSSSSSHHHMMMMMMMMMMMMDDDDDMMMMDDDD", "WWWWWWWWWWWWWWWGGGWWWWWWWWWWWWWWWWWWWWWWWGGGGSSSSSSHMMHMMMMHMMHHDDDDMMMDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGWGGGGGGGGSSSSHHHGGGGHHHHDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGBGGGGGGGGSSSSSGGGGGGDDDDDDDDDDDDDDDDM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGGSSSSGGGSSSDDDDDSSSSSDDDDDDMM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGSSSGGGSSSSDDDSSSSSSSDDDDDMMM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSSSSSSSSSSDDSSSSSSSDDDDDDMMM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSWWSSSSSSSSSSSSSSSSSSSSSSSDDDDDDMMMM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSSSSSSSSSSSSSSSSSSSSSSSDDDDDDDDMMMM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSSSSSSDDDDDDDDSSSSSSSSDDDDDDDDDDMMM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSSSSSDDDDDDSSDDDSSSSSSSDDDDDDDDDDDDM", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSSDDDDDDDDSSSSDDDSSSSSDDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSSDDDDDDDDDSSSSDDDSSDDDDDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSGSSSDDDDDDDDDSDDDDDDDDDDDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWSSGGSSSSSDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD"];
/* == TILE OVERRIDE == */
export const TILE_OVERRIDE = [
    "35 27 G", 
    "57 16 W", "58 17 W", "58 16 W", "18 15 W", "57 15 W", "56 14 B", "56 13 W", "56 12 W", // laketown
    "58 18 B", "59 19 W", "60 19 W", "64 21 W", "63 21 W", "62 21 W", "60 21 B", "60 20 W", "61 21 W", // rhovanion
    "61 36 W", "60 36 W", "60 35 S", "56 31 L", "56 30 L", "49 30 L", // mordor
    "50 22 F", "50 21 F", // gondor
    "34 37 M", "35 37 H", // gondor coast
    "65 51 W", "66 51 S", "51 44 W", "52", "43 W", "53 46 D", "54 47 D", // harad
    "22 12 W", "24 13 F", "14 6 F", "14 10 F", // eriador
    "14 15 S", "12 0 A", "13 0 A", "13 2 A", "14 2 A", "13 1 A", "25 0 A", "26 0 A", "37 0 A", // north
    "36 5 A", "37 5 A", "37 4 A", "36 4 F", "35 4 F", // gundabad pass
    "71 15 M", "70 16 H", "70 15 H", "70 14 M" // far east
];

/* == MAP LOCATIONS == */
export const CITIES: Array<Array<string>> = [
    /* KEY NAME POS_STR MESH_NAME EMPIRE_KEY PRIMARY_RES BONUS? IS_COLONY? */
    // good
    ["MNT", "Minas Tirith", "42 29", "roman", "GND", "paper"],
    ["EDR", "Rohan", "35 27", "weur", "RHH", "wood"],
    ["DAL", "Dale", "57 11", "barbarian", "DLE", "pelts"],
    ["LKT", "Laketown", "57 17", "cloister", "DLE", "clothes"],
    ["BRE", "Bree", "23 12", "weur", "HBB", "grain"],
    ["SHR", "Shire", "18 11", "cloister", "HBB", "wood"],
    ["DLA", "Dol Amr.", "35 37", "hellenic", "DAM", "tools"],
    ["LIN", "Linhir", "40 36", "hellenic", "GND", "fish"],
    ["ANF", "Anfalas", "29 33", "cloister", "DAM", "fish"],
    ["FNG", "Fangorn", "37 23", "tribal", "I", "clothes"],
    ["BLW", "Blackwood Frst.", "36 31", "cloister", "GND", "ore"],
    ["CAN", "Cair Andros", "45 25", "hellenic", "GND", "tools"],
    ["RNC", "Ranger Camp", "50 23", "barbarian", "RNG", "pelts"],
    // evil
    ["OSG", "Osgiliath", "45 34", "roman", "MRD", "grain"],
    ["MTD", "Mt. Doom", "55 31", "evil", "MRD", "pottery"],
    ["DLG", "Dol Guldur", "52 20", "evil", "DLG", "wood"],
    ["ISN", "Isengard", "31 23", "dwarven", "ISG", "weapons"],
    ["MOR", "Moria", "37 15", "dwarven", "MOR", "gems"],
    ["MTM", "M. Morgul", "49 31", "evil", "MRD", "ore"],
    ["GND", "Mt. Gundab.", "39 3", "evil", "GND", "ore"],
    ["ANG", "Angmar", "18 2", "evil", "ANG", "ivory"],
    ["TRL", "Trolls", "29 7", "barbarian", "I", "wood"],
    ["WRH", "W. Rhun", "66 21", "asian", "RHU", "glass"],
    ["SPF", "Spider Forest", "50 16", "tribal", "DLG", "pelts"],
    ["ORC", "Orc Camp", "43 19", "barbarian", "ORC", "wood"],
    ["NRN", "Nurn", "60 35", "weur", "MRD", "grain", "conn RHN"],
    // southern
    ["UMB", "Umbar", "39 54", "vivec", "UMB", "pearls"],
    ["MOG", "Moghurok", "39 48", "mideast", "UMB", "fish"],
    ["KAR", "Karna", "45 54", "mideast", "I", "pottery"],
    ["HVR", "Hivar", "51 55", "mideast", "I", "pelts"],
    ["GML", "G. Mirlond", "41 42", "mideast", "I", "grain"],
    ["HAR", "Harad", "53 49", "mideast", "HAR", "gold"],
    ["EHR", "E. Harad", "66 51", "tribal", "HAR", "ivory"],
    ["TLF", "Tolfalas", "36 42", "venetian", "HAR", "pearls"],
    ["ASH", "Ashlands", "64 40", "mideast", "I", "ivory"],
    ["NHR", "Near Harad", "50 44", "mideast", "HAR", "grain"],
    ["HRN", "Harondor", "45 40", "mideast", "I", "spices"],
    // east
    ["KHN", "Khand", "60 45", "mideast", "VRG", "spices"],
    ["RHN", "Rhun", "70 26", "asian", "RHU", "clothes"],
    ["RMN", "Red Mtds.", "71 15", "dwarven", "I", "tools"],
    // neutral
    ["THR", "Tharbad", "24 19", "barbarian", "I", "ale"],
    ["DUN", "Dunland", "31 16", "barbarian", "I", "pelts"],
    ["LND", "Lon Daer", "21 25", "barbarian", "I", "wool"],
    ["RHV", "Rhovanion", "59 22", "elven", "I", "wool"],
    ["AND", "Andrast", "22 36", "weur", "I", "grain"],
    ["END", "Enedwaith", "24 29", "tribal", "I", "pelts"],
    ["EEN", "E. Enedw", "27 25", "tribal", "I", "grain"],
    ["FOR", "Fornost", "14 6", "barbarian", "I", "paper"],
    // elves
    ["RIV", "Rivendell", "36 8", "elven", "HLV", "silk"],
    ["GRH", "Grey Havens", "11 9", "elven", "HLV", "fish"],
    ["WDE", "Wood Elves", "48 10", "elven", "WLV", "pelts"],
    ["LTH", "Lothlorien", "41 12", "elven", "HLV", "fish"],
    ["EHB", "Elven Harbour", "3 11", "venetian", "HLV", "gold"],
    // dwarves
    ["LNL", "Erebor", "56 6", "dwarven", "EDW", "gold"],
    ["BLM", "Blue Mtds.", "14 15", "dwarven", "WDW", "ale"],
    ["ERL", "Ered Luin", "7 4", "dwarven", "WDW", "ore"],
    ["IRN", "Iron Mtds.", "50 4", "dwarven", "EDW", "gems"]

];

/* == SPECIAL RESOURCES == */
export const SPECIAL_RESOURCES = [
    { "id": "X1", "name": "adamantium", "colorName": "ROPE", "price": 55, "requires": [], "bldType": "spot" },
    { "id": "X2", "name": "herbs", "colorName": "HEATHER", "price": 40, "requires": [], "bldType": "quarry" },
    { "id": "X3", "name": "rings", "colorName": "MANDY", "price": 220, "requires": ["X1"], "bldType": "manufacture" }
];

/* == FAMILIES == */
export const CUSTOM_FAMILIES = [
    { "name": "Hobbits", "home": "SHR", "lang": "western", "customStartPos": "20 11" },
    { "name": "Dwarves", "home": "IRN", "lang": "germanic", "customStartPos": "50 6" },
    { "name": "Elves", "home": "GRH", "lang": "elven", "customStartPos": "13 9" },
    { "name": "Pirates", "home": "UMB", "lang": "arabic", "customStartPos": "48 45" },
    { "name": "Orcs", "home": "ORC", "lang": "germanic", "customStartPos": "45 19" }
];

/* == DECORATION == */
export const LANDSCAPE = [
    "volcano 51 37", "oasis 54 49", // all
    "village 44 33", "watchtower_white 40 29", "lighttower 41 37", // gondor
    "treasure 38 41", "temple 41 48", "shipwreck 41 45", // near harad
    "pyramids 62 53", "temple 56 48", "ruins 64 44", // far harad
    "temple 34 11", // eriador
    "temple 24 24", // enedwaith
    "farmstead 40 26", // rohan
    "volcano 55 30", "watchtower 49 36", // mordor
    "lighttower 37 52", // umbar
    "island 67 18", // rhun & khand
    "shrooms 48 15", // woodelf forests
    "forest 57 20", "village 54 12", // dale river
    "shipwreck 25 39", "island 17 26" // western coast
];

/* == PLAYER DATA == */
export const RND_START_POS = ["42 27", "23 16", "62 20"];

/* == EMPIRES == */
export const CUSTOM_EMPIRES = [
    // good
    { "key": "GND", "name": "Gondor", "lang": "western", "flag": "lightsteel bird on opal", "traits": [ "is-colonial" ] },
    { "key": "RNG", "name": "Rangers", "lang": "western", "flag": "verdigris tower on christi", "traits": [ ] },
    { "key": "HBB", "name": "Hobbits", "lang": "western", "flag": "atlantis flower on elfgreen", "traits": [ ] },
    { "key": "DLE", "name": "Dale", "lang": "germanic", "flag": "cornflower axes on veniceblue", "traits": [ ] },
    { "key": "RHH", "name": "Rohan", "lang": "western", "flag": "twine horse on dell", "traits": [ ] },
    { "key": "DAM", "name": "Dol Amroth", "lang": "western", "flag": "lightsteel horse on veniceblue", "traits": [ ] },
    // evil
    { "key": "MRD", "name": "Mordor", "lang": "germanic", "flag": "tahitigold tower on brown", "traits": [ ] },
    { "key": "RHU", "name": "Rhun", "lang": "easian", "flag": "stinger trident on tahitigold", "traits": [ ] },
    { "key": "VRG", "name": "Variags", "lang": "arabic", "flag": "opal horse on pancho", "traits": [ ] },
    { "key": "HAR", "name": "Harad", "lang": "arabic", "flag": "pancho crescent on tahititgold", "traits": [ ] },
    { "key": "UMB", "name": "Umbar", "lang": "arabic", "flag": "tahitigold trident on opal", "traits": [ ] },
    { "key": "MOR", "name": "Moria", "lang": "arabic", "flag": "heather axes on opal", "traits": [ ] },
    { "key": "GND", "name": "Gundabad", "lang": "germanic", "flag": "many axes on opal", "traits": [ ] },
    { "key": "ANG", "name": "Angmar", "lang": "germanic", "flag": "veniceblue tower on lightsteel", "traits": [ ] },
    { "key": "ORC", "name": "Orcs", "lang": "germanic", "flag": "pancho axes on dell", "traits": [ ] },
    { "key": "DLG", "name": "Dol Guldur", "lang": "germanic", "flag": "veniceblue tower on lightsteel", "traits": [ ] },
    // elves & dwarves
    { "key": "HLV", "name": "Highelves", "lang": "western", "flag": "christi ship on verdigris", "traits": [ ] },
    { "key": "WLV", "name": "Woodelves", "lang": "western", "flag": "atlantis flower on elfgreen", "traits": [ ] },
    { "key": "EDW", "name": "Eastern Dwarves", "lang": "germanic", "flag": "opal tower on tahitigold", "traits": [ ] },
    { "key": "WDW", "name": "Western Dwarves", "lang": "germanic", "flag": "lightsteel tower on veniceblue", "traits": [ ] }
];