/* == MAP PROPERTIES == */
export const STARTING_YEAR = 900;
export const END_YEAR = 1500;
export const SEASONS_PER_YEAR = 2;
export const PLAY_RANDOM_FAMILY = true;

/* == MAP TILES == */
export const TILE_ROWS = ["WWWWWWWWWWWWWWWWWWWWWWWWWAAAGAAWWWAAFGGWWWWWAAFGWWWWWWWWWWWWWWWWWGAGGAWWWWWWWWWWWWWWWWWWWWWWWW", "WWWWWWWWWWWWWWWWWWWWWWAWWAAFGGAAWWGAFGWWWWWWAAFGWWWWWWWWWWWWWWWWWAAGGGWWWWWWWWWWWWWWWWWWWWWWWW", "WWWWWWWWWWWWWWWWWWWWWWWWWGAGGGGWWWGGGGWWWWWWGGFWWWWWWWWWWWWWWWWWGGGGGWWWWWWWWWWWWWWWWWWWWWWWWW", "WWWWWWWWWWWWWWWWWWWWWWWAWGGGGGGWWWWWGWWWWWWWWCGWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", "WWWWWWWWWWWWWWWWWWWWWWAGWWGGFGGGWWWWWGGWWWWWWGGAAGWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", "WWWWWWWWWWWWWWWWWWWWWWGGWWGGFGGWWWWWWGWWCGWWGGFGWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW", "WWWWWWWWWWWWWWWWWWWWGWGGWWGGFCGGGWWWWWWWGGGGGGGWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWGGDDDWWWW", "WWWWWWWWWWWWWWWWWWWWWWWWWGGGGFGGGWWWWWWWWWGGGGWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWWDWWGGGGGDWWWW", "WWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGGGWWWWWWWWWWWWGWWWWWWWWGWWWWWWWWWWWWWWWWWWWWWWWWWDGDWWWGGGGWWWW", "WWWWWWWWWWWWWWWWWWWWWWWWWWWGGGGGWWWWWWWWWWWWWWWWWWWWWGGHGWWWWWWWWWWWWWWWWWWWWGGGGGGGGWWDGGWWWW", "WWWWWWWWWWWWWWWWWWWWWWWWGGGGGGCGGWWWWWWGGWWWWWWWWWWWWWGHWWWWWWWWWWWWWWWWWWWWDGGGGGGGGDWDGGCWWW", "WWWWWWWWWWWWWWWWWWWWWWWGGGGGGGGGGWDDWWWWWWWWWWWWWDDGGWDGGWWWWWWWWWWWWWWWWWWWDGGGGGGGGGDGGGGGWD", "WWWWWWWWWWWWWWWWWWWWWWWWWGGGGFGGGGGGDDDWWWWWWWWWDGGFCBWWWWWWWWWWWWWWWWWWWWWDGGGGGGGFFGGGGGGGWG", "WWWWWWWWWWWWWWWWWWWWWWWWWGGGGGFFFGGGGGGWWWWWWWWGGGGFFGGWWWWWWWWWWWWWWWWWWCDDGGGGGGGGFHHGGGGGWG", "WWWWWWWWWWWWWWWWWWWWWWWWWWGGGGFFGGGHGGWWWWWWDCWGGGGGGGGGWWWWWWWWWWWWWWWGGGGGFGFGWGGGGHHHGGGWWC", "WWWWWWWWWWWWWWWWWWWWWWDDWWGDDGGGGGGGGGWWWWGDGGBGGGGGGGGGDWWWWWWWWWWDDGWWWWGGFFFGGWGGGHHHGGGWGG", "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWDGFGGGGGCGDDFFGGGWGGFFFGGGGDWWWWWWWWGDGGGGGGWGGGGGGGWGGGGGGGGGWWG", "WWWWWWWWWWWWWWWGDWWWWDWDDWWWWDGFGGGFFGGGGFCGGWWGGFGGGGGGGDWWGDDGWGFFFFGGGWWGGWGGGWWWWGGGGGGWWG", "WWWWWWWWWWDGWDDGGDDWWGWDGGGWWWGGGGGFFFGGGGGWWWGGGGGWGGGGGGGWWWWWGGGGGGGGGGGGGWWWWWGGWWGGGGGWGG", "WGWWWWWWWWWWWDGGGGGDWWWGGGGDWWWGGGGFGGGGGGWWGGFFGWWWGGGGGGGGDDWWGGGFFGGGGGGGGGGGGGGGGGWGGGGWGG", "WGWDDDGWWWWWWGGGGGGWWWWGGGGGDWWWWCGGGGGGGGWGGFFGGGFFGGFFGGGGGGDWWGGFFFFGGGGGGGGGHHHGFGWGGGWWGG", "WWGGGGGGDDDWWGGGGGGDWDWGGFGGGGGWWWGGGGGGGGGGGFGWGWWFFGGFFGGGGGGGWGGFFFGGWWGGGGGFHHHGFGWGGGWGGG", "GWGGGGGGCGGGWWWGGSGGGGWGFFGGGGGGGWWBWWGGGGFGGGGFGWWFGGHGGGFFGGGCBGGGGGGGWGGFFGGFFHHGFGWGGWGGGG", "WWGGSGFGGSGGGGWCGSSGGGWGGGGGGGGGGGGGGWGGGGFFGGFHHGWGGHHHGCFFGGGWWGWWGGGWWGGFFFGGFFFGGGWGWWGGGF", "WGGSSGFFGGSSGGWGGGSGGGWGGGGGFFGGGGGCGWWWGGGGGGHHHGWGGHHGGGFGGGGWWWWWWGGWWGGFFFFGGFFGGGWWWGGGGG", "WGGSSGGFGGGSGGWGGGGGGFWWCGGGGFFGGGGGGGGWWGGGGGGGGGGGGGGGGGFGGGWWGGGGGGGGWGGGGGFFGGGGCGGWWGGCGG", "WWWGGGGGGGGGGWWGGGGGGFGWWWGGGFFFGGGGGGGGWWWGGGGGGGGFGGGGGGGGGGWGGFFGFGGGWGGGGGGGGGFFGGGGGWWGGG", "WWGGGGSSSGGGGWGGGFFGGFGGGWWGGGFFGGGFFGGGGGWWGGGGGGGFFFCGGGGGGWGGFFFFFGGGWWGGWWWGGGFFGGGGGGWWGG", "WWGGGGSSSSGGGWGGGFFGGGGGGGWWWGFFGGGGFFFGGGGGWWGGGGGGFFGGGGGGWGGGFFGGGGGGWWWWWGGGGFFFFGGGGGGGWW", "WWWWGGGGGSGGGWGGGFFGGGGGGGWGWGFFGGGGGFFFGGGGGWGGGGGGGGFGGGGGWGGFGGGGGWWWWGGGGGGGGFFFFGGWWGGGGG", "WWWGGGFGGGGGGWGGGFFGSSSGGGWGGGFGGFGGGGFFGGGGGBGGGGGGGGGGGGGGGWGGWWWWWWWGGGGGGGGGGGFFGGGGWWGGGG", "WWCGGFFGGGGGGWGGGFFGSSSGGGWWGGFGGFFGGGGGGGGGGWGFFGGGGGGGGGGGGWWWWGGGGGWWWWWGGGGGGGGGGGGGWWGGGG", "WWGGGFFFGGGGGGGGGFFGGSGGGWWWGGGGGGFFGGGGGGGGWWGGFGGGGGGCGGFGGGGWGGGGGGGGGGWWWWWWWGGGGGGGGGGGGF", "GGGGGGFFFGGGGGGGGGGGGGGGGWGGGCGGGGFFFGGGGGGGWGGHHFGGGGGGGFFGGGGWGGGGGGGGGGGGGGGCWGGFFFGGGGGGGF", "GGGGGGFGGGGGGGGGGGCGGGGGWWGGGGGGGGFGGGGGGGGGWGFHHCFGGGGGGGFGGGGWWGGGFFFGGGGGGGGGWGGFGFGGGGGGGG", "GGGGGGGGGSSGGFGGGGGGGFFGWGGGGGGGGGGCGGGGGGGGWGFHHFFGGGGGGGGGGGGCBGGGFFFFFGGGGGGGWGGGGGGWWWWWGG", "GGGGGGGGSSSGGFFGGGGGGGGGGWWGGGGGGGGGGGGGGGGWWGFFFGGGGGGGGGGGGGGGWGGGGGGGGGGFGGGGWGGGWWWGGGGGWG", "WWWWGGGGGSSGGFCFFGGGGGGGGGWWGGGGGGGGGGGGGGCWGGGGGGGGFGGGGGGGGGGGWGGGGGGGGGGFGGGGGWWWWWWGGGGGWG", "GGCWWWWGGGSGGFFGGGGGGGGGGGGGGGGGGGGGFFGGWGGWGGGGGGGGFFFFHHGGGGGGGWWWWBWWGGGGGGGGGGGGGGWGGGGGWG", "GGGGGGWWGGGGGGGGGGGGGGGGGGGGGGGGGGCFFFGGWWWWWBWGGCGGFFHHHHHFGGGGGGGWGGGWGGGGGGGFGGGGGGGWGGGGWG", "GGGGGGGWCGGGGGGGGGGGGGGGGGGGGGGGGGFFGGGWGGGWGGGWWWGGFFFHHHHFGGGGGGGWGGGGWGGGGGGFFFGGGGGWWGGGWG", "GGGGGGGWWGGGFGGGGFFGGGCGGGGGGGGGGGFFGWWWGFGWGGGGGGWGGGGFHHHFGGGGGGGWWGGGWWGGGGGFHHFGGGGGWGGGGW", "GHHHHGGGWGGGFGGGGGCGGGGGGGFGGGGGGGFFGGGHHFGWGGGGGGWGGGGGFFFGGGGGGGGGWGGGGWWWGGGFHHHGGGGGWGGGGG", "GHHMHGGGWGGGGGGGGGGGGGGGGFFGGGGGGGGFHHHHHHGWWGMMGGGWWGGGGGGGGGGGGGGGWGGGGGGGWGGGFHHGGFGGWWGGGG", "GHMMHCGGWGFGGGGGGGGGGGGGGGGGGGGGGGGHHMMMMHHGWWMMMMGGWGGGGGGGGGGFFGGGGWGGGGGGWGGGGGGGGFGGGWGGGH", "GHMMHHFGWGFGGGFHHGGGGGGGGGGGGGGGGGHHMMMMMMHFGBGMMMFGBWGGGGGGGGGFGGGGGWGGGFGGWWGGGGGGGFGGGWGGGH", "GFMHHFFGWGGGGFHHHFGGGGGGGGGCGGGGGFHMMMMHHHFGGWGCMMFGGWGGGGGGGGGGGGGGGWGGGFFGGWGGGGGGGGGGGWWGGH", "GFHHHGGGWWGGGFHHHFGGGGGFGGGGGGGGFFHMMMMHFFGGWGGGFFFFGGWWGGGGGGGGGGGGGWGGGFFGGWWGWWGGGGGGGGWGGG", "GFGGGGGGGWGGGGFHFFGGGGGFFGGGGGGGFFFHHHHHGGGWGGGGGGGGFFFWWGGGGGGGGGGCGWGGGGGGGGWWWWWGGGGGGGWWGG", "FFFFGGGGCWGGGFGFFHGGGGGGFGGGGFGGGGGFFGGGGGGWGGGGGGGGFHHFWWGGGGGGFFFFGWWGGGGGGGGGGCWWWGGGGGGWGG", "FHCHFGFGGWWGGFGFHHHHGGGGGGGGGFFGGGGGGGCGGGWWGGGGGGGGFHHHHFWWGGGFFHHHHHHHGGGGGGGGGGGGWWGFHGGGWW", "HHMMHGFFGGWSGGGHHMMHGGGGGGGGFFFGGGHHGGGGGWGGFFGGGGFFFHCMHHFWWWFHHHHHMMMHHFFFFGGGGFGGGWWFHHGGGW", "HMMMHGGGGGWSSGGHMMMHFFGGGGGGGGGGGGHMMHGGWGGGFFGGFFFFFHHHHHFFFBFHMMHHMMMMMHHHFFGGGGGGGWGWFHHGGG", "HMMHGGFFGGWWSGGHMMHHFHFGGGGGGGGGGGHMMHGGBGGGGGGGGGFHHHMMHHHHFWFHMMHHHMMMMMHHHHHHHGGGGWGWGFFGGF", "HHHGGFHHHGSWWSGHHHHFFHFGGGGGCFFFGGHHHHHGWWWGGGGGGFFHMMMMHHHFWGFHHHHHHHHHMMHHMMMMHGGGWWGWGGFFFH", "MHHGFHMMHGGSWWGGFFFFGGGGGGGGGFFGGGGHHMHHGGGWGGGGGFFHMMMMMHFGWGFGGGFHHHHHHHHHMMMMHGGWWGGGWGFHHH", "MMHGHMMHGGSSWCGGGGFHHHGGGGGGGFGGGGFFHHMHHGGWGGGGFFHHHHMMMHGGWWGGGGFFFFFFHHHHHHHHHGWWGGGGWWGHHH", "HHFGHHHFGGWWWWGGGHHMMHGGGGGGGGGGGGFFFFHHHGGWWGGHHHCHHHHHHHGGGGWFFGFFFGGGGGGGHHHHGGGGGGGGGWWGGG", "FFFGGFFGGSWGGWWGGHHHHGGGGGGGGGWWWGGFFGGGGGGWWFHHMMMHHFGFFFFGFFWFFFGGGGGGGGGGGGGGGGGHHHHGGGWGGG", "GGGGGGGGSSWGGGWGGGGGGCWWGGGGGWWGGGGGGGWBWGWWFFHMMMMHHGGFHHFFFFWFFFFGGFFGGGGFFGGGGGHHMHHHHHWWGG", "WWGGGSSWWWGGGGWWWWWWWWWWGGGGWGGGGGGGGGWGWWWFFFHMMHHHFGGFHMCHFFWCGGGGFFGGFFFFFHHGGHHMMMHHMHHWGG", "GWWGSSWWSSGHHGGGSCWSSGGWGGGWWGGGHGGGGWGGGGFFFWHHHHHFFGGFHMMHFFWGGGFFFCGGGFFHHHHHGHHMMMHHMMHWGG", "FFSWWWSSGGHMMHGGGSWSFGGGWGGWGGMMHGGGCWGGGGGGFWWHHHHFFGGFHMMHFFGGGGGGGGGGGFHMMMHHHHHMMHHHHHHGGG", "MCFSWGGGGHMMHHHGGGBGFFGGWWWWGCMHHGGGGWGGGFGGFFWHHHHHFGGFHMMHHHGGGGGGGGGGGFHMMMMMHGHHHHGHHHGGGG", "MMFWGGGGGFHHHHMHGCWSFFFHFGGGGHMHGGGGGWWGGGFFGGWFHMHHGGGFHHHMMHFGGGGGGGGGGFHMMHMMHGGWGGGGGGGGGH", "FFWWGGGGFFHMMHMHGGWSSSGHHHFFFGHGGGGGGGWWGGFFGGWFMMHHHGGGFFHHHHGGGGGGGFFFGGHHHHHHHFGCWGGGGGGCHH", "GWWGGGHHMHHHHMMHGGWWBWGFFFFFGGGGGHHHHGGWGGFGGGWFHMMHHFFFFFFFFFGGGGFFFFHFFGGFFHHHHHGGWGGGGHHHHM", "GWGGGFHMMHFFGHHGGGWSCWWGGGGGGGGGHMHMHHCWWGGGGGWFHMMHHFFHFFFGGGFFFFFHHHHHFGGGHHMMMHFGWWGGGHHHMM", "GWGGGFHMMHFGGGGGGSBSSGGWWGHHHHGGHHHHMHGGGGGGGWFFHHMMHHFHHFFGGGFHHFFHHHHHFGGGHHMMMHHFGWGGHHHMMM", "WWGGGFHHHHGGWWGGGSWSGGGGWGHMHGGGGGGHHHGGGFGGGWFFFHMMMHFFHFFGGGHHHHFFHHHHFGGGFHMMHHHFGGWGHMHHHH", "WGGGHHHHFGGGGWWGSWWSGGGGWGHMHGGGGGGGGGGGGFGGWWFFFHMMMHHFFGGGGGFHHHFFFFFFGGFFHHMCHHHFFGWHMHHGGH", "WGGGHHHHFFGGGGWWWWSGGGGWWGHMCGFFGGGGGGGGFFGGWGGGGHHHHHHHHHHGGGFFHFFHFFGGGGFFHHHHHHHFFGWHMHGGGM", "WWGGFHMMHFGGGGSWWGGGGGGBGGHHGFFFGGGGWWGGFFGGWWWGGGFFHHMMHHHHHGGGFFHHFGGGGFFFHHHMMHHGGWHHMHGGGM", "WGGGGHMMHHFGGGWWGGHHHGGWWGGHFGGHHHHGGWWGGGGWGCWWWWGFFHMMMMMHHHGGFHHHGGGGFFHHHHHMMHFGWGHMMHHHHH", "WCGGGHHMHHMGGGWSGHMMHGGCWWGGFHHMHCHHGGWWGGWWGGGGGWWFFHHMMMMMHHHGFHHHGGGFHHHHHHHHMHHGWGHHHHMMHH", "WWWWGGHHHHMGGWSGHMMHHGGGGWGGFHMMHHHGGGGWWWGGGFFGGBWWFFHHMMMMMHHGFHHHGGHHHHMMMMHHHHHWGGGGGHHMMH", "GWGWWGGHHMMGCBSGHMHHGGGGGWGFHMHHGGWWWWWWGGGGGGGGWWGGWGGHHHHMMHHHGGHHGHHHHHHMMMHHHHWGGGGGGHHMMM", "GWGGWWGGGGGGSWGGHHHFGGWWWWFHHHHGGWWGGGGGGGFGGWWWWGGGGWWWGGHHHHHHHGGGHHMMMHHHHMMHGGWGGGGGGHHHMM", "GWWGGWWGGGGSWSGHHHGFGWWFFGFHGGGWBWGGGGHHGFGGGWGGFFHHFGCGWWFFHMMMHHGGHMMMHHHHHHHFFGWGGGGGGGHHHM", "GGWWGGWGHHGWWSGHMHGFGGGFFFGGGCWWGGGGCHMMFWWWWGFHFHHHFFGGWWFFFMMMMHGGHMMHHGSSSGGFGWWGSSGGGGGHHH", "GGGWGGWGHGGWSGGHHHGFFHHFFGGWWWGGGHHHMMMHFWGGGGHFFFGGGGWWWFWWFFMMGGGGHHHHWWBWWWWSGWGGSSSGGGGGGH", "GFGWGGWGHGGWSGHHHGCFFHHGGGWWGGGGHMMMMHHFWHHHGGFFWFWWWWGGFFGGWBWWGGGGSSSWGGGGGCWSSBGGCGGGSSGGGG", "GFGWWGGGGGFWGGHHFWFFFFGGWWWGGGGGHHHMHGGWWCMHGGGWFWWGGGFFFFFGSSSWWWWWWWWSFFFGGSWWWWWWWWGGSSSGGG", "GFGGWGGGGFFWGCHHHFWWWWBWGGGGGFFGGHHHGWWGGMMMGGSWFHHHGGGHHHHGGGCSSSSGGGGFFHHHHSSSGGGGGWGGGGSSSG", "GFFGWGGGGFWWGHMHGFWWGGGGGGGGGFFFGGGGGWWGGMHHGSSWHMMHGHHHMHHFFFFHHHHHGFFHHHHMHGGSSSSGGWWGGGSSSG", "GGGGGGGFFFBSGHHHGFFFGGGGFFFFGGGGGGGGGGWWHMHHGSSWHHMHGHMMMMHFFHHHHMMHFFHHHMMMMHHHSWSGSSWWGGGGGG", "GGGGGGGFFSWSGGGHGFFFGGWGGFFFGGGFFGGFFGGWHHHHGSWWWHHHCHMMMMHFFHHMMMMHHHHHMMMMHHHHWWHGGSSSWBWWWW", "GGGGGFGFFSWWSFFFFSSWWBWWWWSFGGGFFGGGFFGHGHMGGWWSSGHMMHHHHHHHHHMMMHHHHHHMMMMHHHHHWWHGGGSGGGGGGG", "GGGGFFGGGGCWWBWWWWWWGSCWWWWSSGGGGGGGGFFHHMMGGWSSGGHMMMHHHHHHHHHMMHHHHHMMMMHHHHHHHHHSGGGGGGGSGG", "GGGGFFGGGHHHHHFFFCHHHGGSSWWWSGGGFGGGGGFHMMMGGWGGGGHMMMMHHHHMHHHHHHHHHHMMMMHHHMMMMMMSSGGGGGSSGG", "GGGGFGGGHHMMMHFHHHMMHHGGGSFFSGGGFFGGGGHHMMGGGWGCSGHHMMMHHHMMMHSSHMMMHHHHHHHHHMMMMMMSSSSGGGSSSG", "GGGGGGGGHMMMMHHHMMMMMHHGGFFFFGGGFFFGGGGHHGWWWWGSSSGHHMMHHMMMHHSSHMMMMHHSSSSHHHMMMMMSSSSSSSSSSG"];

/* == TILE OVERRIDE == */
export const TILE_OVERRIDE = [
    // stettin haff
    "62 20 W", "63 21 W", "61 20 D", "62 21 D", "64 19 D", "64 18 D", "61 26 W", "60 30 W"
];

/* == MAP LOCATIONS == */
export const CITIES: Array<Array<string>> = [
    /* KEY NAME POS_STR MESH_NAME EMPIRE_KEY PRIMARY_RES BONUS? IS_COLONY? */
    // major cities
    ["BRM", "Bremen", "24 25", "weur", "BRM", "ale"],
    ["ZUE", "Zuerich", "17 89", "weur", "S_Z", "ale"],
    ["NYM", "Nymweegen", "3 38", "weur", "HLL", "ale"],
    ["LUX", "Luxembourg", "3 63", "weur", "I", "ale"],
    ["PRA", "Prague", "63 60", "weur", "I", "weapons"],
    // german baltic coast
    ["ROS", "Rostock", "46 15", "hanseatic", "BHA", "ale"],
    ["LUE", "Luebeck", "38 15", "hanseatic", "BHA", "tools"],
    ["WIS", "Wismar", "42 17", "weur", "I", "grain"],
    ["STR", "Stralsund", "52 12", "weur", "BHA", "fish"],
    ["LWL", "Ludwigslust", "43 22", "weur", "I", "wood "],
    // netherlands
    ["WSL", "Wesel", "8 40", "weur", "CLE", "ale"],
    ["GRN", "Groningen", "8 22", "weur", "I", "ale"],
    ["LEE", "Leer", "15 23", "cloister", "I", "tools"],
    ["EBB", "Elburg", "3 31", "weur", "I", "grain"],
    // hannover & frisia
    ["HNV", "Hannover", "29 33", "weur", "I", "grain"],
    ["HMB", "Hamburg", "33 20", "weur", "I", "weapons"],
    ["LUN", "Lueneburg", "35 24", "cloister", "I", "pelts"],
    ["PDB", "Paderborn", "22 41", "cloister", "I", "wool"],
    ["OSN", "Osnabrueck", "18 34", "cloister", "I", "wool"],
    ["MNS", "Muenster", "14 37", "cloister", "I", "wool"],
    // brandenburg
    ["PRN", "Prenzlau", "57 23", "weur", "BRA", "ore"],
    ["BER", "Berlin", "55 32", "weur", "BRA", "grain"],
    ["FUE", "Fuestenberg", "54 27", "cloister", "I", "wood"],
    ["FRO", "Frankf. (Oder)", "63 35", "weur", "I", "fish"],
    ["BRN", "Brandenb.", "49 34", "weur", "BRA", "ore"],
    ["WIT", "Wittenburg", "49 39", "cloister", "I", "grain"],
    // polish coast
    ["STT", "Stettin", "64 22", "hanseatic", "POM", "pearls"],
    ["GDA", "Danzig", "90 10", "slavic", "I", "ale"],
    // middle rhine
    ["KOE", "Cologne", "8 49", "weur", "I", "ale"],
    ["ACH", "Aachen", "5 50", "cloister", "I", "wood"],
    ["KBL", "Koblenz", "13 56", "weur", "I", "ale"],
    ["MNZ", "Mainz", "17 61", "cloister", "HSS", "ore"],
    ["FRF", "Frankfurt", "21 59", "weur", "I", "ale"],
    ["SOE", "Soest", "18 42", "weur", "I", "ale"],
    // westphalia & brunswick
    ["FRF", "Frankfurt", "21 59", "weur", "I", "ale"],
    ["FRB", "Freiburg", "13 83", "cloister", "I", "ale"],
    ["KSS", "Kassel", "27 46", "weur", "I", "wood"],
    ["BRU", "Brunswick", "35 35", "weur", "I", "ale"],
    ["GOS", "Goslar", "34 39", "cloister", "I", "ale"],
    // palatinate
    ["HDL", "Heidelberg", "20 67", "weur", "PAL", "weapons"],
    ["SPY", "Speyer", "16 70", "cloister", "PAL", "ale"],
    // swabia
    ["STU", "Stuttgart", "23 74", "weur", "SWB", "weapons"],
    ["ULM", "Ulm", "27 79", "cloister", "I", "ale"],
    ["HLL", "Hall", "28 71", "cloister", "I", "pelts"],
    ["AUG", "Augsburg", "34 78", "cloister", "I", "tools"],
    ["KNZ", "Konstanz", "22 88", "weur", "I", "ale"],
    ["STS", "Strasbourg", "10 81", "cloister", "I", "ale"],
    ["RTT", "Rottweil", "18 81", "weur", "I", "weapons"],
    ["FRE", "Freiburg", "13 83", "weur", "I", "ale"],
    // upper bavaria & switzerland
    ["MUN", "Munich", "41 82", "weur", "MUN", "wool"],
    ["PSS", "Passau", "54 78", "weur", "UBV", "paper"],
    ["BAS", "Basel", "10 88", "weur", "I", "ale"],
    // franconia
    ["NUE", "Nuremburg", "38 67", "weur", "FRC", "clothes"],
    ["REG", "Regensburg", "45 73", "weur", "I", "pottery"],
    ["BAM", "Bamberg", "36 62", "weur", "I", "wood"],
    ["WUE", "Wuerzburg", "29 63", "weur", "FRC", "ale"],
    ["FUL", "Fulda", "28 54", "weur", "I", "ale"],
    // elbian
    ["LEI", "Leipzig", "47 46", "weur", "PSX", "clothes"],
    ["DRS", "Dresden", "58 48", "weur", "SAX", "pelts"],
    ["BAM", "Bamberg", "54 51", "cloister", "I", "wood"],
    ["MAG", "Magdeburg", "42 37", "weur", "PSX", "pelts"],
    ["FRE", "Freiberg", "36 62", "weur", "SAX", "ore"],
    ["ERF", "Erfurt", "38 50", "cloister", "THU", "ale", "conn KSS"],
    ["GOE", "Goerlitz", "67 48", "cloister", "I", "ale"],
    ["STJ", "St. Joachimstal", "50 57", "cloister", "FRC", "wool"],
    // east of elbe
    ["BUE", "Buerglitz", "58 60", "weur", "I", "grain"],
    // danish
    ["VRD", "Vordingsb.", "45 3", "weur", "DAN", "wood"],
    ["NSK", "Naskov", "40 5", "weur", "DAN", "fish"],
    ["SLV", "Schlesvig.", "30 10", "weur", "I", "fish"]
];

/* == SPECIAL RESOURCES == */
export const SPECIAL_RESOURCES = [
    { "id": "X1", "name": "reliquaries", "colorName": "ROPE", "price": 55, "requires": [], "bldType": "spot" },
    { "id": "X2", "name": "herbs", "colorName": "HEATHER", "price": 40, "requires": [], "bldType": "quarry" },
    { "id": "X3", "name": "rings", "colorName": "MANDY", "price": 220, "requires": ["X1"], "bldType": "manufacture" }
];

/* == FAMILIES == */
export const CUSTOM_FAMILIES = [
    { "name": "Hochstetter", "home": "ROS", "lang": "germanic", "customStartPos": "36 77" },
    { "name": "Fugger", "home": "BRM", "lang": "germanic", "customStartPos": "25 25" },
    { "name": "Baring", "home": "ZUE", "lang": "italian", "customStartPos": "9 24" },
    { "name": "D'Anjou", "home": "LUX", "lang": "western", "customStartPos": "48 45" },
    { "name": "Metzler", "home": "FRF", "lang": "western", "customStartPos": "21 62" }
];

/* == DECORATION == */
export const LANDSCAPE = [
    "farmstead 21 71" // baden & swabia
];

/* == PLAYER DATA == */
export const RND_START_POS = ["42 27", "23 16", "62 20"];

/* == EMPIRES == */
export const CUSTOM_EMPIRES = [
    // northern germany
    { "key": "BHA", "name": "Baltic Hansa", "lang": "germanic", "flag": "lightsteel ship on opal", "traits": [ "is-colonial" ] },
    { "key": "BRA", "name": "Brandenburg", "lang": "germanic", "flag": "lightsteel horse on verdigris", "traits": [ "is-colonial" ] },
    { "key": "POM", "name": "Pomerania", "lang": "germanic", "flag": "lightsteel trident on brown", "traits": [ "is-colonial" ] },
    // netherlands
    { "key": "CLE", "name": "Cleves", "lang": "germanic", "flag": "lightsteel horse on verdigris", "traits": [ "is-colonial" ] },
    { "key": "UTR", "name": "Utrecht", "lang": "germanic", "flag": "lightsteel tower on brown", "traits": [ "is-colonial" ] },
    // middle rhinish
    { "key": "PAL", "name": "Palatinate", "lang": "germanic", "flag": "lightsteel tower on veniceblue", "traits": [ "is-colonial" ] },
    { "key": "HSS", "name": "Hesse", "lang": "germanic", "flag": "lightsteel crown on brown", "traits": [ "is-colonial" ] },
    // elbian
    { "key": "SAX", "name": "Loyalist Saxonia", "lang": "germanic", "flag": "lightsteel cup on opal", "traits": [ "is-colonial" ] },
    { "key": "PSX", "name": "Protestant Saxonia", "lang": "germanic", "flag": "lightsteel lion on christi", "traits": [ "is-colonial" ] },
    { "key": "THU", "name": "Thuringia", "lang": "germanic", "flag": "lightsteel horse on dell", "traits": [ "is-colonial" ] },
    // bavaria & franconia
    { "key": "FRC", "name": "Franconia", "lang": "germanic", "flag": "lightsteel ship on opal", "traits": [ "is-colonial" ] },
    { "key": "UBV", "name": "Upper Bavaria", "lang": "germanic", "flag": "lightsteel tower on veniceblue", "traits": [ "is-colonial" ] },
    { "key": "MUN", "name": "Munich", "lang": "germanic", "flag": "lightsteel lion on cornflower", "traits": [ "is-colonial" ] },
    // other southern germans
    { "key": "SWB", "name": "Swabia", "lang": "germanic", "flag": "lightsteel lion on goldenfizz", "traits": [ "is-colonial" ] },
    { "key": "BDN", "name": "Baden", "lang": "germanic", "flag": "lightsteel diagonal on brown", "traits": [ "is-colonial" ] },
    // swiss
    { "key": "S_Z", "name": "Zuerich", "lang": "germanic", "flag": "lightsteel ship on opal", "traits": [ "is-colonial" ] },
    { "key": "S_B", "name": "Berne", "lang": "germanic", "flag": "lightsteel ship on opal", "traits": [ "is-colonial" ] },
    // danish
    { "key": "DEN", "name": "Denmark", "lang": "germanic", "flag": "lightsteel ship on opal", "traits": [ "is-colonial" ] }
]